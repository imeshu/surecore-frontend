import { TestBed } from '@angular/core/testing';

import { NotificationDataTransferService } from './notification-data-transfer.service';

describe('NotificationDataTransferService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotificationDataTransferService = TestBed.get(NotificationDataTransferService);
    expect(service).toBeTruthy();
  });
});
