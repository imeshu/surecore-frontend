import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('Route should be guarded!');
    // TODO: should be able to verify validity of access token
    // const authToken = sessionStorage.getItem('access_token');
    // if (authToken === null) {
    if (sessionStorage.getItem('access_token') === null && localStorage.getItem('access_token') === null) {
      this.router.navigate(['auth/sign_in']);
      return false;
    }
    return true;
  }
}
