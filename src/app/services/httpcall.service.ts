import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { baseUrl, clientID, clientSecret, grantType } from '../config/enum';

@Injectable({
  providedIn: 'root'
})
export class HTTPCallService {

  protected baseAPI = baseUrl;

  constructor(private http: HttpClient) { }

  getResource(path): Observable<HttpResponse<Object>> {
    const authToken = sessionStorage.getItem('access_token');
    return this.http.get(this.baseAPI + path,
      { headers: { 'Authorization': 'Bearer ' + authToken }, observe: 'response' });
  }

  postResource(path, data): Observable<HttpResponse<Object>> {
    const authToken = sessionStorage.getItem('access_token');
    return this.http.post(this.baseAPI + path, data,
      { headers: { 'Authorization': 'Bearer ' + authToken }, observe: 'response' });
  }
  postImage(path, data): Observable<HttpEvent<Object>> {
    const authToken = sessionStorage.getItem('access_token');
    return this.http.post(path, data,
      { reportProgress: true, observe: 'events' });
    // {reportProgress:true, observe: 'response'});

  }

  putResource(path, data): Observable<HttpResponse<Object>> {
    const authToken = sessionStorage.getItem('access_token');
    return this.http.put(this.baseAPI + path, data,
      { headers: { 'Authorization': 'Bearer ' + authToken }, observe: 'response' });
  }

  deleteResource(path): Observable<HttpResponse<Object>> {
    const authToken = sessionStorage.getItem('access_token');
    return this.http.delete(this.baseAPI + path,
      { headers: { 'Authorization': 'Bearer ' + authToken }, observe: 'response' });
  }

  authentication(path, data): Observable<HttpResponse<Object>> {
    const formdata = new FormData();
    formdata.append('username', data.username);
    formdata.append('password', data.password);
    formdata.append('grant_type', grantType);
    formdata.append('client_id', clientID);
    formdata.append('client_secret', clientSecret);

    return this.http.post(this.baseAPI + path, formdata, { observe: 'response' });
  }


  downloadFile(path): Observable<Blob> {
    const authToken = sessionStorage.getItem('access_token');
    return this.http.get(this.baseAPI + path, { responseType: 'blob', headers: { 'Authorization': 'Bearer ' + authToken} });
  }

  // downloadTest(): Observable<Blob> {
  //   const authToken = sessionStorage.getItem('access_token');
  //   return this.http.get('http://52.221.145.163:5000/uw/policies/SOLRL1904000000000247/weatherIndexSchedule/print', { responseType: 'blob', headers: { 'Authorization': 'Bearer ' + authToken} });
  //   // return this.http.get('http://52.221.145.163:5000/uw/policies/SOLRL1904000000000247/weatherIndexSchedule/print',
  //   //   { headers: { 'Authorization': 'Bearer ' + authToken }, observe: 'response' });
  // }

  //get resource with body
  getResourceWithBody(path, data): Observable<HttpResponse<Object>> {
    const authToken = sessionStorage.getItem('access_token');
    return this.http.get(this.baseAPI + path,
      { headers: { 'Authorization': 'Bearer ' + authToken }, observe: 'response' });
  }

}
