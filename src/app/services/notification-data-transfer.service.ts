import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationDataTransferService {

  isMessageDisplayed = false;
  messageContent = '';
  messageType = 'success';

  error(msg: string){
    this.isMessageDisplayed = true;
    this.messageContent = msg;
    this.messageType = 'error';
  }

  success(msg: string){
    this.isMessageDisplayed = true;
    this.messageContent = msg;
    this.messageType = 'success';
  }

  constructor() { }
}
