import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationDataTransferService } from './services/notification-data-transfer.service';
import { timer } from 'rxjs';
declare var google: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'surecore-sanasa-farmer-login-portal-frontend';
  showFiller = false;
  constructor(private router: Router, private notificationService: NotificationDataTransferService) {

  }


  isMessageVisible() {
    const timeSource = timer(3000);
    if (this.notificationService.isMessageDisplayed) {
      timeSource.subscribe(value => {
        this.notificationService.isMessageDisplayed = false;
        return this.notificationService.isMessageDisplayed;
      });
    }
    return this.notificationService.isMessageDisplayed;
  }

  getNotificationMessageContent() {
    return this.notificationService.messageContent;
  }

  getMsgType() {
    return this.notificationService.messageType;
  }
}
