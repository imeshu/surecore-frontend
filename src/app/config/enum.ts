// app level constants
export const baseUrl = '/api/';
export const fileServer = '/api/';
// export const fileServer = 'http://52.221.145.163:9999/';
// export const baseUrl = 'http://52.221.145.163:5000/';
// export const baseUrl = 'http://localhost:5000/';

// Auth service
export const grantType = '';
export const clientID = '';
export const clientSecret = '';

export const RECENTLY_LISTED = 60;
export const LIST_ALL = 0;

// Notifications
export const SERVER_UNREACHABLE = 'Server unreachable, please check your internet connection or contact an Administrator';
export const UNAUTHORIZED = 'Your session has been expired. Please login again.';

// notification service
export const messageType = '';

// footer
export const VERSION = '0.0.0';
