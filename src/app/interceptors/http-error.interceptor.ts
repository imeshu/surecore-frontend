import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ErrorResponse } from '../model/ErrorResponse';
import {Router} from '@angular/router';
import {NotificationDataTransferService} from '../services/notification-data-transfer.service';
import {UNAUTHORIZED} from '../config/enum';
import {Injectable} from '@angular/core';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private router: Router, private notificationService: NotificationDataTransferService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(

      // If tbe request needs to be retried before taking it as error
      // retry(1),
      catchError((error: HttpErrorResponse) => {

          if (error.status === 401) {
            this.logout();
            this.notificationService.isMessageDisplayed = true;
            this.notificationService.messageContent = UNAUTHORIZED;
            this.notificationService.messageType = 'error';
          } else {
            const x: ErrorResponse = new ErrorResponse();
            x.timestamp = error.error.timestamp;
            x.status = error.error.status;
            x.error = error.error.error;
            x.message = error.error.message;
            x.path = error.error.path;

            return throwError(x);
          }

        }

      )
    );
  }

  logout() {
    if (localStorage.getItem('access_token')) {
      localStorage.removeItem('access_token');
    }

    if (sessionStorage.getItem('access_token')) {
      sessionStorage.removeItem('access_token');
    }

    if (sessionStorage.getItem('partyCode')) {
      sessionStorage.removeItem('partyCode');
    }
    if (sessionStorage.getItem('asCustomer')) {
      sessionStorage.removeItem('asCustomer');
    }
    if (sessionStorage.getItem('asAgent')) {
      sessionStorage.removeItem('asAgent');
    }

    console.log(sessionStorage.getItem('access_token'));
    console.log(localStorage.getItem('access_token'));
    this.router.navigate(['/auth/sign_in']);
  }

}
