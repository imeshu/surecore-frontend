import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthRoutingModule } from './modules/auth/auth-routing.module';
import { DashboardRoutingModule } from './modules/dashboard/dashboard-routing.module';
import { DashboardComponent } from './modules/dashboard/dashboard/dashboard.component';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthGuardService],
    component: DashboardComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes), AuthRoutingModule, DashboardRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
