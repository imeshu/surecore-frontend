import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImageViewerComponent} from './image-viewer/image-viewer.component';
import {CustomPipeModule} from '../../../pipes/CustomPipesModule';
import {MatDialogModule} from '@angular/material';

@NgModule({
  declarations: [ImageViewerComponent],
  imports: [CommonModule, CustomPipeModule, MatDialogModule],
  exports: [ImageViewerComponent],
  entryComponents: [ImageViewerComponent]
})
export class ImageViewerModule {
}
