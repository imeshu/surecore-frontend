import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {ImageModel} from '../../../../model/image.model';

@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer.component.html',
  styleUrls: ['./image-viewer.component.scss']
})
export class ImageViewerComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: ImageModel) {
  }

  ngOnInit() {
  }

}
