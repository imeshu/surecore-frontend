import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ImageViewerComponent} from '../image-viewer/image-viewer.component';
import {ImageModel} from '../../../../model/image.model';

@Injectable({
  providedIn: 'root'
})
export class ImageViewerService {

  constructor(public dialog: MatDialog) {
  }

  openDialog(dialogData: ImageModel): MatDialogRef<ImageViewerComponent> {
    const dialogRef = this.dialog.open(ImageViewerComponent, {
      panelClass: 'custom-dialog-container',
      data: dialogData,
    });
    return dialogRef;
  }
}
