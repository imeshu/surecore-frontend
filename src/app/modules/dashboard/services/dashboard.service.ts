import { Injectable } from '@angular/core';
import { HTTPCallService } from 'src/app/services/httpcall.service';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(public http: HTTPCallService) { }


  getLoggedUserProfile(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles';
    return this.http.getResource(path);
  }

  getCustomerBranchCode(): Observable<HttpResponse<Object>> {
    const path = 'base/headOfficeBranchCode';
    return this.http.getResource(path);
  }

}
