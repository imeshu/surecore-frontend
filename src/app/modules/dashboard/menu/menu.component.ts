import {Component, Input, KeyValueDiffer, KeyValueDiffers, OnInit} from '@angular/core';
import {SidebarItemModel} from '../models/sidebar-item-model';
import {ImageModel} from '../../common-components/carousel/carousel.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Input() data: string;
  private sidebarItems: SidebarItemModel[] = [];
  //
  private isCustomer = false;
  private isAgent = false;
  private isInternalStaff = false;
  differ: KeyValueDiffer<string, any>;

  constructor(private differs: KeyValueDiffers) {
    this.differ = this.differs.find({this: ImageModel}).create();
  }

  ngOnInit() {
    this.addItems();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngDoCheck() {
    const change = this.differ.diff(this);
    if (change) {
      change.forEachChangedItem(item => {
        if (item.key === 'data') {
          this.addItems();
        }

      });
    }
  }

  addItems() {
    this.isAgent = sessionStorage.getItem('asAgent') === 'Y';
    this.isCustomer = sessionStorage.getItem('asCustomer') === 'Y';
    this.isInternalStaff = sessionStorage.getItem('partyAsInternalStaff') === 'Y';
    this.sidebarItems = [
      {
        label: 'Dashboard',
        link: '/',
        linkActive: true
      },
      {
        label: 'Customers',
        link: '/customers',
        linkActive: (this.isAgent || this.isInternalStaff)
      },
      {
        label: 'Lands',
        link: '/land',
        linkActive: this.isCustomer
      },
      {
        label: 'Quotations',
        link: '/quotations',
        linkActive: true
      },
      {
        label: 'Policy',
        link: '/policies',
        linkActive: true
      },
      {
        label: 'News',
        link: '/news',
        linkActive: true
      },
      {
        label: 'Market Price',
        link: '/market-price',
        linkActive: true
      },
      {
        label: 'Marketplace',
        link: '/marketplace',
        linkActive: true
      },
      {
        label: 'Consultation',
        link: '/consultation',
        linkActive: true
      },
      {
        label: 'Outstanding Debt',
        link: '/outstanding',
        linkActive: true
      },
      {
        label: 'Forum',
        link: '/forum',
        linkActive: true
      },
      {
        label: 'Receipting',
        link: '/receipting',
        linkActive: this.isAgent
      },
      {
        label: 'Deposit Slip List',
        link: '/receipting/deposit-slip-list',
        linkActive: this.isAgent
      },
      {
        label: 'Claims',
        link: '/claim-list',
        linkActive: true
      },
      {
        label: 'Loyalty',
        link: '/loyalty',
        linkActive: this.isCustomer
      }
    ];
  }
}
