import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule, MatCheckboxModule, MatToolbarModule, MatCardModule, MatListModule, MatInputModule } from '@angular/material';
import { MatIconModule } from '@angular/material';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { CustomPipeModule } from 'src/app/pipes/CustomPipesModule';
import { CommonComponentsModule } from '../common-components/common-components.module';
import {LandModule} from '../land/land.module';
import {MatProgressBarModule} from '@angular/material/progress-bar';


const materials = [
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatDatepickerModule,

  FormsModule,
];
@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CustomPipeModule,
    BrowserAnimationsModule,
    CommonModule,
    DashboardRoutingModule,
    materials,
    CommonComponentsModule,
    LandModule,
    MatProgressBarModule
  ],
  entryComponents: [MenuComponent, DashboardComponent],
  exports: [DashboardComponent, materials]

})
export class DashboardModule { }
