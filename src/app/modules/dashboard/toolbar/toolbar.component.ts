import {Component, OnInit, Output, EventEmitter, NgModule} from '@angular/core';
import {Router} from '@angular/router';
import {DashboardService} from '../services/dashboard.service';
import {TitleCasePipe} from '@angular/common';
import {ImgUpdateService} from '../../user-profile/services/img-update.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(private router: Router, public service: DashboardService, public imgService: ImgUpdateService) {
  }

  @Output() isHBClicked = new EventEmitter();

  // user profile
  private name = '';
  private designation = '';
  // imageSrc = '';
  private address = '';

  private test = true;

  private partyCode = '';
  private asCustomer = '';
  private asAgent = '';
  private branchCode = '';

  private userIsCustomer = false;
  private userIsAgent = false;
  userIsStaff = false;

  // Split name when have initials
  nameArray = [];

  ngOnInit() {
    this.partyCode = sessionStorage.getItem('partyCode');
    this.asCustomer = sessionStorage.getItem('asCustomer');
    this.asAgent = sessionStorage.getItem('asAgent');
    this.branchCode = sessionStorage.getItem('branchCode');
    this.name = '';
    this.designation = '';
    this.getUserLoggedUserProfile();
  }

  CheckIsHBClicked() {
    this.isHBClicked.emit(false);
  }

  logout() {
    if (localStorage.getItem('access_token')) {
      localStorage.removeItem('access_token');
    }

    if (sessionStorage.getItem('access_token')) {
      sessionStorage.removeItem('access_token');
    }

    if (sessionStorage.getItem('partyCode')) {
      sessionStorage.removeItem('partyCode');
    }
    if (sessionStorage.getItem('asCustomer')) {
      sessionStorage.removeItem('asCustomer');
    }
    if (sessionStorage.getItem('asAgent')) {
      sessionStorage.removeItem('asAgent');
    }


    console.log(sessionStorage.getItem('access_token'));
    console.log(localStorage.getItem('access_token'));
    this.router.navigate(['auth/sign_in']);
    this.imgService.setImg('');
  }

  /**
   * set logged user information in the session storage
   */
  getUserLoggedUserProfile() {
    this.service.getLoggedUserProfile().subscribe(resp => {
      const body = resp.body as any;
      if (body.data.address !== null) {
        this.address = body.data.address.address;
      } else {
        this.address = '';
      }
      this.name = body.data.profile.name;
      this.splitName();
      this.designation = body.data.profile.designation;
      if (body.data.image !== null) {
        this.imgService.setImg(body.data.image.image);
      } else {
      }

      // check agent or farmer or internal staff
      if (body.data.profile.partyAsInternalStaff === 'Y') {
        this.userIsStaff = true;
        this.userIsAgent = false;
        this.userIsCustomer = false;
      } else if (body.data.profile.partyAsIntermediary === 'Y') {
        this.userIsAgent = true;
        this.userIsCustomer = false;
        this.userIsStaff = false;
      } else if (body.data.profile.partyAsCustomer === 'Y') {
        this.userIsAgent = false;
        this.userIsCustomer = true;
        this.userIsStaff = false;
      }


    }, error => {
      console.log(error);
    });
  }

  splitName() {
    const format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(this.name)) {
      let newName = '';
      this.nameArray = this.name.split('.');
      for (let i = 0; i < this.nameArray.length; i++) {
        if (i === (this.nameArray.length - 1)) {
          newName += new TitleCasePipe().transform(this.nameArray[i]);
        } else {
          newName += new TitleCasePipe().transform(this.nameArray[i]) + '. ';
          console.log(this.name);
        }
      }
      this.name = newName;
    } else {
      this.name = new TitleCasePipe().transform(this.name);
    }
  }
}
