import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {MenuComponent} from '../menu/menu.component';
import {DashboardService} from '../services/dashboard.service';
import {TitleCasePipe} from '@angular/common';
import {ImgUpdateService} from '../../user-profile/services/img-update.service';

declare let $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(public router: Router, public dialog: MatDialog, public service: DashboardService, public imgServie: ImgUpdateService) {
  }


  // party information
  partyCode = '';
  asCustomer = '';
  asAgent = '';
  addressSequnce = '';
  branchCode = '';
  intermediaryType = '';
  asInternalStaff = '';

  address = '';
  name = '';

  public profileCompleteness = 70;


  // Split name when have initials
  nameArray = [];

  ngOnInit() {
    this.getUserLoggedUserProfile();
  }

  openDialog() {
    this.dialog.open(MenuComponent, {
      data: {
        animal: 'panda'
      }
    });
  }


  /**
   * set logged user information in the session storage
   */
  getUserLoggedUserProfile() {
    const userBranchCode = '';
    this.service.getLoggedUserProfile().subscribe(resp => {
      const body = resp.body as any;
      this.partyCode = body.data.profile.code;
      this.asCustomer = body.data.profile.partyAsCustomer;
      this.asAgent = body.data.profile.partyAsIntermediary;
      this.asInternalStaff = body.data.profile.partyAsInternalStaff;

      if (this.asAgent === 'Y') {
        this.intermediaryType = body.data.profile.intermediaryType;
      } else {
        this.intermediaryType = 'DIRCT';
      }


      if (body.data.address !== null) {
        this.addressSequnce = body.data.address.sequence;
        this.address = body.data.address.address;
        // set profile completeness  - completed
        this.profileCompleteness = 100;
      } else {
        this.address = '';
        this.addressSequnce = '';
        // set profile completeness -  not completed
        this.profileCompleteness = 70;
      }
      this.name = body.data.profile.name;
      this.splitName();
      if (body.data.image !== null) {
        this.imgServie.setImg(body.data.image.image);
      } else {
      }

      if (this.asInternalStaff === 'Y') {
        this.asAgent = 'N';
      }
      
      if (this.asCustomer === 'Y') {
        // get branch code
        // tslint:disable-next-line:no-shadowed-variable
        this.service.getCustomerBranchCode().subscribe(resp => {
          const body1 = resp.body as any;
          this.branchCode = body1.data;
          // tslint:disable-next-line:no-debugger

          sessionStorage.setItem('partyAsInternalStaff', this.asInternalStaff);
          sessionStorage.setItem('partyCode', this.partyCode);
          sessionStorage.setItem('asCustomer', this.asCustomer);
          sessionStorage.setItem('asAgent', this.asAgent);
          sessionStorage.setItem('addressSequnce', this.addressSequnce);
          sessionStorage.setItem('branchCode', this.branchCode);
          sessionStorage.setItem('intermediaryType', this.intermediaryType);
        }, error => {

        });
      } else {
        this.branchCode = body.data.profile.attachedLocationCode;
        sessionStorage.setItem('partyAsInternalStaff', this.asInternalStaff);
        sessionStorage.setItem('partyCode', this.partyCode);
        sessionStorage.setItem('asCustomer', this.asCustomer);
        sessionStorage.setItem('asAgent', this.asAgent);
        sessionStorage.setItem('addressSequnce', this.addressSequnce);
        sessionStorage.setItem('branchCode', this.branchCode);
        sessionStorage.setItem('intermediaryType', this.intermediaryType);
      }
    }, error => {

    });
  }

  splitName() {
    const format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(this.name)) {
      let newName = '';
      this.nameArray = this.name.split('.');
      for (let i = 0; i < this.nameArray.length; i++) {
        if (i === (this.nameArray.length - 1)) {
          newName += new TitleCasePipe().transform(this.nameArray[i]);
        } else {
          newName += new TitleCasePipe().transform(this.nameArray[i]) + '. ';

        }
      }
      this.name = newName;
    } else {
      this.name = new TitleCasePipe().transform(this.name);
    }
  }


}
