export class SidebarItemModel {
    label: string;
    link: string;
    linkActive: boolean;
}
