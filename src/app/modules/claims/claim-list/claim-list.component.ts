import {Component, OnInit} from '@angular/core';
import {ClaimListItem} from '../models/ClaimListItem';
import {ClaimsService} from '../services/claims.service';
import {ClaimSummary, IMG} from '../models/ClaimSummary';
import {ClaimPolicySummary} from '../models/ClaimPolicySummary';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import * as _ from 'lodash';
import {forEach} from '@angular/router/src/utils/collection';
import {AssessmentReportsSummary} from '../models/AssessmentReportsSummary';
import {ActionsSummary} from '../models/ActionsSummary';
import {Router} from '@angular/router';


@Component({
  selector: 'app-claim-list',
  templateUrl: './claim-list.component.html',
  styleUrls: ['./claim-list.component.scss']
})
export class ClaimListComponent implements OnInit {

  public ClaimList: ClaimListItem[] = [];
  public selectedClaim: ClaimListItem = new ClaimListItem();
  public currntClaimSummary: ClaimSummary = new ClaimSummary();
  public policyClaimSummary: ClaimPolicySummary = new ClaimPolicySummary();
  public assessmentReportsSummary: AssessmentReportsSummary[] = [];
  public ActionClaimSummary: ActionsSummary[] = [];
  public activeClaimIndex: number;

  // Risk Details
  public livestock;
  public breed;
  public gender;
  public usage;
  public age;
  public owner;
  public date;


  // Assessment
  public assessmentType;
  // Search
  selectedValue = '';
  public searchValue = '';

  // infinity scroll
  public throttle = 300;
  public scrollDistance = 1;
  public scrollUpDistance = 2;
  public fromRoot = true;
  public page = 1;
  public numberOfPages: number;

  public frontImages: IMG[];
  public backImages: IMG[];
  public leftImages: IMG[];
  public rightImages: IMG[];
  public earImages: IMG[];
  public otherImage: IMG[];

  // Session check
  public CustomerCode = '';
  public AgentCode = '';


  constructor(public service: ClaimsService, public route: Router) {
  }

  ngOnInit() {
    this.activeClaimIndex = 0;
    this.CheckSession();
    this.getSearchedClaims('CN');
    this.selectedValue = 'CustomerName';
  }

  getSearchedClaims(searchFeild) {
    this.service.getclaimlist(this.searchValue, searchFeild, this.AgentCode, this.CustomerCode, this.page).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).toPromise().then(resp => {
      const model = resp.body as any;
      if (model.data != null) {
        this.ClaimList = model.data.claimList;
        this.selectedClaim = this.ClaimList[0];
        this.getClaimSummary();
      }
    });

  }

  getClaimList(searchFeild) {
    this.service.getclaimlist(this.searchValue, searchFeild, this.AgentCode, this.CustomerCode, this.page).toPromise().then(resp => {
      const model = resp.body as any;
      this.ClaimList = model.data.claimList;
    });
  }

  getClaimSummary() {
    this.service.getclist(this.selectedClaim.sequence).toPromise().then(resp => {
      const model = resp.body as any;
      this.currntClaimSummary = model.data.claim;
      this.policyClaimSummary = model.data.policy;
      this.assessmentReportsSummary = model.data.assessmentReports;
      this.ActionClaimSummary = model.data.actions;


      // check Assessment
      this.assessmentReportsSummary.forEach(item => {
        if (item.finalization.finalized) {
          this.assessmentType = 'FINISHED';
        } else {
          this.assessmentType = 'PENDING';
        }
      });
      // Risk Details
      if (this.policyClaimSummary !== null) {

        this.policyClaimSummary.risk.riskInformation.forEach(item => {
          switch (item.description) {
            case 'LIVESTOCK TYPE' : {
              this.livestock = item.value;
              break;
            }
            case 'BREED' : {
              this.breed = item.value;
              break;
            }
            case 'GENDER' : {
              this.gender = item.value;
              break;
            }
            case 'USAGE' : {
              this.usage = item.value;
              break;
            }
            case 'AGE IN YEARS' : {
              this.age = item.value;
              break;
            }
            case 'OWNER NAME' : {
              this.owner = item.value;
              break;
            }
            case 'PURCHASE DATE' : {
              this.date = item.value;
              break;
            }
          }

        });
      }


      this.filterImages(this.currntClaimSummary.images);

    });
  }

// infinity
  onScrollDown(event, searchFeild) {
    if (this.numberOfPages >= this.page) {
      this.page++;
      this.service.getclaimlist(this.searchValue, searchFeild, this.AgentCode, this.CustomerCode, this.page).subscribe(resp => {
        const body = resp.body as any;
        const list: ClaimListItem[] = body.ClaimList.ClaimList;

        list.forEach(item => {
          this.ClaimList.push(item);
        });
      });
    }
  }

  onSelectList(claimListItem: any) {

    this.selectedClaim = claimListItem;
    this.getClaimSummary();

  }

  filterImages(images: IMG[]) {
    this.frontImages = _.filter(images, {'name': 'FRONT'});
    this.backImages = _.filter(images, {'name': 'BACK'});
    this.leftImages = _.filter(images, {'name': 'LEFT'});
    this.rightImages = _.filter(images, {'name': 'RIGHT'});
    this.earImages = _.filter(images, {'name': 'EARS'});
    this.otherImage = _.filter(images, {'name': 'OTHER'});
  }

  // Search Field
  searchByFeild() {
    let searchFeild = '';
    // const selectedValue = this.selectedValue;
    if (this.selectedValue === 'CustomerName') {
      searchFeild = 'CN';
    } else if (this.selectedValue === 'RiskName') {
      searchFeild = 'RN';
    } else if (this.selectedValue === 'PolicyNumber') {
      searchFeild = 'PN';
    } else if (this.selectedValue === 'ClaimNumber') {
      searchFeild = 'CLN';
    }
    if (this.searchValue !== '') {
      this.getSearchedClaims(searchFeild);
    }
  }

  CheckSession() {
    // Session check
    if (sessionStorage.getItem('asCustomer') === 'Y') {
      this.AgentCode = '';
      this.CustomerCode = sessionStorage.getItem('partyCode');

    } else if (sessionStorage.getItem('asAgent') === 'Y') {
      this.AgentCode = sessionStorage.getItem('partyCode');
      this.CustomerCode = '';
    }
  }

}
