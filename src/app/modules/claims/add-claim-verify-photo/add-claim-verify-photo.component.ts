import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExportImgs, ImageModel, PostImgModel} from '../models/img-model';
import {HttpEventType} from '@angular/common/http';
import {ClaimsService} from '../services/claims.service';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-add-claim-verify-photo',
  templateUrl: './add-claim-verify-photo.component.html',
  styleUrls: ['./add-claim-verify-photo.component.scss']
})
export class AddClaimVerifyPhotoComponent implements OnInit {
  @Input() notificationSequence: string;
  @Output() cancel = new EventEmitter();


  // image
  private frontImageList: ImageModel[] = [];
  private backImageList: ImageModel[] = [];
  private RightSideImageList: ImageModel[] = [];
  private LeftSideImageList: ImageModel[] = [];
  private OtherImageList: ImageModel[] = [];
  private earImages: ImageModel[] = [];

  private allImages: PostImgModel = new PostImgModel();
  panelOpenState = true;

  constructor(private service: ClaimsService, private msg: NotificationDataTransferService, private route: Router) {
  }

  ngOnInit() {
  }

  /*    Front    */
  FrontAddImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    this.frontImageList.push(img);
    this.FrontUploadImg(img.file, this.frontImageList.indexOf(img));
    this.panelOpenState = true;
  }

  FrontUploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.frontImageList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.frontImageList[index].imageKey = body.data.object.key;
        this.frontImageList[index].value = body.data.object.url;
        this.frontImageList[index].file = null;
      }
    });
  }

  FrontRemoveImg(index: number) {
    this.frontImageList.splice(index, 1);
  }

  /*    Back    */
  BackSideAddImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    this.backImageList.push(img);
    this.BackSideUploadImg(img.file, this.backImageList.indexOf(img));
    this.panelOpenState = true;
  }

  BackSideUploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.backImageList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.backImageList[index].imageKey = body.data.object.key;
        this.backImageList[index].value = body.data.object.url;
        this.backImageList[index].file = null;
      }
    });
  }

  BackSideRemoveImg(index: number) {
    this.backImageList.splice(index, 1);
  }

  /*    Right Side    */
  RightSideAddImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    this.RightSideImageList.push(img);
    this.RightSideUploadImg(img.file, this.RightSideImageList.indexOf(img));
    this.panelOpenState = true;
  }

  RightSideUploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.RightSideImageList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.RightSideImageList[index].imageKey = body.data.object.key;
        this.RightSideImageList[index].value = body.data.object.url;
        this.RightSideImageList[index].file = null;
      }
    });
  }

  RightSideRemoveImg(index: number) {
    this.RightSideImageList.splice(index, 1);
  }

  /*    Left Side   */
  LeftSideAddImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    this.LeftSideImageList.push(img);
    this.LeftSideUploadImg(img.file, this.LeftSideImageList.indexOf(img));
    this.panelOpenState = true;
  }

  LeftSideUploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.LeftSideImageList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.LeftSideImageList[index].imageKey = body.data.object.key;
        this.LeftSideImageList[index].value = body.data.object.url;
        this.LeftSideImageList[index].file = null;
      }
    });
  }

  LeftSideRemoveImg(index: number) {
    this.LeftSideImageList.splice(index, 1);
  }

  /*    Other   */
  OtherAddImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    this.OtherImageList.push(img);
    this.OtherUploadImg(img.file, this.OtherImageList.indexOf(img));
    this.panelOpenState = true;
  }

  OtherUploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.OtherImageList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.OtherImageList[index].imageKey = body.data.object.key;
        this.OtherImageList[index].value = body.data.object.url;
        this.OtherImageList[index].file = null;
      }
    });
  }

  OtherRemoveImg(index: number) {
    this.OtherImageList.splice(index, 1);
  }

  /*    Eye     */
  EarAddImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    this.earImages.push(img);
    this.EarUploadImg(img.file, this.earImages.indexOf(img));
    this.panelOpenState = true;
  }

  EarUploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.earImages[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.earImages[index].imageKey = body.data.object.key;
        this.earImages[index].value = body.data.object.url;
        this.earImages[index].file = null;
      }
    });
  }

  EarRemoveImg(index: number) {
    this.earImages.splice(index, 1);
  }


  setAllImage() {
    this.allImages.createdUserCode = sessionStorage.getItem('partyCode');
    this.frontImageList.forEach(item => {
      const img: ExportImgs = new ExportImgs();
      img.name = 'FRONT';
      img.imageKey = item.imageKey;
      this.allImages.images.push(img);
    });

    this.backImageList.forEach(item => {
      const img: ExportImgs = new ExportImgs();
      img.name = 'BACK';
      img.imageKey = item.imageKey;
      this.allImages.images.push(img);
    });

    this.RightSideImageList.forEach(item => {
      const img: ExportImgs = new ExportImgs();
      img.name = 'RIGHT';
      img.imageKey = item.imageKey;
      this.allImages.images.push(img);
    });

    this.LeftSideImageList.forEach(item => {
      const img: ExportImgs = new ExportImgs();
      img.name = 'LEFT';
      img.imageKey = item.imageKey;
      this.allImages.images.push(img);
    });
    this.earImages.forEach(item => {
      const img: ExportImgs = new ExportImgs();
      img.name = 'EARS';
      img.imageKey = item.imageKey;
      this.allImages.images.push(img);
    });

    this.OtherImageList.forEach(item => {
      const img: ExportImgs = new ExportImgs();
      img.name = 'OTHER';
      img.imageKey = item.imageKey;
      this.allImages.images.push(img);
    });


    this.allImages.sequence = this.notificationSequence;
    this.service.submitImgs(this.allImages).subscribe(resp => {
      this.msg.success('Successfully saved images');
      this.route.navigate(['claim-list']);
    }, error1 => {
      this.msg.error('Images Upload failed');

    });
  }
}
