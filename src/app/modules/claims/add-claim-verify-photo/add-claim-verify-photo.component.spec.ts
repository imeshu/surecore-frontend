import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClaimVerifyPhotoComponent } from './add-claim-verify-photo.component';

describe('AddClaimVerifyPhotoComponent', () => {
  let component: AddClaimVerifyPhotoComponent;
  let fixture: ComponentFixture<AddClaimVerifyPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClaimVerifyPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClaimVerifyPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
