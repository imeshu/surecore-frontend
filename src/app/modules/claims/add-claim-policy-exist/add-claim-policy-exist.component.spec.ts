import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClaimPolicyExistComponent } from './add-claim-policy-exist.component';

describe('AddClaimPolicyExistComponent', () => {
  let component: AddClaimPolicyExistComponent;
  let fixture: ComponentFixture<AddClaimPolicyExistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClaimPolicyExistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClaimPolicyExistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
