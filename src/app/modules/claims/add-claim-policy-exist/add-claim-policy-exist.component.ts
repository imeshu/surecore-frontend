import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PolicyDetail} from '../models/PolicyDetail';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {CommonType} from '../../../model/CommonType';
import {ClaimsService} from '../services/claims.service';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {ClaimData} from '../models/ClaimData';
import * as moment from 'moment';


@Component({
  selector: 'app-add-claim-policy-exist',
  templateUrl: './add-claim-policy-exist.component.html',
  styleUrls: ['./add-claim-policy-exist.component.scss']
})
export class AddClaimPolicyExistComponent implements OnInit {
  @Input() policy: PolicyDetail;
  @Output() data = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Input() editable: boolean = false;
  private verifyClaimForm: FormGroup;
  private ownerName = '';
  private typeOfLosses: CommonType[];
  private causeOfLosses: CommonType[];
  private today = new Date();
  private currentCurrencyCode: string;
  private policyAvailable: boolean;

  constructor(
    private fb: FormBuilder,
    private service: ClaimsService,
    public notificationService: NotificationDataTransferService
  ) {
  }

  ngOnInit() {

    this.fetchTypesOfLosses();
    this.fetchCauseOfLosses();
    if (this.policy.policyNumber) {
      this.policyAvailable = true;
    } else {
      this.policyAvailable = false;
    }
    this.verifyClaimForm = this.fb.group({
      'lossDate': ['', Validators.required],
      'lossTypeCode': ['', Validators.required],
      'lossCauseCode': ['', Validators.required],
      'expectedLossAmount': ['', Validators.required],
      'contactName': ['', Validators.required],
      'contactPhone': ['', Validators.required],
      'contactAddress': ['', Validators.required]
    });
    /* get owner name */
    if (this.policy.risk == null) {
    } else {
      this.policy.risk.riskInformation.forEach(item => {
        if (item.description === 'OWNER NAME') {
          this.ownerName = item.value;
        }
      });
    }
    this.service.getCurrencyType().subscribe(resp => {
      const body = resp.body as any;
      this.currentCurrencyCode = body.data;
    });
    this.verifyClaimForm.controls['lossDate'].setValue(this.policy.date);
  }

  fetchTypesOfLosses() {
    this.service.getTypesOfLosses().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.typeOfLosses = typeList;
    });
  }

  fetchCauseOfLosses() {
    this.service.getCauseOfLosses().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.causeOfLosses = typeList;
    });
  }

  saveClaim() {
    if (this.verifyClaimForm.dirty && this.verifyClaimForm.valid) {
      const saveData: ClaimData = this.verifyClaimForm.value;
      saveData.lossDate =  moment(this.policy.date).format('YYYY-MM-DD HH:MM:SS');
      if (this.policyAvailable) {
        saveData.riskName = this.policy.risk.riskName;
      } else {
        saveData.riskName = this.policy.riskName;
      }
      saveData.modeOfInfoCode = 'ONLINE';
      saveData.currencyCode = this.currentCurrencyCode;
      saveData.branchCode = sessionStorage.getItem('branchCode');
      saveData.policyNumber = this.policy.policyNumber;

      this.service.notifyClaim(saveData).subscribe(resp => {
        const response = resp.body as any;
        const notificationSequence = response.data;
        this.data.emit(notificationSequence);
      }, error => {
        this.notificationService.error(error.message);
      });
    }
  }


}
