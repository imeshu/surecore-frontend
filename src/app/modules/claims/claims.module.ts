import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ClaimsRoutingModule} from './claims-routing.module';
import {AddClaimNotificationComponent} from './add-claim-notification/add-claim-notification.component';
import {AddClaimPolicyExistComponent} from './add-claim-policy-exist/add-claim-policy-exist.component';
import {AddClaimVerifyPhotoComponent} from './add-claim-verify-photo/add-claim-verify-photo.component';
import {ClaimsMainComponent} from './claims-main/claims-main.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatIconModule,
  MatCalendarHeader,
  MatDatepicker,
  MatFormField,
  MatOption,
  MatSelect,
  MatIcon,
  MatInput,
  MatError,
  MatExpansionModule,
  MatButtonModule,
  MatDialogModule,
  MatAutocompleteModule
} from '@angular/material';
import {CommonComponentsModule} from '../common-components/common-components.module';
import {CustomPipeModule} from '../../pipes/CustomPipesModule';
import {PopupComponent} from './add-claim-notification/popup/popup.component';
import {ClaimListComponent} from './claim-list/claim-list.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';


@NgModule({
  declarations: [
    AddClaimNotificationComponent,
    AddClaimPolicyExistComponent,
    AddClaimVerifyPhotoComponent,
    ClaimsMainComponent,
    PopupComponent,
    ClaimListComponent
  ],
  imports: [
    CommonComponentsModule,
    CustomPipeModule,
    CommonModule,
    ClaimsRoutingModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    MatExpansionModule,
    CommonComponentsModule,
    MatButtonModule,
    MatDialogModule,
    MatAutocompleteModule,
    InfiniteScrollModule
  ],
  exports: [
    MatCalendarHeader,
    MatDatepicker,
    MatFormField,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatOption,
    MatSelect,
    MatIcon,
    MatInput,
    MatError],
  entryComponents: [PopupComponent],
})
export class ClaimsModule {
}
