import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimsMainComponent } from './claims-main.component';

describe('ClaimsMainComponent', () => {
  let component: ClaimsMainComponent;
  let fixture: ComponentFixture<ClaimsMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimsMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimsMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
