import {Component, OnInit} from '@angular/core';
import {PolicyDetail} from '../models/PolicyDetail';

@Component({
  selector: 'app-claims-main',
  templateUrl: './claims-main.component.html',
  styleUrls: ['./claims-main.component.scss']
})
export class ClaimsMainComponent implements OnInit {
  private status: string;
  private policyDetail: PolicyDetail;
  private notificationSequence: string;

  constructor() {
  }

  ngOnInit() {
    this.status = '1';
  }

  setPolicyDetails(event) {
    this.policyDetail = event;
    this.status = '2';
  }

  setNotificationSequence(event) {
    this.notificationSequence = event;
    this.status = '3';
  }
}
