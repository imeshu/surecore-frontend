import {Injectable} from '@angular/core';
import {HTTPCallService} from '../../../services/httpcall.service';
import {Observable} from 'rxjs';
import {HttpEvent, HttpResponse} from '@angular/common/http';
import {CommonType} from '../../../model/CommonType';
import {map} from 'rxjs/operators';
import {ClaimData} from '../models/ClaimData';
import {PostImgModel} from '../models/img-model';
import {TagNumbers} from '../models/TagNumbers';
import {fileServer} from '../../../config/enum';

@Injectable({
  providedIn: 'root'
})
export class ClaimsService {

  constructor(public http: HTTPCallService) {
  }

  verifyClaim(riskName, claimDate, policyNumber): Observable<HttpResponse<Object>> {
    const path = 'cl/verifyClaim?riskName=' + riskName + '&date=' + claimDate + '&policyNumber=' + policyNumber + '';
    return this.http.getResource(path);
    console.log(riskName);
  }

  getTypesOfLosses(): Observable<CommonType[]> {
    const path = 'base/references?type=CLLSTY';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

// get TAG Numbers
  getTags(tags, page, pageSize): Observable<TagNumbers[]> {
    if (sessionStorage.getItem('asAgent') === 'Y') {
      const path = 'cl/risks/search?riskName=' + tags
        + '&classCode=&productCode=LS&policyNumber=&customerCode=&intermediaryCode='
        + sessionStorage.getItem('partyCode') + '&page=' + page + '&limit=' + pageSize;

      return this.http.getResource(path).pipe(
        map(resp => {
          // @ts-ignore
          return resp.body.data.map(item => {
            const tag = new TagNumbers();
            tag.riskName = item.riskName;
            tag.riskReferenceNumber = item.riskReferenceNumber;
            tag.policyNumber = item.policyNumber;
            return tag;
          });
        })
      );
    } else {
      const path = 'cl/risks/search?riskName=' + tags
        + '&classCode=&productCode=LS&policyNumber=&customerCode=' + sessionStorage.getItem('partyCode')
        + '&intermediaryCode=&page=' + page + '&limit=' + pageSize;

      return this.http.getResource(path).pipe(
        map(resp => {
          // @ts-ignore
          return resp.body.data.map(item => {
            const tag = new TagNumbers();
            tag.riskName = item.riskName;
            tag.riskReferenceNumber = item.riskReferenceNumber;
            tag.policyNumber = item.policyNumber;
            return tag;
          });
        })
      );
    }
  }

  getCauseOfLosses(): Observable<CommonType[]> {
    const path = 'base/references?type=CLLSCU';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  notifyClaim(claim: ClaimData): Observable<HttpResponse<Object>> {
    const path = 'cl/claim';
    return this.http.postResource(path, claim);
  }

  uploadImage(file: File): Observable<HttpEvent<Object>> {
    const fd = new FormData();
    fd.append('file', file);
    const path = fileServer + 'v1/files/upload?path=claim';
    return this.http.postImage(path, fd);
  }

  submitImgs(model: PostImgModel): Observable<HttpResponse<Object>> {
    const path = 'cl/claim/' + model.sequence + '/images';
    // const path = 'cl/claimImage';
    return this.http.postResource(path, model.images);
  }


  getclaimlist(searchValue, searchFeild, AgentCode, CustomerCode, page): Observable<HttpResponse<Object>> {
    if (searchFeild === 'CN') {
      const path = 'cl/claim/search?claimNo=&policyNo=&customerName='
        + searchValue
        + '&intimationDate=&riskName=&intermediaryCode=' + AgentCode + '&customerCode=' + CustomerCode + '&page=' + page + '&limit=100';

      return this.http.getResource(path);
    } else if (searchFeild === 'PN') {
      // tslint:disable-next-line:max-line-length
      const path = 'cl/claim/search?claimNo=&policyNo=' + searchValue + '&customerName=&intimationDate=&riskName=&intermediaryCode=' + AgentCode + '&customerCode=' + CustomerCode + '&page=' + page + '&limit=100';
      return this.http.getResource(path);
    } else if (searchFeild === 'RN') {
      // tslint:disable-next-line:max-line-length
      const path = 'cl/claim/search?claimNo=&policyNo=&customerName=&intimationDate=&riskName=' + searchValue + '&intermediaryCode=' + AgentCode + '&customerCode=' + CustomerCode + '&page=' + page + '&limit=100';
      return this.http.getResource(path);
    } else if (searchFeild === 'CLN') {
      // tslint:disable-next-line:max-line-length
      const path = 'cl/claim/search?claimNo=' + searchValue + '&policyNo=&customerName=&intimationDate=&riskName=&intermediaryCode=' + AgentCode + '&customerCode=' + CustomerCode + '&page=' + page + '&limit=100';
      return this.http.getResource(path);
    }
    // 'cl/claim/search?claimNo=&policyNo=&customerName=&intimationDate=&riskName=&intermediaryCode=&customerCode=&page=1&limit=100';

  }

  getclist(notfSeqNo): Observable<HttpResponse<Object>> {
    const path = 'cl/claim/' + notfSeqNo + '/summary';
    return this.http.getResource(path);
  }

  getCurrencyType(): Observable<HttpResponse<Object>> {
    const path = 'base/localCurrencyCode';
    return this.http.getResource(path);
  }

}
