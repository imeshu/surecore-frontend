import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ClaimsMainComponent} from './claims-main/claims-main.component';
import {ClaimListComponent} from './claim-list/claim-list.component';
import {AddClaimNotificationComponent} from './add-claim-notification/add-claim-notification.component';

const routes: Routes = [{
  path: 'claim',
  component: ClaimsMainComponent
},
  {
    path: 'claim-list',
    component: ClaimListComponent
  },
  {
    path: 'app-claims-main',
    component: ClaimsMainComponent
  },
  {
    path: 'app-add-claim-notification',
    component: AddClaimNotificationComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClaimsRoutingModule {
}
