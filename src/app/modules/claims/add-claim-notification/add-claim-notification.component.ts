import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {ClaimsService} from '../services/claims.service';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {PolicyDetail} from '../models/PolicyDetail';
import {MatDialog} from '@angular/material';
import {PopupComponent} from './popup/popup.component';
import {TagNumbers} from '../models/TagNumbers';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Router} from '@angular/router';


@Component({
  selector: 'app-add-claim-notification',
  templateUrl: './add-claim-notification.component.html',
  styleUrls: ['./add-claim-notification.component.scss']
})
export class AddClaimNotificationComponent implements OnInit {

  private tagNumbers: TagNumbers[] = new Array();
  @Output() data = new EventEmitter();
  @Output() tagNumber = new EventEmitter();
  private verifyClaimForm: FormGroup;
  private test: string;
  private details: PolicyDetail;
  private today = new Date();
  private riskName;
  private policyNumber = '';
  private riskNameCheck = true;
  private page = 1;
  private pageSize = 20;


  /*infinity scroll*/
  private throttle: 300;
  private scrollDistance: 1;
  private scrollUpDistance: 2;
  private fromRoot = true;


  constructor(private fb: FormBuilder,
              private service: ClaimsService,
              public notificationService: NotificationDataTransferService,
              public dialog: MatDialog,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.verifyClaimForm = this.fb.group({
      'tag_number': ['', Validators.required],
      'claim_date': ['', Validators.required],
    });
    this.getTagNumber();
    this.riskNameCheck = true;
  }

  getTagNumber() {
    this.service.getTags(this.verifyClaimForm.controls['tag_number'].value, this.page, this.pageSize).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(Tags => {
      this.tagNumbers = Tags;
    });
  }

  verifyClaim() {
    if (this.verifyClaimForm.dirty && this.verifyClaimForm.valid) {
      const formattedDate = moment(this.verifyClaimForm.get('claim_date').value).format('YYYY-MM-DD HH:mm:ss');
      if (this.verifyClaimForm.value.tag_number != null) {
        this.riskName = this.verifyClaimForm.value.tag_number;
      }
      this.service.verifyClaim(this.riskName, formattedDate, this.policyNumber).subscribe(resp => {
        const body = resp.body as any;
        this.details = body.data;
        if (body.data) {
          this.data.emit(this.details);
          this.details.date = this.verifyClaimForm.controls['claim_date'].value;
          this.details.selectTagNumber = this.verifyClaimForm.controls['tag_number'].value;

        } else {
          this.openDialog();
          this.details = new PolicyDetail();
          this.details.riskName = this.verifyClaimForm.value.tag_number;
          this.details.date = this.verifyClaimForm.controls['claim_date'].value;
        }
      }, error => {
        this.notificationService.error(error.message);
      });
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(PopupComponent, {
      backdropClass: 'pop-overlay-s'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.data.emit(this.details);
      }
    });
  }

  selectTags(tag: TagNumbers) {
    this.riskName = tag.riskName;
    this.policyNumber = tag.policyNumber;
    this.verifyClaimForm.controls['tag_number'].setValue(tag.riskName);
  }

  searchTags() {
    this.getTagNumber();
  }

  // infinity
  onScrollDown() {
    console.log('ScrollDown');
    // TODO
    // if (this.numberOfPages >= this.page) {
    //   this.page++;
    //   this.service.getTags(this.verifyClaimForm.controls['tag_number'].value, this.page, this.pageSize).pipe(
    //     catchError(error => {
    //       return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    //     })
    //   ).subscribe(Tags => {
    //     this.tagNumbers = Tags;
    //   });
  }

  // }

}
