import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClaimNotificationComponent } from './add-claim-notification.component';

describe('AddClaimNotificationComponent', () => {
  let component: AddClaimNotificationComponent;
  let fixture: ComponentFixture<AddClaimNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClaimNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClaimNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
