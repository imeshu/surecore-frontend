export class AssessmentReportsSummary {
  date: string;
  finalization: Finalization;
}


export class Finalization {
  finalized: string;
}
