export class ImageModel {
  imageKey: string;
  value: string;
  progress: string;
  file: File;

  alias: string;
  key: string;
  reference: string;
  url: string;
}

export class ExportImgs {
  key: string;
  imageKey: string;
  imageType: string;
  name: string;
}

export class PostImgModel {
  sequence: string;
  createdUserCode: string;
  images: ExportImgs[] = new Array();

  constructor() {
    this.sequence = '17';
  }
}
