export class ClaimSummary {
  sequence: number;
  riskName: string;
  lossDate: Date;
  lossLocation: string;
  notifiedDate: Date;
  lossTypeCode: string;
  lossTypeDescription: string;
  causeOfLossCode: string;
  causeOfLossDescription: string;
  contactName: string;
  contactPhoneNumber: string;
  contactAddress: string;
  contactNicNumber: string;
  contactPartyCode: string;
  contactPartyName: string;
  modeOfInfoCode: string;
  modeOfInfoDescription: string;
  extentOfDamage: string;
  expectedLossAmount: string;
  remarks: string;
  policyNumber: string;
  riskReferenceNumber: string;
  intimatedDate: Date;
  intimatedUserCode: string;
  status: string;
  version: string;
  claimNumber: string;
  productCode: string;
  productDescription: string;
  classCode: string;
  classDescription: string;
  branchCode: string;
  branchDescription: string;
  policyBranchCode: string;
  policyBranchDescription: string;
  currencyCode: string;
  currencyDescription: string;
  currencyRate: string;
  customerCode: string;
  customerName: string;
  riskInformation: string;
  images: IMG[] = new Array();

}

export class IMG {
  imageKey: string;
  imageUrl: string;
  imageType: string;
  name: string;
  sequence: string;
  version: string;
}
