export class PolicyDetail {
  policyNumber: string;
  riskReferenceNumber: string;
  riskName: string;
  ownerName: string;
  customerName: string;
  risk: RiskModel;
  date: string;
  selectTagNumber: string;
}

export class RiskModel {
  riskName: string;
  riskInformation: RiskInformation[];
}

export class RiskInformation {
  description: string;
  value: string;
}
