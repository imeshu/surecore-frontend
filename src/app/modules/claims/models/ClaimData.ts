export class ClaimData {
  // riskName: string;
  // lossDate: string;
  // lossTypeCode: string;
  // lossCauseCode: string;
  // contactName: string;
  // contactPhone: string;
  // contactAddress: string;
  // modeOfInfoCode = 'ONLINE';
  // expectedLossAmount: number;
  // policyNumber: string;
  // riskReferenceNumber: string;
  // intimatedUserCode = '';
  // createdUserCode: string;
  // branchCode: string;
  // currencyCode: string;

  riskName: string;
  lossDate: string;
  lossTypeCode: string;
  lossCauseCode: string;
  contactName: string;
  contactPhone: string;
  contactAddress: string;
  modeOfInfoCode: string;
  expectedLossAmount: string;
  branchCode: string;
  currencyCode: string;
  policyNumber: string;
}
