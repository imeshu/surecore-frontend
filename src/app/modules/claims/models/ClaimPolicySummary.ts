export class ClaimPolicySummary {
  classCode: string;
  classDescription: string;
  productCode: string;
  productDescription: string;
  branchCode: string;
  branchDescription: string;
  policyNumber: string;
  customerCode: string;
  customerName: string;
  policyFromDate: string;
  policyEndDate: string;
  effectiveDate: string;
  currencyCode: string;
  eventLimit: string;
  annualLimit: string;
  policyInformation: string;
  policyExcess: string;
  policyDocuments: string;
  // location: location[] = new Array();
  risk: Risks;
}

export class Risks {
  riskName: string;
  riskReferenceNumber: string;
  riskSumInsured: string;
  eventLimit: string;
  annualLimit: string;
  financialInterest: string;
  riskInformation: Riskinfo[] = new Array();
}

export class Riskinfo {
  description: string;
  value: string;
}
