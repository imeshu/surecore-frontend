
export class ClaimListItem {
  sequence: number;
  claimNumber: string;
  riskName: string;
  lossDate: Date;
  lossTypeCode: string;
  lossTypeDescription: string;
  lossCauseCode: string;
  lossCauseDescription: string;
  contactName: string;
  contactPhone: string;
  contactAddress: string;
  modeOfInfoCode: string;
  extentOfDamage: string;
  expectedLossAmount: string;
  remarks: string;
  policyNumber: string;
  customerName: string;
  riskReferenceNumber: string;
  intimatedUserCode: string;
  createdUserCode: string;
  active: boolean;

}
