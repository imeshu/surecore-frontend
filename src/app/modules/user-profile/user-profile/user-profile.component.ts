import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../services/user-profile.service';
import { Profile } from '../models/profile';
import * as moment from 'moment';
import { TitleCasePipe } from '@angular/common';
import { ImgUpdateService } from '../services/img-update.service';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  public profile: Profile;
  nicNumberDeafult: string = '';
  email: string = '';
  mobleNumber: string = '';
  isDataLoaded = true;
  imageUrl = '';
  address = '';
  name = '';
  selectedItem = '';
  constructor(public service: UserProfileService, public imgUpdateService: ImgUpdateService) { }

  ngOnInit() {
    this.getUserProfile();
  }

  getUserProfile() {
    this.isDataLoaded = true;
    this.service.getUserProfile().subscribe(resp => {
      const body = resp.body as any;
      this.profile = new Profile();
      this.profile.userProfile = body.data.profile;
      this.profile.address = body.data.address;
      this.profile.contact = body.data.contact;
      this.profile.identification = body.data.identification;
      this.profile.image = body.data.image;
      this.name = body.data.profile.name;
      this.splitName();
      if (this.profile.image !== null) {
        // this.imageUrl = this.profile.image.image;
        this.imgUpdateService.setImg(this.profile.image.image);
        this.imgUpdateService.getImg()
      } else {
        this.imageUrl = '';
      }
      if (this.profile.address !== null) {
        this.address = this.profile.address.address;
      } else {
        this.address = '';
      }
      // make date time format
      this.profile.userProfile.dateOfBirth = moment(this.profile.userProfile.dateOfBirth, 'YYYY-MM-DD HH:mm:ss').format('DD MMMM YYYY');
      if (this.profile.identification !== null) {
        for (let i = 0; i < this.profile.identification.length; i++) {
          if (this.profile.identification[i]['registrationType'] === 'NIDN') {
            this.profile.userProfile.nicNumberDeafult = this.profile.identification[i]['registrationNumber'];
            this.profile.identification[i]['validFromDate'] = moment(this.profile.identification[i]['validFromDate']).format('DD MMMM YYYY');
            this.profile.identification[i]['validToDate'] = moment(this.profile.identification[i]['validToDate']).format('DD MMMM YYYY');
          } else {
            this.profile.identification[i]['validFromDate'] = moment(this.profile.identification[i]['validFromDate']).format('DD MMMM YYYY');
            this.profile.identification[i]['validToDate'] = moment(this.profile.identification[i]['validToDate']).format('DD MMMM YYYY');
          }
        }
      }
      if (this.profile.contact !== null) {
        for (let i = 0; i < this.profile.contact.length; i++) {
          if (this.profile.contact[i]['contactType'] === 'CEMIL') {
            this.profile.userProfile.email = this.profile.contact[i]['contactNumber'];
          }
          if (this.profile.contact[i]['contactType'] === 'CMOBL') {
            this.profile.userProfile.mobleNumber = this.profile.contact[i]['contactNumber'];
          }
        }
      }
      this.isDataLoaded = false;
    }, error => {
      console.log(error);
      this.isDataLoaded = false;
    });

  }
  item1ActiveClass = true;
  item2ActiveClass = false;
  item3ActiveClass = false;
  item4ActiveClass = false;
  item5ActiveClass = false;

  listClick(event, newValue) {
    console.log(newValue);
    this.selectedItem = newValue;
    switch (newValue) {
      case 'item1': {
        this.item1ActiveClass = true;
        this.item2ActiveClass = false;
        this.item3ActiveClass = false;
        this.item4ActiveClass = false;
        this.item5ActiveClass = false;
        break;
      }
      case 'item2': {
        this.item1ActiveClass = false;
        this.item2ActiveClass = true;
        this.item3ActiveClass = false;
        this.item4ActiveClass = false;
        this.item5ActiveClass = false;
        break;
      }
      case 'item3': {
        this.item1ActiveClass = false;
        this.item2ActiveClass = false;
        this.item3ActiveClass = true;
        this.item4ActiveClass = false;
        this.item5ActiveClass = false;
        break;
      }
      case 'item4': {
        this.item1ActiveClass = false;
        this.item2ActiveClass = false;
        this.item3ActiveClass = false;
        this.item4ActiveClass = true;
        this.item5ActiveClass = false;
        break;
      }
      case 'item5': {
        this.item1ActiveClass = false;
        this.item2ActiveClass = false;
        this.item3ActiveClass = false;
        this.item4ActiveClass = false;
        this.item5ActiveClass = true;
        break;
      }
    }
  }

  // Split name when have initials
  nameArray = [];
  splitName() {
    let format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(this.name)) {
      let newName = '';
      this.nameArray = this.name.split('.');
      for (let i = 0; i < this.nameArray.length; i++) {
        if (i === (this.nameArray.length - 1)) {
          newName += new TitleCasePipe().transform(this.nameArray[i]);
        } else {
          newName += new TitleCasePipe().transform(this.nameArray[i]) + '. ';
          console.log(this.name);
        }
      }
      this.name = newName;
    } else {
      this.name = new TitleCasePipe().transform(this.name);
    }
  }
}
