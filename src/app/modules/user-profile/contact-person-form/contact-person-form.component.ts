import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { UserProfile } from '../models/userProfile';
import { UserProfileService } from '../services/user-profile.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';

@Component({
  selector: 'app-contact-person-form',
  templateUrl: './contact-person-form.component.html',
  styleUrls: ['./contact-person-form.component.scss']
})
export class ContactPersonFormComponent implements OnInit {

  phoneNumber: string = '';
  address: string = '';
  mobileNumber: string = '';
  email: string = '';
  name: string = '';
  relationship: string = '';
  contactPersonSequence: string = '';
  version: number;
  skypeId: string = '';
  status: string = '';

  isDataLoaded = true;


  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  relationshipFormControl = new FormControl('', [
    Validators.required
  ]);
  addressFormControl = new FormControl('', [
    Validators.required
  ]);
  phoneNumberFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
  ]);
  mobileNumberFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router, private route: ActivatedRoute) {
    if (this.router.url.includes('user-profile/manage-contact-person')) {
      this.contactPersonSequence = this.route.snapshot.paramMap.get('sequnce');
     this.getContactPerson();
      this.isDataLoaded = false;
    }
    this.isDataLoaded = false;
    console.log('tt');
  }

  ngOnInit() {
  }



  saveContactPerson() {
    this.isDataLoaded = true;
    console.log('test');
    let contactPerson = {
      'name': this.name,
      'address': this.address,
      'emailAddress': this.email,
      'mobileNumber': this.mobileNumber,
      'phoneNumber': '+94' + this.phoneNumber,
      'relationship': this.relationship
    }
    this.service.saveContactPerson(contactPerson).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/contact-person']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });

  }

  getContactPerson(){
    this.service.getContactPerson(this.contactPersonSequence).subscribe(resp => {
      const body = resp.body as any;
      this.contactPersonSequence = body.data.sequence;
      this.address = body.data.address;
      this.email = body.data.emailAddress;
      this.mobileNumber = body.data.mobileNumber;
      this.version = parseInt(body.data.version, 0);
      this.name = body.data.name;
      this.phoneNumber = body.data.phoneNumber;
      this.relationship = body.data.relationship;
      this.skypeId = body.data.skypeId;
      this.status = body.data.status;
      this.isDataLoaded = false;
    }, error => {
      this.isDataLoaded = false;
    });
  }

  updateContactPerson() {
    this.isDataLoaded = true;
    let contactPerson = {
      'sequence': this.contactPersonSequence,
      'name': this.name,
      'address': this.address,
      'emailAddress': this.email,
      'mobileNumber': this.mobileNumber,
      'phoneNumber': this.phoneNumber,
      'relationship': this.relationship,
      'version': this.version,
      'status': this.status
    }
    this.service.updateConatctPerson(this.contactPersonSequence, contactPerson).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/contact-person']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  deleteContactPerson(sequence) {
    this.isDataLoaded = true;
    // alert(sequence);
    this.service.deleteConatctPerson(sequence).subscribe(resp => {
      const body = resp.body as any;
      this.displaySuccessMessage(body);
      this.isDataLoaded = false;
      this.router.navigate(['user-profile/contact-person']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

/**
   * common functions 
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }
  
}
