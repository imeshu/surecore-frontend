import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../services/user-profile.service';
import { FormControl, Validators } from '@angular/forms';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { invalid } from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'app-identification-form',
  templateUrl: './identification-form.component.html',
  styleUrls: ['./identification-form.component.scss']
})
export class IdentificationFormComponent implements OnInit {


  identificationTypes = [];
  identificationType = '';

  identificationSequnce: string = '';
  registrationNo: string = '';
  issuingAuthority: string = '';
  validFrom = null;
  validTo = null;
  version: number;
  status: string = '';
  registrationTypeDescription: string = '';

  isDataLoaded = true;
  identificationTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  registrationNoFormControl = new FormControl('', [
    Validators.required
  ]);
  nicFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^([0-9]{9}[x|X|v|V]|[0-9]{12})$')
  ]);
  issuingAuthorityFormControl = new FormControl('', [
    Validators.required
  ]);
  validFromDateFormControl = new FormControl('', [
    Validators.required
  ]);
  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router, private route: ActivatedRoute) {
    if (this.router.url.includes('user-profile/manage-identification')) {
      this.identificationSequnce = this.route.snapshot.paramMap.get('sequnce');
     this.getIdentification();
      this.isValidated = true;
      this.isDataLoaded = false;
    }
    this.isDataLoaded = false;
  }

  ngOnInit() {
    this.getIdentificationTypes();
  }


  getIdentificationTypes() {
    this.service.getIndentificationTypes().subscribe(resp => {
      const body = resp.body as any;
      this.identificationTypes = body.data;
    }, error => {
      console.log(error);
    });
  }


  saveIdentification() {
    this.isDataLoaded = true;
    if (this.validTo === null) {
      let identification = {
        'registrationNumber': this.registrationNo,
        'registrationType': this.identificationType,
        'issueingAuthority': this.issuingAuthority,
        'validFromDate': moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss'),
        'validToDate': null
      }
      this.service.saveIdentification(identification).subscribe(resp => {
        const body = resp.body as any;
        this.isDataLoaded = false;
        this.displaySuccessMessage(body);
        this.router.navigate(['user-profile/identification']);
        this.ngOnInit();
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
        console.log(error);
      });
    } else {
      let identification = {
        'registrationNumber': this.registrationNo,
        'registrationType': this.identificationType,
        'issueingAuthority': this.issuingAuthority,
        'validFromDate': moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss'),
        'validToDate': moment(this.validTo).format('YYYY-MM-DD HH:mm:ss'),
      }

      this.service.saveIdentification(identification).subscribe(resp => {
        const body = resp.body as any;
        this.isDataLoaded = false;
        this.displaySuccessMessage(body);
        this.router.navigate(['user-profile/identification']);
        this.ngOnInit();
      }, error => {
        this.displayErrorMessage(error);
        this.isDataLoaded = false;
        console.log(error);
      });
    }


  }

  updateIdentification() {
    this.isDataLoaded = true;
    console.log(moment(this.validTo).isValid);
    console.log(moment(this.validFrom).isValid);
    this.validateFeilds();

    if (this.identificationType === 'NIDN') {
      let identification = {
        'registrationNumber': this.registrationNo,
        'registrationType': this.identificationType,
        'issueingAuthority': this.issuingAuthority,
        // "Unparseable date: "2019-06-18T18:30:00.000Z""
        'validFromDate': moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss'),
        'validToDate': null,//moment(this.validTo, 'YYYY-MM-DDTHH:mm:ss.000z').format('YYYY-MM-DD HH:mm:ss'),
        'sequence': this.identificationSequnce,
        'version': this.version,
        'status': this.status
      }
      this.service.updateIdentification(this.identificationSequnce, identification).subscribe(resp => {
        const body = resp.body as any;
        this.isDataLoaded = false;
        this.displaySuccessMessage(body);
        this.router.navigate(['user-profile/identification']);
        this.ngOnInit();
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
        console.log(error);
      });
    } else {
      if (this.validTo === null) {
        let identification = {
          'registrationNumber': this.registrationNo,
          'registrationType': this.identificationType,
          'issueingAuthority': this.issuingAuthority,
          // "Unparseable date: "2019-06-18T18:30:00.000Z""
          'validFromDate': moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss'),
          'validToDate': null,
          'sequence': this.identificationSequnce,
          'version': this.version,
          'status': this.status
        }
        this.service.updateIdentification(this.identificationSequnce, identification).subscribe(resp => {
          const body = resp.body as any;
          this.isDataLoaded = false;
          this.displaySuccessMessage(body);
          this.router.navigate(['user-profile/identification']);
          this.ngOnInit();
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
          console.log(error);
        });
      } else {
        let identification = {
          'registrationNumber': this.registrationNo,
          'registrationType': this.identificationType,
          'issueingAuthority': this.issuingAuthority,
          // "Unparseable date: "2019-06-18T18:30:00.000Z""
          'validFromDate': moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss'),
          'validToDate': moment(this.validTo).format('YYYY-MM-DD HH:mm:ss'),
          'sequence': this.identificationSequnce,
          'version': this.version,
          'status': this.status
        }
        this.service.updateIdentification(this.identificationSequnce, identification).subscribe(resp => {
          const body = resp.body as any;
          this.isDataLoaded = false;
          this.displaySuccessMessage(body);
          this.router.navigate(['user-profile/identification']);
          this.ngOnInit();
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
          console.log(error);
        });
      }

    }


  }



  isValidated: boolean = false;
  appyClass = false;

  validateFeilds() {
    if (this.identificationType === 'NIDN') {
      if (this.identificationTypeFormControl.hasError('required') || this.nicFormControl.hasError('required') || this.nicFormControl.hasError('pattern') || this.issuingAuthorityFormControl.hasError('required') || this.validFromDateFormControl.hasError('required')) {
        this.isValidated = false;
      } else {
        this.isValidated = true;
      }
    } else {
      if (this.identificationTypeFormControl.hasError('required') || this.registrationNoFormControl.hasError('required') || this.issuingAuthorityFormControl.hasError('required') || this.validFromDateFormControl.hasError('required')) {
        this.isValidated = false;
      } else {
        this.isValidated = true;
      }
    }
    if (this.router.url.includes('user-profile/manage-identification')) {
      if (this.identificationType === 'NIDN') {
        if (this.validateNIC(this.registrationNo) && this.validFrom !== null && this.issuingAuthority !== '') {
          this.isValidated = true;
          this.appyClass = false;
        } else {
          this.isValidated = false;
          this.appyClass = true;
        }
      } else {
        if (this.registrationNo !== '' && this.validFrom !== null && this.issuingAuthority !== '') {
          this.isValidated = true;
        } else {
          this.isValidated = false;
        }
      }

    }
  }


  validateNIC(nic) {
    //NIC validation
    const PhoneNumberRegex = new RegExp('^([0-9]{9}[x|X|v|V]|[0-9]{12})$');
    let test = PhoneNumberRegex.test(nic);
    return test;
  }

  deleteIdentification(sequence) {
    // alert(sequence);
    this.isDataLoaded = true;
    this.service.deleteIdentification(sequence).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/identification']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  getIdentification(){
    this.isDataLoaded = true;
    this.service.getIdentication(this.identificationSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.registrationNo = body.data.registrationNumber;
      this.issuingAuthority =  body.data.issueingAuthority;
      if (this.issuingAuthority === null || this.issuingAuthority === 'null') {
        this.issuingAuthority = '';
      }
      this.identificationType =  body.data.registrationType;
      this.version =  parseInt(body.data.version, 0);
      this.status =  body.data.status;
      this.registrationTypeDescription =  body.data.registrationTypeDescription;
      this.validFrom = new Date( body.data.validFromDate);
      if (this.validFrom === null || this.validFrom === 'null') {
        this.validFrom = '';
      }
      this.validTo = new Date( body.data.validToDate);
      if (this.validTo === null || this.validTo === 'null') {
        this.validTo = '';
      }
    }, error => {
      this.isDataLoaded = false;
    });
  }

  /**
     * common functions 
     */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }


}
