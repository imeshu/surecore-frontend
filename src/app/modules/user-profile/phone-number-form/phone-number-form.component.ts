/* tslint:disable:max-line-length */
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { UserProfile } from '../models/userProfile';
import { UserProfileService } from '../services/user-profile.service';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-phone-number-form',
  templateUrl: './phone-number-form.component.html',
  styleUrls: ['./phone-number-form.component.scss']
})
export class PhoneNumberFormComponent implements OnInit {
  // mobileFormControl = new FormControl('', [Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$'), Validators.required])


  // tslint:disable-next-line:max-line-length
  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router, private route: ActivatedRoute) {

    if (this.router.url.includes('user-profile/manage-contact')) {
      this.contactSequnce = this.route.snapshot.paramMap.get('sequnce');
     this.getPhoneNumber();
      this.isValidated = true;
      this.isDataLoaded = false;
    }
    this.isDataLoaded = false;
  }

  contactTypes = [];
  contactType = '';
  contactNumber = '';
  version: number;
  contactSequnce = '';
  status = '';
  contactTypeDescription: string;
  defaultStatus;

  contactTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  contactNoFormControl = new FormControl('', [
    Validators.required
  ]);
  phoneNumberFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);

  isDataLoaded = true;


  isValidated = false;
  appyClass = false;

  ngOnInit() {
    this.getContactTypes();
  }

  savePhoneNumber() {
    this.isDataLoaded = true;
    const contact = {
      'contactType': this.contactType,
      'contactNumber': this.contactNumber,
      'default': false
    };
    this.service.savePhoneNumber(contact).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/contact']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  updatePhoneNumber() {
    this.isDataLoaded = true;
    const contact = {
      'contactType': this.contactType,
      'contactNumber': this.contactNumber,
      'status': this.status,
      'sequence': this.contactSequnce,
      'version': this.version,
      'default': this.defaultStatus
    };

    this.service.updatePhoneNumber(this.contactSequnce, contact).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/contact']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  getContactTypes() {
    this.service.getContactTypes().subscribe(resp => {
      const body = resp.body as any;
      this.contactTypes = body.data;
    }, error => {
      console.log(error);
    });
  }

  deletePhoneNumber(sequence) {
    // alert(sequence);
    this.isDataLoaded = true;
    this.service.deletePhoneNumber(sequence).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/contact']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }



  validateMobileNumber(data) {
    // Mobile  number validation
    const PhoneNumberRegex = new RegExp('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$');
    const test = PhoneNumberRegex.test(data);
    return test;
  }


  validateEmail(data) {
    const emailValidator = '^[^@]+@[^@]+\.[^@]+$';
    if (!data.match(emailValidator)) {
      return false;
    } else {
      return true;
    }

  }

  validateFeilds() {
    if (this.contactType === 'CPHON') {
      // tslint:disable-next-line:max-line-length
      if (this.phoneNumberFormControl.hasError('required') || this.phoneNumberFormControl.hasError('pattern') || this.contactTypeFormControl.hasError('required')) {
        this.isValidated = false;
      } else {
        this.isValidated = true;
      }
    } else if (this.contactType === 'CMOBL') {
      if (this.phoneNumberFormControl.hasError('required') || this.phoneNumberFormControl.hasError('pattern') || this.contactTypeFormControl.hasError('required')) {
        this.isValidated = false;
      } else {
        this.isValidated = true;
      }
    } else if (this.contactType === 'CEMIL') {
      if (this.emailFormControl.hasError('required') || this.emailFormControl.hasError('email') || this.contactTypeFormControl.hasError('required')) {
        this.isValidated = false;
      } else {
        this.isValidated = true;
      }
    } else if (this.contactType === 'CSKYP') {
      if (this.contactNoFormControl.hasError('required') || this.contactTypeFormControl.hasError('required')) {
        this.isValidated = false;
      } else {
        this.isValidated = true;
      }
    }
    if (this.router.url.includes('user-profile/manage-contact')) {
      if (this.contactType === 'CEMIL' && this.validateEmail(this.contactNumber) ) {
        this.isValidated = true;
        this.appyClass = false;
      } else if ((this.contactType === 'CPHON' || this.contactType === 'CMOBL') && this.validateMobileNumber(this.contactNumber) ) {
        this.isValidated = true;
        this.appyClass = false;
      } else if (this.contactType === 'CSKYP' && this.contactNumber !== '') {
        this.isValidated = true;
        this.appyClass = false;
      } else {
        this.appyClass = true;
        this.isValidated = false;
      }
    }
  }

getPhoneNumber() {
  this.isDataLoaded = true;
  this.service.getPhoneNumber(this.contactSequnce).subscribe(resp => {
    const body = resp.body as any;
    this.contactType = body.data.contactType;
    this.version = parseInt(body.data.version, 0);
    this.status = body.data.status;
    this.contactTypeDescription = body.data.contactTypeDescription;
    this.defaultStatus = body.data.default;
    this.contactNumber = body.data.contactNumber;
    this.isDataLoaded = false;
  }, error => {
    this.isDataLoaded = false;
  });
}

  /**
     * common functions
     */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }

}
