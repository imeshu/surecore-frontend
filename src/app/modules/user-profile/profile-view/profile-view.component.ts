import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';

import { UserProfileService } from '../services/user-profile.service';
import * as moment from 'moment';
import { Profile } from '../models/profile';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit {

  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router, private route: ActivatedRoute) { }

  public profile;

  public lastUpdatedDate = '';

  public displayingView: string;

  public viewChangedPassword: boolean;

  oldPasswordFormControl = new FormControl('', [
    Validators.required
  ]);

  newPasswordFormControl = new FormControl('', [
    Validators.required
  ]);

  isDataLoaded = true;

  hideOld = true;
  hideNew = true;
  dateOfBirth = '';
  imageUrl = '';
  address = '';
  name = '';
  nicNumberDeafult = '';
  mobleNumber = '';
  email = '';
  genderDescription = '';
  languageDescription = '';
  maritalStatusDescription = '';


  // passwords
  oldPassword = '';
  newPassword = '';

  id = '';
  token = '';
  type = '';

  newPasswordVisible = false;
  oldPasswordVisible = false;

  isValidatedPassword = false;
  isMatchedPassword = false;

  showWeekPasswordlbl = false;
  showStrongPasswordLbl = false;
  showStrongMatchingPasswordlbl = false;
  showDoNotMatchLbl = false;

  isfocusNewPasswordfeild = false;

  // password validate checkboxes
  checkedMinimum8Characters = false;
  checkedUppsercaseLetter = false;
  checkedLowerCaseLetter = false;
  checkedNumber = false;
  checkedSymbol = false;

  // Split name when have initials
  nameArray = [];




  ngOnInit() {
    this.displayingView = 'list-form';
    this.getUserProfile();
    this.viewChangedPassword = false;
  }

  getUserProfile() {
    this.isDataLoaded = true;
    this.service.getUserProfile().subscribe(resp => {
      const body = resp.body as any;
      this.profile = new Profile();
      this.profile.userProfile = body.data.profile;
      this.profile.address = body.data.address;
      this.profile.contact = body.data.contact;
      this.profile.identification = body.data.identification;
      this.profile.image = body.data.image;
      if (this.profile.image !== null) {
        this.imageUrl = this.profile.image.image;
      } else {
        this.imageUrl = '';
      }

      if (this.profile.address !== null) {
        this.address = this.profile.address.address;
      } else {
        this.address = '';
      }

      if (body.data.profile.dateOfBirth === null) {
        this.dateOfBirth = '';
      } else {
        this.dateOfBirth = moment(body.data.profile.dateOfBirth, 'YYYY-MM-DD HH:mm:ss').format('DD MMMM YYYY');
      }

      this.lastUpdatedDate = moment(body.data.profile.lastUpdatedDate, 'YYYY-MM-DD HH:mm:ss').format('DD MMMM YYYY');
      // make date time format
      // this.profile.userProfile.dateOfBirth = moment(this.profile.userProfile.dateOfBirth, 'YYYY-MM-DD HH:mm:ss').format('DD MMMM YYYY');
      if (this.profile.identification !== null) {
        for (let i = 0; i < this.profile.identification.length; i++) {
          if (this.profile.identification[i]['registrationType'] === 'NIDN') {
            this.nicNumberDeafult = this.profile.identification[i]['registrationNumber'];
            this.profile.identification[i]['validFromDate'] = moment(this.profile.identification[i]['validFromDate']).format('DD MMMM YYYY');
            this.profile.identification[i]['validToDate'] = moment(this.profile.identification[i]['validToDate']).format('DD MMMM YYYY');
          } else {
            this.profile.identification[i]['validFromDate'] = moment(this.profile.identification[i]['validFromDate']).format('DD MMMM YYYY');
            this.profile.identification[i]['validToDate'] = moment(this.profile.identification[i]['validToDate']).format('DD MMMM YYYY');
          }
        }
      }
      if (this.profile.contact !== null) {
        for (let i = 0; i < this.profile.contact.length; i++) {
          if (this.profile.contact[i]['contactType'] === 'CEMIL') {
            this.email = this.profile.contact[i]['contactNumber'];
          }
          if (this.profile.contact[i]['contactType'] === 'CMOBL') {
            this.mobleNumber = this.profile.contact[i]['contactNumber'];
          }
        }
      }
      this.isDataLoaded = false;
      this.name = this.profile.userProfile.name;
      this.splitName();
      this.maritalStatusDescription = this.profile.userProfile.maritalStatusDescription;
      this.genderDescription = this.profile.userProfile.genderDescription;
      this.languageDescription = this.profile.userProfile.languageDescription;

    }, error => {
      this.isDataLoaded = false;
      console.log(error);
    });

  }



  checkPassowrd() {

    if (this.newPassword !== '') {

      // contains minimum 8 characters
      if (new RegExp('^(?=.{8,})').test(this.newPassword)) {
        this.checkedMinimum8Characters = true;
      } else {
        this.checkedMinimum8Characters = false;
      }

      // contains a Uppercase letter
      if (new RegExp('^(?=.*[A-Z])').test(this.newPassword)) {
        this.checkedUppsercaseLetter = true;
      } else {
        this.checkedUppsercaseLetter = false;
      }

      // contains a lowercase letter
      if (new RegExp('^(?=.*[a-z])').test(this.newPassword)) {
        this.checkedLowerCaseLetter = true;
      } else {
        this.checkedLowerCaseLetter = false;
      }

      // contains a number
      if (new RegExp('^(?=.*[0-9])').test(this.newPassword)) {
        this.checkedNumber = true;
      } else {
        this.checkedNumber = false;
      }

      // contains a symbol
      if (new RegExp('^(?=.*[!@#\$%\^&\*\-])').test(this.newPassword)) {
        this.checkedSymbol = true;
      } else {
        this.checkedSymbol = false;
      }


    }

  }



  validatePassword() {

    if (this.newPassword !== '' && this.newPassword.length >= 8) {
      const regex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\-])(?=.{8,})');

      this.isValidatedPassword = regex.test(this.newPassword);
      if (this.isValidatedPassword) {

      } else {
        this.showWeekPasswordlbl = true;
        this.showStrongPasswordLbl = false;
        this.showDoNotMatchLbl = false;
        this.showStrongMatchingPasswordlbl = false;
      }
      this.isfocusNewPasswordfeild = false;
      //  }
      // password validation checkbox

    } else {
      this.showWeekPasswordlbl = false;
      this.isValidatedPassword = false;
      this.showStrongPasswordLbl = false;
      this.showDoNotMatchLbl = false;
      this.showStrongMatchingPasswordlbl = false;
      this.isfocusNewPasswordfeild = true;
    }

    // password validation checkbox
    this.checkPassowrd();
  }


  newPasswordToggleEye() {
    if (this.newPasswordVisible === false) {
      this.newPasswordVisible = true;
    } else {
      this.newPasswordVisible = false;
    }

  }

  oldPasswordToggleEye() {
    if (this.oldPasswordVisible === false) {
      this.oldPasswordVisible = true;
    } else {
      this.oldPasswordVisible = false;
    }
  }

  onfocusNewPasswordfeild() {
    this.isfocusNewPasswordfeild = true;
  }

  onBlurNewPasswordfeild() {
    this.isfocusNewPasswordfeild = false;
  }
  updatePassword() {
    this.isDataLoaded = true;
    const passwords = {
      'oldPassword': this.oldPassword,
      'newPassword': this.newPassword
    };

    this.service.changePassword(passwords).subscribe(resp => {
      this.isDataLoaded = true;
      const body = resp.body as any;
      this.displaySuccessMessage(body);
      this.isDataLoaded = false;
    }, error => {
      this.displayErrorMessage(error);
      this.isDataLoaded = false;
      console.log(error);
    });
  }
  splitName() {
    const format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(this.name)) {
      let newName = '';
      this.nameArray = this.name.split('.');
      for (let i = 0; i < this.nameArray.length; i++) {
        if (i === (this.nameArray.length - 1)) {
          newName += new TitleCasePipe().transform(this.nameArray[i]);
        } else {
          newName += new TitleCasePipe().transform(this.nameArray[i]) + '. ';
          console.log(this.name);
        }
      }
      this.name = newName;
    } else {
      this.name = new TitleCasePipe().transform(this.name);
    }
  }
  /**
   * common functions
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }



}
