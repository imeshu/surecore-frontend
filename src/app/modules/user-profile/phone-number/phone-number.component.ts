import { Component, OnInit, Input } from '@angular/core';
import { UserProfileService } from '../services/user-profile.service';
import { Profile } from '../models/profile';
import { Contact } from '../models/contact';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-phone-number',
  templateUrl: './phone-number.component.html',
  styleUrls: ['./phone-number.component.scss']
})
export class PhoneNumberComponent implements OnInit {

  public contacts: Contact[];
  isDataLoaded = true;
  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router) { }
  ngOnInit() {
    this.getUserProfilePhoneNumbers();
  }
  getUserProfilePhoneNumbers(){
    this.isDataLoaded = true;
    this.service.getUserProfilePhoneNumbers().subscribe(resp => {
      const body = resp.body as any;
      this.contacts = body.data;
      this.isDataLoaded = false;
    }, error => {
      this.isDataLoaded = false;
      console.log(error);
    });
  }

  deletePhoneNumber(sequence){
    // alert(sequence);
    this.isDataLoaded = true;
    this.service.deletePhoneNumber(sequence).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/phone-number']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      console.log(error);
      this.displayErrorMessage(error);
    });
  }

  /**
   * common functions 
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }
  

}
