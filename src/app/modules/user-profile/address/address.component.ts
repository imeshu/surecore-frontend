import { Component, OnInit, Input } from '@angular/core';
import { UserProfileService } from '../services/user-profile.service';
import { Address } from '../models/address';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {

   public addresses: Address[];
   isDataLoaded = true;
  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router) { }

  ngOnInit() {
    this.isDataLoaded = false;
    this.getUserProfileAddresses();
  }

  getUserProfileAddresses(){
    this.service.getUserProfileAddress().subscribe(resp => {
      const body = resp.body as any;
      this.addresses = body.data;
    }, error => {
      console.log(error);
    });
  }

  deleteAddress(sequence){
    this.isDataLoaded = true;
    // alert(sequence);
    this.service.deleteAddress(sequence).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/address']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  /**
   * common functions 
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }
  
}
