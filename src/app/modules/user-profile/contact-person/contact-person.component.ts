import { Component, OnInit, Input } from '@angular/core';
import { UserProfileService } from '../services/user-profile.service';
import { Profile } from '../models/profile';
import { ContactPerson } from '../models/contactPerson';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router } from '@angular/router';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-contact-person',
  templateUrl: './contact-person.component.html',
  styleUrls: ['./contact-person.component.scss']
})
export class ContactPersonComponent implements OnInit {

  public contactPersons: ContactPerson[];
  isDataLoaded = true;
  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router) { }

  ngOnInit() {
    this.getUserProfileContactPersons();
  }

  getUserProfileContactPersons() {
    this.isDataLoaded = true;
    this.service.getUserProfileContactPersons().subscribe(resp => {
      const body = resp.body as any;
      this.contactPersons = body.data;
      if (this.contactPersons !== null) {
        for (let i = 0; i < this.contactPersons.length; i++) {
          let nameArray = [];
          const format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
          if (format.test(this.contactPersons[i]['name'])) {
            let newName = '';
            nameArray = this.contactPersons[i]['name'].split('.');
            // tslint:disable-next-line:no-shadowed-variable
            for (let i = 0; i < nameArray.length; i++) {
              if (i === (nameArray.length - 1)) {
                newName += new TitleCasePipe().transform(nameArray[i]);
              } else {
                newName += new TitleCasePipe().transform(nameArray[i]) + '. ';
              }
            }
            this.contactPersons[i]['name'] = newName;
          } else {
            this.contactPersons[i]['name'] = new TitleCasePipe().transform(this.contactPersons[i]['name']);
          }
        }
      }

      this.isDataLoaded = false;
    }, error => {
      this.isDataLoaded = false;
    });

  }
  deleteContactPerson(sequence) {
    // alert(sequence);
    this.isDataLoaded = true;
    this.service.deleteConatctPerson(sequence).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/contact-person']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }
  /**
   * common functions
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }


}
