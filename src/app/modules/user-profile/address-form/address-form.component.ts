import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { UserProfileService } from '../services/user-profile.service';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit {


  address: string = '';
  addressTypes = [];
  addressType = '';
  version: number;
  addressSequnce: string = '';
  status: string = '';
  addressTypeDescription: string;
  geometry = [];
  defaultAddressFlag: string = '';

  addressTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  addressFormControl = new FormControl('', [
    Validators.required
  ]);
  public isDataLoaded = true;
  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router, private route: ActivatedRoute) {

    if (this.router.url.includes('user-profile/manage-address')) {
      this.addressSequnce = this.route.snapshot.paramMap.get('sequnce');
     this.getAddress();
      this.isDataLoaded = false;
    }
    this.isDataLoaded = false;
  }

  ngOnInit() {

    this.getAddressTypes();
  }

  saveAddress() {
    this.isDataLoaded = true;
    let address = {
      'address': this.address,
      'addressType': this.addressType,
      'defaultAddressFlag': 'N'
    };
    this.service.saveAddress(address).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/address']);
    
      this.ngOnInit();

    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  updateAddress() {
    this.isDataLoaded = true;
    let address = {
      'addressType': this.addressType,
      'address': this.address,
      'status': this.status,
      'sequence': this.addressSequnce,
      'version': this.version,
      'defaultAddressFlag': this.defaultAddressFlag,
      'geometry': [],
    }

    this.service.updateAddress(this.addressSequnce, address).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/address']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  getAddress(){
    this.isDataLoaded = true;
    this.service.getAddress(this.addressSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.addressSequnce = body.data.sequence;
      this.address = body.data.address;
      this.addressType = body.data.addressType;
      this.version = parseInt(body.data.version, 0);
      this.status = body.data.status;
      this.addressTypeDescription = body.data.addressTypeDescription;
      this.defaultAddressFlag = body.data.defaultAddressFlag;
      this.isDataLoaded = false;
    }, error => {
      this.isDataLoaded = false;
    });
  }

  deleteAddress(sequence) {
    this.isDataLoaded = true;
    // alert(sequence);
    this.service.deleteAddress(sequence).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/address']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  getAddressTypes() {
    this.service.getAddressTypes().subscribe(resp => {
      const body = resp.body as any;
      this.addressTypes = body.data;

    }, error => {
      console.log(error);
    });
  }

  /**
   * common functions 
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }
}
