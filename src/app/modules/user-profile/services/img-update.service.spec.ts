import { TestBed } from '@angular/core/testing';

import { ImgUpdateService } from './img-update.service';

describe('ImgUpdateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImgUpdateService = TestBed.get(ImgUpdateService);
    expect(service).toBeTruthy();
  });
});
