import {Injectable} from '@angular/core';
import {HTTPCallService} from 'src/app/services/httpcall.service';
import {Observable} from 'rxjs';
import {HttpResponse, HttpEvent} from '@angular/common/http';
import {fileServer} from '../../../config/enum';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  constructor(public http: HTTPCallService) {
  }

  getUserProfile(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles';
    return this.http.getResource(path);
  }

  getUserProfilePhoneNumbers(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles/contacts';
    return this.http.getResource(path);
  }

  getUserProfileAddress(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles/addresses';
    return this.http.getResource(path);
  }

  getUserProfileIdentifications(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles/identifications';
    return this.http.getResource(path);
  }

  getUserProfileContactPersons(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles/contactPerson';
    return this.http.getResource(path);
  }


  getNationalities(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=NATON';
    return this.http.getResource(path);
  }

  getUserProfileAddresses(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles/address';
    return this.http.getResource(path);
  }


  getLanguages(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=PTLNG';
    return this.http.getResource(path);
  }

  getGenderTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=GENDR';
    return this.http.getResource(path);
  }

  getIndentificationTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=RGTYP';
    return this.http.getResource(path);
  }

  getContactTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=CNTYP';
    return this.http.getResource(path);
  }

  getAddressTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=ADTYP';
    return this.http.getResource(path);
  }

  getMaritalStatus(): Observable<HttpResponse<Object>> {
    const path = 'base/maritalStatuses';
    return this.http.getResource(path);
  }


  savePhoneNumber(data): Observable<HttpResponse<object>> {
    const path = 'base/users/profiles/contacts';
    return this.http.postResource(path, data);
  }

  updatePhoneNumber(sequence, data): Observable<HttpResponse<object>> {
    const path = 'base/parties/contacts/' + sequence;
    return this.http.putResource(path, data);
  }

  deletePhoneNumber(sequence): Observable<HttpResponse<object>> {
    const path = 'base/parties/contacts/' + sequence;
    return this.http.deleteResource(path);
  }

  saveAddress(data): Observable<HttpResponse<object>> {
    const path = 'base/users/profiles/addresses';
    return this.http.postResource(path, data);
  }

  updateAddress(sequence, data): Observable<HttpResponse<object>> {
    const path = 'base/parties/addresses/' + sequence;
    return this.http.putResource(path, data);
  }

  deleteAddress(sequence): Observable<HttpResponse<object>> {
    const path = 'base/parties/addresses/' + sequence;
    return this.http.deleteResource(path);
  }


  saveIdentification(data): Observable<HttpResponse<object>> {
    const path = 'base/users/profiles/identifications';
    return this.http.postResource(path, data);
  }

  updateIdentification(sequence, data): Observable<HttpResponse<object>> {
    const path = 'base/parties/identifications/' + sequence;
    return this.http.putResource(path, data);
  }

  deleteIdentification(sequence): Observable<HttpResponse<object>> {
    const path = 'base/parties/identifications/' + sequence;
    return this.http.deleteResource(path);
  }


  saveContactPerson(data): Observable<HttpResponse<object>> {
    const path = 'base/users/profiles/contactPerson';
    return this.http.postResource(path, data);
  }

  updateConatctPerson(sequence, data): Observable<HttpResponse<object>> {
    const path = 'base/parties/contactPerson/' + sequence;
    return this.http.putResource(path, data);
  }

  deleteConatctPerson(sequence): Observable<HttpResponse<object>> {
    const path = 'base/parties/contactPerson/' + sequence;
    return this.http.deleteResource(path);
  }

  changePassword(data): Observable<HttpResponse<Object>> {
    const path = 'base/changePassword';
    return this.http.putResource(path, data);
  }

  /**
   * image upload
   */
  uploadImage(file: File): Observable<HttpEvent<Object>> {
    const fd = new FormData();
    fd.append('file', file);
    const path = fileServer + 'v1/files/upload?path=userProfile';
    return this.http.postImage(path, fd);
  }

  updateProfile(data): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles/summary';
    return this.http.putResource(path, data);
  }

  getAddress(addressSequnce): Observable<HttpResponse<Object>> {
    const path = 'base/parties/addresses/' + addressSequnce;
    return this.http.getResource(path);
  }

  getPhoneNumber(phoneNumberSequnce): Observable<HttpResponse<Object>> {
    const path = 'base/parties/contacts/' + phoneNumberSequnce;
    return this.http.getResource(path);
  }

  getContactPerson(contactPersonSequnce): Observable<HttpResponse<Object>> {
    const path = 'base/parties/contactPerson/' + contactPersonSequnce;
    return this.http.getResource(path);
  }

  getIdentication(identificationSequnce): Observable<HttpResponse<Object>> {
    const path = 'base/parties/identifications/' + identificationSequnce;
    return this.http.getResource(path);
  }


}
