import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImgUpdateService {
   imgUrl: string = '';
  constructor() { }

  setImg(url: string) {
    this.imgUrl = url;
  }

  getImg(): string {
    return this.imgUrl;
  }
}
