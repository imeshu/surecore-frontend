import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import { IdentificationComponent } from './identification/identification.component';
import { ContactPersonComponent } from './contact-person/contact-person.component';
import { AddressComponent } from './address/address.component';
import { PhoneNumberComponent } from './phone-number/phone-number.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ProfileFormComponent } from './profile-form/profile-form.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { urlDecoder } from 'src/app/pipes/urlDecoder';
import { CustomPipeModule } from 'src/app/pipes/CustomPipesModule';
import { IdentificationFormComponent } from './identification-form/identification-form.component';
import { AddressFormComponent } from './address-form/address-form.component';
import { PhoneNumberFormComponent } from './phone-number-form/phone-number-form.component';
import { ContactPersonFormComponent } from './contact-person-form/contact-person-form.component';
import { MatDatepickerModule, MatCalendarHeader, MatDatepicker } from '@angular/material/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatFormField, MatNativeDateModule, MatInputModule, MatOption,
  MatOptionModule, MatSelect, MatIcon, MatIconModule, MatInput, MatError } from '@angular/material';
import { MatSelectModule } from '@angular/material';
import { CommonComponentsModule } from '../common-components/common-components.module';



@NgModule({
  declarations: [
    IdentificationComponent,
    ContactPersonComponent,
    AddressComponent,
    PhoneNumberComponent,
    UserProfileComponent,
    ProfileFormComponent,
    ProfileViewComponent,
    IdentificationFormComponent,
    AddressFormComponent,
    PhoneNumberFormComponent,
    ContactPersonFormComponent],
  imports: [
    CommonModule,
    UserProfileRoutingModule,
    DashboardModule,
    CustomPipeModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonComponentsModule
  ],
  exports: [

    IdentificationComponent,
    ContactPersonComponent,
    AddressComponent,
    PhoneNumberComponent,
    UserProfileComponent,
    ProfileFormComponent,
    ProfileViewComponent,
    IdentificationFormComponent,
    AddressFormComponent,
    PhoneNumberFormComponent,
    ContactPersonFormComponent,
    MatCalendarHeader,
    MatDatepicker,
    MatFormField,
    MatDatepickerModule, MatInputModule, MatNativeDateModule,
    MatOption,
    MatSelect,
    MatIcon,
    MatInput,
    MatError
  ]
})
export class UserProfileModule { }
