import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { PhoneNumberComponent } from './phone-number/phone-number.component';
import { AddressComponent } from './address/address.component';
import { IdentificationComponent } from './identification/identification.component';
import { ContactPersonComponent } from './contact-person/contact-person.component';
import { ProfileFormComponent } from './profile-form/profile-form.component';
import { PhoneNumberFormComponent } from './phone-number-form/phone-number-form.component';
import { AddressFormComponent } from './address-form/address-form.component';
import { IdentificationFormComponent } from './identification-form/identification-form.component';
import { ContactPersonFormComponent } from './contact-person-form/contact-person-form.component';

const routes: Routes = [
  {
    path: 'user-profile',
    component: UserProfileComponent,
    children: [
      {
        path: '',
        component: ProfileViewComponent
      },
      {
        path: 'manage-profile',
        component: ProfileFormComponent
      },
      {
        path: 'contact',
        component: PhoneNumberComponent
      },
      {
        path: 'manage-contact/:sequnce',
        component: PhoneNumberFormComponent,
      },
      {
        path: 'add-contact',
        component: PhoneNumberFormComponent
      },
      {
        path: 'address',
        component: AddressComponent
      },
      {
        path: 'manage-address/:sequnce',
        component: AddressFormComponent
      },
      {
        path: 'add-address',
        component: AddressFormComponent
      },
      {
        path: 'identification',
        component: IdentificationComponent
      },
      {
        path: 'manage-identification/:sequnce',
        component: IdentificationFormComponent
      },
      {
        path: 'add-identification',
        component: IdentificationFormComponent
      },
      {
        path: 'contact-person',
        component: ContactPersonComponent
      },
      {
        path: 'manage-contact-person/:sequnce',
        component: ContactPersonFormComponent
      },
      {
        path: 'add-contact-person',
        component: ContactPersonFormComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserProfileRoutingModule { }
