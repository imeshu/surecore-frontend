import { Component, OnInit, Input } from '@angular/core';
import { UserProfileService } from '../services/user-profile.service';
import { Validators, FormControl } from '@angular/forms';
import { Profile } from '../models/profile';
import * as moment from 'moment';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpEventType } from '@angular/common/http';
import { pipe } from 'rxjs';
import { ImgUpdateService } from '../services/img-update.service';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {



  constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router, private route: ActivatedRoute, public imgService: ImgUpdateService) {
    if (this.router.url.includes('user-profile/manage-profile')) {

      this.getUserProfile();
    }
  }


  public profile: Profile = null;


  nationalities = [];
  nationality = '';

  languages = [];
  language = '';

  genderTypes = [];
  genderType = '';

  maritalStatuses = [];
  maritalStatus = '';

  // profile

  // name: string = '';
  email = '';
  address = '';
  mobileNumber = '';
  dateOfBirth = null;
  nic = '';

  lastUpdatedDate = '';

  imageKey = '';


  emailFormControl = new FormControl('', [
    Validators.email,
  ]);
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  nicFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^([0-9]{9}[x|X|v|V]|[0-9]{12})$')
  ]);
  mobileNumberFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
  ]);
  addressFormControl = new FormControl('', [
    Validators.required
  ]);



  // passwords
  oldPassword = '';
  // newPassword: string = '';

  isDataLoaded = true;

  hideOld = true;
  hideNew = true;

  imageUrl = '';


  newPassword = '';


  id = '';
  token = '';
  type = '';

  newPasswordVisible = false;
  oldPasswordVisible = false;

  isValidatedPassword = false;
  isMatchedPassword = false;

  showWeekPasswordlbl = false;
  showStrongPasswordLbl = false;
  showStrongMatchingPasswordlbl = false;
  showDoNotMatchLbl = false;

  isfocusNewPasswordfeild = false;

  // password validate checkboxes
  checkedMinimum8Characters = false;
  checkedUppsercaseLetter = false;
  checkedLowerCaseLetter = false;
  checkedNumber = false;
  checkedSymbol = false;
  ngOnInit() {
    this.getGenderTypes();
    this.getNationalities();
    this.getLanguages();
    this.getMaritalStatuses();
    this.getUserProfile();
  }

  getGenderTypes() {
    this.service.getGenderTypes().subscribe(resp => {
      const body = resp.body as any;
      this.genderTypes = body.data;
    }, error => {
      console.log(error);
    });
  }

  getNationalities() {
    this.service.getNationalities().subscribe(resp => {
      const body = resp.body as any;
      this.nationalities = body.data;
    }, error => {
      console.log(error);
    });
  }

  getLanguages() {
    this.service.getLanguages().subscribe(resp => {
      const body = resp.body as any;
      this.languages = body.data;
    }, error => {
      console.log(error);
    });
  }

  getMaritalStatuses() {
    this.service.getMaritalStatus().subscribe(resp => {
      const body = resp.body as any;
      this.maritalStatuses = body.data;
    }, error => {
      console.log(error);
    });
  }

  /**
   * validate nic and mobile number
   */
  validateMobileNumber(mobileNumber) {
    const PhoneNumberRegex = new RegExp('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$');
    const test = PhoneNumberRegex.test(mobileNumber);
    return test;
  }
  validateNIC(nic) {
    const PhoneNumberRegex = new RegExp('^([0-9]{9}[x|X|v|V]|[0-9]{12})$');
    const test = PhoneNumberRegex.test(nic);
    return test;
  }

  getUserProfile() {

    this.isDataLoaded = false;
    this.service.getUserProfile().subscribe(resp => {
      const body = resp.body as any;
      console.log(body);
      this.profile = new Profile();
      this.profile.userProfile = body.data.profile;

      this.profile.contact = body.data.contact;
      this.profile.contactPerson = body.data.contactPerson;
      this.profile.identification = body.data.identification;
      this.profile.image = body.data.image;
      this.profile.address = body.data.address;
      // set drop down data
      this.nationality = body.data.profile.nationality;
      this.language = body.data.profile.languageCode;
      this.genderType = body.data.profile.gender;
      this.maritalStatus = body.data.profile.maritalStatus;

      if (this.profile.address !== null) {
        this.address = body.data.address.address;
      }

      if (this.profile.image !== null) {
        this.imageUrl = this.profile.image.image;
      } else {
        this.imageUrl = '';
      }

      this.lastUpdatedDate = moment(body.data.profile.lastUpdatedDate, 'YYYY-MM-DD HH:mm:ss').format('DD MMMM YYYY');

      if (this.profile.userProfile.dateOfBirth !== null) {
        this.dateOfBirth = new Date(this.profile.userProfile.dateOfBirth);
      } else {
        this.dateOfBirth = '';
      }
       // moment( this.profile.userProfile.dateOfBirth).format('utc');
      if (this.profile.identification !== null) {
        for (let i = 0; i < this.profile.identification.length; i++) {
          if (this.profile.identification[i]['registrationType'] === 'NIDN') {
            this.profile.userProfile.nicNumberDeafult = this.profile.identification[i]['registrationNumber'];
          }
        }
      }
      if (this.profile.contact !== null) {
        for (let i = 0; i < this.profile.contact.length; i++) {
          if (this.profile.contact[i]['contactType'] === 'CEMIL') {
            this.profile.userProfile.email = this.profile.contact[i]['contactNumber'];
          }
          if (this.profile.contact[i]['contactType'] === 'CMOBL') {
            this.profile.userProfile.mobleNumber = this.profile.contact[i]['contactNumber'];
          }
        }
      }
      this.profile.userProfile.dateOfBirth = moment(this.profile.userProfile.dateOfBirth).format('DD-MM-YYYY');
      this.isDataLoaded = false;
    }, error => {
      this.isDataLoaded = false;
      console.log(error);
    });

  }


  updateUserProfile() {
    this.isDataLoaded = true;
    this.profile.userProfile.dateOfBirth = this.dateOfBirth;

    this.profile.userProfile.nationality = this.nationality;
    this.profile.userProfile.languageCode = this.language;
    this.profile.userProfile.dateOfBirth = this.dateOfBirth;
    this.profile.userProfile.gender = this.genderType;
    this.profile.userProfile.maritalStatus = this.maritalStatus;
    if (this.profile.contact !== null) {
      for (let i = 0; i < this.profile.contact.length; i++) {
        if (this.profile.contact[i]['contactType'] === 'CEMIL') {
          this.profile.contact[i]['contactNumber'] = this.profile.userProfile.email;
        }
        if (this.profile.contact[i]['contactType'] === 'CMOBL') {
          this.profile.contact[i]['contactNumber'] = this.profile.userProfile.mobleNumber;
        }
      }

      if ((this.profile.contact.length === 1) && (this.profile.contact[0]['contactType'] === 'CEMIL')) {
        const contact = {
          'contactType': 'CMOBL',
          'contactNumber': this.profile.userProfile.mobleNumber,
          'default': true
        };
        this.service.savePhoneNumber(contact).subscribe(resp => {
          const body = resp.body as any;
          console.log(body.data);
        }, error => {
          console.log(error);
        });
      }

      if ((this.profile.contact.length === 1) && (this.profile.contact[0]['contactType'] === 'CMOBL')) {
        const contact = {
          'contactType': 'CEMIL',
          'contactNumber': this.profile.userProfile.email,
          'default': true
        };
        this.service.savePhoneNumber(contact).subscribe(resp => {
          const body = resp.body as any;
          console.log(body.data);
        }, error => {
          console.log(error);
        });
      }
    }

    // update identification

    if (this.profile.identification !== null) {
      for (let i = 0; i < this.profile.identification.length; i++) {
        if (this.profile.identification[i]['registrationType'] === 'NIDN') {
          this.profile.identification[i]['registrationNumber'] = this.profile.userProfile.nicNumberDeafult;
        }
      }
    }




    if (this.profile.address === null) {
      const address = {
        'address': this.address,
        'addressType': 'ADHOM',
        'defaultAddressFlag': 'Y'
      };

      this.service.saveAddress(address).subscribe(resp => {
        const body = resp.body as any;
        this.service.getAddress(body.data).subscribe(resp => {
          const body1 = resp.body as any;
          this.profile.address = body1.data;
          let dOB = this.profile.userProfile.dateOfBirth;
          if (dOB === '') {
            dOB = null;
          } else {
            dOB = moment(this.profile.userProfile.dateOfBirth).format('YYYY-MM-DD HH:mm:ss');
          }

          if (this.imageKey === '') {
            const obj = {
              'name': this.profile.userProfile.name,
              'languageCode': this.profile.userProfile.languageCode,
              'dateOfBirth': dOB,
              'gender': this.profile.userProfile.gender,
              'maritalStatus': this.profile.userProfile.maritalStatus,
              'address': this.profile.address,
              'contact': this.profile.contact,
              'identification': this.profile.identification,
              'contactPerson': this.profile.contactPerson
            };

            console.log(obj);

            // tslint:disable-next-line:no-shadowed-variable
            this.service.updateProfile(obj).subscribe(resp => {
              // tslint:disable-next-line:no-shadowed-variable
              const body = resp.body as any;
              this.displaySuccessMessage(body);
              this.router.navigate(['user-profile']);
              this.ngOnInit();
              this.isDataLoaded = false;
            }, error => {
              this.displayErrorMessage(error);
              console.log(error);
              this.isDataLoaded = false;
            });

          } else {
            // tslint:disable-next-line:no-shadowed-variable
            let dOB = this.profile.userProfile.dateOfBirth;
            if (dOB === '') {
              dOB = null;
            } else {
              dOB = moment(this.profile.userProfile.dateOfBirth).format('YYYY-MM-DD HH:mm:ss');
            }
            const obj = {
              'name': this.profile.userProfile.name,
              'languageCode': this.profile.userProfile.languageCode,
              'dateOfBirth': dOB,
              'gender': this.profile.userProfile.gender,
              'imageKey': this.imageKey,
              'maritalStatus': this.profile.userProfile.maritalStatus,
              'address': this.profile.address,
              'contact': this.profile.contact,
              'identification': this.profile.identification,
              'contactPerson': this.profile.contactPerson
            };

            console.log(obj);

            this.service.updateProfile(obj).subscribe(resp => {
              // tslint:disable-next-line:no-shadowed-variable
              const body = resp.body as any;
              this.displaySuccessMessage(body);
              this.router.navigate(['user-profile']);
              this.ngOnInit();
              window.location.reload();
              this.isDataLoaded = false;
              this.imgService.setImg(this.imageUrl);

            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
              console.log(error);
            });

          }
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
          console.log(error);
        });


      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
        console.log(error);
      });
    } else {

      if (this.imageKey === '') {
        let dOB = this.profile.userProfile.dateOfBirth;
        if (dOB === '') {
          dOB = null;
        } else {
          dOB = moment(this.profile.userProfile.dateOfBirth).format('YYYY-MM-DD HH:mm:ss');
        }
        const obj = {
          'name': this.profile.userProfile.name,
          'languageCode': this.profile.userProfile.languageCode,
          'dateOfBirth': dOB,
          'gender': this.profile.userProfile.gender,
          'maritalStatus': this.profile.userProfile.maritalStatus,
          'address': this.profile.address,
          'contact': this.profile.contact,
          'identification': this.profile.identification,
          'contactPerson': this.profile.contactPerson
        };

        console.log(obj);

        this.service.updateProfile(obj).subscribe(resp => {
          const body = resp.body as any;
          this.displaySuccessMessage(body);
          this.router.navigate(['user-profile']);
          this.ngOnInit();
          this.isDataLoaded = false;
        }, error => {
          console.log(error);
          this.displayErrorMessage(error);
          this.isDataLoaded = false;
        });
      } else {
        let dOB = this.profile.userProfile.dateOfBirth;
        if (dOB === '') {
          dOB = null;
        } else {
          dOB = moment(this.profile.userProfile.dateOfBirth).format('YYYY-MM-DD HH:mm:ss');
        }
        const obj = {
          'name': this.profile.userProfile.name,
          'languageCode': this.profile.userProfile.languageCode,
          'dateOfBirth': dOB,
          'gender': this.profile.userProfile.gender,
          'maritalStatus': this.profile.userProfile.maritalStatus,
          'imageKey': this.imageKey,
          'address': this.profile.address,
          'contact': this.profile.contact,
          'identification': this.profile.identification,
          'contactPerson': this.profile.contactPerson
        };


        this.service.updateProfile(obj).subscribe(resp => {
          const body = resp.body as any;
          this.displaySuccessMessage(body);
          this.router.navigate(['user-profile']);
          this.ngOnInit();
          this.isDataLoaded = false;
          this.imgService.setImg(this.imageUrl);
        }, error => {
          this.displayErrorMessage(error);
          console.log(error);
          this.isDataLoaded = false;
        });
      }
    }
  }

  addDataItem(event) {
    this.isDataLoaded = true;
    this.uploadImage(event.target.files[0]);
  }

  uploadImage(file: File) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        // this.dataList[index].sequence = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imageKey = body.data.object.key;
        this.imageUrl = body.data.object.url;
        this.isDataLoaded = false;
      }
    });
  }





  checkPassowrd() {

    if (this.newPassword !== '') {

      // contains minimum 8 characters
      if (new RegExp('^(?=.{8,})').test(this.newPassword)) {
        this.checkedMinimum8Characters = true;
      } else {
        this.checkedMinimum8Characters = false;
      }

      // contains a Uppercase letter
      if (new RegExp('^(?=.*[A-Z])').test(this.newPassword)) {
        this.checkedUppsercaseLetter = true;
      } else {
        this.checkedUppsercaseLetter = false;
      }

      // contains a lowercase letter
      if (new RegExp('^(?=.*[a-z])').test(this.newPassword)) {
        this.checkedLowerCaseLetter = true;
      } else {
        this.checkedLowerCaseLetter = false;
      }

      // contains a number
      if (new RegExp('^(?=.*[0-9])').test(this.newPassword)) {
        this.checkedNumber = true;
      } else {
        this.checkedNumber = false;
      }

      // contains a symbol
      if (new RegExp('^(?=.*[!@#\$%\^&\*\-])').test(this.newPassword)) {
        this.checkedSymbol = true;
      } else {
        this.checkedSymbol = false;
      }


    }

  }



  validatePassword() {

    if (this.newPassword !== '' && this.newPassword.length >= 8) {
      const regex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\-])(?=.{8,})');

      this.isValidatedPassword = regex.test(this.newPassword);

      //  }
      // password validation checkbox

    } else {
      this.showWeekPasswordlbl = false;
      this.isValidatedPassword = false;
      this.showStrongPasswordLbl = false;
      this.showDoNotMatchLbl = false;
      this.showStrongMatchingPasswordlbl = false;
      this.isfocusNewPasswordfeild = true;
    }

    // password validation checkbox
    this.checkPassowrd();
  }

  oldPasswordToggleEye() {
    if (this.oldPasswordVisible === false) {
      this.oldPasswordVisible = true;
    } else {
      this.oldPasswordVisible = false;
    }
  }
  newPasswordToggleEye() {
    if (this.newPasswordVisible === false) {
      this.newPasswordVisible = true;
    } else {
      this.newPasswordVisible = false;
    }

  }

  onfocusNewPasswordfeild() {
    this.isfocusNewPasswordfeild = true;
  }

  onBlurNewPasswordfeild() {
    this.isfocusNewPasswordfeild = false;
  }


  updatePassword() {
    this.isDataLoaded = true;
    const passwords = {
      'oldPassword': this.oldPassword,
      'newPassword': this.newPassword
    };

    this.service.changePassword(passwords).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }

  /**
   * common functions
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }


}
