export class ContactPerson {

    sequence: string = '';
    address: string = '';
    emailAddress: string = '';
    mobileNumber: string = '';
    name: string = '';
    phoneNumber: string = '';
    relationship: string = '';
    skypeId: string = '';
    status: string = '';
    version: number = 0;

}