import { Identification } from './identification';
import { Contact } from './contact';
import { Address } from './address';
import { ProfileImage } from './profileImage';
import { UserProfile } from './userProfile';
import { ContactPerson } from './contactPerson';


export class Profile {

    userProfile: UserProfile = new UserProfile();
    identification: Identification[] = [];
    contact: Contact[] = [];
    address: Address = null;
    image: ProfileImage = null;
    contactPerson: ContactPerson[] = [];
}