export class ProfileImage {
    sequence: string = '';
    image: string = '';
    status: string = '';
    version: string = '';
    constructor(sequence: string,
        image: string,
        status: string,
        version: string) {
        this.sequence = sequence;
        this.image = image;
        this.status = status;
        this.version = version;

    }
}
