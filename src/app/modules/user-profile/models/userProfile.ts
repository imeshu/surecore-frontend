export class UserProfile {

    code: string = '';
    partyAsClaimAssessor: string = '';
    partyAsCustomer: string = '';
    partyAsInternalStaff: string = '';
    partyAsIntermediary: string = '';
    partyAsReinsurer: string = '';
    partyAsServiceMedical: string = '';
    partyAsServiceMotor: string = '';
    partyAsServiceOther: string = '';
    partyAsUwAssessor: string = '';
    dateOfBirth: string = '';
    gender: string = '';
    joinedDate: string = '';
    maritalStatus: string = '';
    name: string = '';
    nationality: string = '';
    referenceNumber: string = '';
    status: string = '';
    title: string = '';
    customerType: string = '';
    internalStaffType: string = '';
    intermediaryType: string = '';
    designation: string = '';
    designationEffectDate: string = '';
    loginName: string = '';
    level: string = '';
    version: string = '';
    genderDescription: string = '';
    maritalStatusDescription: string = '';
    nationalityDescription: string = '';
    titleDescription: string = '';
    customerTypeDescription: string = '';
    designationDescription: string = '';
    attachedInstituteDescription: string = '';
    attachedLocationDescription: string = '';
    reportingDescription: string = '';
    partyPoint: string = '';
    locationCoordiates: string = '';
    languageCode: string = '';



    //additional properties
    nicNumberDeafult: string = '';
    email: string = '';
    mobleNumber: string = '';
}
