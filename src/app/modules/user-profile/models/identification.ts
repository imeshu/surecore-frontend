export class Identification {
    issueingAuthority: string = '';
    registrationNumber: string = '';
    registrationType: string = '';
    status: string = '';
    validFromDate: string= '';
    validToDate: string = '';
    sequence: string= '';
    version: string = '';
    registrationTypeDescription: string = '';
}