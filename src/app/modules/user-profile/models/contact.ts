export class Contact{
    contactType: string = '';
    contactNumber: string = '';
    status: string = '';
    sequence: string = '';
    version: string = '';
    contactTypeDescription: string = ''; 
    default: boolean = false;
}