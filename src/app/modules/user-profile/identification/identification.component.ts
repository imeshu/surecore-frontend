import { Component, OnInit, Input } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';
import { UserProfileService } from '../services/user-profile.service';
import { Profile } from '../models/profile';
import { Identification } from '../models/identification';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.scss']
})
export class IdentificationComponent implements OnInit {

   public identifications: Identification[];
   isDataLoaded = true;
   constructor(public service: UserProfileService, public notificationService: NotificationDataTransferService, public router: Router) { }

  ngOnInit() {
    this.getUserProfileIdentifications();
  }

  getUserProfileIdentifications(){
    this.isDataLoaded = true;
    this.service.getUserProfileIdentifications().subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.identifications = body.data;
    }, error => {
      this.isDataLoaded = false;
      console.log(error);
    });

  }

  deleteIdentification(sequence){
    this.isDataLoaded = true;
    // alert(sequence);
    this.service.deleteIdentification(sequence).subscribe(resp => {
      const body = resp.body as any;
      this.isDataLoaded = false;
      this.displaySuccessMessage(body);
      this.router.navigate(['user-profile/identification']);
      this.ngOnInit();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
      console.log(error);
    });
  }
  /**
   * common functions 
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }
  
}
