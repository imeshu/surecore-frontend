import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OutstandingDebtListComponent} from './outstanding-debt-list/outstanding-debt-list.component';

const routes: Routes = [
  {
    path: 'outstanding',
    component: OutstandingDebtListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OutstandingDebtRoutingModule { }
