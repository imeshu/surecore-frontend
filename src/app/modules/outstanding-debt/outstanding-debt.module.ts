import { NgModule } from '@angular/core';
import {CommonModule, CurrencyPipe} from '@angular/common';
import { OutstandingDebtListComponent } from './outstanding-debt-list/outstanding-debt-list.component';
import {OutstandingDebtRoutingModule} from './outstanding-debt-routing.module';
import {CommonComponentsModule} from '../common-components/common-components.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {
  MatDatepickerModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatOptionModule, MatSelectModule
} from '@angular/material';


@NgModule({
  declarations: [OutstandingDebtListComponent],
  imports: [
    CommonModule,
    OutstandingDebtRoutingModule,
    CommonComponentsModule,
    FormsModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule

  ],
  providers: [CurrencyPipe]
})
export class OutstandingDebtModule { }
