import { TestBed } from '@angular/core/testing';

import { OutstandingService } from './outstanding.service';

describe('OutstandingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutstandingService = TestBed.get(OutstandingService);
    expect(service).toBeTruthy();
  });
});
