import {Injectable} from '@angular/core';
import {HTTPCallService} from '../../../../services/httpcall.service';
import {Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {Settlement} from '../models/Settlement';
import {CommonType} from '../../../../model/CommonType';
import {map} from 'rxjs/operators';
import {Outstanding} from '../../../policy/models/Outstanding';

@Injectable({
  providedIn: 'root'
})
export class OutstandingService {

  constructor(private http: HTTPCallService) {
  }


  OutstandingService(page, pageSize, searchValue, searchField, agentCode, customerCode): Observable<HttpResponse<Object>> {
    if (searchField === 'CN') {
      const p = 'rc/outstandings/paginated_search?policyNumber=&customerCode=' + customerCode
        + '&intermediaryCode=' + agentCode + '&debitNoteNumber=Dn&page=' + page + '&size=' + pageSize + '&customerName=' + searchValue
        + '&customerNic=';
      return this.http.getResource(p);
    } else if (searchField === 'NIC') {
      const path = 'rc/outstandings/paginated_search?policyNumber=&customerCode=' + customerCode
        + '&intermediaryCode=' + agentCode + '&debitNoteNumber=Dn&page=' + page + '&size=' + pageSize
        + '&customerName=&customerNic=' + searchValue;
      return this.http.getResource(path);
    } else if (searchField === 'PN') {
      const path = 'rc/outstandings/paginated_search?policyNumber=' + searchValue + '&customerCode=' + customerCode
        + '&intermediaryCode=' + agentCode + '&debitNoteNumber=Dn&page=' + page + '&size=' + pageSize
        + '&customerName=&customerNic=';
      return this.http.getResource(path);
    }

  }


  saveSettlement(settlement: Settlement): Observable<HttpResponse<Object>> {
    const path = 'rc/settlements';
    return this.http.postResource(path, settlement);
  }

  getBanks(): Observable<CommonType[]> {
    const path = 'rc/banks';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getMobileOperators(): Observable<CommonType[]> {
    const path = 'base/mobileOperators';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getBranches(code: string): Observable<CommonType[]> {
    const path = 'base/references?type=BKBRN&parentCode=' + code;
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /************************************** Receipting *************************************/
  getPaymentModes(): Observable<CommonType[]> {
    const path = 'base/paymentModes';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getOutstandingDetails(debitNoteNumber: string): Observable<Outstanding> {
    const path = 'rc/outstandings/search?debitNoteNumber=' + debitNoteNumber;
    return this.http.getResource(path).pipe(
      map(response => {
        let outstandingData: Outstanding = new Outstanding();
        // @ts-ignore
        outstandingData = response.body.data;
        return outstandingData;
      })
    );
  }

  downloadDebt(sequence) {
    const path = 'rc/debitReceipt/' + sequence + '/print';
    return this.http.downloadFile(path);
  }
}
