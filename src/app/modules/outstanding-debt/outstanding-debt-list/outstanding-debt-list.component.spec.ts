import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutstandingDebtListComponent } from './outstanding-debt-list.component';

describe('OutstandingDebtListComponent', () => {
  let component: OutstandingDebtListComponent;
  let fixture: ComponentFixture<OutstandingDebtListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutstandingDebtListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutstandingDebtListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
