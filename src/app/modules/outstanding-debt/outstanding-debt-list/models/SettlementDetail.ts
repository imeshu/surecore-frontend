export class SettlementDetail {
  debitNoteNumber: string;
  paidAmount: number;
  loyaltyPointsUtilization: number;
}
