export class Outstanding {
  sequence: number;
  debitNoteNumber: string;
  currencyCode: string;
  currencyRate: number;
  debitAmount: number;
  settledAmount: number;
  transactionDate: string;
  productDescription: string;
  customerCode: string;
  customerName: string;
  loyaltyPointsBalance: number;
  settlementMethodCode: string;
}
