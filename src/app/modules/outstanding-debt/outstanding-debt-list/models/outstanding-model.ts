export class OutstandingModel {

  sequence: number;
  debitNoteNumber: string;
  currencyCode: string;
  currencyRate: number;
  debitAmount: string;
  settledAmount: string;
  transactionDate: Date;
  productDescription: string;
  customerCode: string;
  customerName: string;
  loyaltyPointsBalance: string;
  settlementMethodCode: string;
  active: boolean;
  policyNumber: string;
  riskName: string;
}
