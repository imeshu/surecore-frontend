import {SettlementCollection} from './SettlementCollection';

export class CashCollection extends SettlementCollection {

  constructor() {
    super('PAYCS');
  }
}
