import {SettlementCollection} from './SettlementCollection';

export class BankDepositCollection extends SettlementCollection {
  depositReferenceNumber: string;
  depositDate: string;
  bankCode: string;
  accountNumber: string;

  constructor() {
    super('PAYBD');
  }
}
