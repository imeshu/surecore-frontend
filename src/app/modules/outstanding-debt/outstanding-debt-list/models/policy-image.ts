export class PolicyImageModel {
    sequence: string;
    type: string;
    value: any;
    order: string;
    status: any;
    version: string;
    imageKey: string;
}
