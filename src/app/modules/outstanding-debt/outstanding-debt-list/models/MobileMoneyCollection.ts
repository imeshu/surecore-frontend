import {SettlementCollection} from './SettlementCollection';

export class MobileMoneyCollection extends SettlementCollection {
   operatorCode: string;
   mobileNumber: string;

  constructor() {
    super('PAYMM');
  }
}
