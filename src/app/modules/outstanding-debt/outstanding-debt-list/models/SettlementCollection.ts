export class SettlementCollection {
  paymentModeCode: string;
  currencyCode: string;

  constructor(paymentModeCode: string) {
    this.paymentModeCode = paymentModeCode;
  }
}
