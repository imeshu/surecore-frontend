import {Component, Input, OnInit} from '@angular/core';
import {OutstandingService} from './services/outstanding.service';
import {OutstandingModel} from './models/outstanding-model';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {CashCollection} from './models/CashCollection';
import {Outstanding} from './models/Outstanding';
import {CardCollection} from './models/CardCollection';
import {ChequeCollection} from './models/ChequeCollection';
import {MobileMoneyCollection} from './models/MobileMoneyCollection';
import {BankDepositCollection} from './models/BankDepositCollection';
import {Settlement} from './models/Settlement';
import {SettlementDetail} from './models/SettlementDetail';
import {CommonType} from '../../../model/CommonType';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {CurrencyPipe} from '@angular/common';
import {isNumber} from 'util';

@Component({
  selector: 'app-outstanding-debt-list',
  templateUrl: './outstanding-debt-list.component.html',
  styleUrls: ['./outstanding-debt-list.component.scss']
})
export class OutstandingDebtListComponent implements OnInit {

  private outData: OutstandingModel[] = new Array();
  private paymentModes: CommonType[];

  private banks: CommonType[];
  private branches: CommonType[];
  private mobile_operators: CommonType[];
  private paymentDetailForm: FormGroup;
  private activeOutstandingIndex: number;
  private settlementSequence;
  // search
  public searchValue = '';
  selectedValue = '';
  public search;
  // infinity scroll
  private throttle = 300;
  private scrollDistance = 1;
  private scrollUpDistance = 2;
  private fromRoot = true;
  // sessionStorage check
  private debitAmount: number;
  private settledAmount: number;
  private outstandingAmount: number;

  private page = 1;
  private pageSize = 80;
  private numberOfPages: number;
// button boolean
  private button = false;

  @Input()
  debitNoteNumber;
  @Input()
  policyNumber;
  @Input()
  productCode;

  private outstanding: Outstanding;
  private loyaltyPointsSpending = 0;
  private amountTobePaid = 0.0;
  private balance: number;
  private paymentMode: string;
  private amountLabelText: string;
  private selectedOutstanding: OutstandingModel;

  private cashCollection: CashCollection = new CashCollection();
  private cardCollection: CardCollection = new CardCollection();
  private chequeCollection: ChequeCollection = new ChequeCollection();
  private mobileMoneyCollection: MobileMoneyCollection = new MobileMoneyCollection();
  private bankCollection: BankDepositCollection = new BankDepositCollection();


  constructor(private service: OutstandingService, public notificationService: NotificationDataTransferService, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.selectedValue = 'CustomerName';
    this.fetchPaymentMethods();
    this.activeOutstandingIndex = 0;
    this.getOutstandingList('CN');

    this.paymentDetailForm = this.fb.group({
      'loyalty_points': new FormControl(this.loyaltyPointsSpending),
      'payment_mode': new FormControl(this.paymentMode),
      'cheque_number': new FormControl(this.chequeCollection.chequeNumber),
      'cheque_bank': new FormControl(this.chequeCollection.bankCode),
      'cheque_branch': new FormControl(this.chequeCollection.branchCode),

      'credit_card_number': new FormControl(this.cardCollection.cardNumber),
      'credit_card_ccv': new FormControl(this.cardCollection.cardCcv),
      'credit_card_date': new FormControl(this.cardCollection.cardExpiry),

      'mobile_operator': new FormControl(this.mobileMoneyCollection.operatorCode),
      'mobile_number': new FormControl(this.mobileMoneyCollection.mobileNumber),

      'deposit_ref': new FormControl(this.bankCollection.depositReferenceNumber),
      'deposit_date': new FormControl(this.bankCollection.depositDate),
      'deposit_bank': new FormControl(this.bankCollection.bankCode),
      'deposit_account': new FormControl(this.bankCollection.accountNumber),

      'amount_to_be_paid': new FormControl(this.amountTobePaid, [Validators.required])
    });

  }

  /**
   * Load outstanding data
   */
  fetchOutstandingDetails() {
    this.service.getOutstandingDetails(this.debitNoteNumber).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.outstanding = typeList[0];
      this.balance = this.outstanding.debitAmount - this.outstanding.settledAmount;
    });
  }

  /**
   * Load payment modes
   */
  fetchPaymentMethods() {
    this.service.getPaymentModes().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      if (sessionStorage.getItem('asCustomer') === 'Y') {
        const customerTypeArr = ['PAYCC', 'PAYMM', 'PAYBD'];
        this.paymentModes = typeList.filter(mode => customerTypeArr.includes(mode.code));
      }
      if (sessionStorage.getItem('asAgent') === 'Y') {
        const agentTypeArr = ['PAYCQ', 'PAYCS'];
        this.paymentModes = typeList.filter(mode => agentTypeArr.includes(mode.code));
      }
    });
  }

  getOutstandingList(searchFeild) {
    if (sessionStorage.getItem('asAgent') === 'Y') {
      this.service.OutstandingService(this.page, this.pageSize, this.searchValue, searchFeild, sessionStorage.getItem('partyCode'), '')
        .subscribe(resp => {
          const model = resp.body as any;
          this.outData = model.data.debitNoteList;
        });
    } else if (sessionStorage.getItem('asCustomer') === 'Y') {
      this.service.OutstandingService(this.page, this.pageSize, this.searchValue, searchFeild, '', sessionStorage.getItem('partyCode'))
        .subscribe(resp => {
          const model = resp.body as any;
          this.outData = model.data.debitNoteList;
        });
    } else {
      this.service.OutstandingService(this.page, this.pageSize, this.searchValue, searchFeild, sessionStorage.getItem('partyCode'), '')
        .subscribe(resp => {
          const model = resp.body as any;
          this.outData = model.data.debitNoteList;
        });
    }
  }

  // infinity
  onScrollDown(event, searchFeild) {
    if (this.numberOfPages >= this.page) {
      this.page++;
      this.service.OutstandingService(this.page, this.pageSize, this.searchValue, searchFeild, '', sessionStorage.getItem('partyCode'))
        .subscribe(resp => {
          const body = resp.body as any;
          const list: OutstandingModel[] = body.data.debitNoteList;
          list.forEach(item => {
            this.outData.push(item);
          });
        });
    }
  }

  // Search Field
  searchByField() {
    let searchField = '';
    // const selectedValue = this.selectedValue;
    if (this.selectedValue === 'CustomerName') {
      searchField = 'CN';
    } else if (this.selectedValue === 'CustomerNIC') {
      searchField = 'NIC';
    } else if (this.selectedValue === 'PolicyNumber') {
      searchField = 'PN';
    }
    // tslint:disable-next-line:triple-equals
    if (this.searchValue !== '') {
      this.getOutstandingList(searchField);
    } else {
      this.getOutstandingList(searchField);
    }
  }

  /**
   * Load Banks
   */
  fetchBanks() {
    this.service.getBanks().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.banks = typeList;
    });
  }

  /**
   * Load Bank Branches
   */
  fetchBranches(bankCode: string) {
    this.chequeCollection.branchCode = null;
    this.service.getBranches(bankCode).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.branches = typeList;
    });
  }

  private fetchMobileOperators() {
    this.mobileMoneyCollection.operatorCode = null;
    this.service.getMobileOperators().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.mobile_operators = typeList;
    });
  }

  checkAmount() {

    this.balance = ((+this.selectedOutstanding.debitAmount - +(this.selectedOutstanding.settledAmount)) + this.loyaltyPointsSpending);

    if (this.paymentDetailForm.controls['amount_to_be_paid'].value > this.balance) {
      this.paymentDetailForm.controls['amount_to_be_paid'].setValue(this.balance);
    } else {
      // TODO
      // const paymentAmount = this.paymentDetailForm.controls['amount_to_be_paid'].value;
      // console.log(paymentAmount);
      // if (+paymentAmount % 1 !== 0) {
      //   console.log(paymentAmount.split('', 2 ));
      //   (this.paymentDetailForm.controls['amount_to_be_paid'].setValue(paymentAmount.toFixed(2)));
      // }
    }
  }

  // checkAmount() {
  //   this.outStanding = (+this.selectedOutstanding.debitAmount - +(this.selectedOutstanding.settledAmount));
  //   console.log(this.paymentDetailForm.controls['amount_to_be_paid'].value);
  //   // if (this.paymentDetailForm.controls['amount_to_be_paid'].value <) {
  //   //
  //   // }
  // }
  clearData() {
    this.loyaltyPointsSpending = 0;
    this.amountTobePaid = 0;
    this.balance = 0;
    this.outstandingAmount = null;


  }

  onPayPremiumButton() {

    // check farmer
    if (sessionStorage.getItem('asCustomer') === 'Y') {
      // Convert to number
      this.debitAmount = +this.selectedOutstanding.debitAmount;
      this.settledAmount = +this.selectedOutstanding.settledAmount;
      // @ts-ignore
      // tslint:disable-next-line:triple-equals
      if (this.amountTobePaid == (this.debitAmount - (this.settledAmount + this.loyaltyPointsSpending)).toFixed(2)) {
        if (this.paymentDetailForm.invalid) {
          return;
        }
        const settlement = new Settlement();
        settlement.branchCode = sessionStorage.getItem('branchCode');
        settlement.debitNoteNumber = this.debitNoteNumber;
        const detail = new SettlementDetail();
        detail.debitNoteNumber = this.debitNoteNumber;
        detail.loyaltyPointsUtilization = this.loyaltyPointsSpending;
        detail.paidAmount = this.amountTobePaid;
        settlement.settlementDetail.push(detail);
        settlement.collection = this.getCollection();
        settlement.collection.currencyCode = 'LKR';
        this.service.saveSettlement(settlement).subscribe(resp => {
          const body = resp.body as any;
          this.settlementSequence = body.data;
          this.notificationService.isMessageDisplayed = true;
          this.notificationService.messageType = 'success';
          this.notificationService.messageContent = 'Payment saved successfully!';
          this.button = true;
          this.completeSlip();
          this.paymentDetailForm.reset();
          this.onPaymentModeChange();
          this.selectedOutstanding.debitAmount = '';
          this.selectedOutstanding.settledAmount = '';
          this.selectedOutstanding.loyaltyPointsBalance = '';
          this.getOutstandingList('CN');
        }, error => {
          this.notificationService.isMessageDisplayed = true;
          this.notificationService.messageContent = error.message;
          this.notificationService.messageType = 'error';
        });
      } else {
        // partial payment error message
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageContent = 'Partial payments are not allowed for Up front policy payments';
        this.notificationService.messageType = 'error';
      }
    } else {
      /// asAgent login
      if (this.paymentDetailForm.invalid) {
        return;
      }
      const settlement = new Settlement();
      settlement.branchCode = sessionStorage.getItem('branchCode');
      settlement.debitNoteNumber = this.debitNoteNumber;
      const detail = new SettlementDetail();
      detail.debitNoteNumber = this.debitNoteNumber;
      detail.loyaltyPointsUtilization = this.loyaltyPointsSpending;
      detail.paidAmount = this.amountTobePaid;
      settlement.settlementDetail.push(detail);
      settlement.collection = this.getCollection();
      settlement.collection.currencyCode = 'LKR';
      this.service.saveSettlement(settlement).subscribe(resp => {
        const body = resp.body as any;
        this.settlementSequence = body.data;
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageType = 'success';
        this.notificationService.messageContent = 'Payment saved successfully!';
        this.button = true;
        this.completeSlip();
        this.paymentDetailForm.reset();
        this.onPaymentModeChange();
        this.selectedOutstanding.debitAmount = '';
        this.selectedOutstanding.settledAmount = '';
        this.selectedOutstanding.loyaltyPointsBalance = '';
        this.getOutstandingList('CN');

      }, error => {
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageContent = error.message;
        this.notificationService.messageType = 'error';
      });
    }
  }

  private getCollection() {
    switch (this.paymentMode) {
      case 'PAYCQ':
        this.chequeCollection.bankCode = this.paymentDetailForm.get('cheque_bank').value;
        this.chequeCollection.branchCode = this.paymentDetailForm.get('cheque_branch').value;
        return this.chequeCollection;
      case 'PAYCS':
        return this.cashCollection;
      case 'PAYBD':
        this.bankCollection.bankCode = this.paymentDetailForm.get('deposit_bank').value;
        this.bankCollection.depositDate = moment(this.paymentDetailForm.get('deposit_date').value).format('YYYY-MM-DD HH:mm:ss');
        return this.bankCollection;
      case 'PAYMM':
        this.mobileMoneyCollection.operatorCode = this.paymentDetailForm.get('mobile_operator').value;
        return this.mobileMoneyCollection;
      case 'PAYCC':
        this.cardCollection.cardExpiry = moment(this.paymentDetailForm.get('credit_card_date').value).format('MM/YYYY');
        return this.cardCollection;
    }
  }

  //// Loyaltypoint input
  updatePointsUtilization() {

    if (this.loyaltyPointsSpending > this.outstanding.loyaltyPointsBalance) {
      this.loyaltyPointsSpending = this.outstanding.loyaltyPointsBalance;
    }
    if (this.loyaltyPointsSpending > this.balance) {
      this.loyaltyPointsSpending = this.outstanding.debitAmount - this.outstanding.settledAmount - this.amountTobePaid;
    }
    if (this.loyaltyPointsSpending < 0) {
      this.loyaltyPointsSpending = 0;
    }
    this.paymentDetailForm.controls['loyalty_points'].setValue(this.loyaltyPointsSpending);
    this.updateBalance();

  }

  updateBalance() {
    this.balance = this.outstanding.debitAmount - this.outstanding.settledAmount - this.loyaltyPointsSpending - this.amountTobePaid;
    this.balance = Math.round(this.balance * 100) / 100;
  }

  onPaymentModeChange() {
    this.paymentMode = this.paymentDetailForm.get('payment_mode').value;
    this.paymentDetailForm.clearValidators();
    switch (this.paymentMode) {
      case 'PAYCQ':
        if (!this.banks) {
          this.fetchBanks();
        }
        this.paymentDetailForm.controls['cheque_number'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['cheque_bank'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['cheque_branch'].setValidators([Validators.required]);
        this.amountLabelText = 'Cheque Amount';
        break;
      case 'PAYCS':
        this.amountLabelText = 'Amount to be Paid';
        break;
      case 'PAYBD':
        if (!this.banks) {
          this.fetchBanks();
        }
        this.paymentDetailForm.controls['deposit_ref'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['deposit_date'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['deposit_bank'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['deposit_account'].setValidators([Validators.required]);
        this.amountLabelText = 'Deposit Amount';
        break;
      case 'PAYMM':
        if (!this.mobile_operators) {
          this.fetchMobileOperators();
        }
        this.paymentDetailForm.controls['mobile_operator'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['mobile_number'].setValidators([Validators.required]);
        this.amountLabelText = 'Amount to be Paid';
        break;
      case 'PAYCC':
        this.paymentDetailForm.controls['credit_card_number'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['credit_card_ccv'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['credit_card_date'].setValidators([Validators.required]);
        this.amountLabelText = 'Amount to be Paid';
        break;
    }
    this.paymentDetailForm.controls['amount_to_be_paid'].setValidators([Validators.required]);
  }

  onBankChange(code: string) {
    this.fetchBranches(code);
  }

  onAmountToBePaidChange() {

    if (this.amountTobePaid > this.balance) {
      this.amountTobePaid = this.outstanding.debitAmount - this.outstanding.settledAmount - this.loyaltyPointsSpending;
    }
    if (this.amountTobePaid < 0) {
      this.amountTobePaid = 0;
    }
    this.amountTobePaid = Math.round(this.amountTobePaid * 100) / 100;
    this.updateBalance();
  }


  onSelectoutstanding(i: number) {
    this.outData[i].active = true;
    if (i !== this.activeOutstandingIndex) {
      this.outData[this.activeOutstandingIndex].active = false;
    }
    this.activeOutstandingIndex = i;
    this.selectedOutstanding = this.outData[i];
    this.debitNoteNumber = this.selectedOutstanding.debitNoteNumber;
    this.fetchOutstandingDetails();
    this.button = false;
    this.clearData();
    this.paymentMode = null;
    this.paymentDetailForm.reset();
    this.paymentDetailForm.controls['amount_to_be_paid'].setValue(0);
    this.paymentDetailForm.controls['loyalty_points'].setValue(0);


  }

  // generate slips
  completeSlip() {
    // Outstanding Individual file
    const fileName1 = 'Outstanding.pdf';
    this.service.downloadDebt(this.settlementSequence).subscribe(x => {
      if (x.size === 0) {
        return;
      }
      const newBlob = new Blob([x], {type: 'application/pdf'});
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      const data = window.URL.createObjectURL(newBlob);
      const link = document.createElement('a');
      link.href = data;
      link.download = fileName1;
      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });
  }

}
