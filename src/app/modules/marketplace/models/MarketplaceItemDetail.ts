import {S3Object} from '../../../model/S3Object';

export class MarketplaceItemDetail {
  id: number;
  imageUrl: S3Object[];
  name: string;
  category: string;
  categoryDescription: string;
  brandName: string;
  amount: number;
  unit: string;
  location: string;
  locationDescription: string;
  publisherCode: string;
  description: string;
  externalLink: string;
  datetime: string;
  status: string;
  version: number;
}
