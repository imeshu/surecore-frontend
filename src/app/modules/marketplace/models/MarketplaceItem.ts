export class MarketplaceItem {
  id: string;
  imageUrl: string;
  name: string;
  category: string;
  categoryDescription: string;
  brandName: string;
  amount: number;
  unit: string;
  location: string;
  locationDescription: string;
}
