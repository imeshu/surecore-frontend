import {MarketplaceItem} from './MarketplaceItem';

export class PaginatedMarketplaceSearchResult {
  productSummary: MarketplaceItem[];
  totalNoOfPages: number;
  totalNoOfRecords: number;
}
