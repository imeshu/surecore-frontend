import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarketplaceListComponent } from './marketplace-list/marketplace-list.component';
import {MarketplaceProductViewComponent} from './marketplace-product-view/marketplace-product-view.component';
import {MarketplaceAddComponent} from './marketplace-add/marketplace-add.component';

const routes: Routes = [
  {
    path: 'marketplace',
    component: MarketplaceListComponent
  },
  {
    path: 'marketplace/product/:productID',
    component: MarketplaceProductViewComponent,
  },
  {
    path: 'marketplace-add',
    component: MarketplaceAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketplaceRoutingModule { }
