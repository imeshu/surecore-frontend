import { Component, OnInit } from '@angular/core';
import { MarketplaceService } from '../services/marketplace.service';
import { MarketplaceItem } from '../models/MarketplaceItem';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { CommonType } from '../../../model/CommonType';
import { LIST_ALL, RECENTLY_LISTED } from '../../../config/enum';
import { MatDialog } from '@angular/material';
import { TermsAndConditionComponent } from '../terms-and-condition/terms-and-condition.component';
import { Router } from '@angular/router';
import { MarketplaceAddComponent } from '../marketplace-add/marketplace-add.component';

@Component({
  selector: 'app-marketplace-list',
  templateUrl: './marketplace-list.component.html',
  styleUrls: ['./marketplace-list.component.scss']
})
export class MarketplaceListComponent implements OnInit {

  // Infinite Scroll Settings
  // tslint:disable-next-line:max-line-length
  array = [];
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  containerName = 'mat-drawer-content';
  fromRoot = true;
  direction = '';

  totalRecords;
  currentRecords;
  page;
  size;

  // search drop down values
  locations: CommonType[];
  categories: CommonType[];

  // search params
  articleTitle = '';
  locationCode = '';
  categoryCode = '';
  listedByMe = false;

  recentlyListed = LIST_ALL;
  recentlyListedBoolean = 'false';
  sortByPrice = '';

  marketplaceItemList: MarketplaceItem[];

  termsAndConditions;

  constructor(private marketplaceService: MarketplaceService, public dialog: MatDialog, private router: Router) { }

  ngOnInit() {
    this.loadDropDownValues();
    this.loadItems();

    this.marketplaceService.getTermsAndConditionStatus().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(result => {
      this.termsAndConditions = result;

      if (this.termsAndConditions === false) {
        this.openTAndCDialog();
      }

    });

  }

  initializeCounters() {
    this.totalRecords = 0;
    this.currentRecords = 0;
    this.page = 1;
    this.size = 12; // FIXED SIZE! NO NEED TO CHANGE FOR THIS UI. Supports resolutions upto 4K (1440p)
  }

  loadDropDownValues() {
    this.marketplaceService.getServiceCenters().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.locations = typeList;
      // this.locations[0].code='10';
    });

    this.marketplaceService.getCategories().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.categories = typeList;
    });
  }

  loadItems() {
    // reset current infinite scroll & other counters
    this.initializeCounters();

    // bug fix : empty value does not get set. instead it becomes undefined. forcefully set it as empty if undefined
    if (this.sortByPrice === null) {
      alert('null');
      this.sortByPrice = '';
    }

    this.marketplaceService.getMarketplaceItems(
      this.categoryCode,
      this.articleTitle,
      this.recentlyListed,
      this.locationCode,
      this.listedByMe,
      this.page,
      this.size,
      this.sortByPrice
    ).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(result => {
      this.marketplaceItemList = result.productSummary;
      this.totalRecords = result.totalNoOfRecords;
    });
  }

  toggleListedByMeValue() {
    this.listedByMe = !this.listedByMe;
    this.loadItems();
  }

  isRecentlyListed() {
    if (this.recentlyListedBoolean === 'true') {
      this.recentlyListed = RECENTLY_LISTED;
    } else {
      this.recentlyListed = LIST_ALL;
    }
    this.loadItems();
  }

  appendItems() {
    // get next data set
    let nextBatch: MarketplaceItem[];
    this.marketplaceService.getMarketplaceItems(
      this.categoryCode,
      this.articleTitle,
      this.recentlyListed,
      this.locationCode,
      this.listedByMe,
      this.page,
      this.size,
      this.sortByPrice
    ).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).toPromise().then(result => {
      nextBatch = result.productSummary;
      this.totalRecords = result.totalNoOfRecords;
      this.currentRecords += nextBatch.length;

      for (let i = 0; i < nextBatch.length; ++i) {
        this.marketplaceItemList.push(nextBatch[i]);
      }
    });

  }

  prependItems(startIndex, endIndex) {
    this.marketplaceItemList.slice(0, this.marketplaceItemList.length - 4);
  }

  onScrollDown(ev) {
    console.log('scrolled down!!', ev);

    if (this.totalRecords >= this.currentRecords) {
      this.page += 1;
      this.appendItems();
    } else {
      console.log('Total Records : ' + this.totalRecords);
      console.log('Current Records : ' + this.currentRecords);
    }

    this.direction = 'down';
  }

  onUp(ev) {
    console.log('scrolled up!', ev);
  }

  getTAndCStatus() {
    this.marketplaceService.getTermsAndConditionStatus().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(result => {
      this.termsAndConditions = result;
    });
  }

  openTAndCDialog(): void {
    const dialogRef = this.dialog.open(TermsAndConditionComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Terms & Conditions dialog box was closed');
    });
  }

  onProductView(productID: string) {
    this.router.navigate(['marketplace/product', productID]);
  }

  openAddNewProductView() {
    const dialogRef = this.dialog.open(MarketplaceAddComponent,
      {
        width: '100%',
        height: '100%',
      });
      dialogRef.afterClosed().subscribe(() => {
        this.loadItems();
      });
  }

}
