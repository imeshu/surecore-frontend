import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketplaceProductViewComponent } from './marketplace-product-view.component';

describe('MarketplaceProductViewComponent', () => {
  let component: MarketplaceProductViewComponent;
  let fixture: ComponentFixture<MarketplaceProductViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketplaceProductViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketplaceProductViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
