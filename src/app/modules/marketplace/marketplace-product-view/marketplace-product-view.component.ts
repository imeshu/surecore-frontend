import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MarketplaceService} from '../services/marketplace.service';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {MarketplaceItemDetail} from '../models/MarketplaceItemDetail';
import {BasicUserDetail} from '../../../model/BasicUserDetail';
import {MarketplaceItem} from '../models/MarketplaceItem';
import {LIST_ALL} from '../../../config/enum';
import {MatDialog} from '@angular/material';
import {RatingComponent} from '../../common-components/rating/rating.component';
import * as _ from 'lodash';
import {MarketplaceEditComponent} from '../marketplace-edit/marketplace-edit.component';

@Component({
  selector: 'app-marketplace-product-view',
  templateUrl: './marketplace-product-view.component.html',
  styleUrls: ['./marketplace-product-view.component.scss']
})
export class MarketplaceProductViewComponent implements OnInit {

  itemID: string;
  item: MarketplaceItemDetail;
  seller: BasicUserDetail;

  greenStars = 0;
  greyStars = 0;
  ratedUserCount: Number;

  showContact = false;

  itemCreatedBySameUser = false;

  similarItems: MarketplaceItem[];

  constructor(private service: MarketplaceService, private router: Router, private route: ActivatedRoute, public dialog: MatDialog) { }

  ngOnInit() {

    this.route.params.subscribe(routeParams => {

      if (this.router.url.includes('marketplace/product')) {
        this.itemID = this.route.snapshot.paramMap.get('productID');

        this.service.getMarketplaceItem(this.itemID).pipe(
          catchError(error => {
            return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
          })
        ).toPromise().then(result => {
          this.item = result;
          this.showContact = false;

          if (sessionStorage.getItem('partyCode') === this.item.publisherCode) {
            this.itemCreatedBySameUser = true;
          }

          this.service.getRatedUserCount(this.item.publisherCode).pipe(
            catchError(error => {
              return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
            })
          ).subscribe(x => {
            // @ts-ignore
            this.ratedUserCount = x.body.data;
          });

          this.service.getSellerDetails(this.item.publisherCode).pipe(
            catchError(error => {
              return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
            })
          ).subscribe(sellerResult => {
            this.seller = sellerResult;

            this.service.checkIfItemAlreadyViewed(this.itemID).pipe(
              catchError(error => {
                return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
              })
            ).subscribe(x => {
              this.showContact = x !== null;
            });

            if (this.seller.rating != null) {
              this.greenStars = Math.round(parseFloat(this.seller.rating));
            }

            this.greyStars = 5 - this.greenStars;
          });

          this.service.getMarketplaceItems(this.item.category, '', LIST_ALL, '', false, 1, 5, '').pipe(
            catchError(error => {
              return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
            })
          ).subscribe(similarResult => {

            // remove current item from similar product list
            const x = _.find(similarResult.productSummary, {'id': this.item.id});
            this.similarItems = _.pull(similarResult.productSummary, x);

            // if the product is not in the list, remove the last product.
            if (this.similarItems.length === 5) {
              this.similarItems.pop();
            }

          });

        });
      }

    });

  }

  getItemDetails(itemID: string) {
    this.service.getMarketplaceItem(itemID).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).toPromise().then(result => {
      this.item = result;
    });
  }

  getSellerDetails(sellerID: string) {
    this.service.getSellerDetails(sellerID).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).toPromise().then(result => {
      this.seller = result;
    });
  }

  goToLink(url: string) {
    if (!url.includes('https://' || 'http://')) {
      const x = '//';
      url = x + url;
    }

    window.open(url, '_blank');
  }

  onProductView(productID: string) {
    this.router.navigate(['marketplace/product', productID]);
  }

  openRatingDialog(): void {
    const dialogRef = this.dialog.open(RatingComponent, {
      width: '100%',
      height: '100%',
      data: { productID: this.item.id }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Rating dialog box was closed');
      this.ngOnInit();
    });
  }

  openEditDialog(): void {
    const dialogRef = this.dialog.open(MarketplaceEditComponent, {
      width: '100%',
      height: '100%',
      data: this.item
    });
    dialogRef.afterClosed().subscribe(result => {
      this. ngOnInit();
    });
  }

  showContactNumbers() {
    this.service.markAsViewed(this.itemID).pipe(
      catchError(error => {
        // 1005 - conflict
        if (error.status === 1005) {
          this.showContact = true;
        } else {
          return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
        }

      })
    ).toPromise().then(result => {
      this.showContact = true;
    });
  }

}
