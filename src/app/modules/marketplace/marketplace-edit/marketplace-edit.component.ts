import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MarketplaceService } from '../services/marketplace.service';
import { CommonType } from '../../../model/CommonType';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { ProductImage } from '../models/ProductImage';
import { HttpEventType } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MarketplaceItemDetail } from '../models/MarketplaceItemDetail';
import { S3Object } from '../../../model/S3Object';
import { NotificationDataTransferService } from '../../../services/notification-data-transfer.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { DialogDeleteConfirmComponent } from './dialog-delete/dialog-delete.component';

@Component({
  selector: 'app-marketplace-edit',
  templateUrl: './marketplace-edit.component.html',
  styleUrls: ['./marketplace-edit.component.scss']
})

export class MarketplaceEditComponent implements OnInit {

  constructor(private service: MarketplaceService,
    private router: Router,
    public notificationService: NotificationDataTransferService,
    public dialogRef: MatDialogRef<MarketplaceEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MarketplaceItemDetail,
    public deleteDialog: MatDialog) {
    this.currentProduct = this.data;
  }

  private currentProduct: MarketplaceItemDetail;
  private categories: CommonType[];
  private productCategory: CommonType;
  private progressBar: string;
  private imageList: ProductImage[] = new Array();
  private tempFile: any;
  private productCategoryError: string;
  private locationError: string;
  locations: CommonType[];
  locationCode = null;
  private inputName = null;
  private inputDescription = null;
  private inputAmount = null;

  productForm: FormGroup;

  requiredTextController = new FormControl('', [
    Validators.required,
  ]);
  private tempCategory: CommonType = new CommonType();

  ngOnInit() {
    this.productForm = new FormGroup({
      name: new FormControl(this.currentProduct.name, Validators.required),
      brandName: new FormControl(this.currentProduct.brandName),
      location: new FormControl('', Validators.required),
      description: new FormControl(this.currentProduct.description, Validators.required),
      amount: new FormControl(this.currentProduct.amount, Validators.required),
      externalLink: new FormControl(this.currentProduct.externalLink)
    });
    this.inputName = this.currentProduct.name;
    this.inputDescription = this.currentProduct.description;
    this.inputAmount = this.currentProduct.amount;

    this.getDropdownValues();
    this.tempCategory.code = this.currentProduct.category;
    this.tempCategory.description = this.currentProduct.categoryDescription;
    this.productCategory = this.tempCategory;
    this.locationCode = this.currentProduct.location;
    const tempImageList: unknown = this.currentProduct.imageUrl;
    this.imageList = tempImageList as ProductImage[];
  }

  getDropdownValues() {
    this.service.getCategories().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.categories = typeList;
    });

    this.service.getServiceCenters().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.locations = typeList;
      console.log(this.locations);
    });
  }

  reomveCategory() {
    this.productCategory = null;
  }

  addCategory(category: CommonType) {
    this.productCategory = category;
  }

  addImage(event) {
    const image: ProductImage = new ProductImage();
    this.imageList.push(image);
    this.uploadImage(event.target.files[0], this.imageList.indexOf(image));
    this.tempFile = null;
  }

  uploadImage(file: File, index: number) {
    console.log('upload image');
    this.progressBar = '';
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.imageList[index].sequence = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imageList[index].key = body.data.object.key;
        this.imageList[index].url = body.data.object.url;
        console.log(resp.body);
        console.log(this.imageList[index].url);
      }
    });
  }

  removeItem(index: number) {
    this.imageList.splice(index, 1);
  }

  asd() {

    if (!this.productCategory) {
      this.productCategoryError = 'true';
    } else {
      this.productCategoryError = null;
    }
    if (!this.locationCode) {
      this.locationError = 'true';
    } else {
      this.locationError = null;
    }
    if (!this.productCategoryError && !this.locationError && this.inputName && this.inputAmount && this.inputDescription) {
      const formValue = this.productForm.value;
      const saveData: MarketplaceItemDetail = formValue;
      saveData.category = this.productCategory.code;
      saveData.unit = 'LKR';
      saveData.location = this.locationCode;
      const tempImageList: unknown = this.imageList;
      saveData.imageUrl = tempImageList as S3Object[];
      saveData.id = this.currentProduct.id;
      saveData.version = this.currentProduct.version;
      console.log(saveData);

      this.service.updateProduct(saveData, this.currentProduct.id).subscribe(resp => {
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageContent = 'Item updated successfully!';
        this.notificationService.messageType = 'success';
        this.onClose();
      }, error => {
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageContent = error.message;
        this.notificationService.messageType = 'error';

      });
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  openDialogDelete() {
    const deleteDialogRef = this.deleteDialog.open(DialogDeleteConfirmComponent);
    deleteDialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        console.log('---------------------------------');
        console.log(this.currentProduct.id);
        this.service.deleteProduct(this.currentProduct.id).subscribe(resp => {
          this.notificationService.isMessageDisplayed = true;
          this.notificationService.messageContent = 'Item deleted successfully!';
          this.router.navigate(['marketplace'], { replaceUrl: true });
          this.onClose();
        }, error => {
          this.notificationService.isMessageDisplayed = true;
          this.notificationService.messageContent = error.message;
          console.log(error);
        });
      }
    });
  }
}
