import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {MarketplaceService} from '../services/marketplace.service';

@Component({
  selector: 'app-terms-and-condition',
  templateUrl: './terms-and-condition.component.html',
  styleUrls: ['./terms-and-condition.component.scss']
})
export class TermsAndConditionComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TermsAndConditionComponent>, private service: MarketplaceService) { }

  ngOnInit() {
  }

  onClose(): void {
    this.dialogRef.close();
  }

  markTAndCAsRead() {
    this.service.setTermsAndConditionStatus().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(result => {
      this.onClose();
    });
  }

}
