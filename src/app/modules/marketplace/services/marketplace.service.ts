import {Injectable} from '@angular/core';
import {HTTPCallService} from '../../../services/httpcall.service';
import {Observable} from 'rxjs';
import {HttpEvent, HttpResponse} from '@angular/common/http';
import {MarketplaceItem} from '../models/MarketplaceItem';
import {map} from 'rxjs/operators';
import {CommonType} from '../../../model/CommonType';
import {PaginatedMarketplaceSearchResult} from '../models/PaginatedMarketplaceSearchResult';
import {S3Object} from '../../../model/S3Object';
import {MarketplaceItemDetail} from '../models/MarketplaceItemDetail';
import {BasicUserDetail} from '../../../model/BasicUserDetail';
import {Contact} from '../../../model/Contact';
import {fileServer} from '../../../config/enum';

@Injectable({
  providedIn: 'root'
})
export class MarketplaceService {

  constructor(private http: HTTPCallService) { }

  getMarketplaceItems(catCode, articleTitle, numberOfDays,
                      locCode, listedByMe, page,
                      size, sorting): Observable<PaginatedMarketplaceSearchResult> {

    const path = 'marketplace/products/paginated_search/portal?' +
      'categoryCode=' + catCode +
      '&articleTitle=' + articleTitle +
      '&numberOfDays=' + numberOfDays +
      '&locationCode=' + locCode +
      '&listedByMe=' + listedByMe +
      '&page=' + page +
      '&size=' + size +
      '&priceOrder=' + sorting;

    return this.http.getResource(path).pipe(
      map(response => {

        const result: PaginatedMarketplaceSearchResult = new PaginatedMarketplaceSearchResult();
        // @ts-ignore
        result.totalNoOfPages = response.body.data.totalNoOfPages;
        // @ts-ignore
        result.totalNoOfRecords = response.body.data.totalNoOfRecords;

        // @ts-ignore
        result.productSummary = response.body.data.productSummary.map(item => {
            const x: MarketplaceItem = new MarketplaceItem();
            x.id = item.id;
            x.imageUrl = item.imageUrl;
            x.name = item.name;
            x.category = item.category;
            x.categoryDescription = item.categoryDescription;
            x.brandName = item.brandName;
            x.amount = item.amount;
            x.unit = item.unit;
            x.location = item.location;
            x.locationDescription = item.locationDescription;
            return x;
          });

        return result;
      })
    );
  }

  getMarketplaceItem(itemID: string): Observable<MarketplaceItemDetail> {
    const path = 'marketplace/products/' + itemID;

    return this.http.getResource(path).pipe(
      map( response => {
        const result: MarketplaceItemDetail = new MarketplaceItemDetail();
        // @ts-ignore
        const data = response.body.data;

        result.id = data.id;
        result.name = data.name;
        result.category = data.category;
        result.categoryDescription = data.categoryDescription;
        result.brandName = data.brandName;
        result.amount = data.amount;
        result.unit = data.unit;
        result.location = data.location;
        result.locationDescription = data.locationDescription;
        result.publisherCode = data.publisherCode;
        result.description = data.description;
        result.externalLink = data.externalLink;
        result.datetime = data.datetime;
        result.status = data.status;
        result.version = data.version;

        result.imageUrl = data.imageUrl.map(image => {
          const x: S3Object = new S3Object();
          x.id = image.id;
          x.name = image.name;
          x.key = image.key;
          x.url = image.url;
          x.status = image.status;
          x.version = image.version;

          return x;
        });

        return result;
      })
    );
  }

  getServiceCenters(): Observable<CommonType[]> {
    const path = 'base/agrarianServiceCenters';

    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getCategories(): Observable<CommonType[]> {
    const path = 'base/marketplace/categories';

    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getSellerDetails(sellerCode: string): Observable<BasicUserDetail> {
    const path = 'base/parties/' + sellerCode + '/profiles';

    return this.http.getResource(path).pipe(
      map(response => {
        const basicUserDetail: BasicUserDetail = new BasicUserDetail();
        // @ts-ignore
        const data = response.body.data;

        basicUserDetail.name = data.profile.name;
        if (data.image != null) {
          basicUserDetail.profileImageURL = data.image.image;
        } else {
          basicUserDetail.profileImageURL = null;
        }
        basicUserDetail.rating = data.profile.partyPoint;

        basicUserDetail.contacts = data.contact.map(contact => {
          const x: Contact = new Contact();
          x.contactType =  contact.contactType;
          x.contactNumber = contact.contactNumber;
          x.status = contact.status;
          x.sequence = contact.sequence;
          x.version = contact.version;
          x.contactTypeDescription = contact.contactTypeDescription;
          x.default = contact.default;
          return x;
        });

        return basicUserDetail;
      })
    );
  }

  uploadImage(file: File): Observable<HttpEvent<Object>> {
    const fd = new FormData();
    fd.append('file', file);
    const path = fileServer + 'v1/files/upload?path=marketplace';
    return this.http.postImage(path, fd);
  }

  saveProduct(product: MarketplaceItemDetail): Observable<HttpResponse<Object>> {
    const path = 'marketplace/products';
    return this.http.postResource(path, product);
  }

  updateProduct(product: MarketplaceItemDetail, id: number): Observable<HttpResponse<Object>> {
    const path = 'marketplace/products/' + id;
    return this.http.putResource(path, product);
  }

  getTermsAndConditionStatus(): Observable<HttpResponse<String>> {
    const path = 'base/parties/users/termsAndConditions';

    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data;
      })
    );
  }

  setTermsAndConditionStatus(): Observable<HttpResponse<Object>> {
    const path = 'base/parties/users/termsAndConditions';

    return this.http.postResource(path, null);
  }

  submitRating(productID: string, ratingValue: number) {
    const path = 'marketplace/products/' + productID + '/rating';
    const rating: RatingDTO = new RatingDTO();
    rating.rating = ratingValue;

    return this.http.postResource(path, rating);
}
  deleteProduct(id: number): Observable<HttpResponse<Object>> {
    const path = 'marketplace/products/' + id;
    return this.http.deleteResource(path);
  }

  getRatedUserCount(partyCode: string): Observable<HttpResponse<Object>> {
    const path = 'marketplace/publisher/' + partyCode + '/ratedUserCount';

    return this.http.getResource(path);
  }

  markAsViewed(productID: string): Observable<HttpResponse<Object>> {
    const path = 'marketplace/products/' + productID + '/hits';

    return this.http.postResource(path, null);
  }

  checkIfItemAlreadyViewed(productID: string): Observable<HttpResponse<Object>> {
    const path = 'marketplace/products/' + productID + '/users/hits';

    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data;
      })
    );

  }

}

export class RatingDTO {
  rating: number;
}
