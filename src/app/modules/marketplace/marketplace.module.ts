import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketplaceListComponent } from './marketplace-list/marketplace-list.component';
import {MarketplaceRoutingModule} from './marketplace-routing.module';
import {DashboardModule} from '../dashboard/dashboard.module';
import {CustomPipeModule} from '../../pipes/CustomPipesModule';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonToggleModule, MatSelectModule, MatInputModule, MatButtonModule} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TermsAndConditionComponent } from './terms-and-condition/terms-and-condition.component';
import { MarketplaceProductViewComponent } from './marketplace-product-view/marketplace-product-view.component';
import { MarketplaceAddComponent } from './marketplace-add/marketplace-add.component';
import {CommonComponentsModule} from '../common-components/common-components.module';
import { MarketplaceEditComponent } from './marketplace-edit/marketplace-edit.component';
import {RatingComponent} from '../common-components/rating/rating.component';
import {DialogDeleteConfirmComponent} from './marketplace-edit/dialog-delete/dialog-delete.component';

@NgModule({
  declarations: [
    MarketplaceListComponent,
    TermsAndConditionComponent,
    MarketplaceProductViewComponent,
    MarketplaceAddComponent,
    MarketplaceEditComponent,
    DialogDeleteConfirmComponent
  ],
  entryComponents: [
    TermsAndConditionComponent,
    RatingComponent,
    MarketplaceEditComponent,
    DialogDeleteConfirmComponent
  ],
  exports: [
    MarketplaceListComponent
  ],
  imports: [
    MarketplaceRoutingModule,
    CommonModule,
    DashboardModule,
    CustomPipeModule,
    FormsModule,
    BrowserModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatInputModule,
    MatButtonModule,
    CommonComponentsModule
  ]
})
export class MarketplaceModule { }
