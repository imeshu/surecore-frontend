import {Component, Input, OnInit, Inject} from '@angular/core';
import {ArticalCategoryModel} from '../../news/models/artical-category';
import {Router} from '@angular/router';
import {MarketplaceService} from '../services/marketplace.service';
import {CommonType} from '../../../model/CommonType';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {ProductImage} from '../models/ProductImage';
import {HttpEventType} from '@angular/common/http';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MarketplaceItemDetail} from '../models/MarketplaceItemDetail';
import {S3Object} from '../../../model/S3Object';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-marketplace-add',
  templateUrl: './marketplace-add.component.html',
  styleUrls: ['./marketplace-add.component.scss']
})
export class MarketplaceAddComponent implements OnInit {
  private categories: CommonType[];
  private productCategory: CommonType;
  private progressBar: string;
  private imageList: ProductImage[] = new Array();
  private tempFile: any;
  private productCategoryError: string;
  private locationError: string;
  locations: CommonType[];
  locationCode = null;
  private isSubmitted = null;
  private inputName = null;
  private inputDescription = null;
  private inputAmount = null;
  productForm = new FormGroup({
    name: new FormControl('', Validators.required),
    brandName: new FormControl(''),
    location: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    amount: new FormControl(0.00, Validators.required),
    externalLink: new FormControl('')
  });

  requiredTextController = new FormControl('', [
    Validators.required,
  ]);

  constructor(private service: MarketplaceService,
              private router: Router,
              public notificationService: NotificationDataTransferService,
              public dialogRef: MatDialogRef<MarketplaceAddComponent>, @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.service.getCategories().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.categories = typeList;
    });

    this.service.getServiceCenters().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.locations = typeList;
      console.log(this.locations);
    });
  }

  reomveCategory() {
    this.productCategory = null;
  }

  addCategory(category: CommonType) {
    this.productCategory = category;
  }

  addImage(event) {
    const image: ProductImage = new ProductImage();
    this.imageList.push(image);
    this.uploadImage(event.target.files[0], this.imageList.indexOf(image));
    this.tempFile = null;
  }

  uploadImage(file: File, index: number) {
    console.log('upload image');
    this.progressBar = '';
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.imageList[index].sequence = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imageList[index].key = body.data.object.key;
        this.imageList[index].url = body.data.object.url;
        console.log(resp.body);
        console.log(this.imageList[index].url);
      }
    });
  }

  removeItem(index: number) {
    this.imageList.splice(index, 1);
  }

  asd() {
    this.isSubmitted = 'true';
    if (!this.productCategory) {
      this.productCategoryError = 'true';
    } else {
      this.productCategoryError = null;
    }
    if (!this.locationCode) {
      this.locationError = 'true';
    } else {
      this.locationError = null;
    }
    const formValue = this.productForm.value;
    this.inputName = formValue.name;
    this.inputDescription = formValue.description;
    this.inputAmount = formValue.amount;
    console.log(this.productForm.valid);
    if (!this.productCategoryError && !this.locationError && this.inputName && this.inputAmount && this.inputDescription) {
      const saveData: MarketplaceItemDetail = formValue;
      saveData.category = this.productCategory.code;
      saveData.unit = 'LKR';
      saveData.location = this.locationCode;
      const tempImageList: unknown = this.imageList;
      saveData.imageUrl =  tempImageList as S3Object[];
      console.log(saveData);

      this.service.saveProduct(saveData).subscribe(resp => {
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageContent = 'Item saved successfully!';
        this.notificationService.messageType = 'success';
        this.onClose();
      }, error => {
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageContent = error.message;
        this.notificationService.messageType = 'error';
        console.log(error);
      });
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
