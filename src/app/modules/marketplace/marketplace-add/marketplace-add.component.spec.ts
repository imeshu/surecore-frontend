import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketplaceAddComponent } from './marketplace-add.component';

describe('MarketplaceAddComponent', () => {
  let component: MarketplaceAddComponent;
  let fixture: ComponentFixture<MarketplaceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketplaceAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketplaceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
