import { ArticalDescriptionModel } from './artical-description';
import { ArticalCategoryModel } from './artical-category';

export class ArticalModel {
    id: string;
    title: string;
    category: ArticalCategoryModel[];
    publisherCode: string;
    authorName: string;
    datetime: string;
    status: string;
    description: ArticalDescriptionModel[];
    version: string;
}
