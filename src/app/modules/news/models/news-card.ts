export class NewsCardModel {
    id: string;
    title: string;
    publisherCode: string;
    authorName: string;
    datetime: string;
    imagePath: string;
    shortDescription: string;
    
    constructor() {
        this.id = '';
        this.title = '';
        this.publisherCode = '';
        this.authorName = '';
        this.datetime = '';
        this.imagePath = '';
        this.shortDescription = '';
    }
}