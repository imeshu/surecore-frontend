export class ArticalCategoryModel {
    sequence: string;
    code: string;
    description: string;
    status: string;
    version: string;
}


