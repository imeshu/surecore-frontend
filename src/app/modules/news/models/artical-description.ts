export class ArticalDescriptionModel {
    sequence: string;
    type: string;
    value: any;
    order: string;
    status: any;
    version: string;
    imageKey: string;
}
