export class GetNewsModel{
    categoryCode: string;
    articleTitle: string;
    recentlyPublishedFlag: boolean;
    publishedByMe: boolean;
    page: string;
    size: string;
}