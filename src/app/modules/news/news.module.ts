import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { NewsMainComponent } from './news-main/news-main.component';
import { NewsCardComponent } from './news-card/news-card.component';
import { MarketPriceModule } from '../market-price/market-price.module';
import { CustomPipeModule } from 'src/app/pipes/CustomPipesModule';
import { NewsPublishArticleComponent } from './news-publish-article/news-publish-article.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewsEditArticleComponent } from './news-edit-article/news-edit-article.component';
import { NewsViewArticalComponent } from './news-view-article/news-view-article.component';

import { MatSelectModule, MatMenuModule, MatMenuTrigger, MatDialogModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DialogDeleteComponent } from './news-edit-article/dialog-delete/dialog-delete.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonComponentsModule } from '../common-components/common-components.module';
@NgModule({
  declarations: [
    NewsMainComponent,
    NewsCardComponent,
    NewsPublishArticleComponent,
    NewsEditArticleComponent,
    NewsViewArticalComponent,
    DialogDeleteComponent

  ],

  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NewsRoutingModule,
    BrowserAnimationsModule,
    CustomPipeModule,

    MatSelectModule,
    MatInputModule,
    InfiniteScrollModule,
    MatMenuModule,
    MatDialogModule,
    CommonComponentsModule
  ],
  exports: [
    NewsMainComponent,
    NewsCardComponent,
    NewsPublishArticleComponent,
  ],
  entryComponents: [NewsViewArticalComponent, NewsPublishArticleComponent, DialogDeleteComponent]
})
export class NewsModule { }
