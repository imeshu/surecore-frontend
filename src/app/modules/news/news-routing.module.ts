import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsEditArticleComponent } from './news-edit-article/news-edit-article.component';
import { NewsMainComponent } from './news-main/news-main.component';

const routes: Routes = [
  {
    path: 'news/edit',
    component: NewsEditArticleComponent
  },
  {
    path: 'news',
    component: NewsMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule { }