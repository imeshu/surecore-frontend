import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NewsCardModel } from '../models/news-card';
import { NewsService } from '../services/news.service';
import { ArticalModel } from '../models/artical';
import { ArticalDescriptionModel } from '../models/artical-description';
import { MatDialog } from '@angular/material';
import { NewsViewArticalComponent } from '../news-view-article/news-view-article.component';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent implements OnInit {
  @Input('card') private card: NewsCardModel;
  @Output() onClose = new EventEmitter();
  private artical: ArticalModel;
  private descriptionsList: ArticalDescriptionModel[];
  private isDataLoaded: boolean = false;
  private isEditClicked: boolean = true;

  constructor(private service: NewsService, public dialog: MatDialog) { }

  ngOnInit() {
    const shortDis = this.card.shortDescription;
    if (this.card.shortDescription) {
      this.card.shortDescription = shortDis.substring(0, 60) + '...';
    }
  }

  getArticle(id: string) {
    this.service.getArtical(id).subscribe(resp => {
      const body = resp.body as any;
      this.artical = body.data;
      this.descriptionsList = this.artical.description;
      // sort by order
      this.descriptionsList.sort(function (a, b) { return parseInt(a.order) - parseInt(b.order) });
      this.isDataLoaded = true;
    }, error => {
      console.log(error);
    });
  }

  output(event) {
    this.isEditClicked = !this.isEditClicked;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NewsViewArticalComponent, {
      width: '100%',
      height: '100%',
      data: this.card.id
    });

    dialogRef.afterClosed().subscribe(result => {
      this. closeModal();
      console.log('The dialog was closed news edit');
    });
  }
  closeModal() {
    this.onClose.emit('all closed');
  }
}
