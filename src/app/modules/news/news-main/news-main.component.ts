import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';
import { Category } from '../../market-price/models/category';
import { NewsCardModel } from '../models/news-card';
import { GetNewsModel } from '../models/get-news';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { NewsPublishArticleComponent } from '../news-publish-article/news-publish-article.component';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { trigger, transition, style, animate } from '@angular/animations';
import { ArticleSummary } from '../models/article-summary';

@Component({
  selector: 'app-news-main',
  templateUrl: './news-main.component.html',
  styleUrls: ['./news-main.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ])
    ])
  ]
})
export class NewsMainComponent implements OnInit {
  private categoriesList: Category[];
  private serchInput: string = '';
  private newsCardsList: NewsCardModel[];
  private cardList: ArticleSummary;
  private selectedCategory: Category = new Category(null, 'All');
  private selectedArticalType: string = 'Recently Published';
  private isOwnArticleShowed = false;
  private isLoaded = true;
  private isSerchedDataLoaded = true;
  private isPublishedByMe = false;



  constructor(private service: NewsService, private router: Router, public dialog: MatDialog,
    private notify: NotificationDataTransferService) { }

  ngOnInit() {
    this.getCategories();
    this.getNews();
    // this.cardList.articleSummary
  }

  getCategories() {
    this.service.getCategories().subscribe(resp => {
      const body = resp.body as any;
      this.categoriesList = body.data;
    });
  }

  setArticalType(type: string) {
    this.selectedArticalType = type;
  }

  getNews() {
    const model: GetNewsModel = new GetNewsModel();
    this.isSerchedDataLoaded = false;
    this.newsCardsList = new Array();
    model.articleTitle = this.serchInput;
    if (this.selectedCategory.code == null) {
      model.categoryCode = '';
    } else {
      model.categoryCode = this.selectedCategory.code;
    }
    model.publishedByMe = this.isPublishedByMe;
    if (this.selectedArticalType === 'Recently Published') {
      model.recentlyPublishedFlag = true;
    } else {
      model.recentlyPublishedFlag = false;
    }
    model.page = '1';
    model.size = '100';
    this.service.getNews(model).subscribe(resp => {
      const body = resp.body as any;
      this.cardList = body.data;
      this.newsCardsList = this.cardList.articleSummary;
      this.isSerchedDataLoaded = true;
    });

  }

  showOwnArticle() {
    this.isOwnArticleShowed = !this.isOwnArticleShowed;
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(NewsPublishArticleComponent, {
      width: '100%',
      height: '100%',
      data: this.categoriesList
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getNews();
      console.log('The dialog was closed');
    });
  }

  onScrollDown($event) {
    console.log('down');

  }
  onUp($event) {
    console.log('up');
  }
  // reload data when edit dialig closed
  execOnClose($event: any) {
    this.getNews();
  }

}