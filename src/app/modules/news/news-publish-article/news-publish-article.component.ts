import { Component, OnInit, Input, Inject } from '@angular/core';
import { ArticalDescriptionModel } from '../models/artical-description';
import { NewsService } from '../services/news.service';
import { ArticalModel } from '../models/artical';
import { ArticalCategoryModel } from '../models/artical-category';
import { HttpEventType } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { CommonType } from 'src/app/model/CommonType';

@Component({
  selector: 'app-news-publish-article',
  templateUrl: './news-publish-article.component.html',
  styleUrls: ['./news-publish-article.component.scss']
})
export class NewsPublishArticleComponent implements OnInit {
  private addCategoryMap = new Map();
  private finalDataList: ArticalDescriptionModel[] = new Array();
  private addCategoryList: ArticalCategoryModel[] = new Array();
  private dataList: ArticalDescriptionModel[] = new Array();
  private progressBar: string;
  private isDataAdded: boolean;
  // floating action button
  private IsActionBarClicked: boolean;
  public author: FormControl = new FormControl('', [Validators.required]);
  public heading: FormControl = new FormControl('', [Validators.required]);
  private paragraphText: string;
  private selectedCategory: CommonType = new CommonType();
  // customValidation
  private isValidAuthor: boolean;
  private isValidTitle: boolean;
  private isHasItem: boolean;
  private isHasCategory: boolean;

  constructor(
    private service: NewsService,
    public dialogRef: MatDialogRef<NewsPublishArticleComponent>,
    @Inject(MAT_DIALOG_DATA) public categories: ArticalCategoryModel[],
    private notify: NotificationDataTransferService
  ) { }

  ngOnInit() {

  }
  // floating action button
  clickActionBar() {
    this.IsActionBarClicked = !this.IsActionBarClicked;
  }

  addCategory(category: ArticalCategoryModel) {
    const list: ArticalCategoryModel[] = new Array();
    if (!this.addCategoryMap.has(category.code)) {
      this.addCategoryMap.set(category.code, category.description);
      this.addCategoryMap.forEach(function (value, key) {
        const category: ArticalCategoryModel = new ArticalCategoryModel();
        category.code = key;
        category.description = value;
        list.push(category);
      });
    }
    if (list[0]) {
      this.addCategoryList = list;
    }

    if (!this.addCategoryList[0]) {
      this.isHasCategory = true;
    } else { this.isHasCategory = false; }
  }

  reomveItemFromaddCategoryMap(key: string) {
    this.addCategoryMap.delete(key);
    const list: ArticalCategoryModel[] = new Array();
    this.addCategoryMap.forEach(function (value, key) {
      let category: ArticalCategoryModel = new ArticalCategoryModel();
      category.code = key;
      category.description = value;
      list.push(category);
    });
    this.addCategoryList = list;
    if (!this.addCategoryList[0]) {
      this.isHasCategory = true;
    } else { this.isHasCategory = false; }
  }

  addDataItem(type: string, event) {
    const desc: ArticalDescriptionModel = new ArticalDescriptionModel();
    if (type == 'NFIMG') {
      desc.type = type;
      desc.status = event.target.files[0];
      this.dataList.push(desc);
      this.uploadImage(desc.status, this.dataList.indexOf(desc));
    } else {
      desc.type = type;
      this.dataList.push(desc);
    }
    this.clickActionBar();
    if (this.dataList[0]) {
      this.isHasItem = true;
    } else { this.isHasItem = false; }
  }

  addNewTextField() {
    const desc: ArticalDescriptionModel = new ArticalDescriptionModel();
    desc.type = 'NFTXT';
    desc.value = this.paragraphText;
    this.dataList.push(desc);
    this.paragraphText = '';
    if (!this.dataList[0]) {
      this.isHasItem = true;
    } else { this.isHasItem = false; }
  }

  removeItem(index: number) {
    this.dataList.splice(index, 1);
    if (!this.dataList[0]) {
      this.isHasItem = true;
    } else { this.isHasItem = false; }
  }

  uploadImage(file: File, index: number) {
    this.progressBar = '';
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.dataList[index].sequence = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.dataList[index].imageKey = body.data.object.key;
        this.dataList[index].value = body.data.object.url;
      }
    });
  }
  postArtical() {
    if (this.author.valid && this.heading.valid && this.addCategoryList[0] && this.dataList[0]) {
      this.finalDataList = new Array();
      const article: ArticalModel = new ArticalModel();
      article.category = this.addCategoryList;
      article.title = this.heading.value;
      article.authorName = this.author.value;
      this.dataList.forEach((data, index) => {
        const model: ArticalDescriptionModel = new ArticalDescriptionModel();
        if (data.type == 'NFIMG') {
          model.type = data.type;
          model.value = data.imageKey;
          model.order = index + '';
          this.finalDataList.push(model);
        } else {
          model.type = data.type;
          model.value = data.value;
          model.order = index + '';
          this.finalDataList.push(model);
        }
      });
      article.description = this.finalDataList;
      this.service.postArticle(article).subscribe(resp => {
        this.notify.isMessageDisplayed = true;
        this.notify.messageContent = 'Article published successfully';
        this.closeDialog();
      }, error => {
        console.log(error);
        this.notify.isMessageDisplayed = true;
        this.notify.messageContent = 'Faild';
        this.notify.messageType = 'error';
      });
      console.log('publish article clicked');
    } else {
      this.notify.isMessageDisplayed = true;
        this.notify.messageContent = 'please check values';
        this.notify.messageType = 'error';
    }
    this.validate();
  }

  getErrorMessage(value: FormControl) {
    return value.hasError('required') ? 'You must enter a value' :
      value.hasError('email') ? 'Not a valid email' : '';
  }

  closeDialog() {
    this.dialogRef.close();
  }

  validate() {
    if (!this.addCategoryList[0]) {
      this.isHasCategory = true;
    } else { this.isHasCategory = false; }

    if (!this.dataList[0]) {
      this.isHasItem = true;
    } else { this.isHasItem = false; }

    if (this.author.value === '') {
      this.isValidAuthor = true;
    } else { this.isValidAuthor = false; }

    if (this.heading.value === '') {
      this.isValidTitle = true;
    } else { this.isValidTitle = false; }
  }

  onSetAuthor() {
    if (this.author.value === '') {
      this.isValidAuthor = true;
    } else { this.isValidAuthor = false; }
  }
  onSetTitle() {
    if (this.heading.value === '') {
      this.isValidTitle = true;
    } else { this.isValidTitle = false; }
  }
}
