import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsEditArticleComponent } from './news-edit-article.component';

describe('NewsEditArticalComponent', () => {
  let component: NewsEditArticleComponent;
  let fixture: ComponentFixture<NewsEditArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsEditArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsEditArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
