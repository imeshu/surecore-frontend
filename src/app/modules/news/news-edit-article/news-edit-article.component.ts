import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { Category } from '../../market-price/models/category';
import { ArticalDescriptionModel } from '../models/artical-description';
import { NewsService } from '../services/news.service';
import { ArticalModel } from '../models/artical';
import { ArticalCategoryModel } from '../models/artical-category';
import { HttpEventType } from '@angular/common/http';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { NewsViewArticalComponent } from '../news-view-article/news-view-article.component';
import { NewsPublishArticleComponent } from '../news-publish-article/news-publish-article.component';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { DialogDeleteComponent } from './dialog-delete/dialog-delete.component';

@Component({
  selector: 'app-news-edit-article',
  templateUrl: './news-edit-article.component.html',
  styleUrls: ['./news-edit-article.component.scss']
})
export class NewsEditArticleComponent implements OnInit {
  @Input('artical') private article: ArticalModel;
  @Output() isEditClicked = new EventEmitter();
  private categories: Category[];
  private addCategoryMap = new Map();
  private paragraphText: string;
  private progressBar: string;
  private finalDataList: ArticalDescriptionModel[] = new Array();
  private isDataAdded: boolean;
  private IsActionBarClicked: boolean;
  // service params
  public author: FormControl = new FormControl('', [Validators.required]);
  public heading: FormControl = new FormControl('', [Validators.required]);
  private addCategoryList: ArticalCategoryModel[] = new Array();
  private dataList: ArticalDescriptionModel[] = new Array();

  constructor(private service: NewsService,
    public dialogRef: MatDialogRef<NewsViewArticalComponent>,
    public dialogRefMain: MatDialogRef<NewsPublishArticleComponent>,
    private notify: NotificationDataTransferService,
    public deleteDialog: MatDialog) { }

  ngOnInit() {
    this.getCategories();
    this.addCategories()
    this.author.setValue(this.article.authorName);
    this.heading.setValue(this.article.title);
    this.dataList = this.article.description;
  }
  // ---- new methotsd
  clickActionBar() {
    this.IsActionBarClicked = !this.IsActionBarClicked;
  }
  getCategories() {
    this.service.getCategories().subscribe(resp => {
      const body = resp.body as any;
      this.categories = body.data;
    });
  }
  addCategories() {
    this.article.category.forEach(data => {
      this.addCategoryMap.set(data.code, data.description);
    });
    this.addCategoryList = this.article.category;
  }
  CheckisEditClicked() {
    this.isEditClicked.emit(false);
  }
  // --------------------------

  addCategory(category: Category) {
    const list: ArticalCategoryModel[] = new Array();
    if (!this.addCategoryMap.has(category.code)) {
      this.addCategoryMap.set(category.code, category.description)
    }
    this.addCategoryMap.forEach(function (value, key) {
      const category: ArticalCategoryModel = new ArticalCategoryModel();
      category.code = key;
      category.description = value;
      list.push(category);
    });
    this.addCategoryList = list;
  }

  reomveItemFromaddCategoryMap(key: string) {
    this.addCategoryMap.delete(key);
    const list: ArticalCategoryModel[] = new Array();
    this.addCategoryMap.forEach(function (value, key) {
      let category: ArticalCategoryModel = new ArticalCategoryModel();
      category.code = key;
      category.description = value;
      list.push(category);
    });
    this.addCategoryList = list;
  }

  addDataItem(type: string, event) {
    const desc: ArticalDescriptionModel = new ArticalDescriptionModel();
    if (type == 'NFIMG') {
      desc.type = type;
      desc.status = event.target.files[0];
      this.dataList.push(desc);
      this.uploadImage(desc.status, this.dataList.indexOf(desc));
    } else {
      desc.type = type;
      this.dataList.push(desc);
    }
    this.clickActionBar();
  }

  addNewTextField() {
    const desc: ArticalDescriptionModel = new ArticalDescriptionModel();
    desc.type = 'NFTXT';
    desc.value = this.paragraphText;
    this.dataList.push(desc);
    this.paragraphText = '';
  }

  removeItem(index: number) {
    this.dataList.splice(index, 1);
  }

  uploadImage(file: File, index: number) {
    this.progressBar = '';
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.dataList[index].sequence = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.dataList[index].imageKey = body.data.object.key;
        this.dataList[index].value = body.data.object.url;
      }
    });
  }
  putArticle() {
    if (this.author.valid && this.heading.valid && this.addCategoryList[0] && this.dataList[0]) {
      this.finalDataList = new Array();
      this.article.category = this.addCategoryList;
      this.article.authorName = this.author.value;
      this.article.title = this.heading.value;
      this.dataList.forEach((data, index) => {
        const model: ArticalDescriptionModel = new ArticalDescriptionModel();
        if (data.type == 'NFIMG') {
          model.type = data.type;
          model.value = data.imageKey;
          model.order = index + '';
          this.finalDataList.push(model);
        } else {
          model.type = data.type;
          model.value = data.value;
          model.order = index + '';
          this.finalDataList.push(model);
        }
      });
      this.article.description = this.finalDataList;
      this.service.putArticle(this.article).subscribe(resp => {
        this.notify.isMessageDisplayed = true;
        this.notify.messageContent = 'Article has been updated now please have a look.';
      }, error => {
        console.log(error);
      });
      console.log('publish article clicked');
    } else {
      this.notify.isMessageDisplayed = true;
      this.notify.messageContent = 'Please check values';
      this.notify.messageType = 'error';
    }

  }
  closeDialog() {
    this.dialogRef.close();
  }
  deleteArticle() {
    this.service.deleteArticle(this.article.id).subscribe(resp => {
      this.notify.isMessageDisplayed = true;
      this.notify.messageContent = 'Article has been deleted now please have a look.';
      this.closeDialog();
    }, error => {
      this.notify.isMessageDisplayed = true;
      this.notify.messageContent = 'Delete Faild';
    });
  }
  // validatoin
  getErrorMessage(value: FormControl) {
    return value.hasError('required') ? 'You must enter a value' :
      value.hasError('email') ? 'Not a valid email' : '';
  }

  openDialogDelete() {
    const deleteDialogRef = this.deleteDialog.open(DialogDeleteComponent);
    deleteDialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteArticle();
      }
    });
  }
}
