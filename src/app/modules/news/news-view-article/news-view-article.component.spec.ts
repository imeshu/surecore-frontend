import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsViewArticalComponent } from './news-view-article.component';

describe('NewsViewArticalComponent', () => {
  let component: NewsViewArticalComponent;
  let fixture: ComponentFixture<NewsViewArticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsViewArticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsViewArticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
