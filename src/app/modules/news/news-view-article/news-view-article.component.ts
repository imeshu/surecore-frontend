import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { ArticalModel } from '../models/artical';
import { ArticalDescriptionModel } from '../models/artical-description';
import { NewsService } from '../services/news.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-news-view-article',
  templateUrl: './news-view-article.component.html',
  styleUrls: ['./news-view-article.component.scss']
})
export class NewsViewArticalComponent implements OnInit {
  private artical: ArticalModel = new ArticalModel();
  private isDataLoaded: boolean;
  public isEditClicked: boolean;
  private descriptionsList: ArticalDescriptionModel[] = new Array();
  private isUserMached: boolean;

  constructor(private service: NewsService,
    public dialogRef: MatDialogRef<NewsViewArticalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit() {
    this.getArticle(this.data);
  }

  getArticle(id: string) {
    this.service.getArtical(id).subscribe(resp => {
      const body = resp.body as any;
      this.artical = body.data;
      this.descriptionsList = this.artical.description;
      // sort by order
      this.descriptionsList.sort(function (a, b) { return parseInt(a.order) - parseInt(b.order) });
      this.isDataLoaded = true;
      this.isUserMached = sessionStorage.getItem('partyCode') === this.artical.publisherCode;
      console.log(this.isUserMached);
    }, error => {
      console.log(error);
    });
  }
  output(event) {
    this.isEditClicked = !this.isEditClicked;
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
