import { Injectable } from '@angular/core';
import { HTTPCallService } from 'src/app/services/httpcall.service';
import { HttpResponse, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetNewsModel } from '../models/get-news';
import { ArticalModel } from '../models/artical';
import {fileServer, RECENTLY_LISTED} from 'src/app/config/enum';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HTTPCallService) { }

  getCategories(): Observable<HttpResponse<Object>> {
    const path = 'base/newsfeed/categories';
    return this.http.getResource(path);
  }

  getNews(model: GetNewsModel): Observable<HttpResponse<Object>> {
    const path = 'newsfeed/articles/paginated_search?categoryCode=' + model.categoryCode +
      '&articleTitle=' + model.articleTitle +
      '&recentlyPublishedFlag=' + model.recentlyPublishedFlag +
      '&publishedByMe=' + model.publishedByMe +
      '&page=' + model.page +
      '&size=' + model.size + '';

    const path2 = 'newsfeed/articles/paginated_search/portal?categoryCode=' + model.categoryCode +
      '&articleTitle=' + model.articleTitle +
      '&numberOfDays=' + (model.recentlyPublishedFlag ? RECENTLY_LISTED : '') +
      '&publishedByMe=' + model.publishedByMe +
      '&page=' + model.page +
      '&size=' + model.size + '';
    return this.http.getResource(path2);
  }
  getArtical(id: string): Observable<HttpResponse<Object>> {
    const path = 'newsfeed/articles/' + id;
    return this.http.getResource(path);
  }
  uploadImage(file: File): Observable<HttpEvent<Object>> {
    const fd = new FormData();
    fd.append('file', file);
    const path = fileServer + 'v1/files/upload?path=newsfeed';
    return this.http.postImage(path, fd);
  }
  postArticle(artical: ArticalModel): Observable<HttpResponse<Object>> {
    const path = 'newsfeed/articles';
    return this.http.postResource(path, artical);
  }


  deleteArticle(id: string): Observable<HttpResponse<Object>> {
    const path = 'newsfeed/articles/' + id;
    return this.http.deleteResource(path);
  }
  putArticle(artical: ArticalModel) {
    const path = 'newsfeed/articles/' + artical.id;
    return this.http.putResource(path, artical);
  }

}
