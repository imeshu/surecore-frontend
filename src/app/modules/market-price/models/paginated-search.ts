export class PaginatedSearch {
    page: string;
    size: string;
    categoryCode: string;
    marketCode: string;
    date: string;

    constructor(page: string, size: string, categoryCode: string, marketCode: string, date: string) {
        this.page = page;
        this.size = size;
        this.categoryCode = categoryCode;
        this.marketCode = marketCode;
        this.date = date;
    }
}