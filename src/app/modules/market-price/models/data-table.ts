export class DataTable {
  categoryCode: string;
  subCategoryCode: string;
  subCategoryDescription: string;
  imagePath: string;

  marketCode1: string;
  retailUnit1: string;
  retailPrice1: string;
  wholesaleUnit1: string;
  wholesalePrice1: string;

  marketCode2: string;
  retailUnit2: string;
  retailPrice2: string;
  wholesaleUnit2: string;
  wholesalePrice2: string;

  marketCode3: string;
  retailUnit3: string;
  retailPrice3: string;
  wholesaleUnit3: string;
  wholesalePrice3: string;
}
