export class SearchDataModel {
  categoryCode: string;
  categoryDescription: string;
  marketCode: string;
  marketDescription: string;
  retailUnit: string;
  retailPrice: string;
  wholesaleUnit: string;
  wholesalePrice: string;
  subCategoryCode: string;
  subCategoryDescription: string;
  imagePath: string;
  masterId: string;
  detailId: string;
}
