import {Component, OnInit} from '@angular/core';
import {MarketPriceService} from '../services/market-price.service';
import {Category} from '../models/category';
import {Market} from '../models/market';
import {SearchDataModel} from '../models/searchData';
import {PaginatedSearch} from '../models/paginated-search';
import {DataTable} from '../models/data-table';
import {SubCategory} from '../models/sub-category';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-market-price-main',
  templateUrl: './market-price-main.component.html',
  styleUrls: ['./market-price-main.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({opacity: 0}),
        animate(500, style({opacity: 1}))
      ])
    ])
  ]
})
export class MarketPriceMainComponent implements OnInit {
  categories: Category[];
  subCategories: SubCategory[];
  markets: Market[];
  allMarketsData: SearchDataModel[];
  filteredMarketData: DataTable[] = [];
  displayMarketData: DataTable[] = [];
  selectedCategory: Category = new Category(null, 'All Categories');
  selectedMarket1: Market = new Market(null, 'Select Market');
  selectedMarket2: Market = new Market(null, 'Select Market');
  selectedMarket3: Market = new Market(null, 'Select Market');
  isDataLoaded = false;
  isMarket1Active = false;
  isMarket2Active = false;
  isMarket3Active = false;
  isMarket1Set = false;
  isMarket2Set = false;
  isMarket3Set = false;
  searchInput = '';

  constructor(private service: MarketPriceService) {
  }

  ngOnInit() {
    this.getCategories();
    this.getMarkets();
    this.getSubCategories();
    this.getAllMarketData();
  }

  checkMarket1() {
    this.isMarket1Active = !this.isMarket1Active;
    if (!this.isMarket1Active) {
      this.isMarket2Active = false;
      this.isMarket3Active = false;
      this.displayMarketData = [];
      this.selectedMarket1.code = null;
      this.selectedMarket1.description = 'Select Market';
    } else {
    }
    this.isMarket1Set = false;
    if (this.isMarket1Set) {
      this.filterData2();
    }
  }

  checkMarket2() {
    this.isMarket2Active = !this.isMarket2Active;
    if (!this.isMarket2Active) {
      this.isMarket3Active = false;
      this.selectedMarket2.code = null;
      this.selectedMarket2.description = 'Select Market';
    }
    this.isMarket2Set = false;
    if (this.isMarket2Set) {
      this.filterData2();
    }
  }

  checkMarket3() {
    this.isMarket3Active = !this.isMarket3Active;
    this.isMarket3Set = false;
    if (!this.isMarket3Active) {
      this.selectedMarket3.code = null;
      this.selectedMarket3.description = 'Select Market';
    }
    if (this.isMarket3Set) {
      this.filterData2();
    }
  }

  getCategories() {
    this.service.getCategories().subscribe(resp => {
      const body = resp.body as any;
      this.categories = body.data;
    });
  }

  setCategory(code, description) {
    if (code == null) {
      this.selectedCategory.code = '';
      this.selectedCategory.description = description;
    }
    this.selectedCategory.code = code;
    this.selectedCategory.description = description;
    this.filterByCategory(this.selectedCategory.code);
  }

  getSubCategories() {
    this.service.getSubCategories(this.selectedCategory.code).subscribe(resp => {
      const body = resp.body as any;
      this.subCategories = body.data;
    });
  }

  getMarkets() {
    this.service.getMarkets().subscribe(resp => {
      const body = resp.body as any;
      this.markets = body.data;
    });
  }

  setMarket1(code, description) {
    this.selectedMarket1.code = code;
    this.selectedMarket1.description = description;
    this.isMarket1Set = true;
    this.filterData2();
  }

  setMarket2(code, description) {
    this.selectedMarket2.code = code;
    this.selectedMarket2.description = description;
    this.isMarket2Set = true;
    this.filterData2();
  }

  setMarket3(code, description) {
    this.selectedMarket3.code = code;
    this.selectedMarket3.description = description;
    this.isMarket3Set = true;
    this.filterData2();
  }

  getAllMarketData() {
    const data: PaginatedSearch = new PaginatedSearch('1', '500', '', '', '');
    this.service.serch(data).subscribe(resp => {
      const body = resp.body as any;
      this.allMarketsData = body.data;
      this.isDataLoaded = true;
    });
  }

  filterData2() {
    if (this.isMarket1Active) {
      this.filteredMarketData = [];
      this.subCategories.forEach(sc => {
        const model: DataTable = new DataTable();
        model.subCategoryDescription = sc.description;
        model.subCategoryCode = sc.code;

        this.allMarketsData.forEach(md => {

          if (this.selectedMarket1.code === md.marketCode) {
            model.marketCode1 = md.marketCode;
            if (sc.code === md.subCategoryCode) {
              if (!model.imagePath) {
                model.imagePath = md.imagePath;
              }
              if (!model.categoryCode) {
                model.categoryCode = md.categoryCode;
              }
              if ('' + md.retailPrice === '0') {
                model.retailPrice1 = 'n/a';
              } else {
                model.retailPrice1 = md.retailPrice;
                model.retailUnit1 = md.retailUnit;
              }
              if ('' + md.wholesalePrice === '0') {
                model.wholesalePrice1 = 'n/a';
              } else {
                model.wholesalePrice1 = md.wholesalePrice;
                model.wholesaleUnit1 = md.wholesaleUnit;
              }
            }
          }

          if (this.isMarket2Active) {
            if (this.selectedMarket2.code === md.marketCode) {
              model.marketCode2 = md.marketCode;
              if (sc.code === md.subCategoryCode) {
                if (!model.imagePath) {
                  model.imagePath = md.imagePath;
                }
                if (!model.categoryCode) {
                  model.categoryCode = md.categoryCode;
                }
                if ('' + md.retailPrice === '0') {
                  model.retailPrice2 = 'n/a';
                } else {
                  model.retailPrice2 = md.retailPrice;
                  model.retailUnit2 = md.retailUnit;
                }
                if ('' + md.wholesalePrice === '0') {
                  model.wholesalePrice2 = 'n/a';
                } else {
                  model.wholesalePrice2 = md.wholesalePrice;
                  model.wholesaleUnit2 = md.wholesaleUnit;
                }
              }
            }
          }

          if (this.isMarket3Active) {
            if (this.selectedMarket3.code === md.marketCode) {
              model.marketCode3 = md.marketCode;
              if (sc.code === md.subCategoryCode) {
                if (!model.imagePath) {
                  model.imagePath = md.imagePath;
                }
                if (!model.categoryCode) {
                  model.categoryCode = md.categoryCode;
                }
                if ('' + md.retailPrice === '0') {
                  model.retailPrice3 = 'n/a';
                } else {
                  model.retailPrice3 = md.retailPrice;
                  model.retailUnit3 = md.retailUnit;
                }
                if ('' + md.wholesalePrice === '0') {
                  model.wholesalePrice3 = 'n/a';
                } else {
                  model.wholesalePrice3 = md.wholesalePrice;
                  model.wholesaleUnit3 = md.wholesaleUnit;
                }
              }
            }
          }
        });

        if (model.wholesalePrice1 || model.wholesalePrice2 || model.wholesalePrice3) {
          this.filteredMarketData.push(model);
        }
      });
      this.filterByCategory(this.selectedCategory.code);
    }
  }

  filterByCategory(code) {
    this.displayMarketData = [];
    if (code == null) {
      this.displayMarketData = this.filteredMarketData;
    } else {
      this.displayMarketData = [];
      this.displayMarketData = this.filteredMarketData.filter(function (data) {
        return data.categoryCode === code;
      });
    }
    this.searchByName();
  }

  searchByName() {
    if (this.isMarket1Active && this.isMarket1Set) {
      this.displayMarketData = [];
      this.subCategories.forEach(sd => {
        if (sd.description.match(this.searchInput.toUpperCase())) {
          this.filteredMarketData.forEach(amd => {
            if (this.selectedCategory.code === amd.categoryCode) {
              if (amd.subCategoryCode === sd.code) {
                this.displayMarketData.push(amd);
              }
            } else {
              if (amd.subCategoryCode === sd.code && this.selectedCategory.code == null) {
                this.displayMarketData.push(amd);
              }
            }
          });
        }
      });
    }
  }

  search() {
    this.selectedCategory.code = null;
    this.selectedCategory.description = 'All Categories';
    this.searchByName();
  }
}
