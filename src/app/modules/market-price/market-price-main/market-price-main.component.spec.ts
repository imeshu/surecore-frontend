import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketPriceMainComponent } from './market-price-main.component';

describe('MarketPriceMainComponent', () => {
  let component: MarketPriceMainComponent;
  let fixture: ComponentFixture<MarketPriceMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketPriceMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketPriceMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
