import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarketPriceMainComponent } from './market-price-main/market-price-main.component';

const routes: Routes = [
  {
    path: 'market-price',
    component: MarketPriceMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketPriceRoutingModule { }
