import {Injectable} from '@angular/core';
import {HTTPCallService} from 'src/app/services/httpcall.service';
import {Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {PaginatedSearch} from '../models/paginated-search';

@Injectable({
  providedIn: 'root'
})
export class MarketPriceService {

  constructor(private http: HTTPCallService) {
  }

  getCategories(): Observable<HttpResponse<Object>> {
    const path = 'base/marketplace/marketprice/categories';
    return this.http.getResource(path);
  }

  getSubCategories(category: string): Observable<HttpResponse<Object>> {
    if (category == null) {
      const path = 'base/references?type=MPPSC';
      return this.http.getResource(path);
    } else {
      const path = 'base/marketplace/marketprice/categories/' + category + '/subCategories';
      return this.http.getResource(path);
    }

  }

  getMarkets(): Observable<HttpResponse<Object>> {
    const path = 'base/marketplace/marketprice/markets';
    return this.http.getResource(path);
  }

  serch(data: PaginatedSearch): Observable<HttpResponse<object>> {
    if (data.categoryCode == null) {
      data.categoryCode = '';
    }
    // tslint:disable-next-line:max-line-length
    const path = 'marketplace/marketprices/paginated_search?page=' + data.page + '&size=' + data.size + '0&categoryCode=' + data.categoryCode + '&marketCode=' + data.marketCode + '&date=' + data.date;
    return this.http.getResource(path);
  }
}
