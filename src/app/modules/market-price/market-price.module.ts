import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketPriceRoutingModule } from './market-price-routing.module';
import { MarketPriceMainComponent } from './market-price-main/market-price-main.component';
import { urlDecoder } from 'src/app/pipes/urlDecoder';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomPipeModule } from 'src/app/pipes/CustomPipesModule';
import { FormsModule } from '@angular/forms';
import { CommonComponentsModule } from '../common-components/common-components.module';

@NgModule({
  declarations: [MarketPriceMainComponent],
  imports: [
    FormsModule,
    CommonModule,
    MarketPriceRoutingModule,
    BrowserAnimationsModule,
    CustomPipeModule,
    CommonComponentsModule
  ],
  exports: [MarketPriceMainComponent]
})
export class MarketPriceModule { }
