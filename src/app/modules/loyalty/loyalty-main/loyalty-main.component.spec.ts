import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyaltyMainComponent } from './loyalty-main.component';

describe('LoyaltyMainComponent', () => {
  let component: LoyaltyMainComponent;
  let fixture: ComponentFixture<LoyaltyMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoyaltyMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoyaltyMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
