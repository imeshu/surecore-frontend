import {Component, OnInit} from '@angular/core';
import {LoyaltyService} from "../services/loyalty.service";
import * as moment from 'moment';
import {LoyaltyModel} from "../models/loyalty-model";
import {forEach} from "@angular/router/src/utils/collection";
import {CustomerModel} from "../../customer/models/customer-model";


@Component({
  selector: 'app-loyalty-main',
  templateUrl: './loyalty-main.component.html',
  styleUrls: ['./loyalty-main.component.scss']
})
export class LoyaltyMainComponent implements OnInit {

  private dataList: LoyaltyModel[] = new Array();
  private earned: boolean = false;
  private redeemed: boolean = false;
  private type: string;
  private point: number;

  private startdate = null;
  private end = null;
  private date = 'all';

  // infinity scroll
  private throttle = 300;
  private scrollDistance = 1;
  private scrollUpDistance = 2;
  private fromRoot = true;

  private page: number = 1;
  private pageSize: number = 80;
  private numberOfPages: number;
  private searchValue: string = '';


  constructor(private service: LoyaltyService) {
    this.end = '';
    this.startdate = '';

  }

  ngOnInit() {
    this.getLoyalty();
    this.getPoint()
  }

  getLoyalty() {

    if (!this.redeemed && !this.earned) {
      this.type = 'A';
    }
    if (this.earned && !this.redeemed) {
      this.type = 'E';
    }
    if (this.redeemed && !this.earned) {
      this.type = 'R';
    }
    if (this.earned && this.redeemed) {
      this.type = 'A';
    }

    this.service.getLoyalty(this.type, this.startdate, this.end, this.page, this.pageSize).subscribe(resp => {
      const model = resp.body as any;
      this.dataList = model.data.loyaltyList;
      this.dataList.forEach((item, index) => {
        if (0 > item.point) {
          this.dataList[index].isMinus = true;
          this.dataList[index].point = Math.abs(item.point);

        }
      })
    })

  }

  getEarned() {
    this.getLoyalty();
  }

  getRedeemed() {
    this.getLoyalty();
  }

  getPoint() {
    this.service.getPoint().subscribe(resp => {
      const model = resp.body as any;
      this.point = model.data;
      // console.log(this.point)
    })
  }

  // DATE

  onSetDate() {
    this.end = moment().format('YYYY-MM-DD');

    if (this.date == '3months') {
      this.startdate = moment().subtract(3, 'months').format('YYYY-MM-DD');
      // console.log(this.startdate);
    }
    if (this.date == '6months') {
      this.startdate = moment().subtract(6, "month").format('YYYY-MM-DD');
      // console.log(this.startdate);
    }
    if (this.date == '1year') {
      this.startdate = moment().subtract(1, "year").format('YYYY-MM-DD');
      // console.log(this.startdate);
    }
    if (this.date == 'all') {
      this.startdate = '';
      this.end = '';
    }


    // console.log(this.enddate);


    console.log(this.startdate);
    console.log(this.end);
    this.getLoyalty();
  }

  // onScroll

  onScrollDown(event) {
    console.log('scroll down');
    if (this.numberOfPages >= this.page) {
      this.page++;
      this.service.getLoyalty(this.type, this.startdate, this.end, this.page, this.pageSize).subscribe(resp => {
        const body = resp.body as any;
        const list: LoyaltyModel[] = body.data.partyProfile;

        list.forEach(item => {
          this.dataList.push(item);
        });
      });
    }


  }

}
