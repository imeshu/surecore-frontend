import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoyaltyRoutingModule } from './loyalty-routing.module';
import { LoyaltyMainComponent } from './loyalty-main/loyalty-main.component';
import {CommonComponentsModule} from "../common-components/common-components.module";
import {MatCheckboxModule, MatFormFieldModule, MatSelectModule} from "@angular/material";
import {FormsModule} from "@angular/forms";
import {InfiniteScrollModule} from "ngx-infinite-scroll";

@NgModule({
  declarations: [LoyaltyMainComponent],
  imports: [
    CommonModule,
    LoyaltyRoutingModule,
    CommonComponentsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    InfiniteScrollModule
  ]
})

export class LoyaltyModule { }
