export class LoyaltyModel {

  balance: string;
  datetime: string;
  partyCode: string;
  partyName: string;
  point: number;
  referenceNumber: string;
  referenceSequence: string;
  sequence: string;
  transactionCode: string;
  transactionDescription: string;
  type: string;
  value: string;
  isMinus: boolean;


}

