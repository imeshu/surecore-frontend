import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpResponse} from "@angular/common/http";
import {HTTPCallService} from "../../../services/httpcall.service";

@Injectable({
  providedIn: 'root'
})
export class LoyaltyService {

  constructor(private http: HTTPCallService) { }



  getLoyalty(type:string, startdate:string , enddate:string ,page, size): Observable<HttpResponse<Object>> {
    console.log(sessionStorage.getItem('partyCode'));
    const path = 'base/loyaltyPoints/paginated_search?type='+ type +'&partyCode='+ sessionStorage.getItem('partyCode') +'&transactionCode=&startDate='+ startdate +'&endDate=' +enddate+ '&page=' + page + '&size=' + size + ''
    // const path = 'base/loyaltyPoints/search?type='+ type +'&partyCode=CC00000024&transactionCode=&startDate='+ startdate +'&endDate=' +enddate+ '&page=1&size=4';
    return this.http.getResource(path);
  }

  getPoint(): Observable<HttpResponse<Object>> {
    const path = 'base/loyaltyPoints/balance?partyCode=' + sessionStorage.getItem('partyCode');
    return this.http.getResource(path);
  }

}




