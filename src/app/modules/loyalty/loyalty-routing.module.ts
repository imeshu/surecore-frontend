import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoyaltyMainComponent} from "./loyalty-main/loyalty-main.component";

const routes: Routes = [
  {
    path:'loyalty' ,
    component: LoyaltyMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoyaltyRoutingModule { }
