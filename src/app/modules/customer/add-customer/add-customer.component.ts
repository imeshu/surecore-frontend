import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomerService} from '../services/customer.service';
import {ContactModel, CustomerModel, IdentificationModel} from '../models/customer-model';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {

  customerForm = new FormGroup({
    type: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    nic: new FormControl('', [Validators.required, Validators.pattern('^([0-9]{9}[x|X|v|V]|[0-9]{12})$')]),
    address: new FormControl('', Validators.required),
    // tslint:disable-next-line:max-line-length
    mobile: new FormControl('', [Validators.required, Validators.pattern('^(?:0|94|\\+94|0094)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|91)(0|2|3|4|5|7|9)|7(0|1|2|5|6|7|8)\\d)\\d{6}$')]),
    // tslint:disable-next-line:max-line-length
    email: new FormControl('', [Validators.pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')]),
    gender: new FormControl(''),
  });

  constructor(private service: CustomerService, private msg: NotificationDataTransferService, private router: Router) {
  }

  ngOnInit() {
  }


  postCustomer() {
    const data: CustomerModel = new CustomerModel();
// profile
    data.party.name = this.customerForm.value.name;
    data.party.gender = this.customerForm.value.gender;
    if (this.customerForm.value.type === 'C') {
      data.party.partyAsCustomer = 'Y';
    } else {
      data.party.partyAsProspect = 'Y';
    }
// address
    data.address.address = this.customerForm.value.address;
    data.address.addressType = 'ADHOM';
    data.address.defaultAddressFlag = 'Y';
// contact
    const contactList: ContactModel[] = [];
    const email: ContactModel = new ContactModel();
    email.contactType = 'CEMIL';
    email.contactNumber = this.customerForm.value.email;
    email.default = true;
    contactList.push(email);

    const mobile: ContactModel = new ContactModel();
    mobile.contactType = 'CMOBL';
    mobile.contactNumber = this.customerForm.value.mobile;
    mobile.default = true;
    contactList.push(mobile);
    data.contact = contactList;

    const identification: IdentificationModel = new IdentificationModel();
    identification.registrationNumber = this.customerForm.value.nic;
    identification.registrationType = 'NIDN';
    data.identification.push(identification);
    this.service.postCustomer(data).subscribe(() => {
      this.msg.success('Success');
      this.router.navigate(['/customers']);
    }, error1 => {
      this.msg.error('Added failed');
      throw error1;
    });

  }
}
