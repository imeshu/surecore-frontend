export class PolicyModel {
  productName: string;
  proposalNumber: string;
  policyStatus: string;
  policyNumber: string;
}
