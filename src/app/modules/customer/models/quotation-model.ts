export class QuotationModel {
  quotationNumber: string;
  productDescription: string;
  quotationStatus: string;
}
