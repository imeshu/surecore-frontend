export class CustomerModel {
  party: PartyModel = new PartyModel();
  profile: ProfileModel = new ProfileModel();
  image: string;
  address: AddressModel = new AddressModel();
  contact: ContactModel[] = new Array();
  identification: IdentificationModel[] = new Array();
  contactPerson: string;
  phone: ContactModel;
  email: ContactModel;
  nic: IdentificationModel = new IdentificationModel();
  name: string;
  gender: string;
  languageCode: string;
  dateOfBirth: Date;
  maritalStatus: string;
  active: boolean;
}

export class ProfileModel {
  code: string;
  partyAsClaimAssessor: string;
  partyAsCustomer: string;
  partyAsInternalStaff: string;
  partyAsIntermediary: string;
  partyAsReinsurer: string;
  partyAsServiceMedical: string;
  partyAsServiceMotor: string;
  partyAsServiceOther: string;
  partyAsUwAssessor: string;
  dateOfBirth: string;
  gender: string;
  individualCorporateFlag: string;
  internalExternalFlag: string;
  joinedDate: string;
  maritalStatus: string;
  name: string;
  nationalityd: string;
  referenceNumber: string;
  status: string;
  title: string;
  claimAssessorType: string;
  customerTyped: string;
  internalStaffType: string;
  intermediaryType: string;
  reinsurerType: string;
  serviceMedicalType: string;
  serviceMotorType: string;
  serviceOtherType: string;
  uwAssessorType: string;
  designation: string;
  designationEffectDate: string;
  languageCode: string;
  twoFactorAuthEnabledFlag: string;
  twoFactorAuthEnbledPhoneNumber: string;
  attachedInstituteEffectDate: string;
  attachLocationEffectDate: string;
  attachedInstituteCode: string;
  attachedLocationCode: string;
  loginName: string;
  reportingCode: string;
  reportingEffectDate: string;
  intermediaryReferenceNumber: string;
  partyAsProspect: string;
  prospectType: string;
  internalStaffReferenceNumber: string;
  customerReferenceNumber: string;
  uwAssessorReferenceNumber: string;
  claimAssessorReferenceNumber: string;
  reinsurerReferenceNumber: string;
  serviceMotorReferenceNumber: string;
  serviceMedicalReferenceNumber: string;
  serviceOtherReferenceNumber: string;
  prospectReferenceNumber: string;
  partyAsFinancialInterest: string;
  financialInterestType: string;
  financialInterestReferenceNumber: string;
  level: string;
  reportingSequence: string;
  version: string;
  genderDescription: string;
  maritalStatusDescription: string;
  nationalityDescription: string;
  titleDescription: string;
  claimAssessorTypeDescription: string;
  customerTypeDescription: string;
  internalStaffTypeDescription: string;
  intermediaryTypeDescription: string;
  reinsurerTypeDescription: string;
  serviceMedicalTypeDescription: string;
  serviceMotorTypeDescription: string;
  serviceOtherTypeDescription: string;
  uwAssessorTypeDescription: string;
  prospectTypeDescription: string;
  financialInterestTypeDescription: string;
  designationDescription: string;
  attachedInstituteDescription: string;
  attachedLocationDescription: string;
  reportingDescription: string;
  languageDescription: string;
  partyPoint: string;
  lastUpdatedDate: string;
  locationCoordiates: string;

}

export class ContactModel {
  contactType: string;
  contactNumber: string;
  status: string;
  sequence: string;
  version: string;
  contactTypeDescription: string;
  default: boolean;
}

export class IdentificationModel {
  issueingAuthority: string;
  registrationNumber: string;
  registrationType: string;
  status: string;
  validFromDate: string;
  validToDate: string;
  sequence: string;
  version: string;
  registrationTypeDescription: string;
}

export class AddressModel {
  sequence: string;
  address: string;
  addressType: string;
  defaultAddressFlag: string;
  geometry: string;
  status: string;
  version: string;
  addressTypeDescription: string;
}

class PartyModel {
  name: string;
  gender: string;
  partyAsCustomer: string;
  partyAsProspect: string;
}
