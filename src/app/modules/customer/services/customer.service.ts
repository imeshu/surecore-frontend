import {Injectable} from '@angular/core';
import {HTTPCallService} from 'src/app/services/httpcall.service';
import {HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CustomerModel} from '../models/customer-model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(public http: HTTPCallService) {
  }

  getCustomers(type: string, searchValue: string, page: number, size: number): Observable<HttpResponse<Object>> {
    if (type === 'nic') {
      const path = 'base/parties/customers/search/profiles?name=&page=' + page + '&size=' + size + '&nicNumber=' + searchValue;
      return this.http.getResource(path);
    } else {
      const path = 'base/parties/customers/search/profiles?name=' + searchValue +
        '&page=' + page + '&size=' + size + '&nicNumber=';
      return this.http.getResource(path);
    }
  }

  getCustomerDetails(customerCode): Observable<HttpResponse<Object>> {
    const path = 'base/parties/' + customerCode + '/profiles';
    return this.http.getResource(path);
  }

  getCustomerLands(customerCode): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/search?partyCode=' + customerCode;
    return this.http.getResource(path);
  }

  getCustomerPolicies(partyCode, customerCode): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/search?branchCode=&classCode=&productCode=&policyNumber=' +
    '&proposalNumber=&customerName=&riskName=&customerNIC=&status=A&salesPersonCode=' + partyCode + '&customerCode=' + customerCode;
    return this.http.getResource(path);
  }


  getGenderTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=GENDR';
    return this.http.getResource(path);
  }

  updateCustomer(customerCode, data): Observable<HttpResponse<Object>> {
    const path = 'base/parties/' + customerCode + '/profiles/summary';
    return this.http.putResource(path, data);
  }

  postCustomer(data: CustomerModel) {
    const path = 'base/parties/profiles/summary';
    return this.http.postResource(path, data);
  }

  getPolicyByCustomer(code: string, salesPersonCode: string): Observable<HttpResponse<Object>> {
    let path = '';
    if (salesPersonCode) {
      path = 'uw/policies/search?branchCode=&classCode=&productCode=&policyNumber=&proposalNumber=' +
      '&customerName=&riskName=&customerNIC=&status=&salesPersonCode= ' + salesPersonCode + ' &customerCode=' + code;
    } else {
      path = 'uw/policies/search?branchCode=&classCode=&productCode=&policyNumber=&proposalNumber=' +
      '&customerName=&riskName=&customerNIC=&status=&salesPersonCode=&customerCode=' + code;
    }

    return this.http.getResource(path);
  }

  getQuotatinList(customerCode: string): Observable<HttpResponse<Object>> {
    // tslint:disable-next-line:max-line-length
    const path = 'uw/quotations/search?salesPersonCode=&status=A&classCode=MC&productCode=&quotationNumber=&customerName=&riskName=&customerCode=' + customerCode;
    return this.http.getResource(path);
  }

}
