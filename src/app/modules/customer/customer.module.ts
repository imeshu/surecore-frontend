import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomerRoutingModule} from './customer-routing.module';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {CustomPipeModule} from 'src/app/pipes/CustomPipesModule';
import {CommonComponentsModule} from '../common-components/common-components.module';
import {CustomerMainComponent} from './customer-main/customer-main.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {ManageCustomerComponent} from './manage-customer/manage-customer.component';
import {AddCustomerComponent} from './add-customer/add-customer.component';
import {MatSelectModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatExpansionModule} from '@angular/material';
import {LandModule} from '../land/land.module';

@NgModule({
  declarations: [

    CustomerMainComponent,
    ManageCustomerComponent,
    AddCustomerComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    CustomPipeModule,
    InfiniteScrollModule,
    FormsModule,
    ReactiveFormsModule,
    CommonComponentsModule,
    // materials
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatExpansionModule,
    // import custom module
    LandModule
  ],

  exports: []
})
export class CustomerModule {

}
