import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AddressModel, CustomerModel} from '../models/customer-model';
import {CustomerService} from '../services/customer.service';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';

@Component({
  selector: 'app-manage-customer',
  templateUrl: './manage-customer.component.html',
  styleUrls: ['./manage-customer.component.scss']
})
export class ManageCustomerComponent implements OnInit {
  @Output() isManageCustomerClicked = new EventEmitter();
  @Input() private model: CustomerModel = new CustomerModel();
  private address: string;
  customerForm = new FormGroup({
    type: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    nic: new FormControl('', [Validators.required, Validators.pattern('^([0-9]{9}[x|X|v|V]|[0-9]{12})$')]),
    address: new FormControl('', Validators.required),
    // tslint:disable-next-line:max-line-length
    mobile: new FormControl('', [Validators.required, Validators.pattern('^(?:0|94|\\+94|0094)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|91)(0|2|3|4|5|7|9)|7(0|1|2|5|6|7|8)\\d)\\d{6}$')]),
    // tslint:disable-next-line:max-line-length
    email: new FormControl('', [Validators.pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')]),
    gender: new FormControl(''),
  });

  constructor(private service: CustomerService, private notify: NotificationDataTransferService) {

  }

  intForm() {
    if (this.model.profile.partyAsCustomer) {
      if (this.model.profile.partyAsCustomer === 'Y') {
        this.customerForm.controls['type'].setValue('C');
      }
    }
    if (this.model.profile.partyAsProspect) {
      if (this.model.profile.partyAsProspect === 'Y') {
        this.customerForm.controls['type'].setValue('P');
      }
    }
    if (this.address) {
      this.customerForm.controls['address'].setValue(this.address);
    }
    if (this.model.phone) {
      this.customerForm.controls['mobile'].setValue(this.model.phone.contactNumber);
    }

    if (this.model.email) {
      this.customerForm.controls['email'].setValue(this.model.email.contactNumber);
    }
    if (this.model.nic) {
      this.customerForm.controls['nic'].setValue(this.model.nic.registrationNumber);
    }
    if (this.model.profile) {
      this.customerForm.controls['name'].setValue(this.model.profile.name);
      this.customerForm.controls['gender'].setValue(this.model.profile.genderDescription);
    }
    // this.customerForm.controls['type'].setValue(this.model.profile.name);

  }

  ngOnInit() {
    if (this.model.address) {
      this.address = this.model.address.address;
    }
    this.intForm();
  }

  onCancel() {
    this.isManageCustomerClicked.emit();
  }

  postCustomer() {
    const model: CustomerModel = new CustomerModel();
    if (this.customerForm.value.type === 'C') {
      model.party.partyAsCustomer = 'Y';
      model.party.partyAsProspect = null;
    } else {
      model.party.partyAsProspect = 'Y';
      model.party.partyAsCustomer = null;

    }
    model.name = this.customerForm.value.name;
    model.gender = this.customerForm.value.gender;
    model.identification = this.model.identification;
    model.identification.forEach(item => {
      if (item.registrationType === 'NIDN') {
        item.registrationNumber = this.customerForm.value.nic;
      }
    });
    if (this.model.address) {
      model.address = this.model.address;
    } else {
      model.address = new AddressModel();
    }

    model.address.address = this.customerForm.value.address;
    model.contact = this.model.contact;
    model.contact.forEach(item => {
      if (item.contactType === 'CMOBL') {
        item.contactNumber = this.customerForm.value.mobile;
      }
      if (item.contactType === 'CEMIL') {
        item.contactNumber = this.customerForm.value.email;
      }
    });
    this.service.updateCustomer(this.model.profile.code, model).subscribe(() => {
      this.notify.success('Successfully Added Customer');
    }, error => {
      console.log(error);
      this.notify.error('Customer Added Failed');
    });
  }


}
