import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../services/customer.service';
import {CustomerModel} from '../models/customer-model';
import {LandModel} from '../models/land-model';
import {PolicyModel} from '../models/policy-model';
import {QuotationModel} from '../models/quotation-model';

@Component({
  selector: 'app-customer-main',
  templateUrl: './customer-main.component.html',
  styleUrls: ['./customer-main.component.scss']
})
export class CustomerMainComponent implements OnInit {
  private customerList: CustomerModel[] = [];
  private selectedCustomer: CustomerModel = new CustomerModel();
  private isManageCustomerClicked: boolean;
  private activeCustomerIndex: number;
  /* search params */
  private searchType: string;
  private searchValue: string;
  private page: number;
  private pageSize: number;
  private numberOfPages: number;
  /*infinity scroll*/
  private throttle: number;
  private scrollDistance: number;
  private scrollUpDistance: number;
  private fromRoot = true;
  /*panels*/
  policyList: PolicyModel[] = [];
  private quotationList: QuotationModel[] = [];
  /*land*/
  private landList: LandModel[] = [];
  private selectedLand: LandModel = new LandModel();
  private isManageLandClicked: boolean;
  private asInternalUser: boolean;
  private partyCode: string;

  constructor(private service: CustomerService) {
  }

  ngOnInit() {
    this.initVariables();
    this.getCustomer();
  }

  initVariables() {
    this.throttle = 300;
    this.scrollUpDistance = 2;
    this.scrollDistance = 1;
    this.searchValue = '';
    this.page = 1;
    this.pageSize = 20;
    this.isManageLandClicked = false;
    this.isManageLandClicked = false;
    this.searchType = 'name';
    this.activeCustomerIndex = 0;
    this.asInternalUser = sessionStorage.getItem('partyAsInternalStaff') === 'Y';
    this.partyCode = sessionStorage.getItem('partyCode');
  }

  getCustomer() {
    this.service.getCustomers(this.searchType, this.searchValue, this.page, this.pageSize).subscribe(resp => {
      const body = resp.body as any;
      if (body.data.partyProfile) {
        this.customerList = body.data.partyProfile;
        this.numberOfPages = body.data.totalNoOfPages;

        if (this.customerList.length) {
          this.onSelectCustomer(0);
        }
      } else {
        this.customerList = [];
      }
    });
  }

  getPolices(code: string) {
    if (this.asInternalUser) {
      this.service.getPolicyByCustomer(code, '').subscribe(resp => {
        const body = resp.body as any;
        this.policyList = body.data;
      });
    } else {
      this.service.getPolicyByCustomer(code, this.partyCode ).subscribe(resp => {
        const body = resp.body as any;
        this.policyList = body.data;
      });
    }
  }

  onSelectCustomer(i: number) {
    this.customerList[i].active = true;
    if (i !== this.activeCustomerIndex) {
      this.customerList[this.activeCustomerIndex].active = false;
    }
    this.activeCustomerIndex = i;

    this.selectedCustomer = this.customerList[i];
    if (this.customerList[i].contact) {
      this.selectedCustomer.contact.forEach(con => {
        if (con.contactType === 'CEMIL') {
          this.selectedCustomer.email = con;
        }
        if (con.contactType === 'CMOBL') {
          this.selectedCustomer.phone = con;
        }
      });
    } else {
      console.log('no any contacts');
    }


    if (this.selectedCustomer.identification) {
      this.selectedCustomer.identification.forEach(id => {
        if (id.registrationType === 'NIDN') {
          this.selectedCustomer.nic = id;
        }
      });
    }

    this.selectedCustomer.address = this.customerList[i].address;
    this.getLands();
    this.getPolices(this.selectedCustomer.profile.code);
    this.getQuotations();
  }

  onScrollDown() {
    if (this.numberOfPages >= this.page) {
      this.page++;
      this.service.getCustomers(this.searchType, this.searchValue, this.page, this.pageSize).subscribe(resp => {
        const body = resp.body as any;
        const list: CustomerModel[] = body.data.partyProfile;
        list.forEach(item => {
          this.customerList.push(item);
        });
      });
    }
  }

  onCancel() {
    this.isManageCustomerClicked = !this.isManageCustomerClicked;
  }

  getLands() {
    this.service.getCustomerLands(this.selectedCustomer.profile.code).subscribe(resp => {
      const body = resp.body as any;
      this.landList = body.data;
    });
  }

  getQuotations() {
    this.service.getQuotatinList(this.selectedCustomer.profile.code).subscribe(resp => {
      const body = resp.body as any;
      this.quotationList = body.data;
    });
  }

  onManageLand(index: number) {
    this.selectedLand = this.landList[index];
    this.isManageLandClicked = true;
  }
}
