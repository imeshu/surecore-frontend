import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CustomerMainComponent} from './customer-main/customer-main.component';
import {AddCustomerComponent} from './add-customer/add-customer.component';
import {AddLandComponent} from '../land/add-land/add-land.component';

const routes: Routes = [
  {
    path: 'customers',
    // component: CustomerListComponent,
    component: CustomerMainComponent,
  },
  {
    path: 'customers/add-customer',
      component: AddCustomerComponent
  },
  {
    path: 'customers/add-land',
      component: AddLandComponent
  }
];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule {
}
