import { Component, OnInit } from '@angular/core';
import {VERSION} from '../../config/enum';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private isHBClicked = false;
  private version: string;
  constructor() { }

  ngOnInit() {
    this.version = VERSION;
  }

  output(event) {
    this.isHBClicked = !this.isHBClicked;
  }
  closeSidebar() {
    this.isHBClicked = false;
  }
}
