import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkLandComponent } from './mark-land.component';

describe('MarkLandComponent', () => {
  let component: MarkLandComponent;
  let fixture: ComponentFixture<MarkLandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkLandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkLandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
