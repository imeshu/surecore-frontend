import {Component, DoCheck, EventEmitter, Input, KeyValueDiffer, KeyValueDiffers, OnInit, Output} from '@angular/core';
import {CommonType} from '../../../model/CommonType';
import {LocationModel} from '../models/location-model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LandService} from '../services/land.service';
import {LandModel} from '../../customer/models/land-model';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {ExportImgs, ImageModel} from '../models/img-model';
import {HttpEventType} from '@angular/common/http';

@Component({
  selector: 'app-manage-land',
  templateUrl: './manage-land.component.html',
  styleUrls: ['./manage-land.component.scss']
})
export class ManageLandComponent implements OnInit, DoCheck {
  @Input() selectedLand: LandModel;
  @Output() isCancelClicked = new EventEmitter();
  private districtsList: CommonType[] = [];
  private divisionsList: CommonType[] = [];
  private gramasevaDivisionsList: CommonType[] = [];
  private getagrarianServiceCentersList: CommonType[] = [];
  private ownershipTypesList: CommonType[] = [];

  private selectedDistrict: string;
  private selectedDivision: string;
  private selectedGramasevaDivision: string;
  private selectedgetagrarianServiceCenter: string;
  private selectedOwnershipType: string;

  private isDrawMap = false;
  private pathList: LocationModel[] = [];
  private pathString = '';
  private isManageClicked = false;
  private imgList: ImageModel[] = [];
  differ: KeyValueDiffer<string, any>;

  private docSequence: string;
  private landSequence: string;

  landForm = new FormGroup({
    plotName: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    division: new FormControl('', Validators.required),
    gramasevaDivision: new FormControl('', Validators.required),
    agrarianServiceCenter: new FormControl('', Validators.required),
    sizeInAcres: new FormControl('', Validators.required),
    sizeInPerches: new FormControl('', [Validators.required, Validators.max(159)]),
    ownership: new FormControl('', Validators.required),
    geometry: new FormControl('', Validators.required),
  });

  constructor(private service: LandService, private differs: KeyValueDiffers, private msg: NotificationDataTransferService) {
    this.differ = this.differs.find({this: ImageModel}).create();
  }

  ngOnInit() {
    this.getDistricts();
    this.getagrarianServiceCenters();
    this.getOwnershipTypes();
    this.initForm();
    this.getImages(this.selectedLand.sequence);

  }

  ngDoCheck() {
    const change = this.differ.diff(this);
    if (change) {
      change.forEachChangedItem(item => {
        if (item.key === 'selectedLand') {
          this.initForm();
          this.getImages(this.selectedLand.sequence);
        }
      });
    }
  }

  initForm() {

    this.landForm.controls['plotName'].setValue(this.selectedLand.plotName);
    this.landForm.controls['address'].setValue(this.selectedLand.address);
    this.landForm.controls['district'].setValue(this.selectedLand.district);
    this.selectedDistrict = this.selectedLand.district;
    this.getDivisions();

    this.landForm.controls['division'].setValue(this.selectedLand.division);
    this.selectedDivision = this.selectedLand.division;
    this.getGramasevaDivisions();

    this.landForm.controls['gramasevaDivision'].setValue(this.selectedLand.gramasevaDivision);
    this.landForm.controls['agrarianServiceCenter'].setValue(this.selectedLand.agrarianServiceCenter);
    this.landForm.controls['sizeInAcres'].setValue(this.selectedLand.sizeInAcres);
    this.landForm.controls['sizeInPerches'].setValue(this.selectedLand.sizeInPerches);
    this.landForm.controls['ownership'].setValue(this.selectedLand.ownership);
    this.pathList = this.selectedLand.geometry;
    this.pathList.forEach(item => {
      this.pathString = this.pathString + item.latitude + ':' + item.longitude + ',';
    });
    this.landForm.controls['geometry'].setValue(this.pathString);
  }

  getDistricts() {
    this.service.getDistricts().subscribe(resp => {
      const body = resp.body as any;
      this.districtsList = body.data;
    });
  }

  getDivisions() {
    this.service.getDivisions(this.selectedDistrict).subscribe(resp => {
      const body = resp.body as any;
      this.divisionsList = body.data;
    });
  }

  getGramasevaDivisions() {
    this.service.getGramasevaDivisions(this.selectedDivision).subscribe(resp => {
      const body = resp.body as any;
      this.gramasevaDivisionsList = body.data;
    });
  }

  getagrarianServiceCenters() {
    this.service.getagrarianServiceCenters().subscribe(resp => {
      const body = resp.body as any;
      this.getagrarianServiceCentersList = body.data;
    });
  }

  getOwnershipTypes() {
    this.service.getOwnershipTypes().subscribe(resp => {
      const body = resp.body as any;
      this.ownershipTypesList = body.data;
    });
  }

  getMarkers(event) {
    if (event.length === 0) {
      this.isDrawMap = false;
    } else {
      this.pathList = event;
      this.pathList.push(this.pathList[0]);
      this.selectedLand.geometry = this.pathList;
      this.isDrawMap = false;
      this.pathList.forEach(item => {
        this.pathString = this.pathString + item.latitude + ':' + item.longitude + ',';
      });
      this.landForm.controls['geometry'].setValue(this.pathString);
    }
  }

  updateLand() {
    const land: LandModel = this.selectedLand;
    land.plotName = this.landForm.value.plotName;
    land.address = this.landForm.value.address;
    land.district = this.landForm.value.district;
    land.division = this.landForm.value.division;
    land.gramasevaDivision = this.landForm.value.gramasevaDivision;
    land.agrarianServiceCenter = this.landForm.value.agrarianServiceCenter;
    land.sizeInAcres = this.landForm.value.sizeInAcres;
    land.sizeInPerches = this.landForm.value.sizeInPerches;
    land.ownership = this.landForm.value.ownership;

    land.geometry = this.pathList;
    land.partyCode = this.selectedLand.partyCode;
    land.status = this.selectedLand.status;
    land.version = this.selectedLand.version;
    land.sequence = this.selectedLand.sequence;

    this.service.putLand(land).subscribe(resp => {
      this.msg.success('Update Success');
      this.isCancelClicked.emit();
      this.isManageClicked = false;
    }, error1 => {
      this.msg.error('Update Failed');

      return error1;
    });
    this.isManageClicked = false;
  }

  getImages(id) {
    this.service.getImages(id).subscribe(resp => {
      const body = resp.body as any;
      if (body.data.length !== 0 && body.data[0].imageList) {
        this.imgList = body.data[0].imageList;
        this.imgList.forEach(img => {
          img.imageKey = img.key;
          img.value = img.url;
        });
      } else {
        this.imgList = [];
      }

    });
  }

  removeImg(index: number) {
    this.imgList.splice(index, 1);
  }

  addImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    if (img.file) {
      this.imgList.push(img);
      this.uploadImg(img.file, this.imgList.indexOf(img));
    }
  }

  saveImages() {
    this.landSequence = this.selectedLand.sequence;
    this.service.checkDoc(this.landSequence).subscribe(resp => {
      const body = resp.body as any;
      if (body.data.length) {
        this.docSequence = body.data[0].sequence;
        this.postImgs();
      } else {
        // tslint:disable-next-line:no-shadowed-variable
        this.service.createDoc(this.landSequence).subscribe(resp => {
          const data2 = resp.body as any;
          this.docSequence = data2.data;
          this.postImgs();
        }, error1 => {
          this.msg.error(error1.message);
        });
      }
    }, error1 => {
      this.msg.error(error1.message);
    });

  }

  postImgs() {
    const imgs: ExportImgs[] = [];

    this.imgList.forEach(itm => {
      const img: ExportImgs = new ExportImgs();
      img.key = itm.imageKey;
      imgs.push(img);
    });
    if (imgs.length) {
      this.service.saveImages(this.docSequence, imgs).subscribe(resp => {
        this.msg.success('Image added Successfully');
      }, error => {
        this.msg.error('Image added Failed');
      });

    }
  }

  uploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.imgList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imgList[index].imageKey = body.data.object.key;
        this.imgList[index].value = body.data.object.url;
        this.imgList[index].file = null;
      }
    });
  }

  cancel() {
    this.isCancelClicked.emit();
  }
}
