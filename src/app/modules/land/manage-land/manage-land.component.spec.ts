import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageLandComponent } from './manage-land.component';

describe('ManageLandComponent', () => {
  let component: ManageLandComponent;
  let fixture: ComponentFixture<ManageLandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageLandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageLandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
