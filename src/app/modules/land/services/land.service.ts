import {Injectable} from '@angular/core';
import {HTTPCallService} from '../../../services/httpcall.service';
import {Observable} from 'rxjs';
import {HttpEvent, HttpResponse} from '@angular/common/http';
import {LandModel} from '../../customer/models/land-model';
import {ExportImgs} from '../models/img-model';
import {fileServer} from '../../../config/enum';

@Injectable({
  providedIn: 'root'
})
export class LandService {

  constructor(private http: HTTPCallService) {
  }

  getDistricts(): Observable<HttpResponse<Object>> {
    const path = 'base/districts';
    return this.http.getResource(path);
  }

  getDivisions(district: string): Observable<HttpResponse<Object>> {
    const path = 'base/districts/' + district + '/divisions';
    return this.http.getResource(path);
  }

  getGramasevaDivisions(division): Observable<HttpResponse<Object>> {
    const path = 'base/divisions/' + division + '/gramasevaDivisions';
    return this.http.getResource(path);
  }

  getagrarianServiceCenters(): Observable<HttpResponse<Object>> {
    const path = 'base/agrarianServiceCenters';
    return this.http.getResource(path);
  }

  getOwnershipTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/ownershipTypes';
    return this.http.getResource(path);
  }


  postLand(land: LandModel): Observable<HttpResponse<Object>> {
    const path = 'utility/locations';
    return this.http.postResource(path, land);
  }

  getLandById(id: String): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/' + id;
    return this.http.getResource(path);
  }

  putLand(land: LandModel): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/' + land.sequence;
    return this.http.putResource(path, land);
  }

  uploadImage(file: File): Observable<HttpEvent<Object>> {
    const fd = new FormData();
    fd.append('file', file);
    const path = fileServer + 'v1/files/upload?path=utility';
    return this.http.postImage(path, fd);
  }

  checkDoc(sequence: String): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/' + sequence + '/documents';
    return this.http.getResource(path);
  }

  createDoc(sequence: String): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/' + sequence + '/documents';
    return this.http.postResource(path, {code: 'IMAGES'});
  }

  saveImages(sequence: String, imgList: ExportImgs[]): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/documents/' + sequence + '/images';

    return this.http.postResource(path, imgList);
  }

  getImages(sequence: String): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/' + sequence + '/documents';
    return this.http.getResource(path);
  }
}
