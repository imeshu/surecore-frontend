export class ImageModel {
  imageKey: string;
  value: string;
  progress: string;
  file: File;

  alias: string;
  key: string;
  reference: string;
  url: string;
}

export class ExportImgs {
  key: string;
}
