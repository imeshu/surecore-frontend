export class LandModel {
  active: boolean;
  sequence: string;
  address: string;
  agrarianServiceCenter: string;
  division: string;
  district: string;
  geometry: GeometryModel [] = [];
  gramasevaDivision: string;
  plotName: string;
  partyCode: string;
  status: string;
  sizeInAcres: string;
  sizeInPerches: string;
  agrarianServiceCenterDescription: string;
  districtDescription: string;
  divisionDescription: string;
  gramasevaDivisionDescription: string;
  ownershipDescription: string;
  partyName: string;
  version: string;
  ownership: string;

  constructor() {
    this.active = false;
    this.sequence = '';
    this.address = '';
    this.agrarianServiceCenter = '';
    this.plotName = '';
    this.partyCode = '';
    this.status = '';
    this.sizeInAcres = '';
    this.sizeInPerches = '';
    this.agrarianServiceCenterDescription = '';
    this.districtDescription = '';
    this.divisionDescription = '';
    this.gramasevaDivisionDescription = '';
    this.ownershipDescription = '';
    this.partyName = '';
    this.version = '';
    this.ownership = '';
  }
}

export class GeometryModel {
  latitude: string;
  longitude: string;
}
