import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonType} from '../../../model/CommonType';
import {LandService} from '../services/land.service';
import {LocationModel} from '../models/location-model';
import {LandModel} from '../../customer/models/land-model';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {HttpEventType} from '@angular/common/http';
import {ExportImgs, ImageModel} from '../models/img-model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add-land',
  templateUrl: './add-land.component.html',
  styleUrls: ['./add-land.component.scss']
})
export class AddLandComponent implements OnInit {
  private districtsList: CommonType[] = [];
  private divisionsList: CommonType[] = [];
  private gramasevaDivisionsList: CommonType[] = [];
  private getagrarianServiceCentersList: CommonType[] = [];
  private ownershipTypesList: CommonType[] = [];

  private selectedDistrict: CommonType;
  private selectedDivision: CommonType;
  private selectedGramasevaDivision: CommonType;
  private selectedgetagrarianServiceCenter: string;
  private selectedOwnershipType: string;
  private sendLandData: LandModel = new LandModel();
  private isDrawMap = false;
  private pathList: LocationModel[] = [];
  private pathString = '';

  private isContinue = false;
  // imag upload
  private imagList: ImageModel[] = [];
  private landSequence = '';
  private docSequence: string;

  // routing
  private routerPartyCode: string;
  private routeString: string;
  private isCustomer: boolean;
  landForm = new FormGroup({
    plotName: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    division: new FormControl('', Validators.required),
    gramasevaDivision: new FormControl('', Validators.required),
    agrarianServiceCenter: new FormControl('', Validators.required),
    sizeInAcres: new FormControl('', Validators.required),
    sizeInPerches: new FormControl('', [Validators.required, Validators.max(159)]),
    ownership: new FormControl('', Validators.required),
    geometry: new FormControl('', Validators.required),
  });

  constructor(private service: LandService,
              private msg: NotificationDataTransferService,
              private router: Router,
              private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    if (this.router.url.includes('/land/add-land')) {
      this.routerPartyCode = this.route.snapshot.paramMap.get('partyCode');
    }
    if (this.routerPartyCode) {
      this.routeString = '/customers';
    } else {
      this.routeString = '/land';
    }
    if (sessionStorage.getItem('asCustomer') === 'Y') {
      this.isCustomer = true;
    } else {
      this.isCustomer = false;
    }
    this.getDistricts();
    this.getagrarianServiceCenters();
    this.getOwnershipTypes();
  }

  getDistricts() {
    this.service.getDistricts().subscribe(resp => {
      const body = resp.body as any;
      this.districtsList = body.data;
    });
  }

  getDivisionns() {
    this.service.getDivisions(this.selectedDistrict.code).subscribe(resp => {
      const body = resp.body as any;
      this.divisionsList = body.data;
    });
  }

  getGramasevaDivisions() {
    this.service.getGramasevaDivisions(this.selectedDivision.code).subscribe(resp => {
      const body = resp.body as any;
      this.gramasevaDivisionsList = body.data;
    });
  }

  getagrarianServiceCenters() {
    this.service.getagrarianServiceCenters().subscribe(resp => {
      const body = resp.body as any;
      this.getagrarianServiceCentersList = body.data;
    });
  }

  getOwnershipTypes() {
    this.service.getOwnershipTypes().subscribe(resp => {
      const body = resp.body as any;
      this.ownershipTypesList = body.data;
    });
  }

  getMarkers(event) {
    if (event.length === 0) {
      this.isDrawMap = false;
    } else {
      this.pathList = event;
      this.pathList.push(this.pathList[0]);
      this.pathList.forEach(item => {
        this.pathString = this.pathString + item.latitude + ':' + item.longitude + ',';
      });
      this.landForm.controls['geometry'].setValue(this.pathString);
      this.isDrawMap = false;
    }

  }

  saveLand() {
    const land: LandModel = this.landForm.value;
    land.geometry = this.pathList;
    land.district = this.selectedDistrict.code;
    land.division = this.selectedDivision.code;
    land.gramasevaDivision = this.selectedGramasevaDivision.code;
    if (this.router.url.includes('/land/add-land')) {
      this.routerPartyCode = this.route.snapshot.paramMap.get('partyCode');
    }
    if (this.routerPartyCode) {
      land.partyCode = this.routerPartyCode;
      this.routeString = '/customers';
    } else {
      land.partyCode = sessionStorage.getItem('partyCode');
      this.routeString = '/land';
    }
    this.service.postLand(land).subscribe(resp => {
      this.msg.success('Added Success');
      const data = resp.body as any;
      this.landSequence = data.data;
      this.isContinue = true;
    }, error1 => {
      this.msg.error('Added Failed');
    });
    this.isContinue = true;
  }

// upload images
  uploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.imagList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imagList[index].imageKey = body.data.object.key;
        this.imagList[index].value = body.data.object.url;
        this.imagList[index].file = null;
      }
    });
  }

  addImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    img.file = event.target.files[0];
    if (img.file) {
      this.imagList.push(img);
      this.uploadImg(img.file, this.imagList.indexOf(img));
    }
  }

  removeImg(index) {
    this.imagList.splice(index, 1);
  }

  saveImages() {

    this.service.checkDoc(this.landSequence).subscribe(resp => {
      const body = resp.body as any;
      if (body.data.length) {
        this.docSequence = body.data[0].sequence;
        this.postImgs();
      } else {
        // tslint:disable-next-line:no-shadowed-variable
        this.service.createDoc(this.landSequence).subscribe(resp => {
          const data2 = resp.body as any;
          this.docSequence = data2.data;
          this.postImgs();
        }, error1 => {
          console.log(error1);
        });
      }
    }, error1 => {
      console.log(error1);
    });

  }

  postImgs() {
    const imgs: ExportImgs[] = [];

    this.imagList.forEach(itm => {
      const img: ExportImgs = new ExportImgs();
      img.key = itm.imageKey;
      imgs.push(img);
    });

    this.service.saveImages(this.docSequence, imgs).subscribe(resp => {
      this.msg.success('Image added Successfully');
      this.router.navigate([this.routeString]);
    }, error => {
      this.msg.error('Failed');
    });
  }


  drawOnMap() {
    this.sendLandData.districtDescription = this.selectedDistrict.description;
    this.sendLandData.divisionDescription = this.selectedDivision.description;
    this.sendLandData.gramasevaDivisionDescription = this.selectedGramasevaDivision.description;
    this.isDrawMap = true;
  }

}
