import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLandFormComponent } from './add-land-form.component';

describe('AddLandFormComponent', () => {
  let component: AddLandFormComponent;
  let fixture: ComponentFixture<AddLandFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLandFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLandFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
