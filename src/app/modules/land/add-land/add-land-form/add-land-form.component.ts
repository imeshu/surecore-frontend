import {Component, OnInit} from '@angular/core';
import {CommonType} from '../../../../model/CommonType';
import {LandModel} from '../../../customer/models/land-model';
import {LocationModel} from '../../models/location-model';
import {ExportImgs, ImageModel} from '../../models/img-model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LandService} from '../../services/land.service';
import {NotificationDataTransferService} from '../../../../services/notification-data-transfer.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpEventType} from '@angular/common/http';

@Component({
  selector: 'app-add-land-form',
  templateUrl: './add-land-form.component.html',
  styleUrls: ['./add-land-form.component.scss']
})
export class AddLandFormComponent implements OnInit {

  private districtsList: CommonType[] = new Array();
  private divisionsList: CommonType[] = new Array();
  private gramasevaDivisionsList: CommonType[] = new Array();
  private getagrarianServiceCentersList: CommonType[] = new Array();
  private ownershipTypesList: CommonType[] = new Array();

  private selectedDistrict: CommonType;
  private selectedDivision: CommonType;
  private selectedGramasevaDivision: CommonType;
  private selectedgetagrarianServiceCenter: string;
  private selectedOwnershipType: string;
  private sendLandData: LandModel = new LandModel();
  private isDrawMap: boolean = false;
  private pathList: LocationModel[] = new Array();
  private pathString: string = '';

  private isContinue: boolean = false;
  panelOpenState = true;
  // imag upload
  private imagList: ImageModel[] = new Array();
  private landSequence: string = '';
  private docSequence: string;

  // routing
  private routerPartyCode: string;
  private routeString: string;
  private isCustomer: boolean;
  landForm = new FormGroup({
    plotName: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    division: new FormControl('', Validators.required),
    gramasevaDivision: new FormControl('', Validators.required),
    agrarianServiceCenter: new FormControl('', Validators.required),
    sizeInAcres: new FormControl('', Validators.required),
    sizeInPerches: new FormControl('', Validators.required),
    ownership: new FormControl('', Validators.required),
    geometry: new FormControl('', Validators.required),
  });

  constructor(
    private service: LandService,
    private msg: NotificationDataTransferService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    if (this.router.url.includes('/land/add-land')) {
      this.routerPartyCode = this.route.snapshot.paramMap.get('partyCode');
    }
    if (this.routerPartyCode) {
      this.routeString = '/customers';
    } else {
      this.routeString = '/land';
    }
    if (sessionStorage.getItem('asCustomer') === 'Y') {
      this.isCustomer = true;
    } else {
      this.isCustomer = false;
    }
    this.getDistricts();
    this.getagrarianServiceCenters();
    this.getOwnershipTypes();
  }

  getDistricts() {
    this.service.getDistricts().subscribe(resp => {
      const body = resp.body as any;
      this.districtsList = body.data;
    });
  }

  getDivisionns() {
    this.service.getDivisions(this.selectedDistrict.code).subscribe(resp => {
      const body = resp.body as any;
      this.divisionsList = body.data;
    });
  }

  getGramasevaDivisions() {
    this.service.getGramasevaDivisions(this.selectedDivision.code).subscribe(resp => {
      const body = resp.body as any;
      this.gramasevaDivisionsList = body.data;
    });
  }

  getagrarianServiceCenters() {
    this.service.getagrarianServiceCenters().subscribe(resp => {
      const body = resp.body as any;
      this.getagrarianServiceCentersList = body.data;
    });
  }

  getOwnershipTypes() {
    this.service.getOwnershipTypes().subscribe(resp => {
      const body = resp.body as any;
      this.ownershipTypesList = body.data;
    });
  }

  getMarkers(event) {
    console.log(event);
    if (event.length === 0) {
      this.isDrawMap = false;
    } else {
      this.pathList = event;
      this.pathList.push(this.pathList[0]);
      this.pathList.forEach(item => {
        this.pathString = this.pathString + item.latitude + ':' + item.longitude + ',';
      });
      this.landForm.controls['geometry'].setValue(this.pathString);
      this.isDrawMap = false;
      console.log(this.pathList);
    }

  }

  saveLand() {
    const land: LandModel = this.landForm.value;
    land.geometry = this.pathList;
    land.district = this.selectedDistrict.code;
    land.division = this.selectedDivision.code;
    land.gramasevaDivision = this.selectedGramasevaDivision.code;
    if (this.router.url.includes('/land/add-land')) {
      this.routerPartyCode = this.route.snapshot.paramMap.get('partyCode');
    }
    if (this.routerPartyCode) {
      land.partyCode = this.routerPartyCode;
      this.routeString = '/customers';
    } else {
      land.partyCode = sessionStorage.getItem('partyCode');
      this.routeString = '/land';
    }
    console.log(land);
    this.service.postLand(land).subscribe(resp => {
      this.msg.success('Added Success');
      const data = resp.body as any;
      this.landSequence = data.data;
      this.isContinue = true;
    }, error1 => {
      this.msg.error('Added Failed');
    });
    this.isContinue = true;
  }

// upload images
  uploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.imagList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imagList[index].imageKey = body.data.object.key;
        this.imagList[index].value = body.data.object.url;
        this.imagList[index].file = null;
      }
    });
  }

  addImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    this.imagList.push(img);
    this.uploadImg(img.file, this.imagList.indexOf(img));
    console.log(img);
  }

  removeImg(index) {
    this.imagList.splice(index, 1);
  }

  saveImages() {
    console.log(this.imagList);

    this.service.checkDoc(this.landSequence).subscribe(resp => {
      const body = resp.body as any;
      console.log('check document');
      console.log(body);
      console.log(this.docSequence);
      if (body.data.length) {
        this.docSequence = body.data[0].sequence;
        console.log('has data');
        this.postImgs();
      } else {
        console.log('no data');
        this.service.createDoc(this.landSequence).subscribe(resp => {
          console.log('create doc');
          console.log(resp.body);
          const data2 = resp.body as any;
          this.docSequence = data2.data;
          this.postImgs();
        }, error1 => {
          console.log(error1);
        });
      }
    }, error1 => {
      console.log(error1);
    });

  }

  postImgs() {
    const imgs: ExportImgs[] = new Array();

    this.imagList.forEach(itm => {
      const img: ExportImgs = new ExportImgs();
      img.key = itm.imageKey;
      imgs.push(img);
    });

    console.log(imgs);
    this.service.saveImages(this.docSequence, imgs).subscribe(resp => {
      console.log('save Imgs');
      console.log(resp);
      this.msg.success('Image added Successfully');
    }, error => {
      this.msg.error('Failed');
      console.log(error);
    });
  }


  asd() {
    this.sendLandData.districtDescription = this.selectedDistrict.description;
    this.sendLandData.divisionDescription = this.selectedDivision.description;
    this.sendLandData.gramasevaDivisionDescription = this.selectedGramasevaDivision.description;

    console.log(this.sendLandData);
    this.isDrawMap = true;
  }


}
