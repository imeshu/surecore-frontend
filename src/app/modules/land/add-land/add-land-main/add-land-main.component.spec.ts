import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLandMainComponent } from './add-land-main.component';

describe('AddLandMainComponent', () => {
  let component: AddLandMainComponent;
  let fixture: ComponentFixture<AddLandMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLandMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLandMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
