import { Component, OnInit } from '@angular/core';
import {LandModel} from '../../../customer/models/land-model';

@Component({
  selector: 'app-add-land-main',
  templateUrl: './add-land-main.component.html',
  styleUrls: ['./add-land-main.component.scss']
})
export class AddLandMainComponent implements OnInit {
  private sendLandData: LandModel = new LandModel();
  constructor() { }

  ngOnInit() {
  }

}
