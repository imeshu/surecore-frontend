import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LandRoutingModule} from './land-routing.module';
import {AddLandComponent} from './add-land/add-land.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatSelectModule} from '@angular/material';
import {MarkLandComponent} from './mark-land/mark-land.component';
import {BrowserModule} from '@angular/platform-browser';
import {LandMainComponent} from './land-main/land-main.component';
import {CommonComponentsModule} from '../common-components/common-components.module';
import {ManageLandComponent} from './manage-land/manage-land.component';
import {CustomPipeModule} from '../../pipes/CustomPipesModule';


// MarkLandComponent
@NgModule({
  declarations: [AddLandComponent, LandMainComponent, MarkLandComponent, ManageLandComponent],

  imports: [
    BrowserModule,
    CommonModule,
    LandRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    // material
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatButtonModule,

    CommonComponentsModule,
    MatExpansionModule,
    CustomPipeModule

  ],

  exports: [MarkLandComponent, ManageLandComponent]

})
export class LandModule {
}
