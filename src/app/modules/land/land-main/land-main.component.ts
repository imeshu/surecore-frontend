import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../../customer/services/customer.service';
import {LandService} from '../services/land.service';
import {ImageModel} from '../models/img-model';
import {LandModel} from '../models/land-model';

@Component({
  selector: 'app-land-main',
  templateUrl: './land-main.component.html',
  styleUrls: ['./land-main.component.scss']
})
export class LandMainComponent implements OnInit {
  private landList: LandModel[] = [];
  private landFilteredList: LandModel[] = [];
  private isManageClicked = false;
  private selectedLand: LandModel = new LandModel();
  private searchText = '';
  private pathString = '';
  private imgList: ImageModel[] = [];
  private activeLandSequence = '';

  constructor(private customerService: CustomerService, private landService: LandService) {
  }

  ngOnInit() {
    this.getLands();
  }

  getLands() {
    this.customerService.getCustomerLands(sessionStorage.getItem('partyCode')).subscribe(resp => {
      const body = resp.body as any;
      if (body.data) {
        this.landList = body.data;
        this.landFilteredList = this.landList;
        this.onSelectLand(0, this.activeLandSequence);
      }
    }, error1 => {
      return error1;
    });
  }

  onSelectLand(id: number, sequence: string) {
    this.activeLandSequence = sequence;
    this.selectedLand = this.landFilteredList[id];
    this.selectedLand.geometry.forEach(item => {
      this.pathString = this.pathString + item.latitude + ':' + item.longitude + ';';
    });
    this.getImgs(this.selectedLand.sequence);
  }

  filterData(text: string) {
    this.landFilteredList = this.landList.filter(item => {
      return item.plotName.toUpperCase().match(text.toUpperCase());
    });
  }

  getImgs(id) {
    this.landService.getImages(id).subscribe(resp => {
      const body = resp.body as any;
      if (body.data.length !== 0 && body.data[0].imageList) {
        this.imgList = body.data[0].imageList;
        this.imgList.forEach(img => {
          img.value = img.url;
        });
      } else {
        this.imgList = [];
      }
    });
  }

  onCancel() {
    this.isManageClicked = false;
    this.getImgs(this.selectedLand.sequence);
  }
}
