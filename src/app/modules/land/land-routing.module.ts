import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddLandComponent} from './add-land/add-land.component';
import {MarkLandComponent} from './mark-land/mark-land.component';
import {LandMainComponent} from './land-main/land-main.component';

const routes: Routes = [
  {
    path: 'land',
    component: LandMainComponent
  },
  {
    path: 'land/add-land',
    component: AddLandComponent
  },
  {
    path: 'customers/add-land/mark-land',
    component: MarkLandComponent
  },
  {
    path: 'land/add-land/mark-land',
    component: MarkLandComponent
  },
  {
    path: 'land/add-land/:partyCode',
    component: AddLandComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandRoutingModule { }
