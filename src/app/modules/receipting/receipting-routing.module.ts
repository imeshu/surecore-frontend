import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CollectionInHandComponent} from './collection-in-hand/collection-in-hand.component';
import {GenerateDepositSlipComponent} from './generate-deposit-slip/generate-deposit-slip.component';
import {DepositSlipListComponent} from './deposit-slip-list/deposit-slip-list.component';

const routes: Routes = [
  {
    path: 'receipting',
    component: CollectionInHandComponent
  },
  {
    path: 'receipting/generate-slip',
    component: GenerateDepositSlipComponent
  },
  {
    path: 'receipting/deposit-slip-list',
    component: DepositSlipListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptingRoutingModule {

}
