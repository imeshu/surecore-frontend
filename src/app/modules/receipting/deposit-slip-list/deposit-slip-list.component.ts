import {Component, OnInit} from '@angular/core';
import {ReceiptingService} from '../services/receipting.service';
import {DepositSlipListModel} from './models/DepositSlipList-Model';
import * as moment from 'moment';

@Component({
  selector: 'app-deposit-slip-list',
  templateUrl: './deposit-slip-list.component.html',
  styleUrls: ['./deposit-slip-list.component.scss']
})
export class DepositSlipListComponent implements OnInit {

  private dataList: DepositSlipListModel[] = new Array();

  private startdate = null;
  private end = null;
  private date = '3months';

  private Sequnce = '';

  constructor(private service: ReceiptingService) {
    this.end = '';
    this.startdate = '';

  }

  ngOnInit() {

    this.getDepositList();
    // this.onSetDate();
  }

  getDepositList() {
    this.service.getDepositList(this.startdate, this.end).subscribe(resp => {
      const model = resp.body as any;
      this.dataList = model.data.bankingSlipList;
    });

  }


  onSetDate() {
    this.end = moment().format('YYYY-MM-DD');
    if (this.date === '3months') {
      this.startdate = moment().subtract(3, 'months').format('YYYY-MM-DD');
    }
    if (this.date === '2months') {
      this.startdate = moment().subtract(2, 'month').format('YYYY-MM-DD');
    }
    if (this.date === '1months') {
      this.startdate = moment().subtract(1, 'month').format('YYYY-MM-DD');

    }
    this.getDepositList();

  }

  completeSlip(i) {
    // Receipting Individual file
    const fileName1 = 'bankdetails.pdf';
    this.service.BankSlip(i).subscribe(x => {
      if (x.size === 0) {
        return;
      }
      const newBlob = new Blob([x], {type: 'application/pdf'});
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      const data = window.URL.createObjectURL(newBlob);
      const link = document.createElement('a');
      link.href = data;
      link.download = fileName1;
      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });
  }

}
