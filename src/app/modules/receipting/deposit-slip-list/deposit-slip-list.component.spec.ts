import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositSlipListComponent } from './deposit-slip-list.component';

describe('DepositSlipListComponent', () => {
  let component: DepositSlipListComponent;
  let fixture: ComponentFixture<DepositSlipListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositSlipListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositSlipListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
