export class DepositSlipListModel {

  collection: CollectionModel[] = new Array();
  cashBookCode: string;
  paymentModeCode: string;
  paymentModeDescription: string;
  currencyCode: string;
  depositSlipNumber: string;
  depositDate: Date;
  bankingSlipNumber: string;
  bankingSlipDate: Date;
  bankCode: number;
  bankDescription: string;
  accountNumber: string;
  bankedAmount: string;
  sequence: string;

}


export class CollectionModel {

  collectionSequence: string;
  receiptNumber: string;
  customerName: string;
  amount: string;
  currencyCode: string;
  settledDate: Date;

}
