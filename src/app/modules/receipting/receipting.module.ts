import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReceiptingRoutingModule} from './receipting-routing.module';
import {CommonComponentsModule} from '../common-components/common-components.module';
import { CollectionInHandComponent } from './collection-in-hand/collection-in-hand.component';
import {MatExpansionModule, MatFormFieldModule, MatInputModule, MatSelectModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { GenerateDepositSlipComponent } from './generate-deposit-slip/generate-deposit-slip.component';
import { DepositSlipListComponent } from './deposit-slip-list/deposit-slip-list.component';

@NgModule({
  declarations: [CollectionInHandComponent, GenerateDepositSlipComponent, DepositSlipListComponent],
  imports: [
    CommonModule,
    ReceiptingRoutingModule,
    CommonComponentsModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatExpansionModule
  ]
})
export class ReceiptingModule {}
