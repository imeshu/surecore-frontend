import { Component, OnInit } from '@angular/core';
import {Collection} from '../models/Collection';
import {ReceiptingService} from '../services/receipting.service';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-collection-in-hand',
  templateUrl: './collection-in-hand.component.html',
  styleUrls: ['./collection-in-hand.component.scss']
})
export class CollectionInHandComponent implements OnInit {

  collections: Collection[] = [];

  collectionType = 'PAYCS';
  totalCollections = 0;

  constructor(private service: ReceiptingService, private router: Router) { }

  ngOnInit() {
    this.loadCollectionData();
  }

  loadCollectionData() {
    this.service.getCollectionsInHand(this.collectionType).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(result => {
      this.collections = result;
      this.totalCollections = 0;

      this.collections.forEach(function (value) {
        // noinspection JSPotentiallyInvalidUsageOfClassThis
        this.totalCollections += value.amount;
      }, this);

    });
  }

  onDepositSlipGeneration() {
    this.router.navigate(['receipting/generate-slip']);
  }

}
