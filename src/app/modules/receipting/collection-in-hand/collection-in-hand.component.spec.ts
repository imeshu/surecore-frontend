import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionInHandComponent } from './collection-in-hand.component';

describe('CollectionInHandComponent', () => {
  let component: CollectionInHandComponent;
  let fixture: ComponentFixture<CollectionInHandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionInHandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionInHandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
