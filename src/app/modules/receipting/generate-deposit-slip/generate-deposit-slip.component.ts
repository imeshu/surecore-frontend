import {Component, OnInit} from '@angular/core';
import {Collection} from '../models/Collection';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {ReceiptingService} from '../services/receipting.service';
import {CommonType} from '../../../model/CommonType';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BankingSlip} from '../models/BankingSlip';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-generate-deposit-slip',
  templateUrl: './generate-deposit-slip.component.html',
  styleUrls: ['./generate-deposit-slip.component.scss']
})
export class GenerateDepositSlipComponent implements OnInit {

  collections: Collection[] = [];
  selectedCollections: Collection[] = [];
  banks: CommonType[];
  selectedBank: CommonType;
  accounts: CommonType[];
  selectedAccount: CommonType;
  slipNumber = '';
  receiptingSequnce = '';
  branchCode = '';

  collectionType = 'PAYCS';
  totalCollections = 0;

  selectedCollectionNumber = 0;
  isSelectedAll = false;

  depositDetailsForm: FormGroup;

  constructor(private service: ReceiptingService, private fb: FormBuilder, private notificationService: NotificationDataTransferService,
              private router: Router) {
  }

  ngOnInit() {
    this.loadCollectionData();
    // initialize form controls
    this.depositDetailsForm = this.fb.group({
      'bankSelection': new FormControl(this.selectedBank),
      'accountSelection': new FormControl(this.selectedAccount),
      'slipNumber': new FormControl(this.slipNumber, [Validators.pattern('^[0-9]*$'), Validators.required])
    });

    this.service.getBanks().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(result => {
      this.banks = result;
    });

  }

  loadCollectionData() {
    this.isSelectedAll = false;
    this.selectAll();

    this.service.getCollectionsInHand(this.collectionType).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(result => {
      this.collections = result;
      this.totalCollections = 0;
    });
  }

  submitDepositDetails() {

    for (const x of this.collections) {
      if (x.isSelected) {
        this.selectedCollections.push(x);
      }
    }

    this.selectedAccount = this.depositDetailsForm.get('accountSelection').value as CommonType;

    const bankingSlip: BankingSlip = new BankingSlip();
    bankingSlip.collection = this.selectedCollections;
    bankingSlip.cashBookCode = this.selectedAccount.code;
    bankingSlip.currencyCode = 'LKR';
    bankingSlip.depositSlipNumber = this.depositDetailsForm.get('slipNumber').value;
    bankingSlip.paymentModeCode = this.collectionType;


    this.service.submitBankSlip(bankingSlip).pipe(
      catchError(error => {
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageType = 'error';
        this.notificationService.messageContent = error.message;
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).toPromise().then(result => {
      this.receiptingSequnce = result.data;
      this.notificationService.isMessageDisplayed = true;
      this.notificationService.messageType = 'success';
      this.notificationService.messageContent = 'Operation completed successfully';
      this.branchCode = sessionStorage.getItem('branchCode');
      this.service.submitBankSequnce(this.receiptingSequnce , this.branchCode).subscribe( resp => {
        this.completeSlip(this.receiptingSequnce);
        this.router.navigate(['receipting']);
      });
    });

  }

  onBankSelect() {
    this.selectedBank = this.depositDetailsForm.get('bankSelection').value;
    this.service.getBankAccounts(this.selectedBank.code).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(result => {
      this.accounts = result;
    });
  }

  selectAll() {
    this.selectedCollectionNumber = 0;
    this.totalCollections = 0;

    if (this.isSelectedAll) {
      this.collections.forEach(function (value) {
        value.isSelected = true;
        // noinspection JSPotentiallyInvalidUsageOfClassThis
        this.selectedCollectionNumber++;
        // noinspection JSPotentiallyInvalidUsageOfClassThis
        this.totalCollections += value.amount;
      }, this);
    } else {
      this.collections.forEach(function (value) {
        value.isSelected = false;
      }, this);
    }
  }

  selectCount(collection: Collection) {
    if (collection.isSelected) {
      this.selectedCollectionNumber++;
      this.totalCollections += collection.amount;
    } else {
      this.selectedCollectionNumber--;
      this.totalCollections -= collection.amount;
    }
  }

  // generate slips
  completeSlip(sequence: string) {
    // Receipting Individual file
    const fileName1 = 'ReceiptingSlip.pdf';
    this.service.downloadReceiptingSlipFile(sequence).subscribe(x => {
      if (x.size === 0) {
        return;
      }
      const newBlob = new Blob([x], {type: 'application/pdf'});
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      const data = window.URL.createObjectURL(newBlob);
      const link = document.createElement('a');
      link.href = data;
      link.download = fileName1;
      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });
  }

}
