import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateDepositSlipComponent } from './generate-deposit-slip.component';

describe('GenerateDepositSlipComponent', () => {
  let component: GenerateDepositSlipComponent;
  let fixture: ComponentFixture<GenerateDepositSlipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateDepositSlipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateDepositSlipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
