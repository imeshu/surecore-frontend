import { TestBed } from '@angular/core/testing';

import { ReceiptingService } from './receipting.service';

describe('ReceiptingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReceiptingService = TestBed.get(ReceiptingService);
    expect(service).toBeTruthy();
  });
});
