import {Injectable} from '@angular/core';
import {HTTPCallService} from '../../../services/httpcall.service';
import {Observable} from 'rxjs';
import {Collection} from '../models/Collection';
import {CommonType} from '../../../model/CommonType';
import {map} from 'rxjs/operators';
import {SuccessResponse} from '../../../model/SuccessResponse';
import {BankingSlip} from '../models/BankingSlip';
import {HttpResponse} from '@angular/common/http';
import {Settlement} from '../../outstanding-debt/outstanding-debt-list/models/Settlement';

@Injectable({
  providedIn: 'root'
})
export class ReceiptingService {

  constructor(private http: HTTPCallService) {
  }

  getCollectionsInHand(collectionMode: string): Observable<Collection[]> {
    const path = 'rc/collectionInHand/search?paymentModeCode=' + collectionMode + '&currencyCode=LKR';

    return this.http.getResource(path).pipe(
      map(response => {

        // @ts-ignore
        return response.body.data.map(collection => {
          const x: Collection = new Collection();
          x.collectionSequence = collection.collectionSequence;
          x.receiptNumber = collection.receiptNumber;
          x.customerName = collection.customerName;
          x.amount = collection.amount;
          x.currencyCode = collection.currencyCode;
          x.settledDate = new Date(collection.settledDate);
          x.isSelected = false;

          return x;
        });
      })
    );
  }

  getBanks(): Observable<CommonType[]> {
    const path = 'rc/banks';

    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getBankAccounts(bankCode: string): Observable<CommonType[]> {
    const path = 'rc/banks/' + bankCode + '/accounts';

    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.cashBookcode;
          x.description = item.accountNumber;
          return x;
        });
      })
    );
  }

  submitBankSlip(slipDetails: BankingSlip): Observable<SuccessResponse> {
    const path = 'rc/bankingSlips';

    return this.http.postResource(path, slipDetails).pipe(
      map(response => {
        // @ts-ignore
        return new SuccessResponse(response.body.data, response.body.message, response.body.success, response.body.code);
      })
    );

    return;
  }

  submitBankSequnce(receiptingSequnce, branchCode): Observable<HttpResponse<Object>> {
    const path = 'rc/bankingSlips/' + receiptingSequnce + '/posted?branchCode=' + branchCode;
    return this.http.postResource(path, {code: 13});
  }


  getCollectionType(): Observable<CommonType[]> {
    const path = 'base/paymentModes';

    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

// Receipting Slip
  downloadReceiptingSlipFile(receiptingSequnce) {
    const path = 'rc/bankingSlip/' + receiptingSequnce + '/print';
    return this.http.downloadFile(path);
  }

  // Deposit List Service

  getDepositList(start: string, end: string): Observable<HttpResponse<Object>> {

    const path = 'rc/bankingSlips/search?page=1&size=10&slipNumber=&paymentModeCode=&glPostedFlag=&startDate=' + start + '&endDate=' + end;
    return this.http.getResource(path);
  }

  BankSlip(Sequnce) {
    const path = 'rc/bankingSlip/' + Sequnce + '/print';
    return this.http.downloadFile(path);
  }

}
