import {Collection} from './Collection';

export class BankingSlip {
  collection: Collection[];
  cashBookCode: string;
  paymentModeCode: string;
  currencyCode: string;
  depositSlipNumber: string;
}
