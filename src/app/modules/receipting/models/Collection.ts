export class Collection {
  collectionSequence: number;
  receiptNumber: string;
  customerName: string;
  amount: number;
  currencyCode: string;
  settledDate: Date;
  isSelected: boolean;
}
