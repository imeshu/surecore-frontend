import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPolicyLivestockComponent } from './add-policy-livestock.component';

describe('AddPolicyLivestockComponent', () => {
  let component: AddPolicyLivestockComponent;
  let fixture: ComponentFixture<AddPolicyLivestockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPolicyLivestockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPolicyLivestockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
