import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {PoliciesService} from '../services/policies.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommonType} from '../../../model/CommonType';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-policy-livestock',
  templateUrl: './add-policy-livestock.component.html',
  styleUrls: ['./add-policy-livestock.component.scss']
})
export class AddPolicyLivestockComponent implements OnInit {
  policyLivestockForm: FormGroup;
  private businessChannels: CommonType[];
  private  paymentTypes: CommonType[];
  private  products: CommonType[];
  @Output() savePolicyEvent = new EventEmitter();
  // tslint:disable-next-line:max-line-length
  constructor(private service: PoliciesService, private fb: FormBuilder, public notificationService: NotificationDataTransferService, private router: Router) { }

  ngOnInit() {
    this.fetchBusinessChannels();
    this.fetchPaymentTypes();
    this.fetchProducts();
    this.policyLivestockForm = this.fb.group({
      plan : ['', Validators.required],
      businessChannel : ['', Validators.required],
      startDate : ['', Validators.required],
      endDate : ['', Validators.required],
      paymentType : ['', Validators.required],
      dayCount : ['']
    });
    this.policyLivestockForm.reset();
  }
  /*
 * Form Control Getters
  */
  get plan() { return this.policyLivestockForm.get('plan'); }
  get businessChannel() { return this.policyLivestockForm.get('businessChannel'); }
  get startDate() { return this.policyLivestockForm.get('startDate'); }
  get endDate() { return this.policyLivestockForm.get('endDate'); }
  get paymentType() { return this.policyLivestockForm.get('paymentType'); }
  /**
   * Load Plans
   */
  fetchProducts() {
      this.service.getProducts('HO').pipe(
        catchError(error => {
          return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
        })
      ).subscribe(typeList => {
        this.products = typeList;
      });
  }
  /**
   * Load Business Channels
   */
  fetchBusinessChannels() {
    this.service.getBusinessChannels().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.businessChannels = typeList;
    });
  }
  /**
   * Load Payment Types
   */
  fetchPaymentTypes() {
    this.service.getPaymentTypes().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.paymentTypes = typeList;
    });
  }
  savePolicy() {
    this.savePolicyEvent.emit(this.policyLivestockForm.value);
  }


}
