import {Component, Input, OnInit} from '@angular/core';
import {CommonType} from '../../../model/CommonType';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {PolicyService} from '../services/policy.service';
import {Outstanding} from '../models/Outstanding';
import {ChequeCollection} from '../models/ChequeCollection';
import {CashCollection} from '../models/CashCollection';
import {BankDepositCollection} from '../models/BankDepositCollection';
import {MobileMoneyCollection} from '../models/MobileMoneyCollection';
import {CardCollection} from '../models/CardCollection';
import {Settlement} from '../models/Settlement';
import {SettlementDetail} from '../models/SettlementDetail';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {Router} from '@angular/router';
import {CurrencyPipe} from '@angular/common';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  @Input()
  debitNoteNumber;
  @Input()
  policyNumber;
  @Input()
  productCode;
  // pdf
  public productType = '';
  policySequnce = '';
  private paymentModes: CommonType[];
  private outstanding: Outstanding;
  private loyaltyPointsSpending = 0;
  private amountTobePaid = 0.0;
  private balance: number;
  private paymentMode: string;
  private amountLabelText: string;
  private data = '';

  private cashCollection: CashCollection = new CashCollection();
  private cardCollection: CardCollection = new CardCollection();
  private chequeCollection: ChequeCollection = new ChequeCollection();
  private mobileMoneyCollection: MobileMoneyCollection = new MobileMoneyCollection();
  private bankCollection: BankDepositCollection = new BankDepositCollection();

  private banks: CommonType[];
  private branches: CommonType[];
  private mobile_operators: CommonType[];
  private paymentDetailForm: FormGroup;
  private submitted = '0';
  private outstandingAmount: number;
  private settledAmount: number;
  private debitAmount: number;

  // tslint:disable-next-line:max-line-length
  constructor(private service: PolicyService, private fb: FormBuilder, public notificationService: NotificationDataTransferService, private router: Router, private currencyPipe: CurrencyPipe) {
  }

  ngOnInit() {
    this.fetchPaymentMethods();
    this.fetchOutstandingDetails();

    this.paymentDetailForm = this.fb.group({
      'loyalty_points': new FormControl(this.loyaltyPointsSpending),
      'payment_mode': new FormControl(this.paymentMode),
      'cheque_number': new FormControl(this.chequeCollection.chequeNumber),
      'cheque_bank': new FormControl(this.chequeCollection.bankCode),
      'cheque_branch': new FormControl(this.chequeCollection.branchCode),

      'credit_card_number': new FormControl(this.cardCollection.cardNumber),
      'credit_card_ccv': new FormControl(this.cardCollection.cardCcv),
      'credit_card_date': new FormControl(this.cardCollection.cardExpiry),

      'mobile_operator': new FormControl(this.mobileMoneyCollection.operatorCode),
      'mobile_number': new FormControl(this.mobileMoneyCollection.mobileNumber),

      'deposit_ref': new FormControl(this.bankCollection.depositReferenceNumber),
      'deposit_date': new FormControl(this.bankCollection.depositDate),
      'deposit_bank': new FormControl(this.bankCollection.bankCode),
      'deposit_account': new FormControl(this.bankCollection.accountNumber),

      'amount_to_be_paid': new FormControl(this.amountTobePaid, [Validators.required])
    });
  }

  /**
   * Load payment modes
   */
  fetchPaymentMethods() {
    this.service.getPaymentModes().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      if (sessionStorage.getItem('asCustomer') === 'Y') {
        const customerTypeArr = ['PAYCC', 'PAYMM'];
        this.paymentModes = typeList.filter(mode => customerTypeArr.includes(mode.code));
      }
      if (sessionStorage.getItem('asAgent') === 'Y' || sessionStorage.getItem('partyAsInternalStaff') === 'Y') {
        const agentTypeArr = ['PAYCQ', 'PAYCS', 'PAYBD'];
        this.paymentModes = typeList.filter(mode => agentTypeArr.includes(mode.code));
      }
    });
  }

  /**
   * Load outstanding data
   */
  fetchOutstandingDetails() {
    this.service.getOutstandingDetails(this.debitNoteNumber).subscribe((resp: any) => {
      this.debitNoteNumber = resp.body.data[0].debitNoteNumber;
      this.outstanding = resp.body.data[0];
      this.balance = this.outstanding.debitAmount - this.outstanding.settledAmount;
    }, error => {
      return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    });
  }

  /**
   * Load Banks
   */
  fetchBanks() {
    this.service.getBanks().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.banks = typeList;
    });
  }

  /**
   * Load Bank Branches
   */
  fetchBranches(bankCode: string) {
    this.chequeCollection.branchCode = null;
    this.service.getBranches(bankCode).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.branches = typeList;
    });
  }

  updatePointsUtilization() {
    if (this.loyaltyPointsSpending > this.outstanding.loyaltyPointsBalance) {
      this.loyaltyPointsSpending = this.outstanding.loyaltyPointsBalance;
    }
    if (this.loyaltyPointsSpending > this.balance) {
      this.loyaltyPointsSpending = this.outstanding.debitAmount - this.outstanding.settledAmount - this.amountTobePaid;
    }
    if (this.loyaltyPointsSpending < 0) {
      this.loyaltyPointsSpending = 0;
    }
    this.paymentDetailForm.controls['loyalty_points'].setValue(this.loyaltyPointsSpending);
    this.updateBalance();
  }

  onAmountToBePaidChange() {
    // if (this.amountTobePaid > this.balance) {
    //   this.amountTobePaid = this.outstanding.debitAmount - this.outstanding.settledAmount - this.loyaltyPointsSpending;
    // }
    // if (this.amountTobePaid < 0) {
    //   this.amountTobePaid = 0;
    // }
    // this.amountTobePaid = Math.round(this.amountTobePaid * 100) / 100;
    this.updateBalance();
    this.checkAmount();
  }

  updateBalance() {
    this.balance = this.outstanding.debitAmount - this.outstanding.settledAmount - this.loyaltyPointsSpending - this.amountTobePaid;
    this.balance = Math.round(this.balance * 100) / 100;

  }

  onPaymentModeChange() {
    this.paymentMode = this.paymentDetailForm.get('payment_mode').value;
    this.paymentDetailForm.clearValidators();
    switch (this.paymentMode) {
      case 'PAYCQ':
        if (!this.banks) {
          this.fetchBanks();
        }
        this.paymentDetailForm.controls['cheque_number'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['cheque_bank'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['cheque_branch'].setValidators([Validators.required]);
        this.amountLabelText = 'Cheque Amount';
        break;
      case 'PAYCS':
        this.amountLabelText = 'Amount to be Paid';
        break;
      case 'PAYBD':
        if (!this.banks) {
          this.fetchBanks();
        }
        this.paymentDetailForm.controls['deposit_ref'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['deposit_date'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['deposit_bank'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['deposit_account'].setValidators([Validators.required]);
        this.amountLabelText = 'Deposit Amount';
        break;
      case 'PAYMM':
        if (!this.mobile_operators) {
          this.fetchMobileOperators();
        }
        this.paymentDetailForm.controls['mobile_operator'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['mobile_number'].setValidators([Validators.required]);
        this.amountLabelText = 'Amount to be Paid';
        break;
      case 'PAYCC':
        this.paymentDetailForm.controls['credit_card_number'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['credit_card_ccv'].setValidators([Validators.required]);
        this.paymentDetailForm.controls['credit_card_date'].setValidators([Validators.required]);
        this.amountLabelText = 'Amount to be Paid';
        break;
    }
    this.paymentDetailForm.controls['amount_to_be_paid'].setValidators([Validators.required]);
  }

  onPayPremiumButton() {
    if (sessionStorage.getItem('asCustomer') === 'Y') {
      // Convert to number
      this.debitAmount = +this.outstanding.debitAmount;
      this.settledAmount = +this.outstanding.settledAmount;
      let loyalityPointsUtilize = this.paymentDetailForm.controls['loyalty_points'].value;
      if (loyalityPointsUtilize === null) {
        loyalityPointsUtilize = 0;
      }
      // @ts-ignore
      // tslint:disable-next-line:triple-equals
      // if (this.amountTobePaid == (this.debitAmount - this.settledAmount).toFixed(2)) {
      const t = (this.debitAmount - (this.amountTobePaid + loyalityPointsUtilize)).toFixed(2);
      if (this.balance === 0) {
        if (this.paymentDetailForm.invalid) {
          return;
        }
        const settlement = new Settlement();
        settlement.branchCode = sessionStorage.getItem('branchCode');
        settlement.debitNoteNumber = this.debitNoteNumber;
        const detail = new SettlementDetail();
        detail.debitNoteNumber = this.debitNoteNumber;
        detail.loyaltyPointsUtilization = this.loyaltyPointsSpending;
        detail.paidAmount = this.amountTobePaid;
        settlement.settlementDetail.push(detail);
        settlement.collection = this.getCollection();
        settlement.collection.currencyCode = 'LKR';
        this.submitted = '1';
        if (sessionStorage.getItem('asCustomer') === 'Y') {
          if (this.balance === 0) {
            this.service.saveSettlement(settlement).subscribe(resp => {
              this.notificationService.isMessageDisplayed = true;
              this.notificationService.messageContent = 'Payment saved successfully!';
              this.notificationService.messageType = 'success';
              this.router.navigate(['']);
              this.submitted = '0';
              const body: any = resp.body;
              this.data = body.data;

              // check premium payment amount (partial or fully payment)
              if (this.balance === 0) {
                // download pdf and redirect to policy list

                if (this.productCode === 'WHINX') {

                  // weather Individual file
                  const fileName1 = 'WeatherIndexSchedule.pdf';
                  this.service.downloadWeatherIndividualFile(this.policyNumber).subscribe(x => {
                    if (x.size === 0) {
                      return;
                    }
                    const newBlob = new Blob([x], {type: 'application/pdf'});
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                      window.navigator.msSaveOrOpenBlob(newBlob);
                      return;
                    }
                    const data = window.URL.createObjectURL(newBlob);
                    const link = document.createElement('a');
                    link.href = data;
                    link.download = fileName1;
                    // this is necessary as link.click() does not work on the latest firefox
                    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                    setTimeout(function () {
                      // For Firefox it is necessary to delay revoking the ObjectURL
                      window.URL.revokeObjectURL(data);
                      link.remove();
                    }, 100);
                  });


                  // weather Group file
                  const fileName2 = 'WeatherIndexGroupSchedule.pdf';
                  this.service.downloadWeatherGroupFile(this.policyNumber).subscribe(x => {
                    if (x.size === 0) {
                      return;
                    }
                    const newBlob = new Blob([x], {type: 'application/pdf'});
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                      window.navigator.msSaveOrOpenBlob(newBlob);
                      return;
                    }
                    const data = window.URL.createObjectURL(newBlob);
                    const link = document.createElement('a');
                    link.href = data;
                    link.download = fileName2;
                    // this is necessary as link.click() does not work on the latest firefox
                    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                    setTimeout(function () {
                      // For Firefox it is necessary to delay revoking the ObjectURL
                      window.URL.revokeObjectURL(data);
                      link.remove();
                    }, 100);
                  });
                } else if (this.productCode === 'LIVST') {
                  // Livestock individual file
                  const fileName1 = 'LivestockSchedule.pdf';
                  this.service.downloadLivestockIndividualFile(this.policyNumber).subscribe(x => {
                    if (x.size === 0) {
                      return;
                    }
                    const newBlob = new Blob([x], {type: 'application/pdf'});
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                      window.navigator.msSaveOrOpenBlob(newBlob);
                      return;
                    }
                    const data = window.URL.createObjectURL(newBlob);
                    const link = document.createElement('a');
                    link.href = data;
                    link.download = fileName1;
                    // this is necessary as link.click() does not work on the latest firefox
                    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                    setTimeout(function () {
                      // For Firefox it is necessary to delay revoking the ObjectURL
                      window.URL.revokeObjectURL(data);
                      link.remove();
                    }, 100);
                  });

                  // Livestock Group file
                  const fileName2 = 'LivestockGroupSchedule.pdf';
                  this.service.downloadLivestockGroupFile(this.policyNumber).subscribe(x => {
                    if (x.size === 0) {
                      return;
                    }
                    const newBlob = new Blob([x], {type: 'application/pdf'});
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                      window.navigator.msSaveOrOpenBlob(newBlob);
                      return;
                    }
                    const data = window.URL.createObjectURL(newBlob);
                    const link = document.createElement('a');
                    link.href = data;
                    link.download = fileName2;
                    // this is necessary as link.click() does not work on the latest firefox
                    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                    setTimeout(function () {
                      // For Firefox it is necessary to delay revoking the ObjectURL
                      window.URL.revokeObjectURL(data);
                      link.remove();
                    }, 100);
                  });
                }
              }
              // download deposit slit pdf f
              const file = 'premiumReceipt.pdf';
              this.service.premiumReceipt(this.data).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], {type: 'application/pdf'});
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = file;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });
              // download pdf and redirect to policy list
            }, error => {
              this.submitted = '0';
              this.notificationService.isMessageDisplayed = true;
              this.notificationService.messageContent = error.message;
              this.notificationService.messageType = 'error';
            });
          }
        } else {
          this.service.saveSettlement(settlement).subscribe(resp => {
            this.notificationService.isMessageDisplayed = true;
            this.notificationService.messageContent = 'Payment saved successfully!';
            this.notificationService.messageType = 'success';
            this.router.navigate(['']);
            this.submitted = '0';
            const body: any = resp.body;
            this.data = body.data;

            // check premium payment amount (partial or fully payment)
            if (this.balance === 0) {
              // download pdf and redirect to policy list

              if (this.productCode === 'WHINX') {

                // weather Individual file
                const fileName1 = 'WeatherIndexSchedule.pdf';
                this.service.downloadWeatherIndividualFile(this.policyNumber).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], {type: 'application/pdf'});
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName1;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });


                // weather Group file
                const fileName2 = 'WeatherIndexGroupSchedule.pdf';
                this.service.downloadWeatherGroupFile(this.policyNumber).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], {type: 'application/pdf'});
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName2;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
              } else if (this.productCode === 'LIVST') {
                // Livestock individual file
                const fileName1 = 'LivestockSchedule.pdf';
                this.service.downloadLivestockIndividualFile(this.policyNumber).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], {type: 'application/pdf'});
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName1;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });

                // Livestock Group file
                const fileName2 = 'LivestockGroupSchedule.pdf';
                this.service.downloadLivestockGroupFile(this.policyNumber).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], {type: 'application/pdf'});
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName2;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
              }
            }
            // download deposit slit pdf f
            const file = 'premiumReceipt.pdf';
            this.service.premiumReceipt(this.data).subscribe(x => {
              if (x.size === 0) {
                return;
              }
              const newBlob = new Blob([x], {type: 'application/pdf'});
              if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
              }
              const data = window.URL.createObjectURL(newBlob);
              const link = document.createElement('a');
              link.href = data;
              link.download = file;
              // this is necessary as link.click() does not work on the latest firefox
              link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

              setTimeout(function () {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
                link.remove();
              }, 100);
            });
            // download pdf and redirect to policy list
          }, error => {
            this.submitted = '0';
            this.notificationService.isMessageDisplayed = true;
            this.notificationService.messageContent = error.message;
            this.notificationService.messageType = 'error';
          });
        }
      } else {
        // partial payment error message
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageContent = 'Partial payments are not allowed for Up front policy payments';
        this.notificationService.messageType = 'error';
      }


    } else {
      if (this.paymentDetailForm.invalid) {
        return;
      }
      const settlement = new Settlement();
      settlement.branchCode = sessionStorage.getItem('branchCode');
      settlement.debitNoteNumber = this.debitNoteNumber;
      const detail = new SettlementDetail();
      detail.debitNoteNumber = this.debitNoteNumber;
      detail.loyaltyPointsUtilization = this.loyaltyPointsSpending;
      detail.paidAmount = this.amountTobePaid;
      settlement.settlementDetail.push(detail);
      settlement.collection = this.getCollection();
      settlement.collection.currencyCode = 'LKR';
      this.submitted = '1';
      if (sessionStorage.getItem('asCustomer') === 'Y') {
        if (this.balance === 0) {
          this.service.saveSettlement(settlement).subscribe(resp => {
            this.notificationService.isMessageDisplayed = true;
            this.notificationService.messageContent = 'Payment saved successfully!';
            this.notificationService.messageType = 'success';
            this.router.navigate(['']);
            this.submitted = '0';
            const body: any = resp.body;
            this.data = body.data;

            // check premium payment amount (partial or fully payment)
            if (this.balance === 0) {
              // download pdf and redirect to policy list

              if (this.productCode === 'WHINX') {

                // weather Individual file
                const fileName1 = 'WeatherIndexSchedule.pdf';
                this.service.downloadWeatherIndividualFile(this.policyNumber).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], {type: 'application/pdf'});
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName1;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });


                // weather Group file
                const fileName2 = 'WeatherIndexGroupSchedule.pdf';
                this.service.downloadWeatherGroupFile(this.policyNumber).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], {type: 'application/pdf'});
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName2;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
              } else if (this.productCode === 'LIVST') {
                // Livestock individual file
                const fileName1 = 'LivestockSchedule.pdf';
                this.service.downloadLivestockIndividualFile(this.policyNumber).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], {type: 'application/pdf'});
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName1;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });

                // Livestock Group file
                const fileName2 = 'LivestockGroupSchedule.pdf';
                this.service.downloadLivestockGroupFile(this.policyNumber).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], {type: 'application/pdf'});
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName2;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
              }
            }
            // download deposit slit pdf f
            const file = 'premiumReceipt.pdf';
            this.service.premiumReceipt(this.data).subscribe(x => {
              if (x.size === 0) {
                return;
              }
              const newBlob = new Blob([x], {type: 'application/pdf'});
              if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
              }
              const data = window.URL.createObjectURL(newBlob);
              const link = document.createElement('a');
              link.href = data;
              link.download = file;
              // this is necessary as link.click() does not work on the latest firefox
              link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

              setTimeout(function () {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
                link.remove();
              }, 100);
            });
            // download pdf and redirect to policy list
          }, error => {
            this.submitted = '0';
            this.notificationService.isMessageDisplayed = true;
            this.notificationService.messageContent = error.message;
            this.notificationService.messageType = 'error';
          });
        }
      } else {
        this.service.saveSettlement(settlement).subscribe(resp => {
          this.notificationService.isMessageDisplayed = true;
          this.notificationService.messageContent = 'Payment saved successfully!';
          this.notificationService.messageType = 'success';
          this.router.navigate(['']);
          this.submitted = '0';
          const body: any = resp.body;
          this.data = body.data;

          // check premium payment amount (partial or fully payment)
          if (this.balance === 0) {
            // download pdf and redirect to policy list

            if (this.productCode === 'WHINX') {

              // weather Individual file
              const fileName1 = 'WeatherIndexSchedule.pdf';
              this.service.downloadWeatherIndividualFile(this.policyNumber).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], {type: 'application/pdf'});
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = fileName1;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });


              // weather Group file
              const fileName2 = 'WeatherIndexGroupSchedule.pdf';
              this.service.downloadWeatherGroupFile(this.policyNumber).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], {type: 'application/pdf'});
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = fileName2;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });
            } else if (this.productCode === 'LIVST') {
              // Livestock individual file
              const fileName1 = 'LivestockSchedule.pdf';
              this.service.downloadLivestockIndividualFile(this.policyNumber).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], {type: 'application/pdf'});
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = fileName1;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });

              // Livestock Group file
              const fileName2 = 'LivestockGroupSchedule.pdf';
              this.service.downloadLivestockGroupFile(this.policyNumber).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], {type: 'application/pdf'});
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = fileName2;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });
            }
          }
          // download deposit slit pdf f
          const file = 'premiumReceipt.pdf';
          this.service.premiumReceipt(this.data).subscribe(x => {
            if (x.size === 0) {
              return;
            }
            const newBlob = new Blob([x], {type: 'application/pdf'});
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
            }
            const data = window.URL.createObjectURL(newBlob);
            const link = document.createElement('a');
            link.href = data;
            link.download = file;
            // this is necessary as link.click() does not work on the latest firefox
            link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

            setTimeout(function () {
              // For Firefox it is necessary to delay revoking the ObjectURL
              window.URL.revokeObjectURL(data);
              link.remove();
            }, 100);
          });
          // download pdf and redirect to policy list
        }, error => {
          this.submitted = '0';
          this.notificationService.isMessageDisplayed = true;
          this.notificationService.messageContent = error.message;
          this.notificationService.messageType = 'error';
        });
      }
    }


  }

  onBankChange(code: string) {
    this.fetchBranches(code);
  }

  checkAmount() {
    const testBalance = this.outstanding.debitAmount - this.loyaltyPointsSpending;
    const amount = this.paymentDetailForm.controls['amount_to_be_paid'].value;
    if (amount === testBalance) {
      this.paymentDetailForm.controls['amount_to_be_paid'].setValue(amount);
      this.amountTobePaid = testBalance;
    } else if (amount > testBalance) {
      this.paymentDetailForm.controls['amount_to_be_paid'].setValue(testBalance);
      this.amountTobePaid = testBalance;
    }
    this.amountTobePaid = Math.round(this.amountTobePaid * 100) / 100;
    this.updateBalance();
  }

  private fetchMobileOperators() {
    this.mobileMoneyCollection.operatorCode = null;
    this.service.getMobileOperators().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.mobile_operators = typeList;
    });
  }

  private getCollection() {
    switch (this.paymentMode) {
      case 'PAYCQ':
        this.chequeCollection.bankCode = this.paymentDetailForm.get('cheque_bank').value;
        this.chequeCollection.branchCode = this.paymentDetailForm.get('cheque_branch').value;
        return this.chequeCollection;
      case 'PAYCS':
        return this.cashCollection;
      case 'PAYBD':
        this.bankCollection.bankCode = this.paymentDetailForm.get('deposit_bank').value;
        this.bankCollection.depositDate = moment(this.paymentDetailForm.get('deposit_date').value).format('YYYY-MM-DD HH:mm:ss');
        return this.bankCollection;
      case 'PAYMM':
        this.mobileMoneyCollection.operatorCode = this.paymentDetailForm.get('mobile_operator').value;
        return this.mobileMoneyCollection;
      case 'PAYCC':
        this.cardCollection.cardExpiry = moment(this.paymentDetailForm.get('credit_card_date').value).format('MM/YYYY');
        return this.cardCollection;
    }
  }


}
