import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PolicyRoutingModule} from './policy-routing.module';
import {PoliciesComponent} from './policies/policies.component';
import {PolicyFormComponent} from './policy-form/policy-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule,
  MatOptionModule, MatSelectModule, MatIconModule, MatCalendarHeader, MatDatepicker,
  MatFormField, MatOption, MatSelect, MatIcon, MatInput, MatError, MatDialogModule, MatCardModule, MatCheckboxModule, MatAutocompleteModule
} from '@angular/material';
import {CommonComponentsModule} from '../common-components/common-components.module';
import {PaymentComponent} from './payment/payment.component';
import {CustomPipeModule} from 'src/app/pipes/CustomPipesModule';
import {MarkLandComponent} from './mark-land/mark-land.component';
import {PoliciesMainComponent} from './policies-main/policies-main.component';
import {PolicyListComponent} from './policy-list/policy-list.component';
import {AddPolicyWeatherComponent} from './add-policy-weather/add-policy-weather.component';
import {AddPolicyLivestockComponent} from './add-policy-livestock/add-policy-livestock.component';
import {AddPolicyLivestockRiskComponent} from './add-policy-livestock-risk/add-policy-livestock-risk.component';
import {AddPolicyWeatherRiskComponent} from './add-policy-weather-risk/add-policy-weather-risk.component';
import {AddPolicyLivestokRiskCoversComponent} from './add-policy-livestok-risk-covers/add-policy-livestok-risk-covers.component';
import {PolicyWeatherAdditionalDetailsComponent} from './policy-weather-additional-details/policy-weather-additional-details.component';
// tslint:disable-next-line:max-line-length
import {PolicyLivestockAdditionalDetailsComponent} from './policy-livestock-additional-details/policy-livestock-additional-details.component';
// tslint:disable-next-line:max-line-length
import {PolicyLivestockRiskAdditionalDetailsComponent} from './policy-livestock-risk-additional-details/policy-livestock-risk-additional-details.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {PolicyPopupComponent} from './policies/policy-popup/policy-popup.component';
import {AddPolicyIndemnityComponent} from './add-policy-indemnity/add-policy-indemnity.component';

@NgModule({
  declarations: [
    PoliciesComponent,
    PolicyFormComponent,
    PaymentComponent,
    MarkLandComponent,
    PoliciesMainComponent,
    PolicyListComponent,
    AddPolicyWeatherComponent,
    AddPolicyLivestockComponent,
    AddPolicyLivestockRiskComponent,
    AddPolicyWeatherRiskComponent,
    AddPolicyLivestokRiskCoversComponent,
    PolicyWeatherAdditionalDetailsComponent,
    PolicyLivestockAdditionalDetailsComponent,
    PolicyLivestockRiskAdditionalDetailsComponent,
    PolicyPopupComponent,
    AddPolicyIndemnityComponent
  ],
  imports: [
    CustomPipeModule,
    CommonModule,
    PolicyRoutingModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    CommonComponentsModule,
    InfiniteScrollModule,
    MatDialogModule,
    MatCardModule,
    MatCheckboxModule,
    MatAutocompleteModule
  ],
  exports: [
    MarkLandComponent,
    MatCalendarHeader,
    MatDatepicker,
    MatFormField,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatOption,
    MatSelect,
    MatIcon,
    MatInput,
    MatError,
  ],
  entryComponents: [
    PolicyPopupComponent
  ]
})
export class PolicyModule {
}
