import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPolicyWeatherComponent } from './add-policy-weather.component';

describe('AddPolicyWeatherComponent', () => {
  let component: AddPolicyWeatherComponent;
  let fixture: ComponentFixture<AddPolicyWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPolicyWeatherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPolicyWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
