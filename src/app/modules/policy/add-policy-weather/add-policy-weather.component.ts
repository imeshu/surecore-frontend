import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {PoliciesService} from '../services/policies.service';
import {CommonType} from '../../../model/CommonType';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-policy-weather',
  templateUrl: './add-policy-weather.component.html',
  styleUrls: ['./add-policy-weather.component.scss']
})
export class AddPolicyWeatherComponent implements OnInit {
  policyWeatherForm: FormGroup;
  private cropTypes: CommonType[];
  private weatherTypes: CommonType[];
  private plans: CommonType[];
  private businessChannels: CommonType[];
  private  paymentTypes: CommonType[];

  @Output() savePolicyEvent = new EventEmitter()
  // tslint:disable-next-line:max-line-length
  constructor(private service: PoliciesService, private fb: FormBuilder, public notificationService: NotificationDataTransferService, private router: Router) { }

  ngOnInit() {
    this.fetchCropTypes();
    this.fetchWeatherTypes();
    this.fetchBusinessChannels();
    this.fetchPaymentTypes();
    this.policyWeatherForm = this.fb.group({
      cropType : ['', Validators.required],
      weatherType : ['', Validators.required],
      plan : ['', Validators.required],
      businessChannel : ['', Validators.required],
      startDate : ['', Validators.required],
      endDate : ['', Validators.required],
      paymentType : ['', Validators.required],
      dayCount : ['']
    });
    this.policyWeatherForm.reset();
  }
  /*
  * Form Control Getters
   */
  get cropType() { return this.policyWeatherForm.get('cropType'); }
  get weatherType() { return this.policyWeatherForm.get('weatherType'); }
  get plan() { return this.policyWeatherForm.get('plan'); }
  get businessChannel() { return this.policyWeatherForm.get('businessChannel'); }
  get startDate() { return this.policyWeatherForm.get('startDate'); }
  get endDate() { return this.policyWeatherForm.get('endDate'); }
  get paymentType() { return this.policyWeatherForm.get('paymentType'); }
  /**
   * Load Crop Types
   */
  fetchCropTypes() {
    this.service.getCropTypes().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.cropTypes = typeList;
    });
  }
  /**
   * Load Weather Types
   */
  fetchWeatherTypes() {
    this.service.getWeatherTypes().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.weatherTypes = typeList;
    });
  }
  /**
   * Load Plans
   */
  fetchPlans() {
    const  cropType = this.policyWeatherForm.get('cropType').value;
    const  weatherType = this.policyWeatherForm.get('weatherType').value;
    if (cropType && weatherType) {
      this.service.getPlans('HO', cropType, weatherType).pipe(
        catchError(error => {
          return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
        })
      ).subscribe(typeList => {
        this.plans = typeList;
      });
    }
  }
  /**
   * Load Business Channels
   */
  fetchBusinessChannels() {
    this.service.getBusinessChannels().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.businessChannels = typeList;
    });
  }
  /**
   * Load Payment Types
   */
  fetchPaymentTypes() {
    this.service.getPaymentTypes().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.paymentTypes = typeList;
    });
  }
  savePolicy() {
  this.savePolicyEvent.emit(this.policyWeatherForm.value);
  }

}
