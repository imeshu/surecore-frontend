import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPolicyLivestokRiskCoversComponent } from './add-policy-livestok-risk-covers.component';

describe('AddPolicyLivestokRiskCoversComponent', () => {
  let component: AddPolicyLivestokRiskCoversComponent;
  let fixture: ComponentFixture<AddPolicyLivestokRiskCoversComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPolicyLivestokRiskCoversComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPolicyLivestokRiskCoversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
