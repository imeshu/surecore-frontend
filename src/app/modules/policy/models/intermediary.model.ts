export class Intermediary {
  intermediaryTypeCode: string;
  intermediaryCode: string;
  isBusinessParty: string;
  isCommissionParty: string;
  businessPartyshare: number;
}
