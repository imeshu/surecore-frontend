import {SettlementCollection} from './SettlementCollection';

export class ChequeCollection extends SettlementCollection{
  chequeNumber: string;
  bankCode: string;
  branchCode: string;

  constructor() {
    super('PAYCQ');
  }
}
