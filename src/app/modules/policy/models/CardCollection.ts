import {SettlementCollection} from './SettlementCollection';

export class CardCollection extends SettlementCollection {
  cardNumber: string;
  cardExpiry: string;
  cardCcv: number;

  constructor() {
    super('PAYCC');
  }
}
