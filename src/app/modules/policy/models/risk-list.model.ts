import {Risk} from './risk.model';
import {RiskCommonInformation} from './risk-common-information.model';

export class RiskList {
  risk: Risk;
  riskInformation: RiskCommonInformation[];
  location: Location;
}
