export class PolicyCommonInformation {
  sequenceNo: string;
  description: string;
  value: string;
  version: string;
}
