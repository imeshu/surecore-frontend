export class RiskCommonInformation {
  sequenceNo: string ;
  description: string;
  dataType: string;
  referenceReq: string;
  referenceType: string;
  typeList: string;
  recordType: string;
  level: string;
  value: string;
  valueDescription: string;
  imageValue: string;
  version: number;
}
