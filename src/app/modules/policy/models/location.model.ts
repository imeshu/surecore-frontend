import {Geometry} from './geometry.model';
export class Location {
  annualLimit: string;
  annualNarration: string;
  annualPercentage: string;
  cancelledAmount: string;
  deletedDate: string;
  deletedEndorsementNo: string;
  effectEndorsementNo: string;
  effectiveDate: string;
  eventLimit: string;
  eventNarration: string;
  eventPercentage: string;
  geometry: Geometry[];
  location: string;
  locationOrder: number;
  locationStatus: string;
  locationSumInsured: number;
  locPolicyNo: string;
  premium: number;
  seqNo: string;
  version: number;
  totalCancelledAmount: string;
  totalPremium: number;
  totalTransactionAmount: number;
  transactionAmount: number;
}

