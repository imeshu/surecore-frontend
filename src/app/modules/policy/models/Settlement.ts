import {SettlementDetail} from './SettlementDetail';
import {SettlementCollection} from './SettlementCollection';

export class Settlement {
  branchCode: string;
  debitNoteNumber: string;
  settlementDetail: SettlementDetail[] =  [];
  collection: SettlementCollection;
}
