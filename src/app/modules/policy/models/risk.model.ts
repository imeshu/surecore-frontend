import {Geometry} from './geometry.model';

export class Risk {
  annualLimit: string;
  annualNarration: string;
  annualPercentage: string;
  cancelledAmount: string;
  certificateNo: string;
  coverNoteNo: null;
  deletedDate: null;
  deletedEndorsementNo: null;
  effectEndorsementNo: null;
  effectiveDate: string;
  effectiveToDate: string;
  eventLimit: string;
  eventNarration: string;
  eventPercentage: string;
  geometry: Geometry[];
  mainRiskRelationship: string;
  premium: number;
  relatedMainRiskCode: string;
  riskDescription: string;
  riskName: string;
  riskOrder: number;
  riskPolicyNo: string;
  riskRefNo: string;
  riskSectionCode: string;
  riskStatus: string;
  riskSumInsured: string;
  seqNo: string;
  totalCancelledAmount: string;
  totalPremium: number;
  totalTransactionAmount: number;
  transactionAmount: number;
  size1: number;
  size2: number;
  size1UnitType: string;
  size2UnitType: string;
  version: number;
}

