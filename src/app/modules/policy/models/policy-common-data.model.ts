export class PolicyCommonData {
  policySequence: string;
  policyNumber: string;
  locationSequence: string;
  proposalNumber: string;
  riskSequence: string;
  perilSequence: string;
  productType: string;
  authorizedPolicy: boolean;
}
