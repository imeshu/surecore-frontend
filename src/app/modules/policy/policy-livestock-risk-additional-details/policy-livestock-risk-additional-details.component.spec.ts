import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyLivestockRiskAdditionalDetailsComponent } from './policy-livestock-risk-additional-details.component';

describe('PolicyLivestockRiskAdditionalDetailsComponent', () => {
  let component: PolicyLivestockRiskAdditionalDetailsComponent;
  let fixture: ComponentFixture<PolicyLivestockRiskAdditionalDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyLivestockRiskAdditionalDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyLivestockRiskAdditionalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
