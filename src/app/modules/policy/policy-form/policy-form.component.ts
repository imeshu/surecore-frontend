/* tslint:disable:no-shadowed-variable radix */
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { PolicyService } from '../services/policy.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpEventType } from '@angular/common/http';
import { ImageModel } from '../models/image-model';
import { LandModel } from '../../customer/models/land-model';
import { LandService } from '../../land/services/land.service';
import { CommonType } from 'src/app/model/CommonType';
import { LocationModel } from '../../land/models/location-model';
import { mergeMap } from 'rxjs-compat/operator/mergeMap';
import { catchError, flatMap, map, takeUntil } from 'rxjs/operators';
import { PoliciesService } from '../services/policies.service';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-policy-form',
  templateUrl: './policy-form.component.html',
  styleUrls: ['./policy-form.component.scss']
})
export class PolicyFormComponent implements OnInit, OnDestroy {

  weatherIndexParameters = [];
  weatherIndexParameter = '';
  displayWeatherIndexParametersDiv = false;

  /**
   * Disabling cultivation feilds
   */
  disabledCultivationFeilds = false;

  fromMonth = '';
  toMonth = '';
  toYear = '';
  fromYear = '';
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'];
  dynamicMonths = [];
  years = ['2019', '2020'];

  fromMonths = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'];
  toMonths = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'];

  fromYears = ['2019', '2020'];
  toYears = ['2019', '2020'];

  policyType = 'weather';
  isDataLoaded = true;
  proposalNumber = '';
  weatherDayCount = 0;
  livestockDayCount = 0;

  // common variables
  branchCode = '';
  policySequnce = '';
  partyCode = '';
  asCustomer = '';
  asInternalStaff = '';
  asAgent = '';
  addressSequnce = '';
  locationSequence = '';
  intermediaryType = '';
  private productName;

  // Intermediary Types
  intermediaryTypes = [];
  intermediaryData = [];
  intermediaryFilteredList = [];
  intermediarySequence = '';
  public intermediaryTypeCode = '';
  public intermediaryTypeName = '';

  // views
  displayWeatherAdditionalDetails = false;
  displayWeatherLivestockForm = true;
  displayAddLandForm = false;
  displayAddLivestockForm = false;
  displayLivestockAdditionalDetails = false;
  displayAddCoversForm = false;

  // weather form
  // Crop Types
  cropTypes = [];
  cropType = '';
  // Weather Type
  weatherTypes = [];
  weatherType = '';
  // Plans
  plans = [];
  plan = '';
  // Business Chanels
  businessChanels = [];
  businessChanel = '';
  // payment types
  paymentTypes = [];
  paymentType = '';

  // weather form form-controls
  cropTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  weatherTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  planFormControl = new FormControl('', [
    Validators.required
  ]);
  businessChanelFormControl = new FormControl('', [
    Validators.required
  ]);
  paymentFormControl = new FormControl('', [
    Validators.required
  ]);
  startDateFormControl = new FormControl('', [
    Validators.required
  ]);
  endDateFormControl = new FormControl('', [
    Validators.required
  ]);
  // Policy
  classCode = '';
  currencyCode = '';
  modeOfBusinessCode = '';
  settlementModeCode = '';
  policyRemark = '';
  productCode = '';
  startDate = moment();
  endDate = moment();

  maxDate1 = new Date(moment().subtract(1, 'days').format());
  maxDate2 = new Date(moment().format());
  minDate = new Date();
  endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());

  // Weather Type
  weatherSequenceNo = '';
  weatherDescription = '';
  weatherVersion = '';
  weatherValue = '';

  // Crop Type
  cropTypeSequence = '';
  cropTypeDescription = '';
  cropTypeVersion = '';
  cropTypeValue = '';


  // Livestock form
  // products
  products = [];
  product = '';
  // business channels
  liveStickBusinessChannels = [];
  liveStickBusinessChannel = '';
  // premium settlement methods
  premiumSettlementMethods = [];
  premiumSettlementMethod = '';

  liveStockStartDate = null;
  liveStockEndDate = null;

  livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());


  // Livestock form controls
  liveStockPlanFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStickBusinessChannelFormControl = new FormControl('', [
    Validators.required
  ]);
  premiumSettlementMethodFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStockStartDateFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStockEndDateFormControl = new FormControl('', [
    Validators.required
  ]);

  // policy additional details
  risks = [];
  images = [];
  totalPremium = '';

  // policy weather land form related variables
  locations = [];
  location = {};

  cropVariances = [];
  cropVariance = '';

  weatherStations = [];
  weatherStation = 0;

  weatherLandPhotos = [];


  // location object
  locationAddress = '';
  locationOrderNo = 1;


  // risk
  riskOrder = 1;
  riskName = '';
  riskRefNo = 1;
  riskSectionCode = '';
  riskSumInsured = '';
  effectiveDate = '';
  effectiveToDate = '';
  size1 = '';
  size1UnitType = 'UNAPR';
  size2 = '';
  size2UnitType = 'UNAAC';
  price = '';
  noOfUnits = '';
  weatherLandStartDate = null;
  weatherLandEndDate = null;
  farmerName = '';
  weatherLandPremium = null;
  markLandPlot = [];
  markLandPlotString = '';

  // risk sequence
  riskSequnce = '';

  // crop variance
  cropVarianceSequence = '';
  cropVarianceDescription = '';
  cropVarianceVersion = '';
  cropVarianceValue = '';

  // weather stations
  weatherStationSequence = '';
  weatherStationDescription = '';
  weatherStationVersion = '';
  weatherStationValue = '';

  // no of unsits
  noOfUnitsSequence = '';
  noOfUnitsDescription = '';
  noOfUnitsVersion = '';
  noOfUnitsValue = '';


  farmerNameSequnce = '';
  farmerNameDescription = '';
  farmerNameVersion = '';
  farmerNameValue = '';


  // policy weather land form controls
  locationFormControl = new FormControl('', [Validators.required]);
  cropVarianceFormControl = new FormControl('', [Validators.required]);
  landWeatherStationFormControl = new FormControl('', [Validators.required]);
  farmerNameFormControl = new FormControl('', [Validators.required]);
  weatherLandStartDateFormControl = new FormControl('', [Validators.required]);
  weatherLandEndDateFormControl = new FormControl('', [Validators.required]);
  size1UnitTypeFormControl = new FormControl('', [Validators.required]);
  size1FormControl = new FormControl('', [Validators.required]);
  markLandPlotFormControl = new FormControl('', [Validators.required]);
  noOfunitsFormControl = new FormControl('', [Validators.required]);
  priceFormControl = new FormControl('', [Validators.required]);


  // add live stock form
  newLivestockStartDate = null;
  // live stock types
  liveStockTypes = [];
  liveStockType = '';

  // Breed types
  breedTypes = [];
  breedType = '';

  // genders
  genders = [];
  gender = '';

  // usage types
  usageTypes = [];
  usageType = '';

  // undewriting assessors
  undewritingAssessors = [];
  undewritingAssessor = '';


  // ages = ['2 Years', '3 Years', '4 Years', '5 Years'];
  age = '';

  tagNumber = '';
  ownerName = '';
  livestockPurchaseDate = null;
  livestockRiskSumInsured = '';

  // Live stock
  liveStockTypeSequnce = '';
  liveStockTypeDescription = '';
  liveStockTypeVersion = '';
  liveStockTypeValue = '';

  // Breed
  breedSequence = '';
  breedDescription = '';
  breedVersion = '';
  breedValue = '';

  // gender
  genderSequence = '';
  genderDescription = '';
  genderVersion = '';
  genderValue = '';

  // usage
  usageSequence = '';
  usageDescription = '';
  usageVersion = '';
  usageValue = '';

  //  age in years
  ageInYearsSequence = '';
  ageInYearsDescription = '';
  ageInYearsVersion = '';
  ageInYearsValue = this.age;

  // owner name
  ownerNameSequence = '';
  ownerNameDescription = '';
  ownerNameVersion = '';
  ownerNameValue = '';

  // purchase date
  purchaseDateSequence = '';
  purchaseDateDescription = '';
  purchaseDateVersion = '';
  purchaseDateValue = null;


  tagNumberFormControl = new FormControl('', [Validators.required]);
  breedTypeFormControl = new FormControl('', [Validators.required]);
  liveStockTypeFormControl = new FormControl('', [Validators.required]);
  genderTypeFormControl = new FormControl('', [Validators.required]);
  ageTypeFormControl = new FormControl('', [Validators.required]);
  ownerNameFormControl = new FormControl('', [Validators.required]);
  usageTypeFormControl = new FormControl('', [Validators.required]);
  undewritingAssessorFormControl = new FormControl('', [Validators.required]);
  newLivestockStartDateFormControl = new FormControl('', [Validators.required]);
  riskSumInsuredFormControl = new FormControl('', [Validators.required]);

  /**
   *  covers / Perils
   */
  otherPerils = [];
  otherPeril = {};

  covers = [];
  riskImages = [];

  perilCode = '';
  perilPerilSumInsured = 0;
  perilPercentage = 0;
  perilRate = 0;
  managePerilDescription = '';

  perilSequnce = '';

  excessAmount = 0;
  excessPercentage = 0;
  riskPremium = '';


  otherPerilFormControl = new FormControl('', [Validators.required]);
  perilPerilSumInsuredFormControl = new FormControl('', [Validators.required]);
  excessPercentageFormControl = new FormControl('', [Validators.required]);
  excessAmountFormControl = new FormControl('', [Validators.required]);

  parentPage = '';

  // authorized variables
  authorizedPolicy = false;

  productType = '';

  customerCode = '';
  finishedPolicyStep1 = false;
  finishedPolicyStep2 = false;

  policyFormActivated = true;
  policyAdditionalDetailsActivated = false;


  disabledLocation = false;
  displayPayment = false;


  manageCover = true;

  documentSequnce = '';

  // selected value
  selectedValue = '';

  // Add land page hide and show variables
  viewAddLand = true;
  viewAddCultivation = true;
  viewAddLandPhotos = true;


  debitNoteNumber = '';

  private progressBar: string;
  private imagList: ImageModel[] = new Array();


  enableContinueButtonWeatherLand = true;
  /**
   *
   *  policy weather land
   */
  landEdit = true;

  manageLivestockRisk = false;

  // Land
  private districtsList: CommonType[] = new Array();
  private divisionsList: CommonType[] = new Array();
  private gramasevaDivisionsList: CommonType[] = new Array();
  private getagrarianServiceCentersList: CommonType[] = new Array();
  private ownershipTypesList: CommonType[] = new Array();

  private selectedDistrict: CommonType;
  private selectedDivision: CommonType;
  private selectedGramasevaDivision: CommonType;
  private selectedgetagrarianServiceCenter: string;
  private selectedOwnershipType: string;
  private sendLandData: LandModel = new LandModel();
  private isDrawMap = false;
  private pathList: LocationModel[] = new Array();
  private pathString = '';

  private isContinue = false;
  panelOpenState = true;
  // imag upload
  private landSequence = '';
  private docSequence: string;

  // routing
  private routerPartyCode: string;
  private routeString: string;
  isCustomer: boolean;
  landForm = new FormGroup({
    plotName: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    division: new FormControl('', Validators.required),
    gramasevaDivision: new FormControl('', Validators.required),
    agrarianServiceCenter: new FormControl('', Validators.required),
    sizeInAcres: new FormControl('', Validators.required),
    sizeInPerches: new FormControl('', [Validators.required, Validators.max(159)]),
    ownership: new FormControl('', Validators.required),
    geometry: new FormControl('', Validators.required),
  });

  // Indemnity
  isFormValid: boolean;
  indemnityPolicyDetails: any;
  displayIndemnityAdditionalDetails = false;
  showIndemnityAddLandsForm = false;
  indemnityAddLandForm: FormGroup;
  indemnityLandStarDateMin: Date;
  indemnityLandEndDateMin: Date;
  indemnityLandMaxDate: Date;
  indemnityPlansList: CommonType[];
  indemnityCropTypes: CommonType[];
  indemnityCrops: CommonType[];

  private componentDestroyed: Subject<any>;

  authorizedDate = null;

  constructor(
    private service: PolicyService,
    private landService: LandService,
    private policiesService: PoliciesService,
    private notificationService: NotificationDataTransferService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    if (this.router.url.includes('/policy')) {
      this.customerCode = this.route.snapshot.paramMap.get('customerCode');
      this.parentPage = this.route.snapshot.paramMap.get('parent');
      this.service.getcustomerDetails(this.customerCode).subscribe(resp => {
        const body = resp.body as any;
        this.addressSequnce = body.data.address.sequence;
      });
    }

    this.componentDestroyed = new Subject();
  }

  ngOnInit() {
    if (sessionStorage.getItem('asCustomer') === 'Y') {
      this.isCustomer = true;
    }
    this.isDataLoaded = false;
    // get party information form session storage
    this.partyCode = sessionStorage.getItem('partyCode');
    this.asCustomer = sessionStorage.getItem('asCustomer');
    this.asInternalStaff = sessionStorage.getItem('partyAsInternalStaff') ? sessionStorage.getItem('partyAsInternalStaff') : 'N';
    this.asAgent = sessionStorage.getItem('asAgent');
    this.branchCode = sessionStorage.getItem('branchCode');
    this.intermediaryType = sessionStorage.getItem('intermediaryType');

    this.indemnityAddLandForm = this.fb.group({
      indemnityFarmerName: ['', Validators.required],
      indemnityCultivationCrop: ['', Validators.required],
      indemnityCultivationCropType: ['', Validators.required],
      indemnityCropVariance: ['', Validators.required],
      indemnitySumInsured: ['', Validators.required],
      indemnityLandStartDate: ['', Validators.required],
      indemnityLandEndDate: ['', Validators.required],
      indemnityAcres: ['', Validators.required],
      indemnityPerches: ['', Validators.required],
      indemnityPlot: ['', Validators.required]
    });

    // get weather form lovs
    this.getBusinessChanels();
    this.getCropTypes();
    this.getPaymentTypes();
    this.getWeatherTypes();

    // get livestock form lovs
    this.loadproducts();
    this.loadBusinessChannels();
    this.loadSettlementMethods();
    this.clearCommonPlaces();

    // intermediary data
    this.getIntermediaryTypes();

    this.indemnityCultivationCropType.valueChanges
      .pipe(takeUntil(this.componentDestroyed)).subscribe(() => {
        this.getIndemnityCrops(this.indemnityCultivationCropType.value);
      });

    this.indemnityCultivationCrop.valueChanges
      .pipe(takeUntil(this.componentDestroyed)).subscribe(() => {
        this.getCropVariance(this.indemnityCultivationCrop.value);
      });
  }

  // Form Getters
  get indemnityCultivationCrop() {
    return this.indemnityAddLandForm.get('indemnityCultivationCrop');
  }

  get indemnityCultivationCropType() {
    return this.indemnityAddLandForm.get('indemnityCultivationCropType');
  }

  get indemnityFarmerName() {
    return this.indemnityAddLandForm.get('indemnityFarmerName');
  }

  get indemnityCropVariance() {
    return this.indemnityAddLandForm.get('indemnityCropVariance');
  }

  get indemnitySumInsured() {
    return this.indemnityAddLandForm.get('indemnitySumInsured');
  }

  get indemnityLandStartDate() {
    return this.indemnityAddLandForm.get('indemnityLandStartDate');
  }

  get indemnityLandEndDate() {
    return this.indemnityAddLandForm.get('indemnityLandEndDate');
  }

  get indemnityAcres() {
    return this.indemnityAddLandForm.get('indemnityAcres');
  }

  get indemnityPerches() {
    return this.indemnityAddLandForm.get('indemnityPerches');
  }

  get indemnityPlot() {
    return this.indemnityAddLandForm.get('indemnityPlot');
  }


  /** get intermediary types   */
  getIntermediaryTypes() {
    this.service.getIntermediaryTypes().subscribe(resp => {
      const body = resp.body as any;
      const arry = body.data;
      this.intermediaryTypes = arry.filter(itm => {
        return itm.code !== 'DIRCT';
      });
    });
  }

  /** get intermediary data */
  getIntermediaryData(value: string) {
    this.intermediaryTypeCode = value;
    this.getIntermediarySequenceNo();
    this.service.getIntermediaryTypeData(value).subscribe(resp => {
      const body = resp.body as any;
      this.intermediaryData = body.data;
      this.intermediaryFilteredList = body.data;
    });
  }

  /** Intermediary Name search */
  searchIntermediaryName(event) {
    const text: string = event.target.value;
    this.intermediaryFilteredList = this.intermediaryData.filter(item => {
      return item.description.toUpperCase().match(text.toUpperCase());
    });

  }

  getIntermediarySequenceNo() {
    this.service.getIntermediarySequence(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.intermediarySequence = body.data[0].sequenceNo;
    });
  }

  selectIntermediaryName(event) {
    this.intermediaryTypeName = event;
  }

  // Crop Types
  getCropTypes() {
    this.service.getCropTypes().subscribe(resp => {
      const body = resp.body as any;
      this.cropTypes = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getIndemnityCropTypes() {
    this.service.getIndemnityCropTypes().subscribe((res: any) => {
      this.indemnityCropTypes = res.body.data;
    }, err => {
      this.indemnityCropTypes = [];
    });
  }

  getIndemnityCrops(cropTypeCode: string) {
    this.service.getIndemnityCrops(cropTypeCode).subscribe((res: any) => {
      this.indemnityCrops = res.body.data;
    }, err => {
      this.indemnityCrops = [];
    });
  }

  getWeatherTypes() {
    this.service.getWeatherTypes().subscribe(resp => {
      const body = resp.body as any;
      this.weatherTypes = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getPlans() {
    const branchCode = this.branchCode;
    this.plans = [];
    if (this.cropType !== '' && this.weatherType !== '') {
      this.service.getPlans(branchCode, this.cropType, this.weatherType).subscribe(resp => {
        const body = resp.body as any;
        this.plans = body.data;
      }, error => {
        this.displayErrorMessage(error);
      });
    }
  }

  getBusinessChanels() {
    this.service.getBusinessChanels().subscribe(resp => {
      const body = resp.body as any;
      this.businessChanels = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getPaymentTypes() {
    this.service.getPaymentTypes().subscribe(resp => {
      const body = resp.body as any;
      if (this.asCustomer === 'Y') {
        for (let i = 0; i < body.data.length; i++) {
          if (body.data[i]['code'] === 'UPFRN') {
            this.paymentTypes.push(body.data[i]);
          }
        }
      } else {
        this.paymentTypes = body.data;
      }
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  saveWeather() {
    // let  startDateT2 = moment([year, month - 1]);
    this.isDataLoaded = true;
    // get class codeend
    this.service.getClassCode(this.plan).subscribe(resp => {
      const body1 = resp.body as any;
      this.classCode = body1.data.code;
      // get currency code
      this.service.getCurrencyCode().subscribe(resp => {
        const body3 = resp.body as any;
        this.currencyCode = body3.data;
        // Mode of Business of the policy -
        if (this.asCustomer === 'Y') {
          this.service.getModeOfBusinessCustomer().subscribe(resp => {
            const body2 = resp.body as any;
            this.modeOfBusinessCode = body2.data;
            // tslint:disable-next-line:radix
            const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
            // @ts-ignore
            const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);
            // tslint:disable-next-line:radix
            const selectedToMonth = parseInt(moment().month(this.toMonth).format('M'));
            let selectedEndDate = moment([this.toYear, selectedToMonth - 1]);
            selectedEndDate = moment(selectedEndDate).endOf('month');

            // @ts-ignore
            this.startDate = new Date(selectedStartDate);
            // @ts-ignore
            this.endDate = new Date(selectedEndDate);

            const policy = {
              'branchCode': this.branchCode,
              'channelOfBuinessCode': this.businessChanel,
              'classCode': this.classCode,
              'currencyCode': this.currencyCode,
              'customerAddressSequence': this.addressSequnce,
              'customerCode': this.customerCode,
              'modeOfBusinessCode': this.modeOfBusinessCode,
              'settlementModeCode': this.paymentType,
              'periodFromDate': moment(selectedStartDate).format('YYYY-MM-DD HH:mm:ss'),
              'periodToDate': moment(selectedEndDate).format('YYYY-MM-DD HH:mm:ss'),
              'policyRemark': this.policyRemark,
              'productCode': this.plan
            };
            this.service.savePolicy(policy).subscribe(resp => {
              const body = resp.body as any;
              this.policySequnce = body.data;
              // get policy function
              this.service.getPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
                const body4 = resp.body as any;
                const data = body4.data;

                // filter weather type and crop type
                for (let i = 0; i < data.length; i++) {
                  const obj = data[i];
                  if (obj['description'] === 'CROP') {
                    this.cropTypeSequence = obj['sequenceNo'];
                    this.cropTypeVersion = obj['version'];
                    this.cropTypeDescription = obj['description'];
                  }
                  if (obj['description'] === 'WEATHER TYPE') {
                    this.weatherSequenceNo = obj['sequenceNo'];
                    this.weatherVersion = obj['version'];
                    this.weatherDescription = obj['description'];
                  }
                }

                // update function
                const updated_josn = [{
                  'sequenceNo': this.weatherSequenceNo,
                  'description': this.weatherDescription,
                  'value': this.weatherType,
                  'version': this.weatherVersion
                }, {
                  'sequenceNo': this.cropTypeSequence,
                  'description': this.cropTypeDescription,
                  'value': this.cropType,
                  'version': this.cropTypeVersion
                }];
                this.service.updatePolicy(this.policySequnce, updated_josn).subscribe(resp => {
                  const body5 = resp.body as any;
                  const intermediaryTypeObj = {
                    'intermediaryTypeCode': this.intermediaryType,
                    'intermediaryCode': this.branchCode,
                    'isBusinessParty': 'Y',
                    'isCommissionParty': 'N',
                    'businessPartyshare': 100
                  };

                  this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj).subscribe(resp => {
                    const body6 = resp.body as any;
                    this.displaySuccessMessage(body6);
                    this.getpolicyDetails();
                    this.displayWeatherAdditionalDetails = true;
                    this.displayWeatherLivestockForm = false;
                    this.policyFormActivated = true;
                    this.policyAdditionalDetailsActivated = true;
                    this.finishedPolicyStep1 = true;
                    this.isDataLoaded = false;
                  }, error => {
                    this.isDataLoaded = false;
                    this.displayErrorMessage(error);
                  });
                  //   this.router.navigate(['/policies']);
                }, error => {
                  this.isDataLoaded = false;
                  this.displayErrorMessage(error);
                });
              }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });

          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        } else if (this.asAgent === 'Y') {
          this.service.getModeOfBusinessAgent().subscribe(resp => {
            const body2 = resp.body as any;
            this.modeOfBusinessCode = body2.data.profile.intermediaryType;
            //  set dates
            // tslint:disable-next-line:radix
            const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
            // @ts-ignore
            const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);
            // tslint:disable-next-line:radix
            const selectedToMonth = parseInt(moment().month(this.toMonth).format('M'));
            let selectedEndDate = moment([this.toYear, selectedToMonth - 1]);
            selectedEndDate = moment(selectedEndDate).endOf('month');

            // @ts-ignore
            this.startDate = new Date(selectedStartDate);
            // @ts-ignore
            this.endDate = new Date(selectedEndDate);

            // use customer code when use agent
            const policy = {
              'branchCode': this.branchCode,
              'channelOfBuinessCode': this.businessChanel,
              'classCode': this.classCode,
              'currencyCode': this.currencyCode,
              'customerAddressSequence': this.addressSequnce,
              'customerCode': this.customerCode,
              'modeOfBusinessCode': this.modeOfBusinessCode,
              'settlementModeCode': this.paymentType,
              'periodFromDate': moment(selectedFromMonth).format('YYYY-MM-DD HH:mm:ss'),
              'periodToDate': moment(selectedEndDate).format('YYYY-MM-DD HH:mm:ss'),
              'policyRemark': this.policyRemark,
              'productCode': this.plan
            };
            this.service.savePolicy(policy).subscribe(resp => {
              const body = resp.body as any;
              this.policySequnce = body.data;
              // get policy function
              this.service.getPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
                const body4 = resp.body as any;
                const data = body4.data;
                // filter weather type and crop type
                for (let i = 0; i < data.length; i++) {
                  const obj = data[i];
                  if (obj['description'] === 'CROP') {
                    this.cropTypeSequence = obj['sequenceNo'];
                    this.cropTypeVersion = obj['version'];
                    this.cropTypeDescription = obj['description'];
                  }
                  if (obj['description'] === 'WEATHER TYPE') {
                    this.weatherSequenceNo = obj['sequenceNo'];
                    this.weatherVersion = obj['version'];
                    this.weatherDescription = obj['description'];
                  }
                }
                // update function
                const updated_josn = [{
                  'sequenceNo': this.weatherSequenceNo,
                  'description': this.weatherDescription,
                  'value': this.weatherType,
                  'version': this.weatherVersion
                }, {
                  'sequenceNo': this.cropTypeSequence,
                  'description': this.cropTypeDescription,
                  'value': this.cropType,
                  'version': this.cropTypeVersion
                }];
                this.service.updatePolicy(this.policySequnce, updated_josn).subscribe(resp => {
                  const body5 = resp.body as any;
                  const intermediaryTypeObj = {
                    'intermediaryTypeCode': this.intermediaryType,
                    'intermediaryCode': this.partyCode,
                    'isBusinessParty': 'Y',
                    'isCommissionParty': 'Y',
                    'businessPartyshare': 100
                  };
                  this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj).subscribe(resp => {
                    const body6 = resp.body as any;
                    this.displaySuccessMessage(body6);
                    this.getpolicyDetails();
                    this.displayWeatherAdditionalDetails = true;
                    this.displayWeatherLivestockForm = false;
                    this.finishedPolicyStep1 = true;
                    this.policyFormActivated = true;
                    this.policyAdditionalDetailsActivated = true;
                    this.isDataLoaded = false;
                  }, error => {
                    this.isDataLoaded = false;
                    this.displayErrorMessage(error);
                  });
                  //  this.router.navigate(['/policies']);
                }, error => {
                  this.isDataLoaded = false;
                  this.displayErrorMessage(error);
                });
              }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });
              // update function
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        } else if (this.asInternalStaff === 'Y') {

          if (this.intermediaryTypeCode && this.intermediaryTypeName) {

            this.service.getModeOfBusinessAgent().subscribe(resp => {
              const body2 = resp.body as any;
              this.modeOfBusinessCode = body2.data;
              // tslint:disable-next-line:radix
              const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
              // @ts-ignore
              const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);
              // tslint:disable-next-line:radix
              const selectedToMonth = parseInt(moment().month(this.toMonth).format('M'));
              let selectedEndDate = moment([this.toYear, selectedToMonth - 1]);
              selectedEndDate = moment(selectedEndDate).endOf('month');

              // @ts-ignore
              this.startDate = new Date(selectedStartDate);
              // @ts-ignore
              this.endDate = new Date(selectedEndDate);

              const policy = {
                'branchCode': this.branchCode,
                'channelOfBuinessCode': this.businessChanel,
                'classCode': this.classCode,
                'currencyCode': this.currencyCode,
                'customerAddressSequence': this.addressSequnce,
                'customerCode': this.customerCode,
                'modeOfBusinessCode': this.intermediaryTypeCode,
                'settlementModeCode': this.paymentType,
                'periodFromDate': moment(selectedStartDate).format('YYYY-MM-DD HH:mm:ss'),
                'periodToDate': moment(selectedEndDate).format('YYYY-MM-DD HH:mm:ss'),
                'policyRemark': this.policyRemark,
                'productCode': this.plan
              };
              this.service.savePolicy(policy).subscribe(resp => {
                const body = resp.body as any;
                this.policySequnce = body.data;
                // get policy function
                this.service.getPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
                  const body4 = resp.body as any;
                  const data = body4.data;

                  // filter weather type and crop type
                  for (let i = 0; i < data.length; i++) {
                    const obj = data[i];
                    if (obj['description'] === 'CROP') {
                      this.cropTypeSequence = obj['sequenceNo'];
                      this.cropTypeVersion = obj['version'];
                      this.cropTypeDescription = obj['description'];
                    }
                    if (obj['description'] === 'WEATHER TYPE') {
                      this.weatherSequenceNo = obj['sequenceNo'];
                      this.weatherVersion = obj['version'];
                      this.weatherDescription = obj['description'];
                    }
                  }

                  // update function
                  const updated_josn = [{
                    'sequenceNo': this.weatherSequenceNo,
                    'description': this.weatherDescription,
                    'value': this.weatherType,
                    'version': this.weatherVersion
                  }, {
                    'sequenceNo': this.cropTypeSequence,
                    'description': this.cropTypeDescription,
                    'value': this.cropType,
                    'version': this.cropTypeVersion
                  }];
                  this.service.updatePolicy(this.policySequnce, updated_josn).subscribe(resp => {
                    const body5 = resp.body as any;
                    let intermediaryTypeObj = {};
                    if (this.intermediaryTypeCode && this.intermediaryTypeName) {
                      intermediaryTypeObj = {
                        'intermediaryTypeCode': this.intermediaryTypeCode,
                        'intermediaryCode': this.intermediaryTypeName,
                        'isBusinessParty': 'Y',
                        'isCommissionParty': 'N',
                        'businessPartyshare': 100
                      };
                    } else {
                      intermediaryTypeObj = {
                        'intermediaryTypeCode': 'DIRCT',
                        'intermediaryCode': this.branchCode,
                        'isBusinessParty': 'Y',
                        'isCommissionParty': 'N',
                        'businessPartyshare': 100
                      };
                    }

                    this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj).subscribe(resp => {
                      const body6 = resp.body as any;
                      this.displaySuccessMessage(body6);
                      this.getpolicyDetails();
                      this.displayWeatherAdditionalDetails = true;
                      this.displayWeatherLivestockForm = false;
                      this.policyFormActivated = true;
                      this.policyAdditionalDetailsActivated = true;
                      this.finishedPolicyStep1 = true;
                      this.isDataLoaded = false;
                    }, error => {
                      this.isDataLoaded = false;
                      this.displayErrorMessage(error);
                    });
                    //   this.router.navigate(['/policies']);
                  }, error => {
                    this.isDataLoaded = false;
                    this.displayErrorMessage(error);
                  });
                }, error => {
                  this.isDataLoaded = false;
                  this.displayErrorMessage(error);
                });
              }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });

            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          } else {

            this.service.getModeOfBusinessCustomer().subscribe(resp => {
              const body2 = resp.body as any;
              this.modeOfBusinessCode = body2.data;
              // tslint:disable-next-line:radix
              const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
              // @ts-ignore
              const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);
              // tslint:disable-next-line:radix
              const selectedToMonth = parseInt(moment().month(this.toMonth).format('M'));
              let selectedEndDate = moment([this.toYear, selectedToMonth - 1]);
              selectedEndDate = moment(selectedEndDate).endOf('month');

              // @ts-ignore
              this.startDate = new Date(selectedStartDate);
              // @ts-ignore
              this.endDate = new Date(selectedEndDate);

              const policy = {
                'branchCode': this.branchCode,
                'channelOfBuinessCode': this.businessChanel,
                'classCode': this.classCode,
                'currencyCode': this.currencyCode,
                'customerAddressSequence': this.addressSequnce,
                'customerCode': this.customerCode,
                'modeOfBusinessCode':  this.intermediaryType,
                'settlementModeCode': this.paymentType,
                'periodFromDate': moment(selectedStartDate).format('YYYY-MM-DD HH:mm:ss'),
                'periodToDate': moment(selectedEndDate).format('YYYY-MM-DD HH:mm:ss'),
                'policyRemark': this.policyRemark,
                'productCode': this.plan
              };
              this.service.savePolicy(policy).subscribe(resp => {
                const body = resp.body as any;
                this.policySequnce = body.data;
                // get policy function
                this.service.getPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
                  const body4 = resp.body as any;
                  const data = body4.data;

                  // filter weather type and crop type
                  for (let i = 0; i < data.length; i++) {
                    const obj = data[i];
                    if (obj['description'] === 'CROP') {
                      this.cropTypeSequence = obj['sequenceNo'];
                      this.cropTypeVersion = obj['version'];
                      this.cropTypeDescription = obj['description'];
                    }
                    if (obj['description'] === 'WEATHER TYPE') {
                      this.weatherSequenceNo = obj['sequenceNo'];
                      this.weatherVersion = obj['version'];
                      this.weatherDescription = obj['description'];
                    }
                  }

                  // update function
                  const updated_josn = [{
                    'sequenceNo': this.weatherSequenceNo,
                    'description': this.weatherDescription,
                    'value': this.weatherType,
                    'version': this.weatherVersion
                  }, {
                    'sequenceNo': this.cropTypeSequence,
                    'description': this.cropTypeDescription,
                    'value': this.cropType,
                    'version': this.cropTypeVersion
                  }];
                  this.service.updatePolicy(this.policySequnce, updated_josn).subscribe(resp => {
                    const body5 = resp.body as any;
                    let intermediaryTypeObj = {};
                    if (this.intermediaryTypeCode && this.intermediaryTypeName) {
                      intermediaryTypeObj = {
                        'intermediaryTypeCode': this.intermediaryTypeCode,
                        'intermediaryCode': this.intermediaryTypeName,
                        'isBusinessParty': 'Y',
                        'isCommissionParty': 'N',
                        'businessPartyshare': 100
                      };
                    } else {
                      intermediaryTypeObj = {
                        'intermediaryTypeCode': 'DIRCT',
                        'intermediaryCode': this.branchCode,
                        'isBusinessParty': 'Y',
                        'isCommissionParty': 'N',
                        'businessPartyshare': 100
                      };
                    }

                    this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj).subscribe(resp => {
                      const body6 = resp.body as any;
                      this.displaySuccessMessage(body6);
                      this.getpolicyDetails();
                      this.displayWeatherAdditionalDetails = true;
                      this.displayWeatherLivestockForm = false;
                      this.policyFormActivated = true;
                      this.policyAdditionalDetailsActivated = true;
                      this.finishedPolicyStep1 = true;
                      this.isDataLoaded = false;
                    }, error => {
                      this.isDataLoaded = false;
                      this.displayErrorMessage(error);
                    });
                    //   this.router.navigate(['/policies']);
                  }, error => {
                    this.isDataLoaded = false;
                    this.displayErrorMessage(error);
                  });
                }, error => {
                  this.isDataLoaded = false;
                  this.displayErrorMessage(error);
                });
              }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });

            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }


        }
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  // *** Livestock functions
  loadproducts() {
    this.service.getProducts(this.branchCode).subscribe(resp => {
      const body = resp.body as any;
      this.products = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  loadBusinessChannels() {
    this.service.getBusinessChanels().subscribe(resp => {
      const body = resp.body as any;
      this.liveStickBusinessChannels = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  loadSettlementMethods() {
    this.service.getPremiumSettlementMethods().subscribe(resp => {
      const body = resp.body as any;
      if (this.asCustomer === 'Y') {
        for (let i = 0; i < body.data.length; i++) {
          if (body.data[i]['code'] === 'UPFRN') {
            this.premiumSettlementMethods.push(body.data[i]);
          }
        }
      } else {
        this.premiumSettlementMethods = body.data;
      }

    }, error => {
      this.displayErrorMessage(error);
    });
  }

  displayWeatherLivestockAdditionalDetails() {
    this.clearCommonPlaces();
    this.getpolicyDetails();
    this.displayWeatherAdditionalDetails = true;
    this.displayWeatherLivestockForm = false;
    this.displayAddLivestockForm = false;
    this.displayLivestockAdditionalDetails = false;
  }

  saveLivestock() {
    this.isDataLoaded = true;
    // get class code
    this.service.getClassCode(this.product).subscribe(resp => {
      const body1 = resp.body as any;
      this.classCode = body1.data.code;
      // get currency code
      // tslint:disable-next-line:no-shadowed-variable
      this.service.getCurrencyCode().subscribe(resp => {
        const body3 = resp.body as any;
        this.currencyCode = body3.data;
        // Mode of Business of the policy -
        if (this.asCustomer === 'Y') {
          this.service.getModeOfBusinessCustomer().subscribe(resp => {
            const body2 = resp.body as any;
            this.modeOfBusinessCode = body2.data;
            const policy = {
              'branchCode': this.branchCode,
              'channelOfBuinessCode': this.liveStickBusinessChannel,
              'classCode': this.classCode,
              'currencyCode': this.currencyCode,
              'customerAddressSequence': this.addressSequnce,
              'customerCode': this.customerCode,
              'modeOfBusinessCode': this.modeOfBusinessCode,
              'settlementModeCode': this.premiumSettlementMethod,
              'periodFromDate': moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
              'periodToDate': moment(this.liveStockEndDate).format('YYYY-MM-DD HH:mm:ss'),
              'policyRemark': this.policyRemark,
              'productCode': this.product
            };
            this.service.savePolicy(policy).subscribe(resp => {
              const body = resp.body as any;
              this.policySequnce = body.data;
              // add default location
           /*   this.service.saveDefaultLocation(this.policySequnce).subscribe(resp => {
                const body = resp.body as any;
                this.locationSequence = body.data;*/

                const intermediaryTypeObj = {
                  'intermediaryTypeCode': this.intermediaryType,
                  'intermediaryCode': this.branchCode,
                  'isBusinessParty': 'Y',
                  'isCommissionParty': 'N',
                  'businessPartyshare': 100
                };
                this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj).subscribe(resp => {
                  const body6 = resp.body as any;
                  this.getpolicyDetails();
                  this.displaySuccessMessage(body);
                  this.finishedPolicyStep1 = true;
                  this.displayWeatherAdditionalDetails = true;
                  this.displayWeatherLivestockForm = false;
                  this.displayLivestockAdditionalDetails = false;
                  this.policyFormActivated = true;
                  this.policyAdditionalDetailsActivated = true;
                  this.isDataLoaded = false;
                }, error => {
                  this.displayErrorMessage(error);
                  this.isDataLoaded = false;
                });

          /*    }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });*/
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        } else if (this.asAgent === 'Y') {
          this.service.getModeOfBusinessAgent().subscribe(resp => {
            const body2 = resp.body as any;
            this.modeOfBusinessCode = body2.data.profile.intermediaryType;
            const policy = {
              'branchCode': this.branchCode,
              'channelOfBuinessCode': this.liveStickBusinessChannel,
              'classCode': this.classCode,
              'currencyCode': this.currencyCode,
              'customerAddressSequence': this.addressSequnce,
              'customerCode': this.customerCode,
              'modeOfBusinessCode': this.modeOfBusinessCode,
              'settlementModeCode': this.premiumSettlementMethod,
              'periodFromDate': moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
              'periodToDate': moment(this.liveStockEndDate).format('YYYY-MM-DD HH:mm:ss'),
              'policyRemark': this.policyRemark,
              'productCode': this.product
            };
            this.service.savePolicy(policy).subscribe(resp => {
              const body = resp.body as any;
              this.policySequnce = body.data;
              // add default location
            /*  this.service.saveDefaultLocation(this.policySequnce).subscribe(resp => {
                const body = resp.body as any;
                this.locationSequence = body.data;*/
                const intermediaryTypeObj = {
                  'intermediaryTypeCode': this.intermediaryType,
                  'intermediaryCode': this.partyCode,
                  'isBusinessParty': 'Y',
                  'isCommissionParty': 'Y',
                  'businessPartyshare': 100
                };
                this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj).subscribe(resp => {
                  const body6 = resp.body as any;
                  this.displaySuccessMessage(body);
                  this.displayWeatherAdditionalDetails = true;
                  this.displayWeatherLivestockForm = false;
                  this.finishedPolicyStep1 = true;
                  this.policyFormActivated = true;
                  this.policyAdditionalDetailsActivated = true;
                  this.isDataLoaded = false;
                  this.getpolicyDetails();
                }, error => {
                  this.displayErrorMessage(error);
                  this.isDataLoaded = false;
                });
             /* }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });*/
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        } else if (this.asInternalStaff === 'Y') {
          if (this.intermediaryTypeCode && this.intermediaryTypeName) {
            this.service.getModeOfBusinessAgent().subscribe(resp => {
              const body2 = resp.body as any;
              this.modeOfBusinessCode = body2.data;
              const policy = {
                'branchCode': this.branchCode,
                'channelOfBuinessCode': this.liveStickBusinessChannel,
                'classCode': this.classCode,
                'currencyCode': this.currencyCode,
                'customerAddressSequence': this.addressSequnce,
                'customerCode': this.customerCode,
                'modeOfBusinessCode': this.intermediaryTypeCode,
                'settlementModeCode': this.premiumSettlementMethod,
                'periodFromDate': moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
                'periodToDate': moment(this.liveStockEndDate).format('YYYY-MM-DD HH:mm:ss'),
                'policyRemark': this.policyRemark,
                'productCode': this.product
              };
              this.service.savePolicy(policy).subscribe(resp => {
                const body = resp.body as any;
                this.policySequnce = body.data;
                // add default location
                /*  this.service.saveDefaultLocation(this.policySequnce).subscribe(resp => {
                    const body = resp.body as any;
                    this.locationSequence = body.data;*/
                let intermediaryTypeObj = {};
                if (this.intermediaryTypeCode && this.intermediaryTypeName) {
                  intermediaryTypeObj = {
                    'intermediaryTypeCode': this.intermediaryTypeCode,
                    'intermediaryCode': this.intermediaryTypeName,
                    'isBusinessParty': 'Y',
                    'isCommissionParty': 'N',
                    'businessPartyshare': 100
                  };
                } else {
                  intermediaryTypeObj = {
                    'intermediaryTypeCode': 'DIRCT',
                    'intermediaryCode': this.branchCode,
                    'isBusinessParty': 'Y',
                    'isCommissionParty': 'N',
                    'businessPartyshare': 100
                  };
                }

                this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj).subscribe(resp => {
                  const body6 = resp.body as any;
                  this.getpolicyDetails();
                  this.displaySuccessMessage(body);
                  this.finishedPolicyStep1 = true;
                  this.displayWeatherAdditionalDetails = true;
                  this.displayWeatherLivestockForm = false;
                  this.displayLivestockAdditionalDetails = false;
                  this.policyFormActivated = true;
                  this.policyAdditionalDetailsActivated = true;
                  this.isDataLoaded = false;
                }, error => {
                  this.displayErrorMessage(error);
                  this.isDataLoaded = false;
                });

                /*}, error => {
                  this.isDataLoaded = false;
                  this.displayErrorMessage(error);
                });*/
              }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          } else {
            this.service.getModeOfBusinessCustomer().subscribe(resp => {
              const body2 = resp.body as any;
              this.modeOfBusinessCode = body2.data;
              const policy = {
                'branchCode': this.branchCode,
                'channelOfBuinessCode': this.liveStickBusinessChannel,
                'classCode': this.classCode,
                'currencyCode': this.currencyCode,
                'customerAddressSequence': this.addressSequnce,
                'customerCode': this.customerCode,
                'modeOfBusinessCode': this.intermediaryType,
                'settlementModeCode': this.premiumSettlementMethod,
                'periodFromDate': moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
                'periodToDate': moment(this.liveStockEndDate).format('YYYY-MM-DD HH:mm:ss'),
                'policyRemark': this.policyRemark,
                'productCode': this.product
              };
              this.service.savePolicy(policy).subscribe(resp => {
                const body = resp.body as any;
                this.policySequnce = body.data;
                // add default location
                /*  this.service.saveDefaultLocation(this.policySequnce).subscribe(resp => {
                    const body = resp.body as any;
                    this.locationSequence = body.data;*/
                let intermediaryTypeObj = {};
                if (this.intermediaryTypeCode && this.intermediaryTypeName) {
                  intermediaryTypeObj = {
                    'intermediaryTypeCode': this.intermediaryTypeCode,
                    'intermediaryCode': this.intermediaryTypeName,
                    'isBusinessParty': 'Y',
                    'isCommissionParty': 'N',
                    'businessPartyshare': 100
                  };
                } else {
                  intermediaryTypeObj = {
                    'intermediaryTypeCode': 'DIRCT',
                    'intermediaryCode': this.branchCode,
                    'isBusinessParty': 'Y',
                    'isCommissionParty': 'N',
                    'businessPartyshare': 100
                  };
                }

                this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj).subscribe(resp => {
                  const body6 = resp.body as any;
                  this.getpolicyDetails();
                  this.displaySuccessMessage(body);
                  this.finishedPolicyStep1 = true;
                  this.displayWeatherAdditionalDetails = true;
                  this.displayWeatherLivestockForm = false;
                  this.displayLivestockAdditionalDetails = false;
                  this.policyFormActivated = true;
                  this.policyAdditionalDetailsActivated = true;
                  this.isDataLoaded = false;
                }, error => {
                  this.displayErrorMessage(error);
                  this.isDataLoaded = false;
                });

                /*}, error => {
                  this.isDataLoaded = false;
                  this.displayErrorMessage(error);
                });*/
              }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }

        }
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  // policy weather and livestock additional details
  getpolicyDetails() {
    this.isDataLoaded = true;
    this.clearCommonPlaces();
    this.images = [];
    this.clearCommonPlaces();
    this.risks = [];
    let documentSequnce = '';
    this.service.getPolicyDetails(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body1 = resp.body as any;
      this.productCode = body1.data.productCode;
      this.productType = body1.data.productType;
      this.totalPremium = body1.data.totalPremium;
      this.proposalNumber = body1.data.proposalNumber;
      this.authorizedDate = body1.data.authorizedDate;
      // check  policy authorized or not
      const status = body1.data.status;
      if (status === 1 || status === 2) {
        this.authorizedPolicy = true;
      } else {
        this.authorizedPolicy = false;
      }
      // set values
      this.service.getPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
        const body2 = resp.body as any;
        // get risks
        this.service.getRisksSelectedPolicySequnce(this.policySequnce, this.authorizedDate).subscribe(resp => {
          const body3 = resp.body as any;
          this.risks = body3.data.riskList;
          this.service.getPolicyImages(this.policySequnce, this.authorizedDate).subscribe(resp => {
            const body4 = resp.body as any;
            documentSequnce = body4.data[0]['sequenceNo'];
            this.service.downloadImages(documentSequnce, this.authorizedDate).subscribe(resp => {
              const body5 = resp.body as any;
              this.images = [];
              this.imagList = [];
              this.images = body5.data.objectList;
              for (let i = 0; i < this.images.length; i++) {
                const im = new ImageModel();
                im.imageKey = this.images[i]['key'];
                im.value = this.images[i]['url'];
                this.imagList.push(im);
              }
              this.isDataLoaded = false;
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  /**
   *
   *  policy weather land
   */
  addLands() {
    this.clearLandForm();
    this.landEdit = false;
    this.clearCommonPlaces();
    this.getLocations();
    this.disabledLocation = false;
    this.getWeatherStations();
    let cropTypePara: string;
    if (this.productType === 'CRPTY') {
      this.showIndemnityAddLandsForm = true;
      this.displayAddLandForm = true;
      cropTypePara = this.indemnityPolicyDetails.cropType;
      this.getIndemnityCropTypes();
      this.indemnityAddLandForm.controls['indemnityCropVariance'].enable();
      this.indemnityAddLandForm.controls['indemnityCultivationCrop'].enable();
      this.indemnityAddLandForm.controls['indemnityCultivationCropType'].enable();

      this.indemnityLandStarDateMin = new Date();
      this.indemnityLandEndDateMin = new Date();
      this.indemnityLandMaxDate = moment(this.indemnityPolicyDetails.endDate).toDate();
    } else {
      this.displayAddLandForm = true;
      this.showIndemnityAddLandsForm = false;
      cropTypePara = this.cropType;
    }
    this.getCropVariance(cropTypePara);
    this.displayWeatherAdditionalDetails = false;
    this.displayIndemnityAdditionalDetails = false;
    this.riskSequnce = '';
    this.enableContinueButtonWeatherLand = true;
    this.riskRefNo = this.riskRefNo + 1;
    this.displayWeatherIndexParametersDiv = false;
    this.disabledCultivationFeilds = false;
  }

  manageLand(data) {
    this.clearLandForm();
    this.clearCommonPlaces();
    this.landEdit = true;
    this.riskSequnce = data;
    this.getLocations();
    this.getWeatherStations();
    this.displayAddLandForm = true;
    this.displayWeatherAdditionalDetails = false;
    this.enableContinueButtonWeatherLand = false;
    this.viewAddLandPhotos = false;
    this.viewAddLandPhotos = false;
    this.getLandDetails();
    this.getCropVariance(this.cropType);
    this.displayWeatherIndexParametersDiv = true;
    this.disabledCultivationFeilds = false;

  }


  // get livestock data
  getLandDetails() {
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.service.getLocations(this.customerCode).subscribe(resp => {
        this.locations = [];
        const body1 = resp.body as any;
        this.locations = body1.data;
        top: for (let i = 0; i < this.locations.length; i++) {
          if (this.locations[i]['address'] === body.data.location.location) {
            this.location = this.locations[i];
            this.getSelectedLocation(this.location);
            break top;
          }
        }
        // risk
        this.riskOrder = body.data.riskOrder;
        this.riskName = body.data.riskName;
        this.farmerName = body.data.riskName;
        this.riskRefNo = body.data.riskRefNo;
        this.riskSectionCode = body.data.riskSectionCode;
        this.riskSumInsured = body.data.riskSumInsured;
        this.weatherLandStartDate = new Date(body.data.effectiveDate);
        this.weatherLandStartDate = new Date(body.data.effectiveDate);
        this.size1 = body.data.size1;
        this.size1UnitType = body.data.size1UnitType;
        this.size2 = body.data.size2;
        this.size2UnitType = body.data.size2UnitType;
        this.weatherLandStartDate = new Date(body.data.effectiveDate);
        this.weatherLandPremium = body.data.premium;
        this.weatherLandEndDate = new Date
          (body.data.effectiveToDate);

        // get risk commont information
        this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_3 = resp.body as any;
          const resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            const obj = resultArr[i];
            if (obj['description'] === 'CROP VARIANCE') {
              // crop variance
              this.cropVariance = obj['value'];
            } else if (obj['description'] === 'WEATHER STATION') {
              this.weatherStation = parseInt(obj['value']);
            } else if (obj['description'] === 'FARMER NAME') {
              this.farmerName = obj['value'];
            } else if (obj['description'] === 'NO OF UNITS') {
              this.noOfUnits = parseInt(obj['value']) + '';
            } else if (obj['description'] === 'CROP CATEGORY') {
              this.indemnityCultivationCropType.setValue(obj['value']);
            } else if (obj['description'] === 'CROP') {
              this.indemnityCultivationCrop.setValue(obj['value']);
            }
          }

          this.getWeatherIndexParameters();
          // get units and price
          this.service.getUnitPriceOnRisk(this.weatherStation).subscribe(resp => {
            const body_4 = resp.body as any;
            // this.size2 = body_4.data.unitPrice;
            this.price = body_4.data.unitPrice;

            this.riskImages = [];
            this.covers = [];
            let documentSequnce = '';
            // get risk images
            this.service.getRiskDocuments(this.riskSequnce, this.authorizedDate).subscribe(resp => {
              const body = resp.body as any;
              documentSequnce = body.data[0]['sequenceNo'];

              this.service.downloadImages(documentSequnce, this.authorizedDate).subscribe(resp => {
                const body1 = resp.body as any;
                this.weatherLandPhotos = body1.data.objectList;

                for (let i = 0; i < this.weatherLandPhotos.length; i++) {
                  const im = new ImageModel();
                  im.imageKey = this.weatherLandPhotos[i]['key'];
                  im.value = this.weatherLandPhotos[i]['url'];
                  this.imagList.push(im);
                }
              }, error => {
                this.displayErrorMessage(error);
              });
            }, error => {
              this.displayErrorMessage(error);
            });

          }, error => {
            this.displayErrorMessage(error);
          });
        }, error => {
          this.displayErrorMessage(error);
        });
      }, error => {
        this.displayErrorMessage(error);
      });
      this.locations = [];
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  // update land details
  updateLandRisk() {
    this.isDataLoaded = true;
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      const obj = {
        'annualLimit': body.data.annualLimit,
        'annualNarration': body.data.annualNarration,
        'annualPercentage': body.data.annualPercentage,
        'cancelledAmount': body.data.cancelledAmount,
        'certificateNo': body.data.certificateNo,
        'coverNoteNo': body.data.coverNoteNo,
        'deletedDate': body.data.deletedDate,
        'deletedEndorsementNo': body.data.deletedEndorsementNo,
        'effectEndorsementNo': body.data.effectEndorsementNo,
        'effectiveDate': moment(this.weatherLandStartDate).format('YYYY-MM-DD HH:mm:ss'),
        'effectiveToDate': moment(this.weatherLandEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        'eventLimit': body.data.eventLimit,
        'eventNarration': body.data.eventNarration,
        'eventPercentage': body.data.eventPercentage,
        'geometry': [],
        'mainRiskRelationship': body.data.mainRiskRelationship,
        'premium': body.data.premium,
        'relatedMainRiskCode': body.data.relatedMainRiskCode,
        'riskDescription': body.data.riskDescription,
        'riskName': body.data.riskName,
        'riskOrder': this.riskOrder,
        'riskPolicyNo': body.data.riskPolicyNo,
        'riskRefNo': body.data.riskRefNo,
        'riskSectionCode': body.data.riskSectionCode,
        'riskStatus': body.data.riskStatus,
        'riskSumInsured': this.riskSumInsured,
        'seqNo': this.riskSequnce,
        'totalCancelledAmount': body.data.totalCancelledAmount,
        'totalPremium': body.data.totalPremium,
        'totalTransactionAmount': body.data.totalTransactionAmount,
        'transactionAmount': body.data.transactionAmount,
        'size1': this.size1,
        'size2': this.size2,
        'size1UnitType': this.size1UnitType,
        'size2UnitType': this.size2UnitType,
        'version': body.data.version
      };

      const locationObj = {
        'annualLimit': body.data.location.annualLimit,
        'annualNarration': body.data.location.annualNarration,
        'annualPercentage': body.data.location.annualPercentage,
        'cancelledAmount': body.data.location.cancelledAmount,
        'deletedDate': body.data.location.deletedDate,
        'deletedEndorsementNo': body.data.location.deletedEndorsementNo,
        'effectEndorsementNo': body.data.location.effectEndorsementNo,
        'effectiveDate': body.data.location.effectiveDate,
        'eventLimit': body.data.location.eventLimit,
        'eventNarration': body.data.location.eventNarration,
        'eventPercentage': body.data.location.eventPercentage,
        'geometry': body.data.location.geometry,
        'location': this.location,
        'locationOrder': this.locationOrderNo,
        'locationStatus': body.data.location.locationOrder,
        'locationSumInsured': body.data.location.locationSumInsured,
        'locPolicyNo': body.data.location.locPolicyNo,
        'premium': body.data.location.premium,
        'seqNo': body.data.location.seqNo,
        'version': body.data.location.version,
        'totalCancelledAmount': body.data.location.totalCancelledAmount,
        'totalPremium': body.data.location.totalPremium,
        'totalTransactionAmount': body.data.totalTransactionAmount,
        'transactionAmount': body.data.transactionAmount
      };

      this.locationSequence = body.data.location.seqNo,
        this.service.updateRisk(this.riskSequnce, obj).subscribe(resp => {
          const body2 = resp.body as any;
          // get risk commont information
          this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
            const body_3 = resp.body as any;
            const resultArr = body_3.data;
            let cropTypeSequence = {};
            let cropSequence = {};

            for (let i = 0; i < resultArr.length; i++) {
              const obj = resultArr[i];
              if (obj['description'] === 'NO OF UNITS') {
                this.noOfUnitsSequence = obj['sequenceNo'];
                this.noOfUnitsDescription = obj['description'];
                this.noOfUnitsVersion = obj['version'];
                this.noOfUnitsValue = this.noOfUnits;
              } else if (obj['description'] === 'CROP VARIANCE') {
                // crop variance
                this.cropVarianceSequence = obj['sequenceNo'];
                this.cropVarianceDescription = obj['description'];
                this.cropVarianceVersion = obj['version'];
                this.cropVarianceValue = this.cropVariance;
              } else if (obj['description'] === 'WEATHER STATION') {
                // weather stations
                this.weatherStationSequence = obj['sequenceNo'];
                this.weatherStationDescription = obj['description'];
                this.weatherStationVersion = obj['version'];
                this.weatherStationValue = this.weatherStation + '';
              } else if (obj['description'] === 'FARMER NAME') {
                this.farmerNameSequnce = obj['sequenceNo'];
                this.farmerNameDescription = obj['description'];
                this.farmerNameVersion = obj['version'];
                this.farmerNameValue = this.farmerName;
              } else if (obj['description'] === 'CROP') {
                cropSequence = {
                  sequenceNo: obj['sequenceNo'],
                  description: obj['description'],
                  version: obj['version'],
                  value: this.indemnityCultivationCrop.value
                };
              } else if (obj['description'] === 'CROP CATEGORY') {
                cropTypeSequence = {
                  sequenceNo: obj['sequenceNo'],
                  description: obj['description'],
                  version: obj['version'],
                  value: this.indemnityCultivationCropType.value
                };
              }
            }
            // update common references
            let jsonObj = [];
            if (this.productType === 'WHINX') {
              jsonObj = [{
                'sequenceNo': this.noOfUnitsSequence,
                'description': this.noOfUnitsDescription,
                'value': this.noOfUnitsValue,
                'version': this.noOfUnitsVersion
              },
              {
                'sequenceNo': this.cropVarianceSequence,
                'description': this.cropVarianceDescription,
                'value': this.cropVariance,
                'version': this.cropVarianceVersion
              },
              {
                'sequenceNo': this.weatherStationSequence,
                'description': this.weatherDescription,
                'value': this.weatherStation + '',
                'version': this.weatherStationVersion
              },
              {
                'sequenceNo': this.farmerNameSequnce,
                'description': this.farmerNameDescription,
                'value': this.farmerNameValue,
                'version': this.farmerNameVersion
              }
              ];
            } else if (this.productType === 'CRPTY') {
              jsonObj = [
                {
                  'sequenceNo': this.cropVarianceSequence,
                  'description': this.cropVarianceDescription,
                  'value': this.indemnityCropVariance.value,
                  'version': this.cropVarianceVersion
                },
                {
                  'sequenceNo': this.farmerNameSequnce,
                  'description': this.farmerNameDescription,
                  'value': this.indemnityFarmerName.value,
                  'version': this.farmerNameVersion
                },
                cropTypeSequence,
                cropSequence
              ];
            }

            this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
              const body_4 = resp.body as any;
              // set msg display
              this.viewAddLand = true;
              this.viewAddCultivation = false;
              this.viewAddLandPhotos = false;
              this.disabledLocation = true;
              this.disabledCultivationFeilds = true;
              if (this.productType === 'CRPTY') {
                this.indemnityCropVariance.disable();
                this.indemnityCultivationCrop.disable();
              }
              this.displaySuccessMessage(body_4);
              this.isDataLoaded = false;
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });

          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });

        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  manageLivestock(data) {
    this.clearLivestockForm();
    this.manageLivestockRisk = true;
    this.clearCommonPlaces();
    this.riskSequnce = data;
    this.displayAddLivestockForm = true;
    this.displayLivestockAdditionalDetails = true;
    this.getRiskAdditionalDetails();
    this.displayLivestockAdditionalDetails = false;
    this.displayWeatherAdditionalDetails = false;
    // get dropdown data
    this.getLiveStockTypes();
    this.getBreedTypes(null);
    this.getGenders();
    this.getUsageTypes();
    this.getUnderwritingAssessors();
    this.getLivestockDetails();
  }

  // get livestock data
  getLivestockDetails() {
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      // risk
      this.riskOrder = body.data.riskOrder;
      this.tagNumber = body.data.riskName;
      this.riskRefNo = body.data.riskRefNo;
      this.riskSectionCode = body.data.riskSectionCode;
      this.riskSumInsured = body.data.riskSumInsured;
      this.locationSequence = body.data.location.seqNo;
      // get risk commont information
      this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
        const body_3 = resp.body as any;
        const resultArr = body_3.data;
        for (let i = 0; i < resultArr.length; i++) {
          const obj = resultArr[i];
          if (obj['description'] === 'LIVESTOCK TYPE') {
            // crop variance
            this.liveStockType = obj['value'];
          }
          if (obj['description'] === 'BREED') {
            this.breedType = obj['value'];

          }
          if (obj['description'] === 'GENDER') {
            this.gender = obj['value'];
          }
          if (obj['description'] === 'USAGE') {
            this.usageType = obj['value'];
          }
          if (obj['description'] === 'AGE IN YEARS') {
            this.age = parseInt(obj['value']) + '';
          }
          if (obj['description'] === 'OWNER NAME') {
            this.ownerName = obj['value'];
          }
          if (obj['description'] === 'PURCHASE DATE') {
            if (obj['value'] === null) {
              this.livestockPurchaseDate = null;
            } else {
              this.livestockPurchaseDate = new Date(obj['value']);
            }
          }
        }
        // get asssessors and date
        this.service.getAssessors(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_4 = resp.body as any;
          this.undewritingAssessor = body_4.data[0]['assessorCode'];
          // get assessor sequnce and get inspectd date
          this.service.getAssessorDetails(body_4.data[0]['sequence']).subscribe(resp => {
            const body_5 = resp.body as any;
            this.newLivestockStartDate = new Date(body_5.data.datetime);
          });
        }, error => {
          this.displayErrorMessage(error);
        });
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }


  updateLivestockRisk() {
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      const obj = {
        'annualLimit': body.data.annualLimit,
        'annualNarration': body.data.annualNarration,
        'annualPercentage': body.data.annualPercentage,
        'cancelledAmount': body.data.cancelledAmount,
        'certificateNo': body.data.certificateNo,
        'coverNoteNo': body.data.coverNoteNo,
        'deletedDate': body.data.deletedDate,
        'deletedEndorsementNo': body.data.deletedEndorsementNo,
        'effectEndorsementNo': body.data.effectEndorsementNo,
        'effectiveDate': moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
        'effectiveToDate': moment(this.liveStockEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        'eventLimit': body.data.eventLimit,
        'eventNarration': body.data.eventNarration,
        'eventPercentage': body.data.eventPercentage,
        'geometry': body.data.geometry,
        'mainRiskRelationship': body.data.mainRiskRelationship,
        'premium': body.data.premium,
        'relatedMainRiskCode': body.data.relatedMainRiskCode,
        'riskDescription': body.data.riskDescription,
        'riskName': this.tagNumber,
        'riskOrder': this.riskOrder,
        'riskPolicyNo': body.data.riskPolicyNo,
        'riskRefNo': body.data.riskRefNo,
        'riskSectionCode': body.data.riskSectionCode,
        'riskStatus': body.data.riskStatus,
        'riskSumInsured': this.riskSumInsured,
        'seqNo': this.riskSequnce,
        'totalCancelledAmount': body.data.totalCancelledAmount,
        'totalPremium': body.data.totalPremium,
        'totalTransactionAmount': body.data.totalTransactionAmount,
        'transactionAmount': body.data.transactionAmount,
        'size1': body.data.size1,
        'size2': body.data.size2,
        'size1UnitType': body.data.size1UnitType,
        'size2UnitType': body.data.size2UnitType,
        'version': body.data.version
      };
      this.service.updateRisk(this.riskSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_3 = resp.body as any;
          const resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            const obj = resultArr[i];
            if (obj['description'] === 'LIVESTOCK TYPE') {
              // live stock type
              this.liveStockTypeSequnce = obj['sequenceNo'];
              this.liveStockTypeDescription = obj['description'];
              this.liveStockTypeVersion = obj['version'];
              this.liveStockTypeValue = this.liveStockType;
            } else if (obj['description'] === 'BREED') {
              // breed
              this.breedSequence = obj['sequenceNo'];
              this.breedDescription = obj['description'];
              this.breedVersion = obj['version'];
              this.breedValue = this.breedType;
            } else if (obj['description'] === 'GENDER') {
              // gender
              this.genderSequence = obj['sequenceNo'];
              this.genderDescription = obj['description'];
              this.genderVersion = obj['version'];
              this.genderValue = this.gender;
            } else if (obj['description'] === 'USAGE') {
              // usage type
              this.usageSequence = obj['sequenceNo'];
              this.usageDescription = obj['description'];
              this.usageVersion = obj['version'];
              this.usageValue = this.usageType;
            } else if (obj['description'] === 'AGE IN YEARS') {
              // age in years
              this.ageInYearsSequence = obj['sequenceNo'];
              this.ageInYearsDescription = obj['description'];
              this.ageInYearsVersion = obj['version'];
              this.ageInYearsValue = this.age;
            } else if (obj['description'] === 'OWNER NAME') {
              // owner name
              this.ownerNameSequence = obj['sequenceNo'];
              this.ownerNameDescription = obj['description'];
              this.ownerNameVersion = obj['version'];
              this.ownerNameValue = this.ownerName;
            } else if (obj['description'] === 'PURCHASE DATE') {
              // purchase date
              this.purchaseDateSequence = obj['sequenceNo'];
              this.purchaseDateDescription = obj['description'];
              this.purchaseDateVersion = obj['version'];
              this.purchaseDateValue = moment(this.livestockPurchaseDate).format('YYYY-MM-DD HH:mm:ss');
            }
          }

          // update common references for risk
          const jsonObj = [{
            'sequenceNo': this.liveStockTypeSequnce,
            'description': this.liveStockTypeDescription,
            'value': this.liveStockTypeValue,
            'version': this.liveStockTypeVersion
          },
          {
            'sequenceNo': this.breedSequence,
            'description': this.breedDescription,
            'value': this.breedValue,
            'version': this.breedVersion
          },
          {
            'sequenceNo': this.genderSequence,
            'description': this.genderDescription,
            'value': this.genderValue,
            'version': this.genderVersion
          },
          {
            'sequenceNo': this.usageSequence,
            'description': this.usageDescription,
            'value': this.usageValue,
            'version': this.usageVersion
          },
          {
            'sequenceNo': this.ageInYearsSequence,
            'description': this.ageInYearsDescription,
            'value': this.ageInYearsValue,
            'version': this.ageInYearsVersion
          },
          {
            'sequenceNo': this.ownerNameSequence,
            'description': this.ownerNameDescription,
            'value': this.ownerNameValue,
            'version': this.ownerNameVersion
          },
          {
            'sequenceNo': this.purchaseDateSequence,
            'description': this.purchaseDateDescription,
            'value': this.purchaseDateValue,
            'version': this.purchaseDateVersion
          }
          ];
          this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
            const body_4 = resp.body as any;
            // get asssessors and date
            this.service.getAssessors(this.riskSequnce, this.authorizedDate).subscribe(resp => {
              const body_5 = resp.body as any;
              const objAssessor = {
                'sequence': body_5.data[0]['sequence'],
                'assessorCode': this.undewritingAssessor,
                'assessorName': body_5.data[0]['assessorName'],
                'assessorType': body_5.data[0]['assessorType'],
                'remarks': body_5.data[0]['remarks'],
                'version': body_5.data[0]['version'],
                'datetime': moment(this.newLivestockStartDate).format('YYYY-MM-DD HH:mm:ss'),
              };
              this.service.updateInspectedDetails(body_5.data[0]['sequence'], objAssessor).subscribe(resp => {
                const body_6 = resp.body as any;
                this.displaySuccessMessage(body_5);
                this.displayAddiotionalDetails();
              }, error => {
                this.displayErrorMessage(error);
              });
            }, error => {
              this.displayErrorMessage(error);
            });
          }, error => {
            this.displayErrorMessage(error);
          });
        }, error => {
          this.displayErrorMessage(error);
        });
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getLocations() {
    this.service.getLocations(this.customerCode).subscribe(resp => {
      const body = resp.body as any;
      this.locations = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  // get weather stations
  getWeatherStations() {
    this.service.getWeatherStations().subscribe(resp => {
      const body = resp.body as any;
      this.weatherStations = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  // get crop variances
  getCropVariance(cropType) {
    this.service.getCropVariances(cropType).subscribe(resp => {
      const body = resp.body as any;
      this.cropVariances = body.data;
    }, error => {
      this.cropVariances = [];
    });
  }

  saveWeatherLand() {
    this.isDataLoaded = true;
    const locationObj = {
      'location': this.location['address'],
      'locationOrder': this.locationOrderNo
    };
    this.service.addLocation(this.policySequnce, locationObj).subscribe(resp => {
      const body_1 = resp.body as any;
      this.locationSequence = body_1.data;
      const riskObj = {
        'riskOrder': null,
        'riskName': this.location['plotName'],
        'riskRefNo': null,
        'riskSumInsured': this.riskSumInsured,
        'effectiveDate': moment(this.weatherLandStartDate).format('YYYY-MM-DD HH:mm:ss'),
        'effectiveToDate': moment(this.weatherLandEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        'size1': this.size1,
        'size1UnitType': this.size1UnitType,
        'size2': this.size2,
        'size2UnitType': this.size2UnitType
      };
      this.service.addRisks(this.locationSequence, riskObj).subscribe(resp => {
        const body_2 = resp.body as any;
        this.riskSequnce = body_2.data;
        // get risk commont information
        this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_3 = resp.body as any;
          const resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            const obj = resultArr[i];
            if (obj['description'] === 'NO OF UNITS') {
              this.noOfUnitsSequence = obj['sequenceNo'];
              this.noOfUnitsDescription = obj['description'];
              this.noOfUnitsVersion = obj['version'];
              this.noOfUnitsValue = this.noOfUnits;
            } else if (obj['description'] === 'CROP VARIANCE') {
              // crop variance
              this.cropVarianceSequence = obj['sequenceNo'];
              this.cropVarianceDescription = obj['description'];
              this.cropVarianceVersion = obj['version'];
              this.cropVarianceValue = this.cropVariance;
            } else if (obj['description'] === 'WEATHER STATION') {
              // weather stations
              this.weatherStationSequence = obj['sequenceNo'];
              this.weatherStationDescription = obj['description'];
              this.weatherStationVersion = obj['version'];
              this.weatherStationValue = this.weatherStation + '';
            } else if (obj['description'] === 'FARMER NAME') {
              this.farmerNameSequnce = obj['sequenceNo'];
              this.farmerNameDescription = obj['description'];
              this.farmerNameVersion = obj['version'];
              this.farmerNameValue = this.farmerName;
            }
          }
          // update common references
          const jsonObj = [{
            'sequenceNo': this.noOfUnitsSequence,
            'description': this.noOfUnitsDescription,
            'value': this.noOfUnitsValue,
            'version': this.noOfUnitsVersion
          },
          {
            'sequenceNo': this.cropVarianceSequence,
            'description': this.cropVarianceDescription,
            'value': this.cropVariance,
            'version': this.cropVarianceVersion
          },
          {
            'sequenceNo': this.weatherStationSequence,
            'description': this.weatherDescription,
            'value': this.weatherStation,
            'version': this.weatherStationVersion
          },
          {
            'sequenceNo': this.farmerNameSequnce,
            'description': this.farmerNameDescription,
            'value': this.farmerNameValue,
            'version': this.farmerNameVersion
          }
          ];
          this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
            const body_4 = resp.body as any;
            // set msg display
            this.viewAddLand = true;
            this.viewAddCultivation = false;
            this.viewAddLandPhotos = false;
            this.disabledLocation = true;
            this.displaySuccessMessage(body_4);
            this.isDataLoaded = false;
            this.enableContinueButtonWeatherLand = false;
            this.disabledCultivationFeilds = true;
            this.displayWeatherIndexParametersDiv = true;
            this.getWeatherIndexParameters();
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });

        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });

    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  closeWeatherAddLandForm() {
    this.displayAddLandForm = false;
    this.displayWeatherAdditionalDetails = true;
  }

  continueToWeatherRiskAdditinal() {
    this.displayWeatherAdditionalDetails = true;
    this.displayAddLandForm = false;
    this.isDataLoaded = false;
    this.getpolicyDetails();
  }

  // Live stock form related function
  addLivestock() {
    this.clearLivestockForm();
    this.manageLivestockRisk = false;
    this.clearCommonPlaces();
    this.displayAddLivestockForm = true;
    this.displayWeatherAdditionalDetails = false;
    // get dropdown data
    this.getLiveStockTypes();
    this.getBreedTypes(null);
    this.getGenders();
    this.getUsageTypes();
    this.getUnderwritingAssessors();
    this.riskSequnce = '';
  }

  getLiveStockTypes() {
    this.service.getLiveStockTypes().subscribe(resp => {
      const body = resp.body as any;
      this.liveStockTypes = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getBreedTypes(livestockType: string) {
    this.service.getBreedTypes(livestockType).subscribe(resp => {
      const body = resp.body as any;
      this.breedTypes = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getGenders() {
    this.service.getGenders().subscribe(resp => {
      const body = resp.body as any;
      this.genders = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getUsageTypes() {
    this.service.getUsageTypes().subscribe(resp => {
      const body = resp.body as any;
      this.usageTypes = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getUnderwritingAssessors() {
    this.service.getUnderwritingAssessors().subscribe(resp => {
      const body = resp.body as any;
      this.undewritingAssessors = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  displayLiveStockAdditionalDetails() {
    this.clearCommonPlaces();
    this.displayAddLivestockForm = false;
    this.displayAddCoversForm = false;
    this.displayWeatherLivestockForm = false;
    if (this.productType === 'CRPTY') {
      this.displayLivestockAdditionalDetails = false;
      this.displayWeatherAdditionalDetails = true;
    } else {
      this.displayLivestockAdditionalDetails = true;
      this.displayIndemnityAdditionalDetails = false;
    }
    this.getRiskAdditionalDetails();
  }

  saveLiveStockRisk() {
    this.isDataLoaded = true;
    this.riskRefNo += this.riskRefNo + 1;
    const riskObj = {
      'riskOrder': null,
      'riskName': this.tagNumber,
      'riskRefNo': null,
      'riskSumInsured': this.riskSumInsured
    };
this.service.getDefaultLocation(this.policySequnce, this.authorizedDate).subscribe(resp => {
  const body = resp.body as any;
  this.locationSequence = body.data[0]['seqNo'];
  this.service.addRisks(this.locationSequence, riskObj).subscribe(resp => {
    const body = resp.body as any;
    this.riskSequnce = body.data;
    // get risk informations
    this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body_3 = resp.body as any;
      const resultArr = body_3.data;
      for (let i = 0; i < resultArr.length; i++) {
        const obj = resultArr[i];
        if (obj['description'] === 'LIVESTOCK TYPE') {
          // live stock type
          this.liveStockTypeSequnce = obj['sequenceNo'];
          this.liveStockTypeDescription = obj['description'];
          this.liveStockTypeVersion = obj['version'];
          this.liveStockTypeValue = this.liveStockType;
        } else if (obj['description'] === 'BREED') {
          // breed
          this.breedSequence = obj['sequenceNo'];
          this.breedDescription = obj['description'];
          this.breedVersion = obj['version'];
          this.breedValue = this.breedType;
        } else if (obj['description'] === 'GENDER') {
          // gender
          this.genderSequence = obj['sequenceNo'];
          this.genderDescription = obj['description'];
          this.genderVersion = obj['version'];
          this.genderValue = this.gender;
        } else if (obj['description'] === 'USAGE') {
          // usage type
          this.usageSequence = obj['sequenceNo'];
          this.usageDescription = obj['description'];
          this.usageVersion = obj['version'];
          this.usageValue = this.usageType;
        } else if (obj['description'] === 'AGE IN YEARS') {
          // age in years
          this.ageInYearsSequence = obj['sequenceNo'];
          this.ageInYearsDescription = obj['description'];
          this.ageInYearsVersion = obj['version'];
          this.ageInYearsValue = this.age;
        } else if (obj['description'] === 'OWNER NAME') {
          // owner name
          this.ownerNameSequence = obj['sequenceNo'];
          this.ownerNameDescription = obj['description'];
          this.ownerNameVersion = obj['version'];
          this.ownerNameValue = this.ownerName;
        } else if (obj['description'] === 'PURCHASE DATE') {
          // purchase date
          this.purchaseDateSequence = obj['sequenceNo'];
          this.purchaseDateDescription = obj['description'];
          this.purchaseDateVersion = obj['version'];
          this.purchaseDateValue = moment(this.livestockPurchaseDate).format('YYYY-MM-DD HH:mm:ss');
        }
      }
      // update common references for risk
      const jsonObj = [{
        'sequenceNo': this.liveStockTypeSequnce,
        'description': this.liveStockTypeDescription,
        'value': this.liveStockTypeValue,
        'version': this.liveStockTypeVersion
      },
        {
          'sequenceNo': this.breedSequence,
          'description': this.breedDescription,
          'value': this.breedValue,
          'version': this.breedVersion
        },
        {
          'sequenceNo': this.genderSequence,
          'description': this.genderDescription,
          'value': this.genderValue,
          'version': this.genderVersion
        },
        {
          'sequenceNo': this.usageSequence,
          'description': this.usageDescription,
          'value': this.usageValue,
          'version': this.usageVersion
        },
        {
          'sequenceNo': this.ageInYearsSequence,
          'description': this.ageInYearsDescription,
          'value': this.ageInYearsValue,
          'version': this.ageInYearsVersion
        },
        {
          'sequenceNo': this.ownerNameSequence,
          'description': this.ownerNameDescription,
          'value': this.ownerNameValue,
          'version': this.ownerNameVersion
        },
        {
          'sequenceNo': this.purchaseDateSequence,
          'description': this.purchaseDateDescription,
          'value': this.purchaseDateValue,
          'version': this.purchaseDateVersion
        }
      ];
      this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
        const body_4 = resp.body as any;
        // insert assessors
        const inspectedObj = {
          'assessorCode': this.undewritingAssessor,
          'datetime': moment(this.newLivestockStartDate).format('YYYY-MM-DD HH:mm:ss')
        };
        this.service.saveInspectedDetails(this.riskSequnce, inspectedObj).subscribe(resp => {
          const body_5 = resp.body as any;
          this.displaySuccessMessage(body_5);
          this.displayAddiotionalDetails();
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }, error => {
    this.isDataLoaded = false;
    this.displayErrorMessage(error);
  });

}, error => {
  this.isDataLoaded = false;
  this.displayErrorMessage(error);
});



  }

  // Livestock additinal details
  displayAddiotionalDetails() {
    this.clearCommonPlaces();
    if (this.productType === 'CRPTY') {
      this.displayLivestockAdditionalDetails = false;
      this.displayWeatherAdditionalDetails = true;
    } else {
      this.displayLivestockAdditionalDetails = true;
      this.displayWeatherAdditionalDetails = false;
    }
    this.displayAddLivestockForm = false;
    this.displayAddCoversForm = false;
    this.getRiskAdditionalDetails();
  }

  // covers related functions
  addCovers() {
    this.clearCoverForm();
    this.manageCover = false;
    this.displayLivestockAdditionalDetails = false;
    this.displayWeatherAdditionalDetails = false;
    this.displayAddCoversForm = true;
    this.getOtherPerils();
  }

  getRiskAdditionalDetails() {
    this.isDataLoaded = true;
    this.riskImages = [];
    this.clearCommonPlaces();
    this.covers = [];
    let documentSequnce = '';
    // get risk images
    this.service.getRiskDocuments(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      documentSequnce = body.data[0]['sequenceNo'];
      this.service.downloadImages(documentSequnce, this.authorizedDate).subscribe(resp => {
        const body1 = resp.body as any;
        this.riskImages = body1.data.objectList;
        for (let i = 0; i < this.riskImages.length; i++) {
          const im = new ImageModel();
          im.imageKey = this.riskImages[i]['key'];
          im.value = this.riskImages[i]['url'];
          this.imagList.push(im);
        }
        this.service.getRiskPerils(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body1 = resp.body as any;
          this.covers = body1.data;
          this.service.getPolicyDetails(this.policySequnce, this.authorizedDate).subscribe(resp => {
            const body2 = resp.body as any;
            this.totalPremium = body2.data.totalPremium;
            this.riskPremium = body2.data.totalPremium;
            this.isDataLoaded = false;
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  getOtherPerils() {
    this.service.getOtherPerils(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.otherPerils = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  manageCovers(data) {
    this.clearCoverForm();
    this.perilSequnce = data;
    this.clearCommonPlaces();
    this.manageCover = true;
    this.displayLivestockAdditionalDetails = false;
    this.displayIndemnityAdditionalDetails = false;
    this.displayWeatherAdditionalDetails = false;
    this.displayAddCoversForm = true;
    this.clearCoverForm();
    this.getOtherPerils();
    this.getCoverDetails();
  }

  getCoverDetails() {
    this.service.getCoverDetails(this.perilSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.perilSequnce = body.data.seqNo;
      this.otherPeril = body.data.perilCode;
      this.perilPerilSumInsured = body.data.perilPerilSumInsured;
      this.managePerilDescription = body.data.perilName;
      this.service.getExcess(this.perilSequnce, this.authorizedDate).subscribe(resp => {
        const body2 = resp.body as any;
        this.excessAmount = body2.data.excessAmount;
        this.excessPercentage = body2.data.excessPercentage;
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  // update covers
  updateRiskCovers() {
    this.service.getCoverDetails(this.perilSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      // update
      const obj = {
        'seqNo': this.perilSequnce,
        'perilCode': body.data.perilCode,
        'perilPerilSumInsured': this.perilPerilSumInsured,
        'perilPercentage': body.data.perilPercentage,
        'perilReinsSumInsured': body.data.perilReinsSumInsured,
        'perilPremium': body.data.perilPremium,
        'perilTotalPremium': body.data.perilTotalPremium,
        'perilTransactionAmount': body.data.perilTransactionAmount,
        'perilotalTransactionAmount': body.data.perilotalTransactionAmount,
        'perilCancelledAmount': body.data.perilCancelledAmount,
        'perilTotalCancelledAmount': body.data.perilTotalCancelledAmount,
        'perilrEffectEndorsementNo': body.data.perilrEffectEndorsementNo,
        'perilEffectiveDate': body.data.perilEffectiveDate,
        'perilPerilStatus': body.data.perilPerilStatus,
        'perilDeletedDate': body.data.perilDeletedDate,
        'perilDeletedEndorsementNo': body.data.perilDeletedEndorsementNo,
        'perilDefaultPercentage': body.data.perilDefaultPercentage,
        'perilDefaultRate': body.data.perilDefaultRate,
        'perilRate': body.data.perilRate,
        'perilEventLimit': body.data.perilEventLimit,
        'perilEventPercentage': body.data.perilEventPercentage,
        'perilEventNarration': body.data.perilEventNarration,
        'perilAnnualLimit': body.data.perilAnnualLimit,
        'perilAnnualPercentage': body.data.perilAnnualPercentage,
        'perilAnnualNarration': body.data.perilAnnualNarration,
        'version': body.data.version,
        'perilReinClassCode': body.data.perilReinClassCode,
        'productPerilSeqNo': body.data.productPerilSeqNo,
        'perilName': body.data.perilName
      };
      this.service.updateCover(this.perilSequnce, obj).subscribe(resp => {
        const body1 = resp.body as any;
        // excess
        this.service.getExcess(this.perilSequnce, this.authorizedDate).subscribe(resp => {
          const body2 = resp.body as any;
          if (body2.data === null) {
            // excess insert
            const excess = {
              'excessAmount': this.excessAmount,
              'excessPercentage': this.excessPercentage
            };
            this.service.saveExcess(this.perilSequnce, excess).subscribe(resp => {
              const body1 = resp.body as any;
              this.displaySuccessMessage(body1);
              if (this.productType === 'CRPTY') {
                this.displayWeatherAdditionalDetails = true;
                this.displayLivestockAdditionalDetails = false;
              } else {
                this.displayLivestockAdditionalDetails = true;
                this.displayWeatherAdditionalDetails = false;
              }
              this.displayAddCoversForm = false;
              this.getRiskAdditionalDetails();
            }, error => {
              this.displayErrorMessage(error);
            });
          } else {
            // make object
            const obj = {
              'sequenceNo': body2.data.sequenceNo,
              'excessLevel': body2.data.excessLevel,
              'excessAmount': this.excessAmount,
              'excessPercentage': this.excessPercentage,
              'excessNarration': body2.data.excessNarration,
              'status': body2.data.status,
              'version': body2.data.version
            };
            this.service.updateExcess(body2.data.sequenceNo, obj).subscribe(resp => {
              const body_3 = resp.body as any;
              this.displaySuccessMessage(body1);
              this.displayLivestockAdditionalDetails = true;
              this.displayAddCoversForm = false;
              this.getRiskAdditionalDetails();
            }, error => {
              this.displayErrorMessage(error);
            });
          }
        }, error => {
          this.displayErrorMessage(error);
        });
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  saveCovers() {
    this.isDataLoaded = true;
    let peril = {};
    if (this.otherPeril['perilPercentage'] === null) {
      peril = {
        'perilCode': this.otherPeril['perilCode'],
        'perilPerilSumInsured': this.perilPerilSumInsured,
        'perilPremium': this.otherPeril['perilPercentage']
      };
    } else {
      peril = {
        'perilCode': this.otherPeril['perilCode'],
        'perilPerilSumInsured': this.perilPerilSumInsured,
        'perilPercentage': this.otherPeril['perilPercentage']
      };
    }
    this.service.saveCovers(this.riskSequnce, peril).subscribe(resp => {
      const body = resp.body as any;
      this.perilSequnce = body.data;
      // excess insert
      const excess = {
        'excessAmount': this.excessAmount,
        'excessPercentage': this.excessPercentage
      };
      this.service.saveExcess(this.perilSequnce, excess).subscribe(resp => {
        const body1 = resp.body as any;
        this.displaySuccessMessage(body1);
        this.displayAddiotionalDetails();
        if (this.productType === 'CRPTY') {
          this.getpolicyDetails();
        }
        this.isDataLoaded = false;
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  // complete policy
  completePolicy() {
    const authArray = [];
    this.service.getCompletePolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      const version = body.data.version;
      // update completed
      const obj = {
        'completed': true,
        'version': version
      };
      this.service.updateCompletePolicy(this.policySequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        // authorize policy
        this.service.getAuthorizedPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
          const body = resp.body as any;
          for (let i = 0; i < body.data.length; i++) {
            const obj = {
              'sequenceNo': body.data[i]['sequenceNo'],
              'levelCode': body.data[i]['levelCode'],
              'authorized': true,
              'version': body.data[i]['version']
            };
            authArray.push(obj);
          }
          this.service.updateAuthorizedPolicy(this.policySequnce, authArray).subscribe(resp => {
            const body = resp.body as any;
            this.debitNoteNumber = body.data;
            this.authorizedPolicy = true;
            this.finishedPolicyStep2 = true;
            this.displaySuccessMessage(body);
            if (this.productType === 'CRPTY') {
              this.paymentType = this.indemnityPolicyDetails.paymentType;
            }
            if ((this.paymentType === 'UPFRN' || this.premiumSettlementMethod === 'UPFRN') && parseInt(this.totalPremium) !== 0) {
              // redirect to payment
              this.displayPayment = true;
            } else {
              // download pdf and redirect to policy list

              if (this.productType === 'WHINX') {
                // weather Individual file
                const fileName1 = 'WeatherIndexSchedule.pdf';
                this.service.downloadWeatherIndividualFile(this.policySequnce).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], { type: 'application/pdf' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName1;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
                // weather Group file
                const fileName2 = 'WeatherIndexGroupSchedule.pdf';
                this.service.downloadWeatherGroupFile(this.policySequnce).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], { type: 'application/pdf' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName2;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
              } else if (this.productType === 'LIVST') {
                // Livestock individual file
                const fileName1 = 'LivestockSchedule.pdf';
                this.service.downloadLivestockIndividualFile(this.policySequnce).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], { type: 'application/pdf' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName1;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
                // Livestock Group file
                const fileName2 = 'LivestockGroupSchedule.pdf';
                this.service.downloadLivestockGroupFile(this.policySequnce).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], { type: 'application/pdf' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName2;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
              }
              if (this.parentPage === 'c') {
                this.router.navigate(['/customers']);
              } else {
                this.router.navigate(['/policies']);
              }
            }
          }, error => {
            this.authorizedPolicy = false;
            this.displayErrorMessage(error);
          });
        }, error => {
          this.authorizedPolicy = false;
          this.displayErrorMessage(error);
        });
        // set disable  steps
        // enable payment link
      }, error => {
        this.authorizedPolicy = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.authorizedPolicy = false;
      this.displayErrorMessage(error);
    });
  }

  cancelWeatherAdditionalDetails() {
    this.displayWeatherAdditionalDetails = false;
    this.displayWeatherLivestockForm = true;
  }

  // save  images keys
  savePolicyImageKeys() {
    // get document sequnce
    this.service.getPolicyDocumentSequnce(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.documentSequnce = body.data[0]['sequenceNo'];
      const obj = [];
      for (let i = 0; i < this.imagList.length; i++) {
        obj.push(this.imagList[i]['imageKey']);
      }
      // make image key array
      this.service.saveWeatherImages(this.documentSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.displaySuccessMessage(body);
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  saveRiskImageKeys() {
    // get document sequnce
    this.service.getRiskDocumentSequnce(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.documentSequnce = body.data[0]['sequenceNo'];
      const obj = [];
      for (let i = 0; i < this.imagList.length; i++) {
        obj.push(this.imagList[i]['imageKey']);
      }
      // make image key array
      this.service.saveRiskImages(this.documentSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.getRiskAdditionalDetails();
        this.displaySuccessMessage(body);
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  calculatePremium() {
    if (this.noOfUnits === null || this.price === null) {
      this.weatherLandPremium = 0;
    } else {
      this.weatherLandPremium = parseFloat(this.noOfUnits) * parseFloat(this.price);
    }
  }

  // set weather policy end date
  setEndDate() {
    this.endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());
  }

  getWeatherIndexParameters() {
    this.weatherIndexParameters = [];
    const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
    const selectedToMonth = parseInt(moment().month(this.toMonth).format('M'));


    // tslint:disable-next-line:max-line-length
    this.service.getWeatherindexParameters(this.fromYear, selectedFromMonth, this.weatherStation, this.toYear, selectedToMonth).subscribe(resp => {
      const body = resp.body as any;
      this.weatherIndexParameters = body.data;

      for (let i = 0; i < this.weatherIndexParameters.length; i++) {
        const month = parseInt(this.weatherIndexParameters[i]['month']) - 1;
        this.weatherIndexParameters[i]['month'] = moment().month(month).format('MMMM');
      }
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  setLivestockPolicyEndDate() {
    this.livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());
  }

  getSelectedLocation(data) {
    if (data === 'add-new') {
      this.getDistricts();
      this.getagrarianServiceCenters();
      this.getOwnershipTypes();
      this.viewAddLand = false;
      this.viewAddCultivation = true;
      this.viewAddLandPhotos = true;
      this.sendLandData.geometry = [];
      this.markLandPlotString = '';
    } else if (data !== 'select-land') {
      this.size1 = this.location['sizeInPerches'];
      this.size2 = this.location['sizeInAcres'];
      this.markLandPlot = this.location['geometry'];
      this.pathList = this.location['geometry'];
      this.sendLandData.districtDescription = this.location['districtDescription'];
      this.sendLandData.divisionDescription = this.location['divisionDescription'];
      this.sendLandData.gramasevaDivisionDescription = this.location['agrarianServiceCenterDescription'];
      this.sendLandData.geometry = this.location['geometry'];

      if (this.showIndemnityAddLandsForm) {
        this.indemnityAcres.setValue(this.location['sizeInAcres']);
        this.indemnityPerches.setValue(this.location['sizeInPerches']);
        let plotString = '';
        for (let i = 0; i < this.pathList.length; i++) {
          plotString = plotString + this.pathList[i]['latitude'] + ':' + this.pathList[i]['longitude'] + ',';
        }
        this.indemnityPlot.setValue(plotString);
      }

      const markLandPlotArr = [];
      this.viewAddLand = true;
      this.viewAddCultivation = false;
      if (this.landEdit) {
        this.viewAddLandPhotos = false;
      } else {
        this.viewAddLandPhotos = true;
      }
      this.weatherStation = 84;
      this.markLandPlotString = '';
      for (let i = 0; i < this.pathList.length; i++) {
        this.markLandPlotString = this.markLandPlotString + this.pathList[i]['latitude'] + ':' + this.pathList[i]['longitude'] + ',';
      }
      // get nearest weather station
      this.service.getNearestWeatherStation(this.location['geometry']).subscribe(resp => {
        const body = resp.body as any;
        this.weatherStation = parseInt(body.data.sequence);
        // get units and price
        this.service.getUnitPriceOnRisk(this.weatherStation).subscribe(resp => {
          const body_4 = resp.body as any;
          this.price = body_4.data.unitPrice;
        }, error => {
          this.displayErrorMessage(error);
        });
      }, error => {
        this.displayErrorMessage(error);
      });
    }
  }

  getDistricts() {
    this.landService.getDistricts().subscribe(resp => {
      const body = resp.body as any;
      this.districtsList = body.data;
    });
  }

  getDivisionns() {
    this.landService.getDivisions(this.selectedDistrict.code).subscribe(resp => {
      const body = resp.body as any;
      this.divisionsList = body.data;
    });
  }

  getGramasevaDivisions() {
    this.landService.getGramasevaDivisions(this.selectedDivision.code).subscribe(resp => {
      const body = resp.body as any;
      this.gramasevaDivisionsList = body.data;
    });
  }

  getagrarianServiceCenters() {
    this.landService.getagrarianServiceCenters().subscribe(resp => {
      const body = resp.body as any;
      this.getagrarianServiceCentersList = body.data;
    });
  }

  getOwnershipTypes() {
    this.landService.getOwnershipTypes().subscribe(resp => {
      const body = resp.body as any;
      this.ownershipTypesList = body.data;
    });
  }

  getMarkers(event) {
    if (event.length === 0) {
      this.isDrawMap = false;
    } else {
      this.pathList = event;
      this.pathList.push(this.pathList[0]);
      this.pathList.forEach(item => {
        this.pathString = this.pathString + item.latitude + ':' + item.longitude + ',';
      });
      this.landForm.controls['geometry'].setValue(this.pathString);
      this.isDrawMap = false;
    }
  }

  saveLand() {
    const land: LandModel = this.landForm.value;
    land.geometry = this.pathList;
    land.district = this.selectedDistrict.code;
    land.division = this.selectedDivision.code;
    land.gramasevaDivision = this.selectedGramasevaDivision.code;
    if (this.asAgent === 'Y' || this.asInternalStaff === 'Y') {
      this.routerPartyCode = this.route.snapshot.paramMap.get('customerCode');
    }
    if (this.routerPartyCode) {
      land.partyCode = this.routerPartyCode;
      this.routeString = '/customers';
    } else {
      land.partyCode = sessionStorage.getItem('partyCode');
      this.routeString = '/land';
    }
    this.landService.postLand(land).subscribe(resp => {
      const body1 = resp.body as any;
      this.service.getLocations(this.customerCode).subscribe(resp => {
        const body = resp.body as any;
        this.locations = body.data;
        this.service.getLocationFromUtility(body1.data).subscribe(resp => {
          const body2 = resp.body as any;
          // this.location = body2.data;
          top: for (let i = 0; i < this.locations.length; i++) {
            if (this.locations[i]['sequence'] === body2.data.sequence) {
              this.location = this.locations[i];
              break top;
            }
          }
          this.getSelectedLocation(this.location);
          this.viewAddLand = true;
          this.viewAddCultivation = false;
          this.viewAddLandPhotos = true;
          this.displaySuccessMessage(body1);
        }, error => {
          this.displayErrorMessage(error);
        });
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.viewAddLand = false;
      this.viewAddCultivation = true;
      this.viewAddLandPhotos = true;
      this.displayErrorMessage(error);
    });
    this.isContinue = true;
  }

  // upload images
  gg() {
  }

  asd() {
    this.sendLandData.districtDescription = this.selectedDistrict.description;
    this.sendLandData.divisionDescription = this.selectedDivision.description;
    this.sendLandData.gramasevaDivisionDescription = this.selectedGramasevaDivision.description;
    this.isDrawMap = true;
  }


  /**
   * common functions
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
    //  this.getpolicyDetails();
  }

  uploadImg(file: File, index: number) {

    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.imagList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imagList[index].imageKey = body.data.object.key;
        this.imagList[index].value = body.data.object.url;
        this.imagList[index].file = null;
      }
    });
  }

  addImg(event) {
    for (let i = 0; i < event.target.files.length; i++) {
      const img: ImageModel = new ImageModel();
      img.file = event.target.files[i];
      this.imagList.push(img);
      this.uploadImg(img.file, this.imagList.indexOf(img));
    }
  }

  removeImg(index) {
    this.imagList.splice(index, 1);
  }

  clearCommonPlaces() {
    this.imagList = [];
  }

  viewRisk(sequence, productCode) {
  }

  removeRisk(data) {

    this.service.removeRisk(data).subscribe(resp => {
      const body = resp.body as any;
      this.getRiskAdditionalDetails();
      this.displaySuccessMessage(body);
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  removeCovers(data) {
    this.service.removeCovers(data).subscribe(resp => {
      const body = resp.body as any;
      this.getRiskAdditionalDetails();
      this.displaySuccessMessage(body);
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  backToPolicyAdditionalDetails() {
    this.getpolicyDetails();
    if (this.productType === 'CRPTY') {
      this.displayIndemnityAdditionalDetails = true;
      this.displayWeatherAdditionalDetails = false;
    } else {
      this.displayWeatherAdditionalDetails = true;
      this.displayIndemnityAdditionalDetails = false;
    }
    this.displayWeatherLivestockForm = false;
    this.finishedPolicyStep1 = true;
    this.displayAddLandForm = false;
    this.displayAddCoversForm = false;
  }

  setDayCountWeather() {

    if (this.fromYear !== '' && this.fromMonth !== '' && this.toYear !== '' && this.toMonth !== '') {
      //  set dates
      // tslint:disable-next-line:radix
      const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
      // @ts-ignore
      const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);
      // tslint:disable-next-line:radix
      const selectedToMonth = parseInt(moment().month(this.toMonth).format('M'));
      let selectedEndDate = moment([this.toYear, selectedToMonth - 1]);
      selectedEndDate = moment(selectedEndDate).endOf('month');
      const start = moment(selectedStartDate);
      const end = moment(selectedEndDate);
      // tslint:disable-next-line:radix
      this.weatherDayCount = parseInt(String(end.diff(start, 'day')));
    }
  }


  changeFromYearDropdown() {
    this.toMonth = '';
    this.toYear = '';
    this.fromMonth = '';
    this.setDayCountWeather();
    const currentYear = moment().get('year');
    // @ts-ignore
    if (parseInt(this.fromYear) === currentYear) {
      const currentMonth = moment().get('M');
      const testArr = [];
      for (let i = currentMonth; i < this.months.length; i++) {
        testArr.push(this.months[i]);
      }
      this.fromMonths = [];
      this.fromMonths = testArr;
    } else {
      this.fromMonths = this.months;
    }
  }

  changeFromMonthDropdown() {
    this.toMonth = '';
    this.toYear = '';
    this.setDayCountWeather();

    const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
    // @ts-ignore
    const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);

    /**
     * adding weather policy max day count
     */
    this.service.getWeatherProductMaxDayCount().subscribe(resp => {
      const body = resp.body as any;
      const maxDayCount = parseInt(body.data.maxNoOfPolicyDays);

      /**
       * get max day count and set to year and to month
       */
      const futureDate = moment(selectedStartDate).add(maxDayCount, 'days');
      const futureYear = moment(futureDate).get('year');
      const futureMonth = moment(futureDate).get('M');
      this.toYears = [];
      for (let i = parseInt(this.fromYear); i <= futureYear; i++) {
        this.toYears.push(String(i));
      }
      /**
       *  - if same year - have to get betwenn months
       *  - if other year
       */

      if (parseInt(this.fromYear) === futureYear) {
        const fromMonth = moment(selectedStartDate).get('M');
        this.toMonths = [];
        for (let i = fromMonth; i <= futureMonth; i++) {
          this.toMonths.push(this.months[i]);
        }
      } else {
        this.toMonths = [];
        for (let i = 0; i <= futureMonth; i++) {
          this.toMonths.push(this.months[i]);
        }
      }


    }, error => {
      this.displayErrorMessage(error);
    });

  }


  setDayCountLivestock() {

    const start = moment(this.liveStockStartDate);
    const end = moment(this.liveStockEndDate);
    this.livestockDayCount = end.diff(start, 'day');

  }

  changeToYearDropdown() {
    this.toMonth = '';
    this.setDayCountWeather();

    const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
    // @ts-ignore
    const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);

    /**
     * adding weather policy max day count
     */
    this.service.getWeatherProductMaxDayCount().subscribe(resp => {
      const body = resp.body as any;
      const maxDayCount = parseInt(body.data.maxNoOfPolicyDays);

      /**
       * get max day count and set to year and to month
       */
      const futureDate = moment(selectedStartDate).add(maxDayCount, 'days');
      const futureYear = moment(futureDate).get('year');
      const futureMonth = moment(futureDate).get('M');

      /**
       *  - if same year - have to get betwenn months
       *  - if other year
       */
      if (parseInt(this.fromYear) === futureYear) {
        const fromMonth = moment(selectedStartDate).get('M');
        this.toMonths = [];
        for (let i = fromMonth; i <= futureMonth; i++) {
          this.toMonths.push(this.months[i]);
        }
      } else {
        if (parseInt(this.toYear) < futureYear) {
          this.toMonths = [];
          const fromMonth = moment(selectedStartDate).get('M');
          for (let i = fromMonth; i < this.months.length; i++) {
            this.toMonths.push(this.months[i]);
          }
        } else {
          this.toMonths = [];
          for (let i = 0; i <= futureMonth; i++) {
            this.toMonths.push(this.months[i]);
          }
        }

      }


    }, error => {
      this.displayErrorMessage(error);
    });

  }

  setDate(data) {
    if (data === 'weather') {
      this.startDate = null;
      this.endDate = null;
      this.startDate = moment();
      this.endDate = moment();
      this.endDateMindate = null;
      this.endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());
      this.minDate = null;
      this.minDate = new Date();
    } else if (data === 'livestock') {
      this.minDate = null;
      this.minDate = new Date();
      this.maxDate1 = null;
      this.maxDate1 = new Date(moment().subtract(1, 'days').format());
      this.maxDate2 = null;
      this.maxDate2 = new Date(moment().format());
      this.liveStockStartDate = null;
      this.liveStockStartDate = moment();
      this.liveStockEndDate = null;
      this.livestockPolicyEnddateMindate = null;
      this.livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());
    }
  }


  clearWeatherPolicyForm() {
    this.cropType = '';
    this.weatherType = '';
    this.plan = '';
    this.businessChanel = '';
    this.startDate = null;
    this.endDate = null;
    this.paymentType = '';
    this.endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());
    this.cropTypeFormControl.reset();
    this.weatherTypeFormControl.reset();
    this.planFormControl.reset();
    this.businessChanelFormControl.reset();
    this.startDateFormControl.reset();
    this.endDateFormControl.reset();
    this.paymentFormControl.reset();
  }

  clearLivestockPolicyForm() {
    this.product = '';
    this.liveStockStartDate = null;
    this.liveStockEndDate = null;
    this.livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());
    this.liveStickBusinessChannel = '';
    this.premiumSettlementMethod = '';
    this.liveStockPlanFormControl.reset();
    this.liveStockStartDateFormControl.reset();
    this.liveStockEndDateFormControl.reset();
    this.liveStickBusinessChannelFormControl.reset();
    this.premiumSettlementMethodFormControl.reset();
  }


  clearLivestockForm() {

    this.tagNumber = '';
    this.liveStockType = '';
    this.breedType = '';
    this.gender = '';
    this.livestockPurchaseDate = null;
    this.ownerName = '';
    this.undewritingAssessor = '';
    this.newLivestockStartDate = null;
    this.riskSumInsured = '';
    this.age = '';
    this.usageType = '';
    this.tagNumberFormControl.reset();
    this.liveStockTypeFormControl.reset();
    this.breedTypeFormControl.reset();
    this.genderTypeFormControl.reset();
    this.ownerNameFormControl.reset();
    this.undewritingAssessorFormControl.reset();
    this.newLivestockStartDateFormControl.reset();
    this.riskSumInsuredFormControl.reset();
    this.ageTypeFormControl.reset();
    this.usageTypeFormControl.reset();


  }

  clearLandForm() {
    this.farmerName = '';
    this.cropVariance = '';
    this.weatherStation = 0;
    this.weatherLandStartDate = null;
    this.weatherLandEndDate = null;
    this.size1 = '';
    this.price = '';
    this.noOfUnits = '';
    this.markLandPlotString = '';
    this.size2 = '';
    this.weatherLandPremium = 0;
    this.farmerNameFormControl.reset();
    this.cropVarianceFormControl.reset();
    this.landWeatherStationFormControl.reset();
    this.weatherLandStartDateFormControl.reset();
    this.weatherLandEndDateFormControl.reset();
    this.size1FormControl.reset();
    this.size1UnitTypeFormControl.reset();
    this.markLandPlotString = '';
    this.noOfunitsFormControl.reset();
    this.priceFormControl.reset();
    this.landForm.reset();
  }

  clearCoverForm() {
    this.otherPeril = {};
    this.excessAmount = 0;
    this.excessPercentage = 0;
    this.perilPerilSumInsured = 0;
    this.excessAmountFormControl.reset();
    this.excessPercentageFormControl.reset();
    this.perilPerilSumInsuredFormControl.reset();
  }

  saveIndemnity() {
    const policy = {
      'branchCode': this.branchCode,
      'channelOfBuinessCode': this.indemnityPolicyDetails.businessChannel,
      'classCode': null,
      'currencyCode': null,
      'customerAddressSequence': this.addressSequnce,
      'customerCode': this.customerCode,
      'modeOfBusinessCode': null,
      'settlementModeCode': this.indemnityPolicyDetails.paymentType,
      'periodFromDate': moment(this.indemnityPolicyDetails.startDate).format('YYYY-MM-DD HH:mm:ss'),
      'periodToDate': moment(this.indemnityPolicyDetails.endDate).format('YYYY-MM-DD HH:mm:ss'),
      'policyRemark': this.policyRemark,
      'productCode': this.indemnityPolicyDetails.plan
    };

    this.service.getClassCode(this.indemnityPolicyDetails.plan).pipe(
      flatMap(response => {
        let body: any;
        body = response.body;
        policy.classCode = body.data.code;
        return this.service.getCurrencyCode();
      }),
      flatMap(response => {
        let body: any;
        body = response.body;
        policy.currencyCode = body.data;
        if (this.asCustomer === 'Y') {
          return this.service.getModeOfBusinessCustomer();
        } else if (this.asAgent === 'Y') {
          return this.service.getModeOfBusinessAgent();
        } else if (this.asInternalStaff === 'Y') {
          if (this.indemnityPolicyDetails.intermediaryTypeCode && this.indemnityPolicyDetails.intermediaryCode) {
            return this.service.getModeOfBusinessAgent();
          } else {
            return this.service.getModeOfBusinessCustomer();
          }
        }
      }),
      flatMap(response => {
        let body: any;
        body = response.body;
        if (this.asCustomer === 'Y') {
          policy.modeOfBusinessCode = body.data;
        } else if (this.asAgent === 'Y') {
          policy.modeOfBusinessCode = body.data.profile.intermediaryType;
        } else if (this.asInternalStaff === 'Y') {
          if (this.indemnityPolicyDetails.intermediaryTypeCode && this.indemnityPolicyDetails.intermediaryCode) {
            policy.modeOfBusinessCode = this.indemnityPolicyDetails.intermediaryTypeCode;
          } else {
            policy.modeOfBusinessCode = body.data;
          }
        }
        return this.service.savePolicy(policy);
      }),
      flatMap(response => {
        const body = response.body as any;
        this.policySequnce = body.data;
        let intermediaryTypeObj = {};
        if (this.asCustomer === 'Y') {
          intermediaryTypeObj = {
            'intermediaryTypeCode': this.intermediaryType,
            'intermediaryCode': this.branchCode,
            'isBusinessParty': 'Y',
            'isCommissionParty': 'N',
            'businessPartyshare': 100
          };
        } else if (this.asAgent === 'Y') {
          intermediaryTypeObj = {
            'intermediaryTypeCode': this.intermediaryType,
            'intermediaryCode': this.partyCode,
            'isBusinessParty': 'Y',
            'isCommissionParty': 'Y',
            'businessPartyshare': 100
          };
        } else if (this.asInternalStaff === 'Y') {
          if (this.indemnityPolicyDetails.intermediaryTypeCode && this.indemnityPolicyDetails.intermediaryCode) {
            intermediaryTypeObj = {
              'intermediaryTypeCode': this.indemnityPolicyDetails.intermediaryTypeCode,
              'intermediaryCode': this.indemnityPolicyDetails.intermediaryCode,
              'isBusinessParty': 'Y',
              'isCommissionParty': 'Y',
              'businessPartyshare': 100
            };
          } else {
            intermediaryTypeObj = {
              'intermediaryTypeCode': this.intermediaryType,
              'intermediaryCode': this.branchCode,
              'isBusinessParty': 'Y',
              'isCommissionParty': 'N',
              'businessPartyshare': 100
            };
          }
        }
        return this.service.addIntermediaries(this.policySequnce, intermediaryTypeObj);
      }),
      flatMap(response => {
        return this.service.getPolicy(this.policySequnce, this.authorizedDate);
      })
    ).subscribe((response) => {
      const body = response.body as any;
      this.displaySuccessMessage(body);
      this.displayIndemnityAdditionalDetails = true;
      this.displayWeatherLivestockForm = false;
      this.finishedPolicyStep1 = true;
      this.policyFormActivated = true;
      this.policyAdditionalDetailsActivated = true;
      this.isDataLoaded = false;
      this.getpolicyDetails();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  saveIndemnityLand() {
    this.isDataLoaded = false;
    const locationObj = {
      'location': this.location['address'],
      'locationOrder': this.locationOrderNo,
      'geometry': this.pathList
    };
    this.service.addLocation(this.policySequnce, locationObj).pipe(
      flatMap(response => {
        const respBody = response.body as any;
        this.locationSequence = respBody.data;
        const riskObj = {
          'riskOrder': null,
          'riskName': this.location['plotName'],
          'riskRefNo': null,
          'riskSumInsured': this.indemnitySumInsured.value,
          'riskSectionCode': this.indemnityCultivationCropType.value,
          'effectiveDate': moment(this.indemnityLandStartDate.value).format('YYYY-MM-DD HH:mm:ss'),
          'effectiveToDate': moment(this.indemnityLandEndDate.value, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
          'size1': this.indemnityAcres.value,
          'size1UnitType': this.size1UnitType,
          'size2': this.indemnityPerches.value,
          'size2UnitType': this.size2UnitType
        };
        return this.service.addRisks(this.locationSequence, riskObj);
      }),
      flatMap(response => {
        const risks = response.body as any;
        this.riskSequnce = risks.data;
        return this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate);
      }),
      flatMap(response => {
        const body_3 = response.body as any;
        const resultArr = body_3.data;
        let cropSequence = {};
        let farmerNameSequence = {};
        let cropTypeSequence = {};
        let cropVarianceSequence = {};

        for (let i = 0; i < resultArr.length; i++) {
          const obj = resultArr[i];
          if (obj['description'] === 'CROP VARIANCE') {
            cropVarianceSequence = {
              sequenceNo: obj['sequenceNo'],
              description: obj['description'],
              version: obj['version'],
              value: this.indemnityCropVariance.value
            };
          } else if (obj['description'] === 'FARMER NAME') {
            farmerNameSequence = {
              sequenceNo: obj['sequenceNo'],
              description: obj['description'],
              version: obj['version'],
              value: this.indemnityFarmerName.value
            };
          } else if (obj['description'] === 'CROP') {
            cropSequence = {
              sequenceNo: obj['sequenceNo'],
              description: obj['description'],
              version: obj['version'],
              value: this.indemnityCultivationCrop.value
            };
          } else if (obj['description'] === 'CROP CATEGORY') {
            cropTypeSequence = {
              sequenceNo: obj['sequenceNo'],
              description: obj['description'],
              version: obj['version'],
              value: this.indemnityCultivationCropType.value,
            };
          }
        }

        const updatedRiskCommonInfo = [
          cropVarianceSequence,
          farmerNameSequence,
          cropSequence,
          cropTypeSequence
        ];
        return this.service.UpdateRiskCommonInformation(this.riskSequnce, updatedRiskCommonInfo);
      })
    ).subscribe(response => {
      const body_4 = response.body as any;
      // set msg display
      this.viewAddLand = true;
      this.viewAddCultivation = false;
      this.viewAddLandPhotos = false;
      this.disabledLocation = true;
      this.displaySuccessMessage(body_4);
      this.isDataLoaded = false;
      this.enableContinueButtonWeatherLand = false;
      this.disabledCultivationFeilds = true;
      this.indemnityCropVariance.disable();
      this.indemnityCultivationCrop.disable();
      this.indemnityCultivationCropType.disable();
      this.displayWeatherIndexParametersDiv = false;
      this.getRiskAdditionalDetails();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  setIndemnityData(data: any) {
    this.indemnityPolicyDetails = data;
    this.indemnityLandMaxDate = moment(this.indemnityPolicyDetails.indemnityEndDate).toDate();
    this.indemnityLandEndDateMin = moment(this.indemnityPolicyDetails.indemnityStartDate).toDate();
    this.indemnityLandStarDateMin = moment(this.indemnityPolicyDetails.indemnityStartDate).toDate();
  }

  activateSaveIndemnity(value: boolean) {
    this.isFormValid = value;
  }

  indemnityLandStartDateChanged() {
    this.indemnityLandEndDateMin = moment(this.indemnityLandStartDate.value).add(1, 'day').toDate();
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }
}






