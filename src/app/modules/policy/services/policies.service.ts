import {Injectable} from '@angular/core';
import {HTTPCallService} from '../../../services/httpcall.service';
import {Observable} from 'rxjs';
import {CommonType} from '../../../model/CommonType';
import {catchError, map} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';
import {PolicyCommonInformation} from '../models/policy-common-information.model';
import {Policy} from '../models/policy.model';
import {Outstanding} from '../models/Outstanding';
import {RiskList} from '../models/risk-list.model';
import {Risk} from '../models/risk.model';
import {Location} from '../models/location.model';
import {RiskCommonInformation} from '../models/risk-common-information.model';
import {error} from 'util';


@Injectable({
  providedIn: 'root'
})
export class PoliciesService {
  constructor(private http: HTTPCallService) {
  }

  /*
 * Crop Types
 */
  getCropTypes(): Observable<CommonType[]> {
    const path = 'base/cropTypes';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
   * Weather Types
   */
  getWeatherTypes(): Observable<CommonType[]> {
    const path = 'base/weatherTypes';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
   * Plans
   */
  getPlans(branchCode, cropTypeCode, weatherTypeCode): Observable<CommonType[]> {
    const path = 'uw/policies/products/search?branchCode=' + branchCode + '&cropTypeCode=' +
      cropTypeCode + '&weatherTypeCode=' + weatherTypeCode;
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*Indemnity Products */
  getIndemnityPlans(branchCode): Observable<CommonType[]> {
    const path = 'uw/policies/products/indemnity?branchCode=' + branchCode;
    return this.http.getResource(path).pipe(
      map((resp) => {
        // @ts-ignore
        return resp.body.data.map(item => {
          const result: CommonType = new CommonType();
          result.code = item.code;
          result.description = item.description;
          return result;
        });
      }),
      catchError(err => {
        return null;
      })
    );
  }

  /*
   * Business Channels
   */
  getBusinessChannels(): Observable<CommonType[]> {
    const path = 'base/businessChannels';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
   * Payment Types
   */
  getPaymentTypes(): Observable<CommonType[]> {
    const path = 'base/premiumSettlementMethods';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
  * Livestock Types
   */
  getLiveStockTypes(): Observable<CommonType[]> {
    const path = 'base/livestockTypes';
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
  * Breed Types
   */
  getBreedTypes(): Observable<CommonType[]> {
    const path = 'base/breedTypes';
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
  * Gender Types
   */
  getGenders(): Observable<CommonType[]> {
    const path = 'base/genders';
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
  * Usage Types
   */
  getUsageTypes(): Observable<CommonType[]> {
    const path = 'base/usageTypes';
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
  * Assessors
   */
  getUnderwritingAssessors(): Observable<CommonType[]> {
    const path = 'base/parties/uwAssessors';
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  /*
  * Products
   */
  getProducts(branchCode): Observable<CommonType[]> {
    const path = 'uw/policies/products/livestock?branchCode=' + branchCode;
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }


  /*
  * save policy
   */
  savePolicy(data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies';
    return this.http.postResource(path, data);
  }

  /**
   * get Policy Details
   */
  getPolicyDetails(sequence): Observable<Policy> {
    const path = 'uw/policies/' + sequence ;
    return this.http.getResource(path).pipe(
      map(response => {
        let policy: Policy = new Policy();
        // @ts-ignore
        policy = response.body.data;
        return policy;
      })
    );
  }
  /*
   *  get policy common information
   */
  getPolicyCommonInformation(sequence): Observable<PolicyCommonInformation[]> {
    const path = 'uw/policies/' + sequence + '/commonInformation';
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: PolicyCommonInformation = new PolicyCommonInformation();
          x.sequenceNo = item.sequenceNo;
          x.description = item.description;
          x.value = item.value;
          x.version = item.version;
          return x;
        });
      })
    );
  }

  /*
  * update policy
   */
  updatePolicyCommonInformation(sequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + sequence + '/commonInformation';
    return this.http.putResource(path, data);
  }

  getClassCode(planCode): Observable<HttpResponse<Object>> {
    const path = 'base/products/' + planCode + '/classes';
    return this.http.getResource(path);
  }
  /*
  * Mode of Business of the policy - CUSTOMER
   */
  getModeOfBusinessCustomer(): Observable<HttpResponse<Object>> {
    const path = 'base/defaultModeOfBusinessCode';
    return this.http.getResource(path);
  }
  /*
  *  Mode of Business of the policy - AGENT
   */
  getModeOfBusinessAgent(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles';
    return this.http.getResource(path);
  }
  getCurrencyCode(): Observable<HttpResponse<Object>> {
    const path = 'base/localCurrencyCode';
    return this.http.getResource(path);
  }
  getCustomerDetails(customerCode) {
    const path = 'base/parties/' + customerCode + '/profiles';
    return this.http.getResource(path);
  }
  saveIntermediaries(policySequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/intermediaries';
    return this.http.postResource(path, data);
  }
  saveDefaultLocation(policySequence): Observable<HttpResponse<Object>> {
    const data = {};
    const path = 'uw/policies/' + policySequence + '/defaultLocation';
    return this.http.postResource(path, data);
  }
  getRiskListSelectedPolicy(policySequence):  Observable<RiskList[]> {
    const path = 'uw/policies/' + 'SOLRL1907000000000701' + '/risks/search?riskName=&page=1&limit=100';
    return this.http.getResource(path).pipe(
      map(response => {
        console.log(response);
        // @ts-ignore
        return response.body.data.map(item => {
          const z: RiskList = new RiskList();
         // item.location.map(itemChild1 => {
         //   const z1: Location = new Location();
         //   z1.annualLimit = itemChild1.location.annualLimit;
         //
         // });
         //  item.riskInformation.map(itemChild2 => {
         //    const z2: RiskCommonInformation = new RiskCommonInformation();
         //    z.riskInformation.push(z2);
         //  });
console.log(item.location);
          console.log(item.location);
          console.log(item.riskInformation);
          return z;
        });
      })
    );
  }


}
