import {Injectable} from '@angular/core';
import {HTTPCallService} from 'src/app/services/httpcall.service';
import {HttpEvent, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CommonType} from '../../../model/CommonType';
import {flatMap, map} from 'rxjs/operators';
import {Outstanding} from '../models/Outstanding';
import {Settlement} from '../models/Settlement';
import {fileServer} from '../../../config/enum';

@Injectable({
  providedIn: 'root'
})
export class PolicyService {

  constructor(private http: HTTPCallService) {
  }

  /* Crop Types */
  getCropTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/cropTypes';
    return this.http.getResource(path);
  }

  /*Weather Types*/
  getWeatherTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/weatherTypes';
    return this.http.getResource(path);
  }

  /*Plans*/
  getPlans(branchCode, cropTypeCode, weatherTypeCode): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/products/search?branchCode=' + branchCode + '&cropTypeCode=' +
      cropTypeCode + '&weatherTypeCode=' + weatherTypeCode;
    return this.http.getResource(path);
  }

  /*Business Chanels*/
  getBusinessChanels(): Observable<HttpResponse<Object>> {
    const path = 'base/businessChannels';
    return this.http.getResource(path);
  }

  /*Payment Types*/
  getPaymentTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/premiumSettlementMethods';
    return this.http.getResource(path);
  }

  savePolicy(data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies';
    return this.http.postResource(path, data);
  }

  /* get policy common information */
  getPolicy(sequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + sequence + '/commonInformation';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + sequence + '/commonInformation';
      return this.http.getResource(path);
    }
  }

  updatePolicy(sequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + sequence + '/commonInformation';
    return this.http.putResource(path, data);
  }

  getClassCode(planCode): Observable<HttpResponse<Object>> {
    const path = 'base/products/' + planCode + '/classes';
    return this.http.getResource(path);
  }

  /*Mode of Business of the policy - CUSTOMER*/
  getModeOfBusinessCustomer(): Observable<HttpResponse<Object>> {
    const path = 'base/defaultModeOfBusinessCode';
    return this.http.getResource(path);
  }

  /*Mode of Business of the policy - AGENT*/
  getModeOfBusinessAgent(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles';
    return this.http.getResource(path);
  }

  /*get Currency code*/
  getCurrencyCode(): Observable<HttpResponse<Object>> {
    const path = 'base/localCurrencyCode';
    return this.http.getResource(path);
  }

  /* get intermediary types */
  getIntermediaryTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=PTSLS';
    return this.http.getResource(path);
  }

  /** getintermediaryTypeCode */
  getIntermediaryTypeData(value): Observable<HttpResponse<Object>> {
    const path = 'uw/intermediaries/search?intermediaryTypeCode=' + value + '';
    return this.http.getResource(path);
  }


  /*-------------------------- Land Details ----------------------------------------------//*/

  /*weather stations*/
  getWeatherStations(): Observable<HttpResponse<Object>> {
    const path = 'base/weatherStations/search?name=';
    return this.http.getResource(path);
  }

  /*crop variances*/
  getCropVariances(cropType): Observable<HttpResponse<Object>> {
    const path = 'base/cropTypes/' + cropType + '/cropVariances';
    return this.http.getResource(path);
  }

  getIndemnityCropTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=CRPCT';
    return this.http.getResource(path);
  }

  getIndemnityCrops(cropTypeCode: string): Observable<HttpResponse<Object>> {
    const path = 'base/references?type=CRPTY&parentCode=' + cropTypeCode;
    return this.http.getResource(path);
  }

  getLocations(customerCode): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/search?partyCode=' + customerCode;
    return this.http.getResource(path);
  }

  addLocation(policySequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/locations';
    return this.http.postResource(path, data);
  }

  addRisks(locationSequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/locations/' + locationSequence + '/risks';
    return this.http.postResource(path, data);
  }

  getRiskCommonInformation(riskSequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/risks/' + riskSequence + '/commonInformation';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/risks/' + riskSequence + '/commonInformation';
      return this.http.getResource(path);
    }

  }

  UpdateRiskCommonInformation(riskSequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/' + riskSequence + '/commonInformation';
    return this.http.putResource(path, data);
  }

  /* Live stock part - when selected on livestock from dropdown*/
  getLiveStockTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/livestockTypes';
    return this.http.getResource(path);
  }

  getBreedTypes(livestockType: string): Observable<HttpResponse<Object>> {
    let path = 'base/breedTypes';
    if (livestockType) {
      path = 'base/references?type=BRDTY&parentCode=' + livestockType;
    }
    return this.http.getResource(path);
  }

  getGenders(): Observable<HttpResponse<Object>> {
    const path = 'base/genders';
    return this.http.getResource(path);
  }

  getUsageTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/usageTypes';
    return this.http.getResource(path);
  }

  /** inspected by */
  getUnderwritingAssessors(): Observable<HttpResponse<Object>> {
    const path = 'base/parties/uwAssessors';
    return this.http.getResource(path);
  }

  /*Livestock policy*/
  getProducts(branchCode): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/products/livestock?branchCode=' + branchCode;
    return this.http.getResource(path);
  }

  getPremiumSettlementMethods(): Observable<HttpResponse<Object>> {
    const path = 'base/premiumSettlementMethods';
    return this.http.getResource(path);
  }

  getDefaultLocation(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence + '/locations';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence + '/locations';
      return this.http.getResource(path);
    }
  }

  /*save inspected by and inspected date feilds*/
  saveInspectedDetails(riskSequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/' + riskSequence + '/assessments';
    return this.http.postResource(path, data);
  }

  /*get assessors to manage*/
  getAssessors(riskSequnce, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path =  'uw/risks/' + riskSequnce + '/assessments';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/risks/' + riskSequnce + '/assessments';
      return this.http.getResource(path);
    }
  }

  /*update assessors*/
  updateInspectedDetails(assessorSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/assessments/' + assessorSequnce;
    return this.http.putResource(path, data);
  }

  getAssessorDetails(assessorSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/assessments/' + assessorSequnce;
    return this.http.getResource(path);
  }

  /* Perils / Covers*/
  saveCovers(riskSequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/' + riskSequence + '/perils';
    return this.http.postResource(path, data);
  }

  getOtherPerils(riskSequence): Observable<HttpResponse<Object>> {
    const path = 'uw/otherPerils?riskSequence=' + riskSequence;
    return this.http.getResource(path);
  }

  getRiskPerils(riskSequence, authorizedDate) {
    if (authorizedDate == null) {
      const path = 'uw/risks/' + riskSequence + '/perils';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/risks/' + riskSequence + '/perils';
      return this.http.getResource(path);
    }
  }

  saveDefaultLocation(policySequence): Observable<HttpResponse<Object>> {
    const data = {};
    const path = 'uw/policies/' + policySequence + '/defaultLocation';
    return this.http.postResource(path, data);
  }

  saveExcess(perilSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/perils/' + perilSequnce + '/excesses';
    return this.http.postResource(path, data);
  }

  getRiskDocuments(riskSequence, authorizedDate): Observable<HttpResponse<Object>> {

    if (authorizedDate == null) {
      const path = 'uw/risks/' + riskSequence + '/documents';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/risks/' + riskSequence + '/documents';
      return this.http.getResource(path);
    }
  }

  /*view and search related services*/

  // tslint:disable-next-line:max-line-length
  searchedPolicies(searchValue, customerCode, salePersonCode, searchFeild, mecode, branchCode, productCode, policyStatus): Observable<HttpResponse<Object>> {
    const classCode = 'MC';


    if (searchFeild === 'CN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/policies/search?branchCode=' + branchCode + '&classCode=' + classCode + '&productCode=' + productCode + '&policyNumber=&proposalNumber=&customerName=' + searchValue + '&riskName=&customerNIC=&status=' + policyStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode;
      return this.http.getResource(path);
    } else if (searchFeild === 'RN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/policies/search?branchCode=' + branchCode + '&classCode=' + classCode + '&productCode=' + productCode + '&policyNumber=&proposalNumber=&customerName=&riskName=' + searchValue + '&customerNIC=&status=' + policyStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode;
      return this.http.getResource(path);
    } else if (searchFeild === 'PN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/policies/search?branchCode=' + branchCode + '&classCode=' + classCode + '&productCode=' + productCode + '&policyNumber=' + searchValue + '&proposalNumber=&customerName=&riskName=&customerNIC=&status=' + policyStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode;
      return this.http.getResource(path);
    } else if (searchFeild === 'PRN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/policies/search?branchCode=' + branchCode + '&classCode=' + classCode + '&productCode=' + productCode + '&policyNumber=&proposalNumber=' + searchValue + '&customerName=&riskName=&customerNIC=&status=' + policyStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode;
      return this.http.getResource(path);
    }

  }

  /*view and search paginated services*/
  searchedPaginatedPolicies(searchValue, customerCode, salePersonCode, searchFeild, page, size): Observable<HttpResponse<Object>> {
    const classCode = 'MC';
    if (searchFeild === 'CN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/policies/paginated_policy_search?branchCode=&classCode=' + classCode + '&productCode=&policyNumber=&proposalNumber=&customerName=' + searchValue + '&riskName=&customerNIC=&status=&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&page=' + page + '&size=' + size;
      return this.http.getResource(path);
    } else if (searchFeild === 'RN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/policies/paginated_policy_search?branchCode=&classCode=' + classCode + '&productCode=&policyNumber=&proposalNumber=&customerName=&riskName=' + searchValue + '&customerNIC=&status=&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&page=' + page + '&size=' + size;
      return this.http.getResource(path);
    } else if (searchFeild === 'PN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/policies/paginated_policy_search?branchCode=&classCode=' + classCode + '&productCode=&policyNumber=' + searchValue + '&proposalNumber=&customerName=&riskName=&customerNIC=&status=&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&page=' + page + '&size=' + size;
      return this.http.getResource(path);
    } else if (searchFeild === 'PRN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/policies/paginated_policy_search?branchCode=&classCode=' + classCode + '&productCode=&policyNumber=&proposalNumber=' + searchValue + '&customerName=&riskName=&customerNIC=&status=&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&page=' + page + '&size=' + size;
      return this.http.getResource(path);
    }
  }

  getIntermediarySequence(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence + '/intermediaries';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence + '/intermediaries';
      return this.http.getResource(path);
    }
  }

  getIntermediaryDetails(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence + '/intermediaries';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence + '/intermediaries';
      return this.http.getResource(path);
    }
  }

  getPolicyDetails(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence;
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence;
      return this.http.getResource(path);
    }
  }


  getRisksSelectedPolicySequnce(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence + '/risks/search?riskName=&page=1&limit=100';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence + '/risks/search?riskName=&page=1&limit=100';
      return this.http.getResource(path);
    }
  }


  /* get risk*/
  getRiskDetails(riskSequnce, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/locations/risks/' + riskSequnce;
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/locations/risks/' + riskSequnce;
      return this.http.getResource(path);
    }
  }

  getPolicyImages(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence + '/documents';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence + '/documents';
      return this.http.getResource(path);
    }
  }

  uploadImage(file: File): Observable<HttpEvent<Object>> {
    const fd = new FormData();
    fd.append('file', file);
    const path = fileServer + 'v1/files/upload?path=policy';
    return this.http.postImage(path, fd);
  }

  updateCompletePolicy(policySequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/completed';
    return this.http.putResource(path, data);
  }

  getCompletePolicy(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence + '/completed';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence + '/completed';
      return this.http.getResource(path);
    }
  }

  updatePolicyForm(policySequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequnce;
    return this.http.putResource(path, data);
  }

  updateIntermediaryDetails(intermediarySequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/intermediaries/' + intermediarySequence;
    return this.http.putResource(path, data);
  }


  // upload images
  saveWeatherImages(documentSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/documents/' + documentSequnce + '/images';
    return this.http.postResource(path, data);
  }

  getPolicyDocumentSequnce(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence + '/documents';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence + '/documents';
      return this.http.getResource(path);
    }
  }

  saveRiskImages(documentSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/documents/' + documentSequnce + '/images';
    return this.http.postResource(path, data);
  }

  getRiskDocumentSequnce(riskSequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/risks/' + riskSequence + '/documents';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/risks/' + riskSequence + '/documents';
      return this.http.getResource(path);
    }
  }

  // get cover deatils
  getCoverDetails(perilSequnce, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/risks/perils/' + perilSequnce;
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/risks/perils/' + perilSequnce;
      return this.http.getResource(path);
    }
  }

  updateCover(perilSequnce, data) {
    const path = 'uw/risks/perils/' + perilSequnce;
    return this.http.putResource(path, data);
  }

  updateRisk(riskSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/locations/risks/' + riskSequnce;
    return this.http.putResource(path, data);
  }

  getExcess(perilSequnce, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/perils/' + perilSequnce + '/excesses';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/perils/' + perilSequnce + '/excesses';
      return this.http.getResource(path);
    }
  }


  updateExcess(perilSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/perils/excesses/' + perilSequnce;
    return this.http.putResource(path, data);
  }

  premiumReceipt(Sequnce) {
    const path = 'rc/premiumReceipt/' + Sequnce + '/print';
    return this.http.downloadFile(path);
  }

  downloadImages(documentSequnce, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path =  'uw/policies/documents/' + documentSequnce + '/download';
      return this.http.getResource(path);
    } else {
      const path =  'uw/default/policies/documents/' + documentSequnce + '/download';
      return this.http.getResource(path);
    }
  }

  getAuthorizedPolicy(policySequence, authorizedDate): Observable<HttpResponse<Object>> {
    if (authorizedDate == null) {
      const path = 'uw/policies/' + policySequence + '/auths';
      return this.http.getResource(path);
    } else {
      const path = 'uw/default/policies/' + policySequence + '/auths';
      return this.http.getResource(path);
    }
  }

  updateAuthorizedPolicy(policySequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/auths';
    return this.http.putResource(path, data);
  }

  downloadWeatherIndividualFile(policySequence) {
    const path = 'uw/policies/' + policySequence + '/weatherIndexSchedule/print ';
    return this.http.downloadFile(path);
  }

  downloadWeatherGroupFile(policySequence) {
    const path = 'uw/policies/' + policySequence + '/weatherGroupSchedule/print';
    return this.http.downloadFile(path);
  }

  downloadLivestockIndividualFile(policySequence) {
    const path = 'uw/policies/' + policySequence + '/livestockSchedule/print';
    return this.http.downloadFile(path);
  }

  downloadLivestockGroupFile(policySequence) {
    const path = 'uw/policies/' + policySequence + '/livestockGroupSchedule/print ';
    return this.http.downloadFile(path);
  }

  getUnitPriceOnRisk(weatherStation): Observable<HttpResponse<Object>> {
    const path = 'base/products/WP/unitPrices/search?weatherStationSequence=' + weatherStation;
    return this.http.getResource(path);
  }

  getLocationFromUtility(seqNo) {
    const path = 'utility/locations/' + seqNo;
    return this.http.getResource(path);
  }

  removeRisk(riskSequence): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/' + riskSequence;
    return this.http.deleteResource(path);
  }

  removeCovers(perilSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/perils/' + perilSequnce;
    return this.http.deleteResource(path);
  }


  getNearestWeatherStation(data): Observable<HttpResponse<Object>> {
    const path = 'uw/nearestWeatherStation';
    return this.http.postResource(path, data);
  }

  getcustomerDetails(customerCode) {
    const path = 'base/parties/' + customerCode + '/profiles';
    return this.http.getResource(path);
  }

  addIntermediaries(policySequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/intermediaries';
    return this.http.postResource(path, data);
  }

  getWeatherProductMaxDayCount(): Observable<HttpResponse<Object>> {
    const path = 'base/products/WP';
    return this.http.getResource(path);
  }

  getLoggedUserProfile(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles';
    return this.http.getResource(path);
  }


  getWeatherindexParameters(fromYear, fromMonth, weatherStationSequnce, toYear, toMonth): Observable<HttpResponse<Object>> {
    // tslint:disable-next-line:max-line-length
    const path = 'base/weatherIndexParameters/search?fromYear=' + fromYear + '&fromMonth=' + fromMonth + '&weatherStationSequence=' + weatherStationSequnce + '&toYear=' + toYear + '&toMonth=' + toMonth;
    return this.http.getResource(path);
  }

  getPolicyOutstandings(policyNumber): Observable<HttpResponse<Object>> {
    const path = 'rc/outstandings/search?policyNumber=' + policyNumber;
    return this.http.getResource(path);
  }

  /************************************** Receipting *************************************/
  getPaymentModes(): Observable<CommonType[]> {
    const path = 'base/paymentModes';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getOutstandingDetails(debitNoteNumber: string): Observable<HttpResponse<Object>> {
    const pathBySequence = 'uw/debitnote/' + debitNoteNumber;
    let pathGetDetails = 'rc/outstandings/search?debitNoteNumber=';
    return this.http.getResource(pathBySequence).pipe(
      flatMap((resp: any) => {
      pathGetDetails = pathGetDetails + resp.body.data.debitNoteNumber;
      return this.http.getResource(pathGetDetails);
    }));
  }

  saveSettlement(settlement: Settlement): Observable<HttpResponse<Object>> {
    const path = 'rc/settlements';
    return this.http.postResource(path, settlement);
  }

  getBanks(): Observable<CommonType[]> {
    const path = 'base/banks';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getBranches(code: string): Observable<CommonType[]> {
    const path = 'base/references?type=BKBRN&parentCode=' + code;
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }

  getMobileOperators(): Observable<CommonType[]> {
    const path = 'base/mobileOperators';
    // @ts-ignore
    return this.http.getResource(path).pipe(
      map(response => {
        // @ts-ignore
        return response.body.data.map(item => {
          const x: CommonType = new CommonType();
          x.code = item.code;
          x.description = item.description;
          return x;
        });
      })
    );
  }



}
