import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PoliciesService} from '../services/policies.service';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {CommonType} from '../../../model/CommonType';
import * as moment from 'moment';
import { PolicyService } from '../services/policy.service';

@Component({
  selector: 'app-add-policy-indemnity',
  templateUrl: './add-policy-indemnity.component.html',
  styleUrls: ['./add-policy-indemnity.component.scss']
})
export class AddPolicyIndemnityComponent implements OnInit {

  indemnityPolicyForm: FormGroup;
  cropTypes: CommonType[];
  businessChannels: CommonType[];
  plans: CommonType[];
  paymentTypes: CommonType[];
  branchCode: string;
  startMinDate: Date;
  endMinDate: Date;
  intermediaryTypes: [];
  intermediaryData: any;
  intermediaryFilteredList: any;
  intermediarySequence: any;
  intermediaryTypeName: any;
  asInternalStaff: string;
  asCustomer: string;

  @Output() savePolicy = new EventEmitter();
  @Output() formValid: EventEmitter <boolean> = new EventEmitter<boolean>();

  constructor(
    private policiesService: PoliciesService,
    private policyService: PolicyService,
    private fb: FormBuilder,
    public notificationService: NotificationDataTransferService  ) { }

  ngOnInit() {
    this.fetchCropTypes();
    this.fetchBusinessChannels();
    this.fetchPaymentTypes();
    this.getIntermediaryTypes();

    this.asInternalStaff = sessionStorage.getItem('partyAsInternalStaff') ? sessionStorage.getItem('partyAsInternalStaff') : 'N';
    this.branchCode = sessionStorage.getItem('branchCode');
    this.startMinDate = new Date();
    this.endMinDate = new Date();
    this.asCustomer = sessionStorage.getItem('asCustomer');

    this.indemnityPolicyForm = this.fb.group({
      plan: ['',  Validators.required ],
      businessChannel : ['', Validators.required],
      startDate : ['', Validators.required],
      endDate : ['', Validators.required],
      dayCount : [''],
      paymentType : ['', Validators.required]
    });

    if (this.asInternalStaff === 'Y') {
      this.indemnityPolicyForm.addControl('intermediaryTypeCode', new FormControl());
      this.indemnityPolicyForm.addControl('intermediaryCode', new FormControl());
      this.indemnityPolicyForm.addControl('intermediaryName', new FormControl());
    }

    this.indemnityPolicyForm.reset();

    this.fetchPlans();

    this.indemnityPolicyForm.statusChanges.subscribe(() => {
      this.formValid.emit(this.indemnityPolicyForm.valid);
      this.savePolicy.emit(this.indemnityPolicyForm.value);
    });
  }

  // form control
  get cropType() {return this.indemnityPolicyForm.get('cropType'); }
  get plan() {return this.indemnityPolicyForm.get('plan'); }
  get businessChannel() {return this.indemnityPolicyForm.get('businessChannel'); }
  get startDate() {return this.indemnityPolicyForm.get('startDate'); }
  get endDate() {return this.indemnityPolicyForm.get('endDate'); }
  get dayCount() {return this.indemnityPolicyForm.get('dayCount'); }
  get paymentType() {return this.indemnityPolicyForm.get('paymentType'); }
  get intermediaryTypeCode() {return this.indemnityPolicyForm.get('intermediaryTypeCode'); }
  get intermediaryCode() {return this.indemnityPolicyForm.get('intermediaryCode'); }
  get intermediaryName() {return this.indemnityPolicyForm.get('intermediaryName'); }


  fetchCropTypes() {
    this.policiesService.getCropTypes().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.cropTypes = typeList;
    });
  }

  fetchPlans() {
    const branchCode = this.branchCode;
    this.policiesService.getIndemnityPlans(branchCode).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.plans = typeList;
    });
  }

  fetchBusinessChannels() {
    this.policiesService.getBusinessChannels().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.businessChannels = typeList;
    });
  }

  fetchPaymentTypes() {
    this.policiesService.getPaymentTypes().pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.paymentTypes = [];
      if (this.asCustomer === 'Y') {
        for (let i = 0; i < typeList.length; i++) {
          if (typeList[i]['code'] === 'UPFRN') {
            this.paymentTypes.push(typeList[i]);
          }
        }
      } else {
        this.paymentTypes = typeList;
      }
    });
  }

  setDayCount() {
    const start = moment(this.startDate.value);
    const end = moment(this.endDate.value);
    this.dayCount.setValue(end.diff(start, 'day'));
  }

  setMinEndDate() {
    this.endMinDate = moment(this.startDate.value).add(1, 'day').toDate();
  }

  /** get intermediary types   */
  getIntermediaryTypes() {
    this.policyService.getIntermediaryTypes().subscribe(resp => {
      const body = resp.body as any;
      const arry = body.data;
      this.intermediaryTypes = arry.filter(itm => {
        return itm.code !== 'DIRCT';
      });
    });
  }

  /** get intermediary data */
  getIntermediaryData(value: string) {
    this.policyService.getIntermediaryTypeData(value).subscribe(resp => {
      const body = resp.body as any;
      this.intermediaryData = body.data;
      this.intermediaryFilteredList = body.data;
    });
  }

  /** Intermediary Name search */
  searchIntermediaryName(event) {
    const text: string = event.target.value;
    this.intermediaryFilteredList = this.intermediaryData.filter(item => {
      return item.description.toUpperCase().match(text.toUpperCase());
    });

  }

  selectIntermediaryName(intermediaryCode: string) {
    this.intermediaryCode.setValue(intermediaryCode);
  }

}
