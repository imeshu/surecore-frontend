import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPolicyIndemnityComponent } from './add-policy-indemnity.component';

describe('AddPolicyIndemnityComponent', () => {
  let component: AddPolicyIndemnityComponent;
  let fixture: ComponentFixture<AddPolicyIndemnityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPolicyIndemnityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPolicyIndemnityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
