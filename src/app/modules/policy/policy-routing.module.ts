import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PoliciesComponent } from './policies/policies.component';
import { PolicyFormComponent } from './policy-form/policy-form.component';
import {PaymentComponent} from './payment/payment.component';
import {PoliciesMainComponent} from './policies-main/policies-main.component';

const routes: Routes = [
  {
    path: 'policies',
    component: PoliciesComponent
  },
  {
    path: 'policy/:customerCode/:parent',
    component: PolicyFormComponent
  },
  {
    path: 'policy/payment',
    component: PaymentComponent
  },
  {
    path: 'policy',
    component: PoliciesMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PolicyRoutingModule { }
