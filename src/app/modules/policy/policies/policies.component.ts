/* tslint:disable:comment-format no-inferrable-types prefer-const quotemark semicolon no-shadowed-variable radix */

import { PolicyService } from '../services/policy.service';
import { Component, EventEmitter, OnInit, Output, Pipe } from '@angular/core';
import * as moment from 'moment';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificationDataTransferService} from 'src/app/services/notification-data-transfer.service';
import {HttpEventType} from '@angular/common/http';
// import {ImageModel} from '../models/image-model';
import {CommonType} from 'src/app/model/CommonType';
import {LocationModel} from '../../land/models/location-model';
import {LandService} from '../../land/services/land.service';
import {LandModel} from '../../customer/models/land-model';
import {ActivatedRoute, Router} from '@angular/router';
import {TitleCasePipe} from '@angular/common';
import {MatDialog} from '@angular/material';
import {PolicyPopupComponent} from './policy-popup/policy-popup.component';
import {Outstanding} from '../models/Outstanding';
import {PoliciesService} from '../services/policies.service';
import {catchError, flatMap} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {ImageViewerService} from '../../common-modules/image-viewer/service/image-viewer.service';
import {ImageModel} from '../../../model/image.model';

@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.scss']
})
export class PoliciesComponent implements OnInit {

  weatherIndexParameters = [];
  displayWeatherIndexParametersDiv = false;
  weatherIndexParameter = '';
  activePolicyIndex: number = 0;
  private outstanding: Outstanding;
  balance: number;
  isDisplayPrintButton = false;

  disabledCultivationFeilds = false;

  // common variables
  branchCode = '';
  policySequnce = '';
  partyCode: string = '';
  asCustomer: string = '';
  asAgent: string = '';
  addressSequnce: string = '';
  locationSequence = '';
  cropTypeGlobal = '';
  riskSequnce = '';
  asInternalStaff = false;
  policyStatus = '';
  salesPersonCode = '';
  meCodeCheck = false;
  policyStartDate: '';
  policyEndDate: '';


  parentPage = 'p';

  weatherDayCount = 0;
  livestockDayCount = 0;

  public policies = [];
  public searchValue: string = '';
  public risks = [];
  public images = [];
  public cropTypeDescription: string = '';
  public weatherDescription: string = '';
  public channelOfBuinessDescription: string = '';
  public periodFromDate = null;
  public periodToDate = null;
  public paySettleModeDescription: string = '';
  public productName: string = '';
  public policyNumber: string = '';
  public totalPremium: string = '';
  public productCode: string = '';
  public policyType = '';
  public customerName: string = '';
  public productType = '';
  public days: number;

  public proposalNumber: string = '';
  // Intermediary Types
  intermediaryTypes = [];
  intermediaryData = [];
  intermediaryFilteredList = [];
  intermediarySequence = '';
  public intermediaryTypeCode: string = '';
  public intermediaryTypeName: string = '';

  // weather form
  // Crop Types
  cropTypes = [];
  cropType = '';
  //Weather Type
  weatherTypes = [];
  weatherType = '';
  //Plans
  plans = [];
  plan = '';
  //Business Chanels
  businessChanels = [];
  businessChanel = '';
  //payment types
  paymentTypes = [];
  paymentType = '';

  //weather form form-controls
  cropTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  weatherTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  planFormControl = new FormControl('', [
    Validators.required
  ]);
  businessChanelFormControl = new FormControl('', [
    Validators.required
  ]);
  paymentFormControl = new FormControl('', [
    Validators.required
  ]);
  startDateFormControl = new FormControl('', [
    Validators.required
  ]);
  endDateFormControl = new FormControl('', [
    Validators.required
  ]);
  //Policy
  classCode = '';
  currencyCode = '';
  modeOfBusinessCode = '';
  settlementModeCode = '';
  policyRemark = '';
  startDate = null;
  endDate = null;
  currentDate = new Date();
  maxDate1 = new Date(moment().subtract(1, 'days').format());
  maxDate2 = new Date(moment().format());
  minDate = new Date();
  endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());


  //Weather Type
  weatherSequenceNo = '';
  weatherVersion = '';
  weatherValue = '';

  //Crop Type
  cropTypeSequence = '';
  cropTypeVersion = '';
  cropTypeValue = '';

  // Livestock form
  // products
  products = [];
  product = '';
  // business channels
  liveStickBusinessChannels = [];
  liveStickBusinessChannel = '';
  // premium settlement methods
  premiumSettlementMethods = [];
  premiumSettlementMethod = '';

  liveStockStartDate = null;
  liveStockEndDate = null;

  livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());


  // Livestock form controls
  liveStockPlanFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStickBusinessChannelFormControl = new FormControl('', [
    Validators.required
  ]);
  premiumSettlementMethodFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStockStartDateFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStockEndDateFormControl = new FormControl('', [
    Validators.required
  ]);


  // weather land
  // policy additional details


  // policy weather land form related variables
  locations = [];
  location = {};

  cropVariances = [];
  cropVariance = '';

  weatherStations = [];
  weatherStation = 0;

  weatherLandPhotos = [];


  //location object
  locationAddress = '';
  locationOrderNo = 1;


  //risk
  riskOrder = 1;
  riskName = '';
  riskRefNo = 1;
  riskSectionCode = '';
  riskSumInsured = '';
  effectiveDate = null;
  effectiveToDate = null;
  size1 = '';
  size1UnitType = 'UNAPR';
  size2 = '';
  size2UnitType = 'UNAAC';
  price = '';
  noOfUnits = '';
  weatherLandStartDate = null;
  weatherLandEndDate = null;
  farmerName = '';
  weatherLandPremium = 0;
  markLandPlot = [];
  markLandPlotString = '';


  //crop variance
  cropVarianceSequence = '';
  cropVarianceDescription = '';
  cropVarianceVersion = '';
  cropVarianceValue = '';

  //weather stations
  weatherStationSequence = '';
  weatherStationDescription = '';
  weatherStationVersion = '';
  weatherStationValue = '';

  //no of unsits
  noOfUnitsSequence = '';
  noOfUnitsDescription = '';
  noOfUnitsVersion = '';
  noOfUnitsValue = '';

  farmerNameSequnce = '';
  farmerNameDescription = '';
  farmerNameVersion = '';
  farmerNameValue = '';

  fromMonth = '';
  toMonth = '';
  toYear = '';
  fromYear = '';
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'];
  dynamicMonths = [];
  years = ['2019', '2020'];

  fromMonths = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'];
  toMonths = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'];

  fromYears = ['2019', '2020'];
  toYears = ['2019', '2020'];

  // policy weather land form controls
  locationFormControl = new FormControl('', [
    Validators.required
  ]);

  cropVarianceFormControl = new FormControl('', [
    Validators.required
  ]);


  landWeatherStationFormControl = new FormControl('', [
    Validators.required
  ]);

  farmerNameFormControl = new FormControl('', [
    Validators.required
  ]);
  weatherLandStartDateFormControl = new FormControl('', [
    Validators.required
  ]);
  weatherLandEndDateFormControl = new FormControl('', [
    Validators.required
  ]);
  size1UnitTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  size1FormControl = new FormControl('', [
    Validators.required
  ]);
  markLandPlotFormControl = new FormControl('', [
    Validators.required
  ]);
  noOfunitsFormControl = new FormControl('', [
    Validators.required
  ]);
  priceFormControl = new FormControl('', [
    Validators.required
  ]);


  // add live stock form
  newLivestockStartDate = null;
  //live stock types
  liveStockTypes = [];
  liveStockType = '';

  //Breed types
  breedTypes = [];
  breedType = '';

  //genders
  genders = [];
  gender = '';

  //usage types
  usageTypes = [];
  usageType = '';

  //undewriting assessors
  undewritingAssessors = [];
  undewritingAssessor = '';


  age = '';

  tagNumber = '';
  ownerName = '';
  livestockPurchaseDate = null;

  //Live stock
  liveStockTypeSequnce = '';
  liveStockTypeDescription = '';
  liveStockTypeVersion = '';
  liveStockTypeValue = '';

  //Breed
  breedSequence = '';
  breedDescription = '';
  breedVersion = '';
  breedValue = '';

  //gender
  genderSequence = '';
  genderDescription = '';
  genderVersion = '';
  genderValue = '';

  //usage
  usageSequence = '';
  usageDescription = '';
  usageVersion = '';
  usageValue = '';

  //  age in years
  ageInYearsSequence = '';
  ageInYearsDescription = '';
  ageInYearsVersion = '';
  ageInYearsValue = this.age;

  // owner name
  ownerNameSequence = '';
  ownerNameDescription = '';
  ownerNameVersion = '';
  ownerNameValue = '';

  //purchase date
  purchaseDateSequence = '';
  purchaseDateDescription = '';
  purchaseDateVersion = '';
  purchaseDateValue = null;


  cropVarianceValueDecription = '';
  cropCategoryValueDecription = '';
  cropTypeValueDecription = '';
  weatherStationValueDescription = '';
  noOfUnitsValueDescription = '';
  unitPriceValueDescription = '';


  tagNumberFormControl = new FormControl('', [
    Validators.required
  ]);

  breedTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  liveStockTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  genderTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  ageTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  ownerNameFormControl = new FormControl('', [
    Validators.required
  ]);

  usageTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  undewritingAssessorFormControl = new FormControl('', [
    Validators.required
  ]);

  newLivestockStartDateFormControl = new FormControl('', [
    Validators.required
  ]);

  riskSumInsuredFormControl = new FormControl('', [
    Validators.required
  ]);

  /**
   *  covers / Perils
   */
  otherPerils = [];
  otherPeril = {};

  covers = [];
  riskImages = [];

  perilCode = '';
  perilPerilSumInsured = 0;
  perilPercentage = 0;
  perilRate = 0;

  perilSequnce = '';

  excessAmount = 0;
  excessPercentage = 0;
  riskPremium = '';

  otherPerilFormControl = new FormControl('', [
    Validators.required
  ]);

  perilPerilSumInsuredFormControl = new FormControl('', [
    Validators.required
  ]);
  excessPercentageFormControl = new FormControl('', [
    Validators.required
  ]);
  excessAmountFormControl = new FormControl('', [
    Validators.required
  ]);

  managePerilDescription = '';


  //views
  public displayManageWeatherForm: boolean = false;
  public displayPolicyDetails: boolean = true;
  public displayManageLivestockForm: boolean = false;
  public displayManageLandForm: boolean = false;
  public displayLivestockAdditionalDetails: boolean = false;
  public displayManageCoversForm: boolean = false;
  public displayWeatherRiskDetailsDiv: boolean = false;
  public displayLivestockRiskDetailsDiv: boolean = false;
  public isDataLoaded = true;

  public authorizedPolicy: boolean = false;


  // selected value
  selectedValue = '';

  // Add land page hide and show variables
  viewAddLand = true;
  viewAddCultivation = true;
  viewAddLandPhotos = true;

  disabledLocation = false;

  displayPayment = false;

  debitNoteNumber = '';

  customerCode = '';

  enableContinueButtonWeatherLand = true;


  /* infinity scroll */
  private throttle: number;
  private scrollDistance: number;
  private scrollUpDistance: number;
  private fromRoot = true;
  page = 1;
  size = 20;
  private numberOfPages: number;


  // manage land form


  /**
   *
   *  policy weather land
   */
  landEdit = true;

  manageLivestockRisk = true;
  documentSequnce = '';

  manageCover = true;

  private progressBar: string;
  imagList: ImageModel[] = new Array();

  // Land
  private districtsList: CommonType[] = new Array();
  private divisionsList: CommonType[] = new Array();
  private gramasevaDivisionsList: CommonType[] = new Array();
  private getagrarianServiceCentersList: CommonType[] = new Array();
  private ownershipTypesList: CommonType[] = new Array();

  private selectedDistrict: CommonType;
  private selectedDivision: CommonType;
  private selectedGramasevaDivision: CommonType;
  private selectedgetagrarianServiceCenter: string;
  private selectedOwnershipType: string;
  private sendLandData: LandModel = new LandModel();
  private isDrawMap: boolean = false;
  private pathList: LocationModel[] = new Array();
  private pathString: string = '';

  private isContinue: boolean = false;
  panelOpenState = true;
  // imag upload
  private landSequence: string = '';
  private docSequence: string;

  // routing
  private routerPartyCode: string;
  private routeString: string;
  isCustomer = false;
  landForm: FormGroup;

  // Indemnity
  isIndemnityAddLandForm: boolean;
  indemnityAddLandForm: FormGroup;
  indemnityPolicyManageForm: FormGroup;
  indemnityLandStarDateMin: Date;
  indemnityLandEndDateMin: Date;
  indemnityLandEndDateMax: Date;
  indemnityPlansList: CommonType[];
  indemnityPolicyStartDateMin: Date;
  indemnityPolicyEndDateMin: Date;
  displayIndemnityAdditionalDetails = false;
  coversLoaded = false;
  indemnityCropTypes = [];

  authorizedDate = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    public service: PolicyService,
    public notificationService: NotificationDataTransferService,
    public landService: LandService,
    public router: Router,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private policiesService: PoliciesService,
    private imageViewerService: ImageViewerService
  ) {
  }

  ngOnInit() {
    this.getIntermediaryTypes();
    this.getIndemnityPlans();
    this.getIndemnityCropTypes();

    //check login
    if (sessionStorage.getItem('partyAsInternalStaff') === 'Y') {
      this.asInternalStaff = true;
    }
    if (sessionStorage.getItem('asCustomer') === 'Y') {
      this.isCustomer = true;
    }
    // get party information form session storage
    this.partyCode = sessionStorage.getItem('partyCode');
    this.asCustomer = sessionStorage.getItem('asCustomer');
    this.asAgent = sessionStorage.getItem('asAgent');
    this.branchCode = sessionStorage.getItem('branchCode');
    //  this.addressSequnce = sessionStorage.getItem('addressSequnce');
    this.selectedValue = 'customerName';
    this.weatherIndexParameters = [];

    this.throttle = 300;
    this.scrollUpDistance = 2;
    this.scrollDistance = 1;

    // check user profile completenes
    this.service.getLoggedUserProfile().subscribe(resp => {
      const body = resp.body as any;
      if (body.data.address !== null) {
        // check are ther any list of policies
        let customerCode = '';
        let salePersonCode = '';
        if (this.asCustomer === 'Y') {
          customerCode = this.partyCode;
          salePersonCode = '';
        } else if (this.asAgent === 'Y') {
          salePersonCode = this.partyCode;
          customerCode = ''; // sessionStorage.getItem('customerCode');
        }
        // tslint:disable-next-line:max-line-length
        this.service.searchedPolicies(this.searchValue, customerCode, salePersonCode, 'CN', this.meCodeCheck, this.branchCode, this.productCode, this.policyStatus).subscribe(resp => {
          const body = resp.body as any;
          this.policies = body.data;
          if (this.policies.length === 0) {
            this.router.navigate(['/policy', this.partyCode, this.parentPage]);
          } else {
            this.isDataLoaded = false;
            for (let i = 0; i < this.policies.length; i++) {
              this.policies[i]['active'] = false;
            }
          }
          if (body.data.length === 0) {
            this.policies = [];
            this.isDataLoaded = false;
          } else {
            this.policies = body.data;
            this.policySequnce = this.policies[0]['sequence'];
            this.getpolicyDetails(this.policySequnce, 0, 'NL', this.policies[0]['authorizedDate']);
            this.isDataLoaded = false;
          }
        }, error => {

        });

      } else {
        this.openDialog();
        this.isDataLoaded = false;
      }
    }, error => {

    });

    this.indemnityAddLandForm = this.fb.group({
      indemnityFarmerName: ['', Validators.required],
      indemnityCultivationCropType: ['', Validators.required],
      indemnityCultivationCrop: ['', Validators.required],
      indemnityCropVariance: ['', Validators.required],
      indemnitySumInsured: ['', Validators.required],
      indemnityLandStartDate: ['', Validators.required],
      indemnityLandEndDate: ['', Validators.required],
      indemnityAcres: ['', Validators.required],
      indemnityPerches: ['', Validators.required],
      indemnityPlot: ['', Validators.required]
    });

    this.indemnityPolicyManageForm = this.fb.group({
      indemnityCropType: ['', Validators.required],
      indemnityPlan: ['', Validators.required],
      indemnityBusinessChannel: ['', Validators.required],
      indemnityStartDate: ['', Validators.required],
      indemnityEndDate: ['', Validators.required],
      indemnityDayCount: [''],
      indemnityPaymentType: ['', Validators.required],
      indemnityIntermediaryType: [''],
      indemnityIntemediary: ['']
    });

    this.landForm = this.fb.group({
      plotName: ['', Validators.required],
      address: ['', Validators.required],
      district: ['', Validators.required],
      division: ['', Validators.required],
      gramasevaDivision: ['', Validators.required],
      agrarianServiceCenter: ['', Validators.required],
      sizeInAcres: ['', Validators.required],
      sizeInPerches: ['', [Validators.required, Validators.max(159)]],
      ownership: ['', Validators.required],
      geometry: ['', Validators.required]
    });

    this.indemnityPolicyManageForm.reset();
    this.indemnityAddLandForm.reset();
    this.indemnityLandEndDateMin = new Date();
    this.indemnityLandEndDateMax = new Date();
    this.indemnityPolicyStartDateMin = new Date();

    this.indemnityStartDate.valueChanges.subscribe(() => {
      if (this.indemnityStartDate.value && this.indemnityEndDate.value) {
        this.setDayCountIndemnity();
      }
      this.indemnityPolicyEndDateMin = moment(this.indemnityStartDate.value).add(1, 'day').toDate();
      this.indemnityLandStarDateMin = moment(this.indemnityStartDate.value).toDate();
    });

    this.indemnityEndDate.valueChanges.subscribe(() => {
      if (this.indemnityStartDate.value && this.indemnityEndDate.value) {
        this.setDayCountIndemnity();
        this.indemnityLandEndDateMin = moment(this.indemnityStartDate.value).add(1, 'day').toDate();
        this.indemnityLandEndDateMax = moment(this.indemnityEndDate.value).toDate();
      }
    });
  }

  // Form Getters
  get indemnityFarmerName() { return this.indemnityAddLandForm.get('indemnityFarmerName') };
  get indemnityCultivationCrop() { return this.indemnityAddLandForm.get('indemnityCultivationCrop') };
  get indemnityCultivationCropType() { return this.indemnityAddLandForm.get('indemnityCultivationCropType') };
  get indemnityCropVariance() { return this.indemnityAddLandForm.get('indemnityCropVariance') };
  get indemnitySumInsured() { return this.indemnityAddLandForm.get('indemnitySumInsured') };
  get indemnityLandStartDate() { return this.indemnityAddLandForm.get('indemnityLandStartDate') };
  get indemnityLandEndDate() { return this.indemnityAddLandForm.get('indemnityLandEndDate') };
  get indemnityAcres() { return this.indemnityAddLandForm.get('indemnityAcres') };
  get indemnityPerches() { return this.indemnityAddLandForm.get('indemnityPerches') };
  get indemnityPlot() { return this.indemnityAddLandForm.get('indemnityPlot') };

  get indemnityPlan() { return this.indemnityPolicyManageForm.get('indemnityPlan') }
  get indemnityBusinessChannel() { return this.indemnityPolicyManageForm.get('indemnityBusinessChannel') }
  get indemnityStartDate() { return this.indemnityPolicyManageForm.get('indemnityStartDate'); }
  get indemnityEndDate() { return this.indemnityPolicyManageForm.get('indemnityEndDate'); }
  get indemnityDayCount() { return this.indemnityPolicyManageForm.get('indemnityDayCount'); }
  get indemnityPaymentType() { return this.indemnityPolicyManageForm.get('indemnityPaymentType'); }



  getSearchedPolicies(searchFeild) {
    this.clearCommonPlaces();
    this.policies = [];
    let customerCode = '';
    let salePersonCode = '';
    if (this.asCustomer === 'Y') {
      customerCode = this.partyCode;
      this.salesPersonCode = '';
    } else if (this.asAgent === 'Y') {
      this.salesPersonCode = this.partyCode;
      customerCode = ''; // sessionStorage.getItem('customerCode');
    }
    // tslint:disable-next-line:max-line-length
    this.service.searchedPolicies(this.searchValue, customerCode, this.salesPersonCode, searchFeild, this.meCodeCheck, this.branchCode, this.productCode, this.policyStatus).subscribe(resp => {
      const body = resp.body as any;
      if (body.data.length === 0) {
        this.policies = [];
        this.isDataLoaded = false;
      } else {
        this.policies = body.data;
        for (let i = 0; i < this.policies.length; i++) {
          this.policies[i]['active'] = false;
        }
        this.policySequnce = this.policies[0]['sequence'];
        this.getpolicyDetails(this.policySequnce, 0, 'L', this.policies[0]['authorizedDate']);
        this.isDataLoaded = false;
      }
    }, error => {
      this.isDataLoaded = false;
    });
  }

  // get ME code policy
  getMECode(event) {
    this.meCodeCheck = !this.meCodeCheck;
    if (event.checked) {
      if (sessionStorage.getItem('partyAsInternalStaff') === 'Y') {
        this.branchCode = 'HO';
        this.classCode = 'MC';
        this.productCode = 'LS';
        this.policyStatus = 'P';
        this.salesPersonCode = 'HO';
        let searchFeild = '';
        let selectedValue = this.selectedValue;
        if (selectedValue === 'customerName') {
          searchFeild = 'CN';
        } else if (selectedValue === 'riskName') {
          searchFeild = 'RN';
        } else if (selectedValue === 'policyNumber') {
          searchFeild = 'PN';
        } else if (selectedValue === 'proposalNumber') {
          searchFeild = 'PRN';
        }
        this.getSearchedPolicies(searchFeild);

      }
    } else {
      this.branchCode = '';
      this.classCode = 'MC';
      this.productCode = '';
      this.policyStatus = '';
      this.salesPersonCode = '';
      let searchFeild = '';
      let selectedValue = this.selectedValue;
      if (selectedValue === 'customerName') {
        searchFeild = 'CN';
      } else if (selectedValue === 'riskName') {
        searchFeild = 'RN';
      } else if (selectedValue === 'policyNumber') {
        searchFeild = 'PN';
      } else if (selectedValue === 'proposalNumber') {
        searchFeild = 'PRN';
      }
      this.getSearchedPolicies(searchFeild);
    }

  }

  getpolicyDetails(data, i: number, str, authorisedDate) {

    this.authorizedDate = authorisedDate;

    if (str === 'L') {
      this.policies[i]['active'] = true;
      if (i !== this.activePolicyIndex) {
        this.policies[this.activePolicyIndex]['active'] = false;
      }
      this.activePolicyIndex = i;
    }

    this.clearCommonPlaces();
    this.imagList = [];
    this.isDataLoaded = true;
    this.clearData();
    this.policySequnce = data;
    let documentSequnce = '';
    this.clearCommonPlaces();
    this.displayWeatherRiskDetailsDiv = false;

    this.service.getPolicyDetails(this.policySequnce, authorisedDate).subscribe(resp => {
      const body1 = resp.body as any;
      this.channelOfBuinessDescription = body1.data.channelOfBuinessDescription;
      let start = moment(body1.data.periodFromDate, 'YYYY-MM-DD HH:mm:ss');
      let end = moment(body1.data.periodToDate, 'YYYY-MM-DD HH:mm:ss');
      this.periodFromDate = start.format('MMMM YYYY');
      this.periodToDate = end.format('MMMM YYYY');
      this.days = end.diff(start, 'day');
      this.startDate = new Date(body1.data.periodFromDate);
      this.endDate = new Date(body1.data.periodToDate);
      this.liveStockStartDate = new Date(body1.data.periodFromDate);
      this.liveStockEndDate = new Date(body1.data.periodToDate);

      this.paySettleModeDescription = body1.data.paySettleModeDescription;
      this.paymentType = body1.data.settlementModeCode;
      this.premiumSettlementMethod = body1.data.settlementModeCode;

      this.productName = body1.data.productName;
      this.productCode = body1.data.productCode;
      this.policyNumber = body1.data.policyNumber;
      this.totalPremium = body1.data.totalPremium;
      this.productType = body1.data.productType;
      this.customerName = body1.data.customerName;
      this.proposalNumber = body1.data.proposalNumber;


      // set from year, from month, toMonth and toYear for weather index parameters
      this.fromYear = moment(this.startDate).format('Y');
      this.fromMonth = moment(this.startDate).format('M');
      this.toYear = moment(this.endDate).format('Y');
      this.toMonth = moment(this.endDate).format('M');

      //set values
      //set customer code
      this.customerCode = body1.data.customerCode;
      //check  policy completed one or not
      let status = body1.data.status;
      if (status === 1 || status === 2) {
        this.authorizedPolicy = true;
      } else {
        this.authorizedPolicy = false;
      }

      this.service.getPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
        const body2 = resp.body as any;
        //filter weather type and crop type
        for (let i = 0; i < body2.data.length; i++) {
          let obj = body2.data[i];
          if (obj['description'] === 'CROP') {
            this.cropTypeDescription = obj['valueDescription'];
            this.cropType = obj['value'];
          }
          if (obj['description'] === 'WEATHER TYPE') {
            this.weatherDescription = obj['valueDescription'];
            this.weatherType = obj['value'];
          }
        }



        //get risks
        this.service.getRisksSelectedPolicySequnce(this.policySequnce, this.authorizedDate).subscribe(resp => {
          const body3 = resp.body as any;
          this.risks = body3.data.riskList;
          this.service.getPolicyImages(this.policySequnce, this.authorizedDate).subscribe(resp => {
            const body4 = resp.body as any;
            this.isDataLoaded = false;
            documentSequnce = body4.data[0]['sequenceNo'];
            this.service.downloadImages(documentSequnce, this.authorizedDate).subscribe(resp => {
              const body5 = resp.body as any;
              this.images = body5.data.objectList;
              this.imagList = [];
              for (let i = 0; i < this.images.length; i++) {
                let im = new ImageModel();
                im.imageKey = this.images[i]['key'];
                im.value = this.images[i]['url'];
                this.imagList.push(im);
              }
              this.isDataLoaded = false;
              this.displayLivestockAdditionalDetails = false;
              this.displayManageCoversForm = false;
              this.displayManageLandForm = false;
              this.displayManageLivestockForm = false;
              this.displayPolicyDetails = true;
              this.displayManageWeatherForm = false;
              this.displayLivestockRiskDetailsDiv = false;
              this.displayWeatherRiskDetailsDiv = false;
              this.getPolicyOutstandings();
            }, error => {
              return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
            });
          }, error => {
            this.isDataLoaded = false;
            return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
          });
        }, error => {
          this.isDataLoaded = false;
          return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
        });
      }, error => {
        this.isDataLoaded = false;
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      });
    }, error => {
      this.isDataLoaded = false;
      return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    });
  }

  managePolicy() {
    this.clearWeatherPolicyForm();
    this.clearCommonPlaces();
    this.displayManageWeatherForm = true;
    this.displayPolicyDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;

    // get weather form lovs
    this.getBusinessChanels();
    this.getCropTypes();
    this.getIndemnityCropTypes();
    this.getPaymentTypes();
    this.getWeatherTypes();


    // get livestock form lovs
    this.loadproducts();
    this.loadBusinessChannels();
    this.loadSettlementMethods();
    this.getPolicyFormDetails();


  }

  /** weather form related functions */

  /* Crop and Weather Functions */
  getCropTypes() {
    this.service.getCropTypes().subscribe(resp => {
      const body = resp.body as any;
      this.cropTypes = body.data;
    }, error => {

    });
  }

  getIndemnityCropTypes() {
    this.service.getIndemnityCropTypes().subscribe((resp: any) => {
      this.indemnityCropTypes = resp.body.data;
    }, err => {
      this.indemnityCropTypes = [];
    })
  }

  getWeatherTypes() {
    this.service.getWeatherTypes().subscribe(resp => {
      const body = resp.body as any;
      this.weatherTypes = body.data;
    }, error => {

    });
  }

  getPlans() {
    let branchCode = this.branchCode;
    this.plans = [];
    if (this.cropType !== '' && this.weatherType !== '') {
      this.service.getPlans(branchCode, this.cropType, this.weatherType).subscribe(resp => {
        const body = resp.body as any;
        this.plans = body.data;
      }, error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      });
    }

  }

  getBusinessChanels() {
    this.service.getBusinessChanels().subscribe(resp => {
      const body = resp.body as any;
      this.businessChanels = body.data;
    }, error => {
      return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    });
  }

  getPaymentTypes() {
    this.service.getPaymentTypes().subscribe(resp => {
      const body = resp.body as any;
      if (this.asCustomer === 'Y') {
        for (let i = 0; i < body.data.length; i++) {
          if (body.data[i]['code'] === 'UPFRN') {
            this.paymentTypes.push(body.data[i]);
          }
        }
      } else {
        this.paymentTypes = body.data;
      }
    }, error => {
      return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    });
  }

  /* Livestock Functions */
  loadproducts() {
    this.service.getProducts(this.branchCode).subscribe(resp => {
      const body = resp.body as any;
      this.products = body.data;
    }, error => {
      return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    });
  }

  loadBusinessChannels() {
    this.service.getBusinessChanels().subscribe(resp => {
      const body = resp.body as any;
      this.liveStickBusinessChannels = body.data;
    }, error => {
      return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    });
  }


  loadSettlementMethods() {
    this.service.getPremiumSettlementMethods().subscribe(resp => {
      const body = resp.body as any;
      if (this.asCustomer === 'Y') {
        for (let i = 0; i < body.data.length; i++) {
          if (body.data[i]['code'] === 'UPFRN') {
            this.premiumSettlementMethods.push(body.data[i]);
          }
        }
      } else {
        this.premiumSettlementMethods = body.data;
      }

    }, error => {
      this.displayErrorMessage(error);
    });
  }

  backToPolicyDetails() {
    this.displayManageWeatherForm = false;
    this.displayPolicyDetails = true;
    this.displayManageLandForm = false;
    this.displayManageLivestockForm = false;
    this.displayLivestockAdditionalDetails = false;
    this.displayIndemnityAdditionalDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
    let sequence = this.policySequnce;
    this.coversLoaded = false;
    this.getpolicyDetails(sequence, 0, 'L', this.authorizedDate);
  }

  backToLiveStockAdditinalDetails() {
    this.displayPolicyDetails = false;
    if (this.productType === 'CRPTY') {
      this.displayManageLandForm = true;
      this.displayManageWeatherForm = false;
      this.displayLivestockAdditionalDetails = false;
      this.displayManageLivestockForm = false;
    } else if (this.productType === 'WHINX') {
      this.displayManageWeatherForm = false;
      this.displayManageLandForm = false;
      this.displayLivestockAdditionalDetails = true;
      this.displayManageLivestockForm = true;
    }
    this.displayManageCoversForm = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
    let sequence = this.policySequnce;
    this.getLandDetails();
    this.getRiskAdditionalDetails();
  }

  /** get intermediary types   */
  getIntermediaryTypes() {
    this.service.getIntermediaryTypes().subscribe(resp => {
      const body = resp.body as any;
      const arry = body.data;
      this.intermediaryTypes = arry.filter(itm => {
        return itm.code !== 'DIRCT';
      });
    });
  }

  /** get intermediary data */
  getInrermediaryData(value: string) {
    this.intermediaryTypeCode = value;
    this.getIntermediarySequenceNo();
    this.service.getIntermediaryTypeData(value).subscribe(resp => {
      const body = resp.body as any;
      this.intermediaryData = body.data;
      this.intermediaryFilteredList = body.data;
    });
  }

  /** Intermediary Name search */
  searchIntermediaryName(event) {
    const text: string = event.target.value;
    this.intermediaryFilteredList = this.intermediaryData.filter(item => {
      return item.description.toUpperCase().match(text.toUpperCase());
    });

  }

  /* Indemnity Functions */
  getIndemnityPlans() {
    this.policiesService.getIndemnityPlans(this.branchCode).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(plans => {
      this.indemnityPlansList = plans
    });
  }

  clearData() {
    this.policySequnce = '';
    this.risks = [];
    this.images = [];
    this.cropTypeDescription = '';
    this.weatherDescription = '';
    this.channelOfBuinessDescription = '';
    this.periodFromDate = null;
    this.periodToDate = null;
    this.paySettleModeDescription = '';
    this.productName = '';
    this.policyNumber = '';
    this.totalPremium = '';
    this.productCode = '';
    this.customerName = '';
  }

  getPolicyFormDetails() {
    this.isDataLoaded = true;
    this.service.getPolicyDetails(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.businessChanel = body.data.channelOfBuinessCode;
      this.isDataLoaded = false;
      if (this.productType === 'WHINX') {
        this.startDate = new Date(body.data.periodFromDate);
        this.endDate = new Date(body.data.periodToDate);
        this.fromYear = moment(this.startDate).format('Y');
        /**
         * fire fromYear select box onChange
         */
        this.toMonth = '';
        this.toYear = '';
        this.fromMonth = '';
        const currentYear = moment().get('year');
        // @ts-ignore
        if (parseInt(this.fromYear) === currentYear) {
          const currentMonth = moment().get('M');
          const testArr = [];
          for (let i = currentMonth; i < this.months.length; i++) {
            testArr.push(this.months[i]);
          }
          this.fromMonths = [];
          this.fromMonths = testArr;
        } else {
          this.fromMonths = this.months;
        }
        this.fromMonth = new TitleCasePipe().transform(moment(this.startDate).format('MMMM'));

        /**
         *  fromMonth onChange event
         */
        const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
        // @ts-ignore
        const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);
        /**
         * adding weather policy max day count
         */
        this.service.getWeatherProductMaxDayCount().subscribe(resp => {
          const body1 = resp.body as any;
          const maxDayCount = parseInt(body1.data.maxNoOfPolicyDays);

          /**
           * get max day count and set to year and to month
           */
          const futureDate = moment(selectedStartDate).add(maxDayCount, 'days');
          const futureYear = moment(futureDate).get('year');
          const futureMonth = moment(futureDate).get('M');
          this.toYears = [];
          for (let i = parseInt(this.fromYear); i <= futureYear; i++) {
            this.toYears.push(String(i));
          }
          /**
           *  - if same year - have to get betwenn months
           *  - if other year
           */

          if (parseInt(this.fromYear) === futureYear) {
            const fromMonth = moment(selectedStartDate).get('M');
            this.toMonths = [];
            for (let i = fromMonth; i <= futureMonth; i++) {
              this.toMonths.push(this.months[i]);
            }
          } else {
            this.toMonths = [];
            for (let i = 0; i < this.months.length; i++) {
              this.toMonths.push(this.months[i]);
            }
          }

          // this.toMonth  = 'September'; //new TitleCasePipe() .transform(moment(this.endDate).format('MMMM'));
          this.toYear = moment(this.endDate).format('Y');
          this.toMonth = new TitleCasePipe().transform(moment(this.endDate).format('MMMM'));
          // this.setEndDate();
          this.setDayCountWeather();
          this.paymentType = body.data.settlementModeCode;
          if (this.productType === 'WHINX') {

            // tslint:disable-next-line:no-shadowed-variable
            this.service.getPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
              const body4 = resp.body as any;
              let data = body4.data;

              //filter weather type and crop type
              for (let i = 0; i < data.length; i++) {
                let obj = data[i];
                if (obj['description'] === 'CROP') {
                  /*this.cropTypeSequence = obj['sequenceNo'];
                  this.cropTypeVersion = obj['version'];
                  this.cropTypeDescription = obj['description'];*/
                  this.cropType = obj['value'];
                  this.cropTypeGlobal = obj['value'];
                }
                if (obj['description'] === 'WEATHER TYPE') {
                  /*   this.weatherSequenceNo = obj['sequenceNo'];
                     this.weatherVersion = obj['version'];
                     this.weatherDescription = obj['description'];*/
                  this.weatherType = obj['value'];
                }
                if (obj['description'] === 'FARMER NAME') {
                  this.farmerName = obj['value'];
                }

                setTimeout(() => {
                  this.isDataLoaded = false;
                }, 2000);

              }
              let branchCode = this.branchCode;
              this.plans = [];
              if (this.cropType !== '' && this.weatherType !== '') {
                this.service.getPlans(branchCode, this.cropType, this.weatherType).subscribe(resp => {
                  const body5 = resp.body as any;
                  this.plans = body5.data;
                  this.plan = body.data.productCode;
                }, error => {
                  this.displayErrorMessage(error);
                });
              }
            }, error => {
              this.isDataLoaded = false;
            });

          }
          this.isDataLoaded = false;


        }, error => {
          this.displayErrorMessage(error);
        });


      }
      if (this.productType === 'LIVST') {
        this.liveStockStartDate = new Date(body.data.periodFromDate);
        this.liveStockEndDate = new Date(body.data.periodToDate);
        this.setLivestockPolicyEndDate();
        this.setDayCountLivestock();
      }
      if (this.productType === 'CRPTY') {
        this.indemnityPolicyManageForm.reset();
        this.indemnityStartDate.setValue(new Date(body.data.periodFromDate));
        this.indemnityPolicyStartDateMin = new Date(body.data.periodFromDate);
        this.indemnityEndDate.setValue(new Date(body.data.periodToDate));
        this.startDate = new Date(body.data.periodFromDate);
        this.endDate = new Date(body.data.periodToDate);
        this.indemnityPlan.setValue(body.data.productCode);
        this.indemnityPaymentType.setValue(body.data.settlementModeCode);
        this.indemnityBusinessChannel.setValue(body.data.channelOfBuinessCode);
      }
      this.businessChanel = body.data.channelOfBuinessCode;
      this.premiumSettlementMethod = body.data.settlementModeCode;
      this.productCode = body.data.productCode;
      this.productType = body.data.productType;
      this.loadproducts();
      this.product = body.data.productCode;
      this.liveStickBusinessChannel = body.data.channelOfBuinessCode;
    }, error => {
      this.isDataLoaded = false;

    });
  }

  updatePolicyWeather() {
    this.isDataLoaded = true;
    if (sessionStorage.getItem('partyAsInternalStaff') === 'Y') {
      this.service.getIntermediaryDetails(this.policySequnce, this.authorizedDate).subscribe(resp => {
        const body1 = resp.body as any;
        const businessParty = 'Y';
        const commissionParty = 'Y';
        let obj = {
          'sequenceNo': this.intermediarySequence,
          'intermediaryTypeCode': this.intermediaryTypeCode,
          'intermediaryCode': this.intermediaryTypeName,
          'isBusinessParty': businessParty,
          'isCommissionParty': commissionParty,
          'version': body1.data[0].version,
        };
        this.service.updateIntermediaryDetails(this.intermediarySequence, obj).subscribe(resp => {
          const body3 = resp.body as any;
          this.isDataLoaded = false;
          this.displaySuccessMessage(body3);
          this.getpolicyDetails(this.policySequnce, 0, 'NL', this.authorizedDate);
          this.displayPolicyDetails = true;
          this.displayManageLivestockForm = false;
          this.displayManageWeatherForm = false;
          this.displayLivestockRiskDetailsDiv = false;
          this.displayWeatherRiskDetailsDiv = false;
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });

      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });

    }
    this.service.getPolicyDetails(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body1 = resp.body as any;
      //  set dates
      // tslint:disable-next-line:radix
      const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
      // @ts-ignore
      const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);
      // tslint:disable-next-line:radix
      const selectedToMonth = parseInt(moment().month(this.toMonth).format('M'));
      let selectedEndDate = moment([this.toYear, selectedToMonth - 1]);
      selectedEndDate = moment(selectedEndDate).endOf('month');

      // @ts-ignore
      this.startDate = new Date(selectedStartDate);
      // @ts-ignore
      this.endDate = new Date(selectedEndDate);

      let obj = {
        'sequence': this.policySequnce,
        'classCode': body1.data.classCode,
        'productCode': this.plan,
        'branchCode': body1.data.branchCode,
        'currencyCode': body1.data.currencyCode,
        'customerAddressSequence': body1.data.customerAddressSequence,
        'proposalNumber': body1.data.proposalNumber,
        'policyNumber': body1.data.policyNumber,
        // "periodFromDate": moment(this.startDate).format('YYYY-MM-DD HH:mm:ss'),
        // "periodToDate": moment(this.endDate).format('YYYY-MM-DD HH:mm:ss'),
        'periodFromDate': moment(selectedStartDate).format('YYYY-MM-DD HH:mm:ss'),
        'periodToDate': moment(selectedEndDate).format('YYYY-MM-DD HH:mm:ss'),
        'modeOfBusinessCode': body1.data.modeOfBusinessCode,
        'settlementModeCode': this.paymentType,
        'debtorCode': body1.data.debtorCode,
        'channelOfBuinessCode': body1.data.channelOfBuinessCode,
        'policyRemark': body1.data.policyRemark,
        'customerCode': body1.data.customerCode,
        'fromNarration': body1.data.fromNarration,
        'toNarration': body1.data.toNarration,
        'status': body1.data.status,
        'sumInsured': body1.data.sumInsured,
        'eventLimit': body1.data.eventLimit,
        'eventPercentage': body1.data.eventPercentage,
        'eventNarration': body1.data.eventNarration,
        'annualLimit': body1.data.annualLimit,
        'annualPercentage': body1.data.annualPercentage,
        'annualNarration': body1.data.annualNarration,
        'premium': body1.data.premium,
        'totalPremium': body1.data.totalPremium,
        'transactionAmount': body1.data.transactionAmount,
        'totalTransactionAmount': body1.data.totalTransactionAmount,
        'reinsuranceExtensionRequiredFlag': body1.data.reinsuranceExtensionRequiredFlag,
        'transactionType': body1.data.transactionType,
        'currencyDate': body1.data.currencyDate,
        'authorisedDate': body1.data.authorisedDate,
        'creditAuthRequiredFlag': body1.data.creditAuthRequiredFlag,
        'restrictListAuthRequiredFlag': body1.data.restrictListAuthRequiredFlag,
        'reinsuranceAuthRequiredFlag': body1.data.reinsuranceAuthRequiredFlag,
        'version': body1.data.version,
        'quotationSequence': body1.data.quotationSequence,
        'quotationNumber': body1.data.quotationNumber,
        'quotationVersionNumber': body1.data.quotationVersionNumber
      };
      this.service.updatePolicyForm(this.policySequnce, obj).subscribe(resp => {
        const body3 = resp.body as any;
        this.service.getPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
          const body4 = resp.body as any;
          let data = body4.data;
          // filter weather type and crop type
          for (let i = 0; i < data.length; i++) {
            let obj = data[i];
            if (obj['description'] === 'CROP') {
              this.cropTypeSequence = obj['sequenceNo'];
              this.cropTypeVersion = obj['version'];
              this.cropTypeDescription = obj['description'];
            }
            if (obj['description'] === 'WEATHER TYPE') {
              this.weatherSequenceNo = obj['sequenceNo'];
              this.weatherVersion = obj['version'];
              this.weatherDescription = obj['description'];
            }
          }

          // update function
          let updated_josn = [{
            'sequenceNo': this.weatherSequenceNo,
            'description': this.weatherDescription,
            'value': this.weatherType,
            'version': this.weatherVersion
          }, {
            'sequenceNo': this.cropTypeSequence,
            'description': this.cropTypeDescription,
            'value': this.cropType,
            'version': this.cropTypeVersion
          }];

          this.service.updatePolicy(this.policySequnce, updated_josn).subscribe(resp => {
            const body5 = resp.body as any;
            this.isDataLoaded = false;
            this.displaySuccessMessage(body5);
            this.getpolicyDetails(this.policySequnce, 0, 'NL', this.authorizedDate);
            this.displayPolicyDetails = true;
            this.displayManageWeatherForm = false;
            this.displayLivestockRiskDetailsDiv = false;
            this.displayWeatherRiskDetailsDiv = false;
            //   this.router.navigate(['/policies']);
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });

  }

  getIntermediarySequenceNo() {
    this.service.getIntermediarySequence(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.intermediarySequence = body.data[0].sequenceNo;
    });
  }

  selectIntermediaryName(event) {
    this.intermediaryTypeName = event;
  }

  updatePolicyLivestock() {
    this.isDataLoaded = true;
    if (sessionStorage.getItem('partyAsInternalStaff') === 'Y') {
      this.service.getIntermediaryDetails(this.policySequnce, this.authorizedDate).subscribe(resp => {
        const body1 = resp.body as any;
        const businessParty = 'Y';
        const commissionParty = 'Y';
        let obj = {
          'sequenceNo': this.intermediarySequence,
          'intermediaryTypeCode': this.intermediaryTypeCode,
          'intermediaryCode': this.intermediaryTypeName,
          'isBusinessParty': businessParty,
          'isCommissionParty': commissionParty,
          'version': body1.data[0].version,
        };
        this.service.updateIntermediaryDetails(this.intermediarySequence, obj).subscribe(resp => {
          const body3 = resp.body as any;
          this.isDataLoaded = false;
          this.displaySuccessMessage(body3);
          this.getpolicyDetails(this.policySequnce, 0, 'NL', this.authorizedDate);
          this.displayPolicyDetails = true;
          this.displayManageLivestockForm = false;
          this.displayManageWeatherForm = false;
          this.displayLivestockRiskDetailsDiv = false;
          this.displayWeatherRiskDetailsDiv = false;
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });

      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });

    }
    this.service.getPolicyDetails(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body1 = resp.body as any;
      let obj = {
        'sequence': this.policySequnce,
        'classCode': body1.data.classCode,
        'productCode': this.productCode,
        'branchCode': body1.data.branchCode,
        'currencyCode': body1.data.currencyCode,
        'customerAddressSequence': body1.data.customerAddressSequence,
        'proposalNumber': body1.data.proposalNumber,
        'policyNumber': body1.data.policyNumber,
        'periodFromDate': moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
        'periodToDate': moment(this.liveStockEndDate).format('YYYY-MM-DD HH:mm:ss'),
        'modeOfBusinessCode': body1.data.modeOfBusinessCode,
        'settlementModeCode': this.premiumSettlementMethod,
        'debtorCode': body1.data.debtorCode,
        'channelOfBuinessCode': this.businessChanel,
        'policyRemark': body1.data.policyRemark,
        'customerCode': body1.data.customerCode,
        'fromNarration': body1.data.fromNarration,
        'toNarration': body1.data.toNarration,
        'status': body1.data.status,
        'sumInsured': body1.data.sumInsured,
        'eventLimit': body1.data.eventLimit,
        'eventPercentage': body1.data.eventPercentage,
        'eventNarration': body1.data.eventNarration,
        'annualLimit': body1.data.annualLimit,
        'annualPercentage': body1.data.annualPercentage,
        'annualNarration': body1.data.annualNarration,
        'premium': body1.data.premium,
        'totalPremium': body1.data.totalPremium,
        'transactionAmount': body1.data.transactionAmount,
        'totalTransactionAmount': body1.data.totalTransactionAmount,
        'reinsuranceExtensionRequiredFlag': body1.data.reinsuranceExtensionRequiredFlag,
        'transactionType': body1.data.transactionType,
        'currencyDate': body1.data.currencyDate,
        'authorisedDate': body1.data.authorisedDate,
        'creditAuthRequiredFlag': body1.data.creditAuthRequiredFlag,
        'restrictListAuthRequiredFlag': body1.data.restrictListAuthRequiredFlag,
        'reinsuranceAuthRequiredFlag': body1.data.reinsuranceAuthRequiredFlag,
        'version': body1.data.version,
        'quotationSequence': body1.data.quotationSequence,
        'quotationNumber': body1.data.quotationNumber,
        'quotationVersionNumber': body1.data.quotationVersionNumber,
      };
      this.service.updatePolicyForm(this.policySequnce, obj).subscribe(resp => {
        const body3 = resp.body as any;
        this.isDataLoaded = false;
        this.displaySuccessMessage(body3);
        this.getpolicyDetails(this.policySequnce, 0, 'NL', this.authorizedDate);
        this.displayPolicyDetails = true;
        this.displayManageLivestockForm = false;
        this.displayManageWeatherForm = false;
        this.displayLivestockRiskDetailsDiv = false;
        this.displayWeatherRiskDetailsDiv = false;
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  updatePolicyIndemnity() {
    this.isDataLoaded = true;
    if (sessionStorage.getItem('partyAsInternalStaff') === 'Y') {
      this.service.getPolicyDetails(this.policySequnce, this.authorizedDate).pipe(
        flatMap(response => {
          const policyDetails = response.body as any;
          const hasIntermediary = this.intermediaryTypeCode && this.intermediaryTypeName;
          let obj = {
            'sequence': this.policySequnce,
            'classCode': policyDetails.data.classCode,
            'productCode': this.indemnityPlan.value,
            'branchCode': policyDetails.data.branchCode,
            'currencyCode': policyDetails.data.currencyCode,
            'customerAddressSequence': policyDetails.data.customerAddressSequence,
            'proposalNumber': policyDetails.data.proposalNumber,
            'policyNumber': policyDetails.data.policyNumber,
            'periodFromDate': moment(this.indemnityStartDate.value).format('YYYY-MM-DD HH:mm:ss'),
            'periodToDate': moment(this.indemnityEndDate.value).format('YYYY-MM-DD HH:mm:ss'),

            "modeOfBusinessCode": hasIntermediary ? this.intermediaryTypeCode : policyDetails.data.modeOfBusinessCode,

            "settlementModeCode": this.indemnityPaymentType.value,
            "debtorCode": policyDetails.data.debtorCode,
            "channelOfBuinessCode": this.indemnityBusinessChannel.value,
            "policyRemark": policyDetails.data.policyRemark,
            "customerCode": policyDetails.data.customerCode,
            "fromNarration": policyDetails.data.fromNarration,
            "toNarration": policyDetails.data.toNarration,
            "status": policyDetails.data.status,
            "sumInsured": policyDetails.data.sumInsured,
            "eventLimit": policyDetails.data.eventLimit,
            "eventPercentage": policyDetails.data.eventPercentage,
            "eventNarration": policyDetails.data.eventNarration,
            "annualLimit": policyDetails.data.annualLimit,
            "annualPercentage": policyDetails.data.annualPercentage,
            "annualNarration": policyDetails.data.annualNarration,
            "premium": policyDetails.data.premium,
            "totalPremium": policyDetails.data.totalPremium,
            "transactionAmount": policyDetails.data.transactionAmount,
            "totalTransactionAmount": policyDetails.data.totalTransactionAmount,
            "reinsuranceExtensionRequiredFlag": policyDetails.data.reinsuranceExtensionRequiredFlag,
            "transactionType": policyDetails.data.transactionType,
            "currencyDate": policyDetails.data.currencyDate,
            "authorisedDate": policyDetails.data.authorisedDate,
            "creditAuthRequiredFlag": policyDetails.data.creditAuthRequiredFlag,
            "restrictListAuthRequiredFlag": policyDetails.data.restrictListAuthRequiredFlag,
            "reinsuranceAuthRequiredFlag": policyDetails.data.reinsuranceAuthRequiredFlag,
            "version": policyDetails.data.version,
            "quotationSequence": policyDetails.data.quotationSequence,
            "quotationNumber": policyDetails.data.quotationNumber,
            "quotationVersionNumber": policyDetails.data.quotationVersionNumber
          };
          return this.service.updatePolicyForm(this.policySequnce, obj);
        }),
        flatMap(response => {
          return this.service.getIntermediaryDetails(this.policySequnce, this.authorizedDate)
        }),
        flatMap(resp => {
          const body1 = resp.body as any;
          const businessParty = 'Y';
          const commissionParty = 'Y';
          let obj = {
            'sequenceNo': this.intermediarySequence,
            'intermediaryTypeCode': this.intermediaryTypeCode,
            'intermediaryCode': this.intermediaryTypeName,
            'isBusinessParty': businessParty,
            'isCommissionParty': commissionParty,
            'version': body1.data[0].version,
          };
          if (this.intermediaryTypeName && this.intermediaryTypeCode) {
            return this.service.updateIntermediaryDetails(this.intermediarySequence, obj);
          } else {
            return this.service.getPolicyDetails(this.policySequnce, this.authorizedDate)
          }
        })
      ).subscribe(response => {
        const body = response.body as any;
        this.isDataLoaded = false;
        this.displaySuccessMessage({message: 'Operation Completed Successfully'});
        this.getpolicyDetails(this.policySequnce, 0, 'NL', this.authorizedDate);
        this.displayPolicyDetails = true;
        this.displayManageWeatherForm = false;
        this.displayLivestockRiskDetailsDiv = false;
        this.displayWeatherRiskDetailsDiv = false;
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    } else {
      this.service.getPolicyDetails(this.policySequnce, this.authorizedDate).pipe(
        flatMap(response => {
          const policyDetails = response.body as any;
          let obj = {
            "sequence": this.policySequnce,
            "classCode": policyDetails.data.classCode,
            "productCode": this.indemnityPlan.value,
            "branchCode": policyDetails.data.branchCode,
            "currencyCode": policyDetails.data.currencyCode,
            "customerAddressSequence": policyDetails.data.customerAddressSequence,
            "proposalNumber": policyDetails.data.proposalNumber,
            "policyNumber": policyDetails.data.policyNumber,
            'periodFromDate': moment(this.indemnityStartDate.value).format('YYYY-MM-DD HH:mm:ss'),
            'periodToDate': moment(this.indemnityEndDate.value).format('YYYY-MM-DD HH:mm:ss'),
            "modeOfBusinessCode": policyDetails.data.modeOfBusinessCode,
            "settlementModeCode": this.indemnityPaymentType.value,
            "debtorCode": policyDetails.data.debtorCode,
            "channelOfBuinessCode": this.indemnityBusinessChannel.value,
            "policyRemark": policyDetails.data.policyRemark,
            "customerCode": policyDetails.data.customerCode,
            "fromNarration": policyDetails.data.fromNarration,
            "toNarration": policyDetails.data.toNarration,
            "status": policyDetails.data.status,
            "sumInsured": policyDetails.data.sumInsured,
            "eventLimit": policyDetails.data.eventLimit,
            "eventPercentage": policyDetails.data.eventPercentage,
            "eventNarration": policyDetails.data.eventNarration,
            "annualLimit": policyDetails.data.annualLimit,
            "annualPercentage": policyDetails.data.annualPercentage,
            "annualNarration": policyDetails.data.annualNarration,
            "premium": policyDetails.data.premium,
            "totalPremium": policyDetails.data.totalPremium,
            "transactionAmount": policyDetails.data.transactionAmount,
            "totalTransactionAmount": policyDetails.data.totalTransactionAmount,
            "reinsuranceExtensionRequiredFlag": policyDetails.data.reinsuranceExtensionRequiredFlag,
            "transactionType": policyDetails.data.transactionType,
            "currencyDate": policyDetails.data.currencyDate,
            "authorisedDate": policyDetails.data.authorisedDate,
            "creditAuthRequiredFlag": policyDetails.data.creditAuthRequiredFlag,
            "restrictListAuthRequiredFlag": policyDetails.data.restrictListAuthRequiredFlag,
            "reinsuranceAuthRequiredFlag": policyDetails.data.reinsuranceAuthRequiredFlag,
            "version": policyDetails.data.version,
            "quotationSequence": policyDetails.data.quotationSequence,
            "quotationNumber": policyDetails.data.quotationNumber,
            "quotationVersionNumber": policyDetails.data.quotationVersionNumber
          };
          return this.service.updatePolicyForm(this.policySequnce, obj);
        })
      ).subscribe(response => {
        const body = response.body as any;
        this.isDataLoaded = false;
        this.displaySuccessMessage(body);
        this.getpolicyDetails(this.policySequnce, 0, 'NL', this.authorizedDate);
        this.displayPolicyDetails = true;
        this.displayManageWeatherForm = false;
        this.displayLivestockRiskDetailsDiv = false;
        this.displayWeatherRiskDetailsDiv = false;
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }
  }

  manageLand(data) {

    this.getPolicyFormDetails();
    this.clearLandForm();
    this.indemnityAddLandForm.reset();
    this.indemnityCultivationCropType.enable();
    this.indemnityCultivationCrop.enable();
    this.indemnityCropVariance.enable();
    this.clearCommonPlaces();
    this.landEdit = true;
    this.riskSequnce = data;
    this.getRiskAdditionalDetails();
    this.getLocations();
    this.getWeatherStations();
    this.displayManageLandForm = true;
    this.displayPolicyDetails = false;
    this.viewAddLandPhotos = false;
    this.enableContinueButtonWeatherLand = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
    this.getLandDetails();
    if (this.productType === 'CRPTY') {
      this.displayWeatherIndexParametersDiv = false;
      this.displayIndemnityAdditionalDetails = true;
      this.isIndemnityAddLandForm = true;
    } else if (this.productType === 'WHINX') {
      this.isIndemnityAddLandForm = false;
      this.displayWeatherIndexParametersDiv = true;
      this.displayIndemnityAdditionalDetails = false;
      this.getCropVariance(this.cropType);
    }
    this.disabledCultivationFeilds = false;
  }

  getLocations() {
    this.locations = [];
    this.service.getLocations(this.customerCode).subscribe(resp => {
      const body = resp.body as any;
      this.locations = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  //get weather stations
  getWeatherStations() {
    this.service.getWeatherStations().subscribe(resp => {
      const body = resp.body as any;
      this.weatherStations = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  //get crop variances
  getCropVariance(data) {
    this.service.getCropVariances(data).subscribe(resp => {
      const body = resp.body as any;
      this.cropVariances = body.data;
    }, error => {
      this.cropVariances = [];
    });
  }

  getIndemnityCrops(cropCategory: string) {
    this.service.getIndemnityCrops(cropCategory).subscribe((resp: any) => {
      this.cropTypes = resp.body.data
    }, error => {
      this.cropTypes = [];
    })
  }


  // livestock form
  //  manage livestock form
  manageLivestock(data) {
    this.clearLivestockForm();
    this.manageLivestockRisk = true;
    this.clearCommonPlaces();
    this.riskSequnce = data;
    this.displayManageLivestockForm = true;
    this.displayLivestockAdditionalDetails = true;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
    this.getRiskAdditionalDetails();
    this.displayPolicyDetails = false;
    // get dropdown data
    this.getLiveStockTypes();
    this.getBreedTypes(null);
    this.getGenders();
    this.getUsageTypes();
    this.getUnderwritingAssessors();
    this.getLivestockDetails();
  }

  getLiveStockTypes() {
    this.service.getLiveStockTypes().subscribe(resp => {
      const body = resp.body as any;
      this.liveStockTypes = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getBreedTypes(livestockType: string) {
    this.service.getBreedTypes(livestockType).subscribe(resp => {
      const body = resp.body as any;
      this.breedTypes = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getGenders() {
    this.service.getGenders().subscribe(resp => {
      const body = resp.body as any;
      this.genders = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getUsageTypes() {
    this.service.getUsageTypes().subscribe(resp => {
      const body = resp.body as any;
      this.usageTypes = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  getUnderwritingAssessors() {
    this.service.getUnderwritingAssessors().subscribe(resp => {
      const body = resp.body as any;
      this.undewritingAssessors = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  // risk additibal details
  getRiskAdditionalDetails() {
    this.riskImages = [];
    this.covers = [];
    this.riskImages = [];
    this.coversLoaded = false;
    let documentSequnce = '';
    this.isDataLoaded = true;
    // get risk images
    this.service.getRiskDocuments(this.riskSequnce, this.authorizedDate).pipe(
      flatMap(resp => {
        const body = resp.body as any;
        documentSequnce = body.data[0]['sequenceNo'];
        return this.service.downloadImages(documentSequnce, this.authorizedDate);
      }),
      flatMap(resp => {
        const body1 = resp.body as any;
        this.riskImages = body1.data.objectList;
        this.imagList = [];
        for (let i = 0; i < this.riskImages.length; i++) {
          let im = new ImageModel();
          im.imageKey = this.riskImages[i]['key'];
          im.value = this.riskImages[i]['url'];
          this.imagList.push(im);
        }
        return this.service.getRiskPerils(this.riskSequnce, this.authorizedDate);
      }),
      flatMap(resp => {
        const body1 = resp.body as any;
        this.covers = body1.data;
        this.coversLoaded = true;
        return this.service.getPolicyDetails(this.policySequnce, this.authorizedDate);
      })
    ).subscribe(resp => {
      const body2 = resp.body as any;
      this.totalPremium = body2.data.totalPremium;
      this.riskPremium = body2.data.totalPremium;
      this.isDataLoaded = false;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  // get livestock data
  getLandDetails() {
    this.disabledCultivationFeilds = false;
    let tempAddress: string;
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).pipe(
      flatMap(resp => {
        const body = resp.body as any;
        //risk
        this.riskOrder = body.data.riskOrder;
        this.riskName = body.data.riskName;
        this.farmerName = body.data.riskName;
        this.riskRefNo = body.data.riskRefNo;
        this.riskSectionCode = body.data.riskSectionCode;
        this.riskSumInsured = body.data.riskSumInsured;
        this.indemnitySumInsured.setValue(body.data.riskSumInsured);
        this.effectiveDate = new Date(body.data.effectiveDate);
        this.indemnityLandStartDate.setValue(new Date(body.data.effectiveDate));
        this.effectiveToDate = new Date(body.data.effectiveToDate);
        this.indemnityLandEndDate.setValue(new Date(body.data.effectiveToDate));
        this.size1 = body.data.size1;
        this.size1UnitType = body.data.size1UnitType;
        this.size2 = body.data.size2;
        this.size2UnitType = body.data.size2UnitType;
        this.weatherLandStartDate = new Date(body.data.effectiveDate);
        this.weatherLandPremium = body.data.premium;
        this.weatherLandEndDate = new Date(body.data.effectiveToDate);

        tempAddress = body.data.location.location;
        return this.service.getLocations(this.customerCode);
      }),
      flatMap(resp => {
        this.locations = [];
        const body1 = resp.body as any;
        this.locations = body1.data;
        top: for (let i = 0; i < this.locations.length; i++) {
          if (this.locations[i]['address'] === tempAddress) {
            this.location = this.locations[i];
            this.getSelectedLocation(this.location);
            break top;
          }
        }
        return this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate);
      }),
      flatMap(resp => {
        const body_3 = resp.body as any;
        let resultArr = body_3.data;
        for (let i = 0; i < resultArr.length; i++) {
          let obj = resultArr[i];
          if (obj['description'] === 'CROP VARIANCE') {
            this.cropVariance = obj['value'];
            this.indemnityCropVariance.setValue(obj['value']);
            this.cropVarianceValueDecription = obj['valueDescription'];
          } else if (obj['description'] === 'WEATHER STATION') {
            this.weatherStation = parseInt(obj['value']);
            this.weatherStationValueDescription = obj['valueDescription'];
          } else if (obj['description'] === 'FARMER NAME') {
            this.farmerName = obj['value'];
            this.indemnityFarmerName.setValue(obj['value']);
          } else if (obj['description'] === 'NO OF UNITS') {
            this.noOfUnits = parseInt(obj['value']) + '';
          } else if (obj['description'] === 'CROP CATEGORY') {
            this.indemnityCultivationCropType.setValue(obj['value']);
            this.cropCategoryValueDecription = obj['valueDescription']
          } else if (obj['description'] === 'CROP') {
            this.indemnityCultivationCrop.setValue(obj['value']);
            this.cropTypeValueDecription = obj['valueDescription']
            this.cropType = obj['value']
            this.getCropVariance(this.cropType);
          }
        }
        return this.service.getUnitPriceOnRisk(this.weatherStation);
      }),
      flatMap(resp => {
        const body_4 = resp.body as any;
        // this.size2 = body_4.data.unitPrice;
        this.price = body_4.data ? body_4.data.unitPrice : 0;
        this.riskImages = [];
        // this.covers = [];
        let documentSequnce = '';
        return this.service.getRiskDocuments(this.riskSequnce, this.authorizedDate);
      }),
      flatMap(resp => {
        const body = resp.body as any;
        let documentSequnce = body.data[0]['sequenceNo'];
        return this.service.downloadImages(documentSequnce, this.authorizedDate);
      }),
      flatMap(resp => {
        const body1 = resp.body as any;
        this.weatherLandPhotos = body1.data.objectList;
        this.imagList = [];
        for (let i = 0; i < this.weatherLandPhotos.length; i++) {
          let im = new ImageModel();
          im.imageKey = this.weatherLandPhotos[i]['key'];
          im.value = this.weatherLandPhotos[i]['url'];
          this.imagList.push(im);
        }
        this.weatherIndexParameters = [];
        const selectedFromMonth = parseInt(this.fromMonth);
        const selectedToMonth = parseInt(this.toMonth);
        return this.service.getWeatherindexParameters(this.fromYear, selectedFromMonth, this.weatherStation, this.toYear, selectedToMonth);
      })
    ).subscribe(resp => {
      const body = resp.body as any;
      this.viewAddLandPhotos = false;

      this.weatherIndexParameters = body.data;
      for (let i = 0; i < this.weatherIndexParameters.length; i++) {
        const month = parseInt(this.weatherIndexParameters[i]['month']) - 1;
        this.weatherIndexParameters[i]['month'] = moment().month(month).format('MMMM');
      }
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  // add new land realted functions
  addLand() {
    this.clearLandForm();
    this.indemnityAddLandForm.reset();
    this.disabledLocation = false;
    this.clearCommonPlaces();
    this.landEdit = false;
    this.getLocations();
    this.getWeatherStations();
    this.getCropVariance(this.cropType);
    this.displayManageLandForm = true;
    if (this.productType === 'CRPTY') {
      this.covers = [];
      this.indemnityAddLandForm.controls['indemnityCropVariance'].enable();
      this.indemnityAddLandForm.controls['indemnityCultivationCrop'].enable();
      this.indemnityAddLandForm.controls['indemnityCultivationCropType'].enable();
      this.isIndemnityAddLandForm = true;
      this.indemnityLandEndDateMax = this.endDate;
      this.indemnityLandStarDateMin = new Date();
      this.indemnityLandEndDateMin = new Date();
    }
    this.displayPolicyDetails = false;
    this.viewAddLandPhotos = true;
    this.enableContinueButtonWeatherLand = true;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
    this.riskRefNo = this.riskRefNo + 1;
    this.displayWeatherIndexParametersDiv = false;
    this.disabledCultivationFeilds = false;
  }

  saveWeatherLand() {
    this.isDataLoaded = true;
    this.riskRefNo = this.riskRefNo + 1;
    let locationObj = {
      'location': this.location['address'],
      'locationOrder': this.locationOrderNo
    };
    this.service.addLocation(this.policySequnce, locationObj).subscribe(resp => {
      const body_1 = resp.body as any;
      this.locationSequence = body_1.data;
      //add risk
      let riskObj = {
        'riskOrder': null,
        'riskName': this.location['plotName'],
        'riskRefNo': null,
        'riskSumInsured': this.riskSumInsured,
        'effectiveDate': moment(this.weatherLandStartDate).format('YYYY-MM-DD HH:mm:ss'),
        'effectiveToDate': moment(this.weatherLandEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        'size1': this.size1,
        'size1UnitType': this.size1UnitType,
        'size2': this.size2,
        'size2UnitType': this.size2UnitType
      };
      this.service.addRisks(this.locationSequence, riskObj).subscribe(resp => {
        const body_2 = resp.body as any;
        this.riskSequnce = body_2.data;
        //get risk commont information
        this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_3 = resp.body as any;
          let resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            let obj = resultArr[i];
            if (obj['description'] === 'NO OF UNITS') {
              this.noOfUnitsSequence = obj['sequenceNo'];
              this.noOfUnitsDescription = obj['description'];
              this.noOfUnitsVersion = obj['version'];
              this.noOfUnitsValue = this.noOfUnits;
            } else if (obj['description'] === 'CROP VARIANCE') {
              //crop variance
              this.cropVarianceSequence = obj['sequenceNo'];
              this.cropVarianceDescription = obj['description'];
              this.cropVarianceVersion = obj['version'];
              this.cropVarianceValue = this.cropVariance;
            } else if (obj['description'] === 'WEATHER STATION') {
              //weather stations
              this.weatherStationSequence = obj['sequenceNo'];
              this.weatherStationDescription = obj['description'];
              this.weatherStationVersion = obj['version'];
              this.weatherStationValue = this.weatherStation + '';
            } else if (obj['description'] === 'FARMER NAME') {
              this.farmerNameSequnce = obj['sequenceNo'];
              this.farmerNameDescription = obj['description'];
              this.farmerNameVersion = obj['version'];
              this.farmerNameValue = this.farmerName;
            }
          }
          //update common references
          let jsonObj = [
            {
              'sequenceNo': this.noOfUnitsSequence,
              'description': this.noOfUnitsDescription,
              'value': this.noOfUnitsValue,
              'version': this.noOfUnitsVersion
            },
            {
              'sequenceNo': this.cropVarianceSequence,
              'description': this.cropVarianceDescription,
              'value': this.cropVariance,
              'version': this.cropVarianceVersion
            },
            {
              'sequenceNo': this.weatherStationSequence,
              'description': this.weatherDescription,
              'value': this.weatherStation,
              'version': this.weatherStationVersion
            },
            {
              'sequenceNo': this.farmerNameSequnce,
              'description': this.farmerNameDescription,
              'value': this.farmerNameValue,
              'version': this.farmerNameVersion
            }
          ];
          this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
            const body_4 = resp.body as any;
            // set msg display
            this.displaySuccessMessage(body_4);
            // set msg display
            this.viewAddLand = true;
            this.viewAddCultivation = false;
            this.viewAddLandPhotos = false;
            this.disabledLocation = true;
            this.enableContinueButtonWeatherLand = false;
            this.displayLivestockRiskDetailsDiv = false;
            this.disabledCultivationFeilds = true;
            this.displayWeatherRiskDetailsDiv = false;
            this.getWeatherIndexParameters();
            this.displayWeatherIndexParametersDiv = true;
            this.displaySuccessMessage(body_4);
            this.isDataLoaded = false;
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  saveIndemnityLand() {
    this.isDataLoaded = false;
    let locationObj = {
      'location': this.location['address'],
      'locationOrder': this.locationOrderNo
    };
    this.service.addLocation(this.policySequnce, locationObj).pipe(
      flatMap(response => {
        const respBody = response.body as any;
        this.locationSequence = respBody.data;
        let riskObj = {
          "riskOrder": null,
          "riskName": this.location['plotName'],
          "riskRefNo": null,
          "riskSumInsured": this.indemnitySumInsured.value,
          'riskSectionCode': this.indemnityCultivationCropType.value,
          "effectiveDate": moment(this.indemnityLandStartDate.value).format('YYYY-MM-DD HH:mm:ss'),
          "effectiveToDate": moment(this.indemnityLandEndDate.value, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
          "size1": this.indemnityAcres.value,
          "size1UnitType": this.size1UnitType,
          "size2": this.indemnityPerches.value,
          "size2UnitType": this.size2UnitType
        };
        return this.service.addRisks(this.locationSequence, riskObj);
      }),
      flatMap(response => {
        const risks = response.body as any;
        this.riskSequnce = risks.data;
        return this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate);
      }),
      flatMap(response => {
        const body_3 = response.body as any;
        let resultArr = body_3.data;

        let cropSequence = {};
        let farmerNameSequence = {};
        let cropTypeSequence = {};
        let cropVarianceSequence = {};

        for (let i = 0; i < resultArr.length; i++) {
          let obj = resultArr[i];
          if (obj['description'] === 'CROP VARIANCE') {
            cropVarianceSequence = {
              sequenceNo: obj['sequenceNo'],
              description: obj['description'],
              version: obj['version'],
              value: this.indemnityCropVariance.value
            };
          } else if (obj['description'] === 'FARMER NAME') {
            farmerNameSequence = {
              sequenceNo: obj['sequenceNo'],
              description: obj['description'],
              version: obj['version'],
              value: this.indemnityFarmerName.value
            };
          } else if (obj['description'] === 'CROP') {
            cropSequence = {
              sequenceNo: obj['sequenceNo'],
              description: obj['description'],
              version: obj['version'],
              value: this.indemnityCultivationCrop.value
            };
          } else if (obj['description'] === 'CROP CATEGORY') {
            cropTypeSequence = {
              sequenceNo: obj['sequenceNo'],
              description: obj['description'],
              version: obj['version'],
              value: this.indemnityCultivationCropType.value,
            };
          }
        }

        let updatedRiskCommonInfo = [
          cropSequence, farmerNameSequence, cropTypeSequence, cropVarianceSequence
        ];
        return this.service.UpdateRiskCommonInformation(this.riskSequnce, updatedRiskCommonInfo);
      })
    ).subscribe(response => {
      const body_4 = response.body as any;
      // set msg display
      this.displaySuccessMessage(body_4);
      this.displayPolicyDetails = false;
      this.displayLivestockAdditionalDetails = false;
      this.displayManageLivestockForm = false;
      this.displayManageCoversForm = false;
      this.displayLivestockRiskDetailsDiv = false;
      this.displayWeatherRiskDetailsDiv = false;
      this.enableContinueButtonWeatherLand = false;
      this.disabledCultivationFeilds = true;
      this.indemnityCropVariance.disable();
      this.indemnityCultivationCropType.disable();
      this.indemnityCultivationCrop.disable();
      this.viewAddLandPhotos = false;
      this.isDataLoaded = false;
      this.getRiskAdditionalDetails();
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  // add live stock
  addLivestock() {
    this.clearLivestockForm();
    this.manageLivestockRisk = false;
    this.clearCommonPlaces();
    // get dropdown data
    this.getLiveStockTypes();
    this.getBreedTypes(null);
    this.getGenders();
    this.getUsageTypes();
    this.getUnderwritingAssessors();
    this.displayManageLivestockForm = true;
    this.displayPolicyDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
  }

  saveLiveStockRisk() {
    this.isDataLoaded = true;
    this.riskRefNo = this.riskRefNo + 1;
    let riskObj = {
      'riskOrder': null,
      'riskName': this.tagNumber,
      'riskRefNo': null,
      'riskSumInsured': this.riskSumInsured
    };
    this.service.getDefaultLocation(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.locationSequence = body.data[0]['seqNo'];
      this.service.addRisks(this.locationSequence, riskObj).subscribe(resp => {
        const body = resp.body as any;
        this.riskSequnce = body.data;
        //get risk informations
        this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_3 = resp.body as any;
          let resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            let obj = resultArr[i];
            if (obj['description'] === 'LIVESTOCK TYPE') {
              //live stock type
              this.liveStockTypeSequnce = obj['sequenceNo'];
              this.liveStockTypeDescription = obj['description'];
              this.liveStockTypeVersion = obj['version'];
              this.liveStockTypeValue = this.liveStockType;
            } else if (obj['description'] === 'BREED') {
              //breed
              this.breedSequence = obj['sequenceNo'];
              this.breedDescription = obj['description'];
              this.breedVersion = obj['version'];
              this.breedValue = this.breedType;
            } else if (obj['description'] === 'GENDER') {
              //gender
              this.genderSequence = obj['sequenceNo'];
              this.genderDescription = obj['description'];
              this.genderVersion = obj['version'];
              this.genderValue = this.gender;
            } else if (obj['description'] === 'USAGE') {
              //usage type
              this.usageSequence = obj['sequenceNo'];
              this.usageDescription = obj['description'];
              this.usageVersion = obj['version'];
              this.usageValue = this.usageType;
            } else if (obj['description'] === 'AGE IN YEARS') {
              //age in years
              this.ageInYearsSequence = obj['sequenceNo'];
              this.ageInYearsDescription = obj['description'];
              this.ageInYearsVersion = obj['version'];
              this.ageInYearsValue = this.age;
            } else if (obj['description'] === 'OWNER NAME') {
              //owner name
              this.ownerNameSequence = obj['sequenceNo'];
              this.ownerNameDescription = obj['description'];
              this.ownerNameVersion = obj['version'];
              this.ownerNameValue = this.ownerName;
            } else if (obj['description'] === 'PURCHASE DATE') {
              //purchase date
              this.purchaseDateSequence = obj['sequenceNo'];
              this.purchaseDateDescription = obj['description'];
              this.purchaseDateVersion = obj['version'];
              this.purchaseDateValue = moment(this.livestockPurchaseDate).format('YYYY-MM-DD HH:mm:ss');
            }
          }
          //update common references for risk
          let jsonObj = [
            {
              'sequenceNo': this.liveStockTypeSequnce,
              'description': this.liveStockTypeDescription,
              'value': this.liveStockTypeValue,
              'version': this.liveStockTypeVersion
            },
            {
              'sequenceNo': this.breedSequence,
              'description': this.breedDescription,
              'value': this.breedValue,
              'version': this.breedVersion
            },
            {
              'sequenceNo': this.genderSequence,
              'description': this.genderDescription,
              'value': this.genderValue,
              'version': this.genderVersion
            },
            {
              'sequenceNo': this.usageSequence,
              'description': this.usageDescription,
              'value': this.usageValue,
              'version': this.usageVersion
            },
            {
              'sequenceNo': this.ageInYearsSequence,
              'description': this.ageInYearsDescription,
              'value': this.ageInYearsValue,
              'version': this.ageInYearsVersion
            },
            {
              'sequenceNo': this.ownerNameSequence,
              'description': this.ownerNameDescription,
              'value': this.ownerNameValue,
              'version': this.ownerNameVersion
            },
            {
              'sequenceNo': this.purchaseDateSequence,
              'description': this.purchaseDateDescription,
              'value': this.purchaseDateValue,
              'version': this.purchaseDateVersion
            }
          ];
          this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
            const body_4 = resp.body as any;
            let inspectedObj = {
              'assessorCode': this.undewritingAssessor,
              'datetime': moment(this.newLivestockStartDate).format('YYYY-MM-DD HH:mm:ss')
            };
            this.service.saveInspectedDetails(this.riskSequnce, inspectedObj).subscribe(resp => {
              const body_5 = resp.body as any;
              this.displaySuccessMessage(body_5);
              this.displayPolicyDetails = false;
              this.displayLivestockAdditionalDetails = true;
              this.displayManageLivestockForm = false;
              this.displayManageCoversForm = false;
              this.displayLivestockRiskDetailsDiv = false;
              this.displayWeatherRiskDetailsDiv = false;
              this.isDataLoaded = false;
              this.getRiskAdditionalDetails();
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  // save  images keys
  savePolicyImageKeys() {
    // get document sequnce
    this.service.getPolicyDocumentSequnce(this.policySequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.documentSequnce = body.data[0]['sequenceNo'];
      let obj = [];
      for (let i = 0; i < this.imagList.length; i++) {
        obj.push(this.imagList[i]['imageKey']);
      }
      this.service.saveWeatherImages(this.documentSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.displaySuccessMessage(body);
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  saveRiskImageKeys() {
    // get document sequnce
    this.service.getRiskDocumentSequnce(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.documentSequnce = body.data[0]['sequenceNo'];
      let obj = [];
      for (let i = 0; i < this.imagList.length; i++) {
        obj.push(this.imagList[i]['imageKey']);
      }
      // make image key array
      this.service.saveRiskImages(this.documentSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.displaySuccessMessage(body);
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  addCovers() {
    this.clearCoverForm();
    this.clearCommonPlaces();
    this.manageCover = false;
    this.displayManageCoversForm = true;
    this.displayLivestockAdditionalDetails = false;
    this.displayIndemnityAdditionalDetails = false;
    this.displayManageLandForm = false;
    this.displayManageLivestockForm = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
    this.getOtherPerils();
  }

  getOtherPerils() {
    this.service.getOtherPerils(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.otherPerils = body.data;
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  saveCovers() {
    this.isDataLoaded = true;
    let peril = {};
    if (this.otherPeril['perilPercentage'] === null) {
      peril = {
        'perilCode': this.otherPeril['perilCode'],
        'perilPerilSumInsured': this.perilPerilSumInsured,
        'perilPremium': this.otherPeril['perilPercentage']
      };
    } else {
      peril = {
        'perilCode': this.otherPeril['perilCode'],
        'perilPerilSumInsured': this.perilPerilSumInsured,
        'perilPercentage': this.otherPeril['perilPercentage']
      };
    }
    this.service.saveCovers(this.riskSequnce, peril).subscribe(resp => {
      let body = resp.body as any;
      this.perilSequnce = body.data;
      // excess insert
      let excess = {
        'excessAmount': this.excessAmount,
        'excessPercentage': this.excessPercentage
      };
      this.service.saveExcess(this.perilSequnce, excess).subscribe(resp => {
        let body1 = resp.body as any;
        this.displaySuccessMessage(body1);

        if (this.productType === 'CRPTY') {
          this.displayManageLandForm = true;
          this.displayIndemnityAdditionalDetails = true;
          this.displayLivestockAdditionalDetails = false;
          this.displayManageLivestockForm = false;
        } else {
          this.displayLivestockAdditionalDetails = true;
          this.displayManageLivestockForm = true;
        }
        this.displayManageCoversForm = false;
        this.displayLivestockRiskDetailsDiv = false;
        this.displayWeatherRiskDetailsDiv = false;
        this.isDataLoaded = false;
        this.getRiskAdditionalDetails();
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  manageCovers(data) {
    this.clearCoverForm();
    this.perilSequnce = data;
    this.clearCommonPlaces();
    this.manageCover = true;
    this.displayManageCoversForm = true;
    this.displayManageLivestockForm = false;
    this.displayLivestockAdditionalDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
    this.displayIndemnityAdditionalDetails = false;
    this.displayManageLandForm = false;
    this.getOtherPerils();
    this.getCoverDetails();
  }

  getCoverDetails() {
    this.service.getCoverDetails(this.perilSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.perilSequnce = body.data.seqNo;
      this.otherPeril = body.data.perilCode;
      this.perilPerilSumInsured = body.data.perilPerilSumInsured;
      this.managePerilDescription = body.data.perilName;
      this.service.getExcess(this.perilSequnce, this.authorizedDate).subscribe(resp => {
        const body2 = resp.body as any;
        this.excessAmount = body2.data.excessAmount;
        this.excessPercentage = body2.data.excessPercentage;
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  // update land details
  updateLandRisk() {
    this.isDataLoaded = true;
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      let obj = {
        'annualLimit': body.data.annualLimit,
        'annualNarration': body.data.annualNarration,
        'annualPercentage': body.data.annualPercentage,
        'cancelledAmount': body.data.cancelledAmount,
        'certificateNo': body.data.certificateNo,
        'coverNoteNo': body.data.coverNoteNo,
        'deletedDate': body.data.deletedDate,
        'deletedEndorsementNo': body.data.deletedEndorsementNo,
        'effectEndorsementNo': body.data.effectEndorsementNo,
        'effectiveDate': moment(this.weatherLandStartDate).format('YYYY-MM-DD HH:mm:ss'),
        'effectiveToDate': moment(this.weatherLandEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        'eventLimit': body.data.eventLimit,
        'eventNarration': body.data.eventNarration,
        'eventPercentage': body.data.eventPercentage,
        'geometry': [],
        'mainRiskRelationship': body.data.mainRiskRelationship,
        'premium': body.data.premium,
        'relatedMainRiskCode': body.data.relatedMainRiskCode,
        'riskDescription': body.data.riskDescription,
        'riskName': body.data.riskName,
        'riskOrder': this.riskOrder,
        'riskPolicyNo': body.data.riskPolicyNo,
        'riskRefNo': body.data.riskRefNo,
        'riskSectionCode': body.data.riskSectionCode,
        'riskStatus': body.data.riskStatus,
        'riskSumInsured': this.riskSumInsured,
        'seqNo': this.riskSequnce,
        'totalCancelledAmount': body.data.totalCancelledAmount,
        'totalPremium': body.data.totalPremium,
        'totalTransactionAmount': body.data.totalTransactionAmount,
        'transactionAmount': body.data.transactionAmount,
        'size1': this.size1,
        'size2': this.size2,
        'size1UnitType': this.size1UnitType,
        'size2UnitType': this.size2UnitType,
        'version': body.data.version
      };
      let locationObj = {
        'annualLimit': body.data.location.annualLimit,
        'annualNarration': body.data.location.annualNarration,
        'annualPercentage': body.data.location.annualPercentage,
        'cancelledAmount': body.data.location.cancelledAmount,
        'deletedDate': body.data.location.deletedDate,
        'deletedEndorsementNo': body.data.location.deletedEndorsementNo,
        'effectEndorsementNo': body.data.location.effectEndorsementNo,
        'effectiveDate': body.data.location.effectiveDate,
        'eventLimit': body.data.location.eventLimit,
        'eventNarration': body.data.location.eventNarration,
        'eventPercentage': body.data.location.eventPercentage,
        'geometry': body.data.location.geometry,
        'location': this.location,
        'locationOrder': this.locationOrderNo,
        'locationStatus': body.data.location.locationOrder,
        'locationSumInsured': body.data.location.locationSumInsured,
        'locPolicyNo': body.data.location.locPolicyNo,
        'premium': body.data.location.premium,
        'seqNo': body.data.location.seqNo,
        'version': body.data.location.version,
        'totalCancelledAmount': body.data.location.totalCancelledAmount,
        'totalPremium': body.data.location.totalPremium,
        'totalTransactionAmount': body.data.totalTransactionAmount,
        'transactionAmount': body.data.transactionAmount
      };
      this.locationSequence = body.data.location.seqNo,
        this.service.updateRisk(this.riskSequnce, obj).subscribe(resp => {
          const body2 = resp.body as any;
          this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
            const body_3 = resp.body as any;
            let resultArr = body_3.data;
            let cropTypeSequence = {};
            let cropSequence = {};

            for (let i = 0; i < resultArr.length; i++) {
              let obj = resultArr[i];
              if (obj['description'] === 'NO OF UNITS') {
                this.noOfUnitsSequence = obj['sequenceNo'];
                this.noOfUnitsDescription = obj['description'];
                this.noOfUnitsVersion = obj['version'];
                this.noOfUnitsValue = this.noOfUnits;
              } else if (obj['description'] === 'CROP VARIANCE') {
                // crop variance
                this.cropVarianceSequence = obj['sequenceNo'];
                this.cropVarianceDescription = obj['description'];
                this.cropVarianceVersion = obj['version'];
                this.cropVarianceValue = this.cropVariance;
              } else if (obj['description'] === 'WEATHER STATION') {
                // weather stations
                this.weatherStationSequence = obj['sequenceNo'];
                this.weatherStationDescription = obj['description'];
                this.weatherStationVersion = obj['version'];
                this.weatherStationValue = this.weatherStation + '';
              } else if (obj['description'] === 'FARMER NAME') {
                this.farmerNameSequnce = obj['sequenceNo'];
                this.farmerNameDescription = obj['description'];
                this.farmerNameVersion = obj['version'];
                this.farmerNameValue = this.farmerName;
              } else if (obj['description'] === 'CROP') {
                cropSequence = {
                  sequenceNo: obj['sequenceNo'],
                  description: obj['description'],
                  version: obj['version'],
                  value: this.indemnityCultivationCrop.value
                };
              } else if (obj['description'] === 'CROP CATEGORY') {
                cropTypeSequence = {
                  sequenceNo: obj['sequenceNo'],
                  description: obj['description'],
                  version: obj['version'],
                  value: this.indemnityCultivationCropType.value
                };
              }
            }
            //update common references
            let jsonObj = [];
            if (this.productType === 'WHINX') {
              jsonObj = [{
                'sequenceNo': this.noOfUnitsSequence,
                'description': this.noOfUnitsDescription,
                'value': this.noOfUnitsValue,
                'version': this.noOfUnitsVersion
              },
              {
                "sequenceNo": this.cropVarianceSequence,
                "description": this.cropVarianceDescription,
                "value": this.cropVariance,
                "version": this.cropVarianceVersion
              },
              {
                "sequenceNo": this.weatherStationSequence,
                "description": this.weatherDescription,
                "value": this.weatherStation + '',
                "version": this.weatherStationVersion
              },
              {
                "sequenceNo": this.farmerNameSequnce,
                "description": this.farmerNameDescription,
                "value": this.farmerNameValue,
                "version": this.farmerNameVersion
              }
              ];
            } else if (this.productType === 'CRPTY') {
              jsonObj = [
                {
                  'sequenceNo': this.cropVarianceSequence,
                  'description': this.cropVarianceDescription,
                  'value': this.indemnityCropVariance.value,
                  'version': this.cropVarianceVersion
                },
                {
                  "sequenceNo": this.farmerNameSequnce,
                  "description": this.farmerNameDescription,
                  "value": this.indemnityFarmerName.value,
                  "version": this.farmerNameVersion
                },
                cropTypeSequence,
                cropSequence
              ]
            }
            this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
              const body_4 = resp.body as any;
              // set msg display
              this.viewAddLand = true;
              this.viewAddCultivation = false;
              this.viewAddLandPhotos = false;
              this.disabledLocation = true;
              this.disabledCultivationFeilds = true;
              this.indemnityCropVariance.disable();
              this.indemnityCultivationCrop.disable();
              this.indemnityCultivationCropType.disable();
              this.displayLivestockRiskDetailsDiv = false;
              this.displayWeatherRiskDetailsDiv = false;
              this.displaySuccessMessage(body_4);
              this.isDataLoaded = false;
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);
          });

        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  // get livestock data
  getLivestockDetails() {
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      //risk
      this.riskOrder = body.data.riskOrder;
      this.tagNumber = body.data.riskName;
      this.riskRefNo = body.data.riskRefNo;
      this.riskSectionCode = body.data.riskSectionCode;
      this.riskSumInsured = body.data.riskSumInsured;
      this.locationSequence = body.data.location.seqNo;
      //get risk commont information
      this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
        const body_3 = resp.body as any;
        let resultArr = body_3.data;
        for (let i = 0; i < resultArr.length; i++) {
          let obj = resultArr[i];
          if (obj['description'] === 'LIVESTOCK TYPE') {
            //crop variance
            this.liveStockType = obj['value'];
          }
          if (obj['description'] === 'BREED') {
            this.breedType = obj['value'];
          }
          if (obj['description'] === 'GENDER') {
            this.gender = obj['value'];
          }
          if (obj['description'] === 'USAGE') {
            this.usageType = obj['value'];
          }
          if (obj['description'] === 'AGE IN YEARS') {
            this.age = parseInt(obj['value']) + '';
          }
          if (obj['description'] === 'OWNER NAME') {
            this.ownerName = obj['value'];
          }
          if (obj['description'] === 'PURCHASE DATE') {
            if (obj['value'] === null) {
              this.livestockPurchaseDate = null;
            } else {
              this.livestockPurchaseDate = new Date(obj['value']);
            }
          }
        }
        // get asssessors and date
        this.service.getAssessors(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_4 = resp.body as any;
          this.undewritingAssessor = body_4.data[0]['assessorCode'];
          // get assessor sequnce and get inspectd date
          this.service.getAssessorDetails(body_4.data[0]['sequence']).subscribe(resp => {
            const body_5 = resp.body as any;
            this.newLivestockStartDate = new Date(body_5.data.datetime);
          });
        }, error => {
        });
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
    });
  }

  updateLivestockRisk() {
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      let obj = {
        'annualLimit': body.data.annualLimit,
        'annualNarration': body.data.annualNarration,
        'annualPercentage': body.data.annualPercentage,
        'cancelledAmount': body.data.cancelledAmount,
        'certificateNo': body.data.certificateNo,
        'coverNoteNo': body.data.coverNoteNo,
        'deletedDate': body.data.deletedDate,
        'deletedEndorsementNo': body.data.deletedEndorsementNo,
        'effectEndorsementNo': body.data.effectEndorsementNo,
        'effectiveDate': moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
        'effectiveToDate': moment(this.liveStockEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        'eventLimit': body.data.eventLimit,
        'eventNarration': body.data.eventNarration,
        'eventPercentage': body.data.eventPercentage,
        'geometry': body.data.geometry,
        'mainRiskRelationship': body.data.mainRiskRelationship,
        'premium': body.data.premium,
        'relatedMainRiskCode': body.data.relatedMainRiskCode,
        'riskDescription': body.data.riskDescription,
        'riskName': this.tagNumber,
        'riskOrder': this.riskOrder,
        'riskPolicyNo': body.data.riskPolicyNo,
        'riskRefNo': body.data.riskRefNo,
        'riskSectionCode': body.data.riskSectionCode,
        'riskStatus': body.data.riskStatus,
        'riskSumInsured': this.riskSumInsured,
        'seqNo': this.riskSequnce,
        'totalCancelledAmount': body.data.totalCancelledAmount,
        'totalPremium': body.data.totalPremium,
        'totalTransactionAmount': body.data.totalTransactionAmount,
        'transactionAmount': body.data.transactionAmount,
        'size1': body.data.size1,
        'size2': body.data.size2,
        'size1UnitType': body.data.size1UnitType,
        'size2UnitType': body.data.size2UnitType,
        'version': body.data.version
      };
      this.service.updateRisk(this.riskSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_3 = resp.body as any;
          let resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            let obj = resultArr[i];
            if (obj['description'] === 'LIVESTOCK TYPE') {
              //live stock type
              this.liveStockTypeSequnce = obj['sequenceNo'];
              this.liveStockTypeDescription = obj['description'];
              this.liveStockTypeVersion = obj['version'];
              this.liveStockTypeValue = this.liveStockType;
            } else if (obj['description'] === 'BREED') {
              //breed
              this.breedSequence = obj['sequenceNo'];
              this.breedDescription = obj['description'];
              this.breedVersion = obj['version'];
              this.breedValue = this.breedType;
            } else if (obj['description'] === 'GENDER') {
              //gender
              this.genderSequence = obj['sequenceNo'];
              this.genderDescription = obj['description'];
              this.genderVersion = obj['version'];
              this.genderValue = this.gender;
            } else if (obj['description'] === 'USAGE') {
              //usage type
              this.usageSequence = obj['sequenceNo'];
              this.usageDescription = obj['description'];
              this.usageVersion = obj['version'];
              this.usageValue = this.usageType;
            } else if (obj['description'] === 'AGE IN YEARS') {
              //age in years
              this.ageInYearsSequence = obj['sequenceNo'];
              this.ageInYearsDescription = obj['description'];
              this.ageInYearsVersion = obj['version'];
              this.ageInYearsValue = this.age;
            } else if (obj['description'] === 'OWNER NAME') {
              //owner name
              this.ownerNameSequence = obj['sequenceNo'];
              this.ownerNameDescription = obj['description'];
              this.ownerNameVersion = obj['version'];
              this.ownerNameValue = this.ownerName;
            } else if (obj['description'] === 'PURCHASE DATE') {
              //purchase date
              this.purchaseDateSequence = obj['sequenceNo'];
              this.purchaseDateDescription = obj['description'];
              this.purchaseDateVersion = obj['version'];
              this.purchaseDateValue = moment(this.livestockPurchaseDate).format('YYYY-MM-DD HH:mm:ss');
            }
          }
          //update common references for risk
          let jsonObj = [{
            'sequenceNo': this.liveStockTypeSequnce,
            'description': this.liveStockTypeDescription,
            'value': this.liveStockTypeValue,
            'version': this.liveStockTypeVersion
          },
          {
            'sequenceNo': this.breedSequence,
            'description': this.breedDescription,
            'value': this.breedValue,
            'version': this.breedVersion
          },
          {
            'sequenceNo': this.genderSequence,
            'description': this.genderDescription,
            'value': this.genderValue,
            'version': this.genderVersion
          },
          {
            'sequenceNo': this.usageSequence,
            'description': this.usageDescription,
            'value': this.usageValue,
            'version': this.usageVersion
          },
          {
            'sequenceNo': this.ageInYearsSequence,
            'description': this.ageInYearsDescription,
            'value': this.ageInYearsValue,
            'version': this.ageInYearsVersion
          },
          {
            'sequenceNo': this.ownerNameSequence,
            'description': this.ownerNameDescription,
            'value': this.ownerNameValue,
            'version': this.ownerNameVersion
          },
          {
            'sequenceNo': this.purchaseDateSequence,
            'description': this.purchaseDateDescription,
            'value': this.purchaseDateValue,
            'version': this.purchaseDateVersion
          }
          ];
          this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
            const body_4 = resp.body as any;
            // get asssessors and date
            this.service.getAssessors(this.riskSequnce, this.authorizedDate).subscribe(resp => {
              const body_5 = resp.body as any;
              let objAssessor = {
                'sequence': body_5.data[0]['sequence'],
                'assessorCode': this.undewritingAssessor,
                'assessorName': body_5.data[0]['assessorName'],
                'assessorType': body_5.data[0]['assessorType'],
                'remarks': body_5.data[0]['remarks'],
                'version': body_5.data[0]['version'],
                'datetime': moment(this.newLivestockStartDate).format('YYYY-MM-DD HH:mm:ss')
              };
              this.service.updateInspectedDetails(body_5.data[0]['sequence'], objAssessor).subscribe(resp => {
                const body_6 = resp.body as any;
                this.displaySuccessMessage(body_5);
              }, error => {
                this.displayErrorMessage(error);
              });
            }, error => {
              this.displayErrorMessage(error);
            });
          }, error => {
            this.displayErrorMessage(error);
          });
        }, error => {
          this.displayErrorMessage(error);
        });
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  setDayCountWeather() {

    if (this.fromYear !== '' && this.fromMonth !== '' && this.toYear !== '' && this.toMonth !== '') {
      //  set dates
      // tslint:disable-next-line:radix
      const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
      // @ts-ignore
      const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);
      // tslint:disable-next-line:radix
      const selectedToMonth = parseInt(moment().month(this.toMonth).format('M'));
      let selectedEndDate = moment([this.toYear, selectedToMonth - 1]);
      selectedEndDate = moment(selectedEndDate).endOf('month');
      const start = moment(selectedStartDate);
      const end = moment(selectedEndDate);
      // tslint:disable-next-line:radix
      this.weatherDayCount = parseInt(String(end.diff(start, 'day')));
    }
  }


  changeFromYearDropdown() {
    this.toMonth = '';
    this.toYear = '';
    this.fromMonth = '';
    this.setDayCountWeather();
    const currentYear = moment().get('year');
    // @ts-ignore
    if (parseInt(this.fromYear) === currentYear) {
      const currentMonth = moment().get('M');
      const testArr = [];
      for (let i = currentMonth; i < this.months.length; i++) {
        testArr.push(this.months[i]);
      }
      this.fromMonths = [];
      this.fromMonths = testArr;
    } else {
      this.fromMonths = this.months;
    }
  }

  changeFromMonthDropdown() {
    this.toMonth = '';
    this.toYear = '';
    this.setDayCountWeather();

    const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
    // @ts-ignore
    const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);

    /**
     * adding weather policy max day count
     */
    this.service.getWeatherProductMaxDayCount().subscribe(resp => {
      const body = resp.body as any;
      const maxDayCount = parseInt(body.data.maxNoOfPolicyDays);

      /**
       * get max day count and set to year and to month
       */
      const futureDate = moment(selectedStartDate).add(maxDayCount, 'days');
      const futureYear = moment(futureDate).get('year');
      const futureMonth = moment(futureDate).get('M');
      this.toYears = [];
      for (let i = parseInt(this.fromYear); i <= futureYear; i++) {
        this.toYears.push(String(i));
      }
      /**
       *  - if same year - have to get betwenn months
       *  - if other year
       */

      if (parseInt(this.fromYear) === futureYear) {
        const fromMonth = moment(selectedStartDate).get('M');
        this.toMonths = [];
        for (let i = fromMonth; i <= futureMonth; i++) {
          this.toMonths.push(this.months[i]);
        }
      } else {
        this.toMonths = [];
        for (let i = 0; i <= futureMonth; i++) {
          this.toMonths.push(this.months[i]);
        }
      }


    }, error => {
      this.displayErrorMessage(error);
    });

  }

  changeToYearDropdown() {
    this.toMonth = '';
    this.setDayCountWeather();

    const selectedFromMonth = parseInt(moment().month(this.fromMonth).format('M'));
    // @ts-ignore
    const selectedStartDate = moment([this.fromYear, selectedFromMonth - 1]);

    /**
     * adding weather policy max day count
     */
    this.service.getWeatherProductMaxDayCount().subscribe(resp => {
      const body = resp.body as any;
      const maxDayCount = parseInt(body.data.maxNoOfPolicyDays);

      /**
       * get max day count and set to year and to month
       */
      const futureDate = moment(selectedStartDate).add(maxDayCount, 'days');
      const futureYear = moment(futureDate).get('year');
      const futureMonth = moment(futureDate).get('M');

      /**
       *  - if same year - have to get betwenn months
       *  - if other year
       */
      if (parseInt(this.fromYear) === futureYear) {
        const fromMonth = moment(selectedStartDate).get('M');
        this.toMonths = [];
        for (let i = fromMonth; i <= futureMonth; i++) {
          this.toMonths.push(this.months[i]);
        }
      } else {
        if (parseInt(this.toYear) < futureYear) {
          this.toMonths = [];
          const fromMonth = moment(selectedStartDate).get('M');
          for (let i = fromMonth; i < this.months.length; i++) {
            this.toMonths.push(this.months[i]);
          }
        } else {
          this.toMonths = [];
          for (let i = 0; i <= futureMonth; i++) {
            this.toMonths.push(this.months[i]);
          }
        }

      }


    }, error => {
      this.displayErrorMessage(error);
    });

  }

  setDayCountLivestock() {
    let start = moment(this.liveStockStartDate);
    let end = moment(this.liveStockEndDate);
    this.livestockDayCount = end.diff(start, 'day');
  }

  setDayCountIndemnity() {
    let start = moment(this.indemnityStartDate.value);
    let end = moment(this.indemnityEndDate.value);
    this.indemnityDayCount.setValue(end.diff(start, 'day'));
  }

  // update covers
  updateRiskCovers() {
    this.isDataLoaded = true;
    this.service.getCoverDetails(this.perilSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      // update
      let obj = {
        'seqNo': this.perilSequnce,
        'perilCode': body.data.perilCode,
        'perilPerilSumInsured': this.perilPerilSumInsured,
        'perilPercentage': body.data.perilPercentage,
        'perilReinsSumInsured': body.data.perilReinsSumInsured,
        'perilPremium': body.data.perilPremium,
        'perilTotalPremium': body.data.perilTotalPremium,
        'perilTransactionAmount': body.data.perilTransactionAmount,
        'perilotalTransactionAmount': body.data.perilotalTransactionAmount,
        'perilCancelledAmount': body.data.perilCancelledAmount,
        'perilTotalCancelledAmount': body.data.perilTotalCancelledAmount,
        'perilrEffectEndorsementNo': body.data.perilrEffectEndorsementNo,
        'perilEffectiveDate': body.data.perilEffectiveDate,
        'perilPerilStatus': body.data.perilPerilStatus,
        'perilDeletedDate': body.data.perilDeletedDate,
        'perilDeletedEndorsementNo': body.data.perilDeletedEndorsementNo,
        'perilDefaultPercentage': body.data.perilDefaultPercentage,
        'perilDefaultRate': body.data.perilDefaultRate,
        'perilRate': body.data.perilRate,
        'perilEventLimit': body.data.perilEventLimit,
        'perilEventPercentage': body.data.perilEventPercentage,
        'perilEventNarration': body.data.perilEventNarration,
        'perilAnnualLimit': body.data.perilAnnualLimit,
        'perilAnnualPercentage': body.data.perilAnnualPercentage,
        'perilAnnualNarration': body.data.perilAnnualNarration,
        'version': body.data.version,
        'perilReinClassCode': body.data.perilReinClassCode,
        'productPerilSeqNo': body.data.productPerilSeqNo,
        'perilName': body.data.perilName
      };
      this.service.updateCover(this.perilSequnce, obj).subscribe(resp => {
        const body1 = resp.body as any;
        // excess
        this.service.getExcess(this.perilSequnce, this.authorizedDate).subscribe(resp => {
          const body2 = resp.body as any;
          if (body2.data === null) {
            // excess insert
            let excess = {
              'excessAmount': this.excessAmount,
              'excessPercentage': this.excessPercentage
            };
            this.service.saveExcess(this.perilSequnce, excess).subscribe(resp => {
              let body1 = resp.body as any;
              this.isDataLoaded = false;
              this.displaySuccessMessage(body1);
              if (this.productType === 'CRPTY') {
                this.displayManageLandForm = true;
                this.displayLivestockAdditionalDetails = false;
                this.displayManageLivestockForm = false;
              } else {
                this.displayLivestockAdditionalDetails = true;
                this.displayManageLivestockForm = true;
              }
              this.displayManageCoversForm = false;
              this.getRiskAdditionalDetails();
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          } else {
            // make object
            let obj = {
              'sequenceNo': body2.data.sequenceNo,
              'excessLevel': body2.data.excessLevel,
              'excessAmount': this.excessAmount,
              'excessPercentage': this.excessPercentage,
              'excessNarration': body2.data.excessNarration,
              'status': body2.data.status,
              'version': body2.data.version
            };
            this.service.updateExcess(body2.data.sequenceNo, obj).subscribe(resp => {
              const body_3 = resp.body as any;
              this.displaySuccessMessage(body);
              if (this.productType === 'CRPTY') {
                this.displayManageLandForm = true;
                this.displayLivestockAdditionalDetails = false;
                this.displayManageLivestockForm = false;
              } else {
                this.displayLivestockAdditionalDetails = true;
                this.displayManageLivestockForm = true;
              }
              this.displayManageCoversForm = false;
              this.displayLivestockRiskDetailsDiv = false;
              this.displayWeatherRiskDetailsDiv = false;
              this.isDataLoaded = false;
              this.getRiskAdditionalDetails();
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  // complete policy
  completePolicy() {
    let authArray = [];
    this.service.getCompletePolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
      let body = resp.body as any;
      let version = body.data.version;
      // update completed
      let obj = {
        'completed': true,
        'version': version
      };
      this.service.updateCompletePolicy(this.policySequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        // authorize policy
        this.service.getAuthorizedPolicy(this.policySequnce, this.authorizedDate).subscribe(resp => {
          const body = resp.body as any;
          for (let i = 0; i < body.data.length; i++) {
            let obj = {
              'sequenceNo': body.data[i]['sequenceNo'],
              'levelCode': body.data[i]['levelCode'],
              'authorized': true,
              'version': body.data[i]['version']
            };
            authArray.push(obj);
          }
          this.service.updateAuthorizedPolicy(this.policySequnce, authArray).subscribe(resp => {
            const body = resp.body as any;
            this.authorizedPolicy = true;
            this.debitNoteNumber = body.data;
            this.displaySuccessMessage(body);
            if ((this.paymentType === 'UPFRN' || this.premiumSettlementMethod === 'UPFRN') && parseInt(this.totalPremium) !== 0) {
              // redirect to payment
              this.displayPayment = true;
            } else {
              // download pdf and redirect to policy list
              if (this.productType === 'WHINX') {
                // weather Individual file
                const fileName1 = 'WeatherIndexSchedule.pdf';
                this.service.downloadWeatherIndividualFile(this.policySequnce).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], { type: 'application/pdf' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName1;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
                // weather Group file
                const fileName2 = 'WeatherIndexGroupSchedule.pdf';
                this.service.downloadWeatherGroupFile(this.policySequnce).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], { type: 'application/pdf' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName2;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
              } else if (this.productType === 'LIVST') {
                // Livestock individual file
                const fileName1 = 'LivestockSchedule.pdf';
                this.service.downloadLivestockIndividualFile(this.policySequnce).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], { type: 'application/pdf' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName1;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
                // Livestock Group file
                const fileName2 = 'LivestockGroupSchedule.pdf';
                this.service.downloadLivestockGroupFile(this.policySequnce).subscribe(x => {
                  if (x.size === 0) {
                    return;
                  }
                  const newBlob = new Blob([x], { type: 'application/pdf' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                  }
                  const data = window.URL.createObjectURL(newBlob);
                  const link = document.createElement('a');
                  link.href = data;
                  link.download = fileName2;
                  // this is necessary as link.click() does not work on the latest firefox
                  link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
                  setTimeout(function () {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    link.remove();
                  }, 100);
                });
              }
              ////////////////////////////////////////////////
              this.router.navigate(['/policies']);
            }
          }, error => {
            this.authorizedPolicy = false;
            this.displayErrorMessage(error);
          });
        }, error => {
          this.authorizedPolicy = false;
          this.displayErrorMessage(error);
        });
        // set disable  steps
        // enable payment link
      }, error => {
        this.authorizedPolicy = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.authorizedPolicy = false;
      this.displayErrorMessage(error);
    });
  }

  CancelLivestockAdditionalDetails() {
    this.displayLivestockAdditionalDetails = false;
    this.displayManageCoversForm = false;
    this.displayManageLivestockForm = true;
    this.displayPolicyDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayWeatherRiskDetailsDiv = false;
  }

  uploadImg(file: File, index: number) {
    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.imagList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imagList[index].imageKey = body.data.object.key;
        this.imagList[index].value = body.data.object.url;
        this.imagList[index].file = null;
      }
    });
  }

  addImg(event) {
    for (let i = 0; i < event.target.files.length; i++) {
      const file: File = event.target.files[0];
      if (((file.size / 1048576) < 5)) {
        if (file.type === 'image/jpeg' || file.type === 'image/png'
          || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          || file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || file.type === 'application/pdf') {
          const img: ImageModel = new ImageModel();
          img.file = event.target.files[i];
          img.fileType = file.type;
          this.imagList.push(img);
          this.uploadImg(img.file, this.imagList.indexOf(img));
        } else {
          this.notificationService.error('Incorrect file Type');
        }
      } else {
        this.notificationService.error('Image size not match. Image size must be smaller than 5MB');
      }


    }
  }

  openPDF(url) {
    // ne
    const decodedUrl = decodeURIComponent(url);
    let w = window.open(decodedUrl);
    w.focus();
  }

  removeImg(index) {
    this.imagList.splice(index, 1);
  }

  viewRisk(sequence, productCode) {
    this.isDataLoaded = true;
    this.clearCommonPlaces();
    this.riskSequnce = sequence;
    this.displayWeatherRiskDetailsDiv = true;
    this.displayPolicyDetails = false;
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      this.service.getLocations(this.customerCode).subscribe(resp => {
        this.locations = [];
        const body1 = resp.body as any;
        this.locations = body1.data;
        top: for (let i = 0; i < this.locations.length; i++) {
          if (this.locations[i]['address'] === body.data.location.location) {
            this.location = this.locations[i];
            this.getSelectedLocation(this.location);
            break top;
          }
        }
        //risk
        this.riskOrder = body.data.riskOrder;
        this.riskName = body.data.riskName;
        this.farmerName = body.data.riskName;
        this.riskRefNo = body.data.riskRefNo;
        this.riskSectionCode = body.data.riskSectionCode;
        this.riskSumInsured = body.data.riskSumInsured;
        this.effectiveDate = moment(body.data.effectiveDate, 'YYYY-MM-DD HH:mm:ss').format(' DD MMMM YYYY');
        this.effectiveToDate = moment(body.data.effectiveToDate, 'YYYY-MM-DD HH:mm:ss').format(' DD MMMM YYYY');
        if (body.data.size1 === null) {
          this.size1 = '';
        } else {
          this.size1 = body.data.size1;
        }
        if (body.data.size2 === null) {
          this.size2 = '';
        } else {
          this.size2 = body.data.size2;
        }
        // this.size1UnitType = body.data.size1UnitType;
        this.size2 = body.data.size2;
        // this.size2UnitType = body.data.size2UnitType;
        this.weatherLandStartDate = new Date(body.data.effectiveDate);
        this.weatherLandPremium = body.data.totalPremium;
        //get risk commont information
        this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_3 = resp.body as any;
          let resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            let obj = resultArr[i];
            if (obj['description'] === 'CROP VARIANCE') {
              //crop variance
              this.cropVariance = obj['value'];
              this.cropVarianceValueDecription = obj['valueDescription'];
            } else if (obj['description'] === 'WEATHER STATION') {
              // tslint:disable-next-line:radix
              this.weatherStation = parseInt(obj['value']);
              this.weatherStationValueDescription = obj['valueDescription'];
            } else if (obj['description'] === 'FARMER NAME') {
              this.farmerName = obj['value'];
            } else if (obj['description'] === 'NO OF UNITS') {
              this.noOfUnits = parseInt(obj['value']) + '';
            } else if (obj['description'] === 'CROP') {
              this.cropType = obj['value'];
              this.cropTypeValueDecription = obj['valueDescription'];
            } else if (obj['description'] === 'CROP CATEGORY') {
              this.cropCategoryValueDecription = obj['valueDescription'];
            }
          }
          // get units and price
          this.service.getUnitPriceOnRisk(this.weatherStation).subscribe(resp => {
            const body_4 = resp.body as any;
            // this.size2 = body_4.data.unitPrice;
            this.price = body_4.data ? body_4.data.unitPrice : 0;
            this.riskImages = [];
            this.covers = [];
            let documentSequnce = '';
            // get risk images
            this.service.getRiskDocuments(this.riskSequnce, this.authorizedDate).subscribe(resp => {
              // tslint:disable-next-line:no-shadowed-variable
              const body = resp.body as any;
              documentSequnce = body.data[0]['sequenceNo'];
              this.service.downloadImages(documentSequnce, this.authorizedDate).subscribe(resp => {
                // tslint:disable-next-line:no-shadowed-variable
                const body1 = resp.body as any;
                this.weatherLandPhotos = body1.data.objectList;
                for (let i = 0; i < this.weatherLandPhotos.length; i++) {
                  let im = new ImageModel();
                  im.imageKey = this.weatherLandPhotos[i]['key'];
                  im.value = this.weatherLandPhotos[i]['url'];
                  this.imagList.push(im);
                }
                this.isDataLoaded = false;
                this.displayWeatherRiskDetailsDiv = true;
                this.displayPolicyDetails = false;

                this.getWeatherIndexParameters();
                this.displayWeatherIndexParametersDiv = true;
              }, error => {
                this.isDataLoaded = false;
                this.displayErrorMessage(error);
              });
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);
            });
          }, error => {
            this.isDataLoaded = false;
          });
        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);
      });
      this.locations = [];
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }

  viewLivestockRisk(sequence, productCode) {
    this.isDataLoaded = true;
    this.clearCommonPlaces();
    this.riskSequnce = sequence;
    this.displayLivestockRiskDetailsDiv = true;
    this.displayPolicyDetails = false;
    this.service.getRiskDetails(this.riskSequnce, this.authorizedDate).subscribe(resp => {
      const body = resp.body as any;
      //risk
      this.riskOrder = body.data.riskOrder;
      this.tagNumber = body.data.riskName;
      this.riskRefNo = body.data.riskRefNo;
      this.riskSectionCode = body.data.riskSectionCode;
      this.riskSumInsured = body.data.riskSumInsured;
      this.locationSequence = body.data.location.seqNo;
      // this.liveStockEndDate = new Date(body.data.effectiveDate);

      //get risk commont information
      this.service.getRiskCommonInformation(this.riskSequnce, this.authorizedDate).subscribe(resp => {
        const body_3 = resp.body as any;
        let resultArr = body_3.data;
        for (let i = 0; i < resultArr.length; i++) {
          let obj = resultArr[i];
          if (obj['description'] === 'LIVESTOCK TYPE') {
            //crop variance
            this.liveStockType = obj['valueDescription'];

          }
          if (obj['description'] === 'BREED') {
            this.breedType = obj['valueDescription'];

          }
          if (obj['description'] === 'GENDER') {
            this.gender = obj['valueDescription'];

          }
          if (obj['description'] === 'USAGE') {
            this.usageType = obj['valueDescription'];
          }
          if (obj['description'] === 'AGE IN YEARS') {
            this.age = parseInt(obj['value']) + '';

          }
          if (obj['description'] === 'OWNER NAME') {
            this.ownerName = obj['value'];
          }
          if (obj['description'] === 'PURCHASE DATE') {
            if (obj['value'] === null) {
              this.livestockPurchaseDate = null;
            } else {
              this.livestockPurchaseDate = moment(obj['value'], 'YYYY-MM-DD HH:mm:ss').format(' DD MMMM YYYY');
            }
          }
        }

        // get asssessors and date
        this.service.getAssessors(this.riskSequnce, this.authorizedDate).subscribe(resp => {
          const body_4 = resp.body as any;
          this.undewritingAssessor = body_4.data[0]['assessorName'];
          // get assessor sequnce and get inspectd date
          this.service.getAssessorDetails(body_4.data[0]['sequence']).subscribe(resp => {
            const body_5 = resp.body as any;
            this.newLivestockStartDate = moment(body_5.data.datetime, 'YYYY-MM-DD HH:mm:ss').format(' DD MMMM YYYY');

            this.riskImages = [];
            this.covers = [];
            let documentSequnce = '';
            // get risk images
            this.service.getRiskDocuments(this.riskSequnce, this.authorizedDate).subscribe(resp => {
              // tslint:disable-next-line:no-shadowed-variable
              const body = resp.body as any;
              documentSequnce = body.data[0]['sequenceNo'];

              this.service.downloadImages(documentSequnce, this.authorizedDate).subscribe(resp => {
                // tslint:disable-next-line:no-shadowed-variable
                const body1 = resp.body as any;
                this.riskImages = body1.data.objectList;

                for (let i = 0; i < this.riskImages.length; i++) {
                  let im = new ImageModel();
                  im.imageKey = this.riskImages[i]['key'];
                  im.value = this.riskImages[i]['url'];
                  this.imagList.push(im);
                }
                this.service.getRiskPerils(this.riskSequnce, this.authorizedDate).subscribe(resp => {
                  // tslint:disable-next-line:no-shadowed-variable
                  const body1 = resp.body as any;
                  this.covers = body1.data;
                  this.isDataLoaded = false;
                }, error => {
                  this.isDataLoaded = false;
                });
              }, error => {
                this.isDataLoaded = false;
              });
            }, error => {
              this.isDataLoaded = false;
            });
          });

        }, error => {
          this.isDataLoaded = false;
        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);

      });
    }, error => {
      this.isDataLoaded = false;

    });
  }

  clearCommonPlaces() {
    this.imagList = [];
  }

  searchByFeild() {
    // tslint:disable-next-line:triple-equals
    if (this.meCodeCheck == true) {

      let searchFeild = '';
      let selectedValue = this.selectedValue;
      if (selectedValue === 'customerName') {
        searchFeild = 'CN';
      } else if (selectedValue === 'riskName') {
        searchFeild = 'RN';
      } else if (selectedValue === 'policyNumber') {
        searchFeild = 'PN';
      } else if (selectedValue === 'proposalNumber') {
        searchFeild = 'PRN';
      }
      this.getSearchedPolicies(searchFeild);

    } else {
      let searchFeild = '';
      let selectedValue = this.selectedValue;
      if (selectedValue === 'customerName') {
        searchFeild = 'CN';
      } else if (selectedValue === 'riskName') {
        searchFeild = 'RN';
      } else if (selectedValue === 'policyNumber') {
        searchFeild = 'PN';
      } else if (selectedValue === 'proposalNumber') {
        searchFeild = 'PRN';
      }
      this.productCode = '';
      this.getSearchedPolicies(searchFeild);
    }
  }

  calculatePremium() {
    if (this.noOfUnits === null || this.price === null) {
      this.weatherLandPremium = 0;
    } else {
      this.weatherLandPremium = parseFloat(this.noOfUnits) * parseFloat(this.price);
    }

  }

  // set weather policy end date
  setEndDate() {
    this.endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());
  }

  setLivestockPolicyEndDate() {
    this.livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());
  }

  getSelectedLocation(data) {
    if (data === 'add-new') {
      this.getDistricts();
      this.getagrarianServiceCenters();
      this.getOwnershipTypes();
      this.viewAddLand = false;
      this.viewAddCultivation = true;
      this.viewAddLandPhotos = true;
      this.sendLandData.geometry = [];
      this.markLandPlotString = '';
    } else if (data !== 'select-land') {
      this.size2 = this.location['sizeInAcres'];
      this.size1 = this.location['sizeInPerches'];
      this.markLandPlot = this.location['geometry'];
      this.pathList = this.location['geometry'];

      if (this.isIndemnityAddLandForm) {
        this.indemnityAcres.setValue(this.location['sizeInAcres']);
        this.indemnityPerches.setValue(this.location['sizeInPerches']);
        let plotString = '';
        for (let i = 0; i < this.pathList.length; i++) {
          plotString = plotString + this.pathList[i]['latitude'] + ':' + this.pathList[i]['longitude'] + ',';
        };
        this.indemnityPlot.setValue(plotString);
      }
      let markLandPlotArr = [];
      this.viewAddLand = true;
      this.viewAddCultivation = false;
      this.viewAddLandPhotos = true;

      this.sendLandData.districtDescription = this.location['districtDescription'];
      this.sendLandData.divisionDescription = this.location['divisionDescription'];
      this.sendLandData.gramasevaDivisionDescription = this.location['agrarianServiceCenterDescription'];
      this.sendLandData.geometry = this.location['geometry'];
      //
      this.weatherStation = 84;
      this.markLandPlotString = '';
      for (let i = 0; i < this.pathList.length; i++) {
        this.markLandPlotString = this.markLandPlotString + this.pathList[i]['latitude'] + ':' + this.pathList[i]['longitude'] + ',';
      }
      ;

      this.getWeatherStations();

      // get nearest weather station
      this.service.getNearestWeatherStation(this.location['geometry']).subscribe(resp => {
        const body = resp.body as any;
        this.weatherStation = parseInt(body.data.sequence);
        // get units and price
        this.service.getUnitPriceOnRisk(this.weatherStation).subscribe(resp => {
          const body_4 = resp.body as any;
          this.price = body_4.data.unitPrice;
        }, error => {

        });
      }, error => {

      });

    }
  }

  generatePdf() {
    // download pdf and redirect to policy list
    if (this.productType === 'WHINX') {

      // weather Individual file
      const fileName1 = 'WeatherIndexSchedule.pdf';
      this.service.downloadWeatherIndividualFile(this.policySequnce).subscribe(x => {
        if (x.size === 0) {
          return;
        }
        const newBlob = new Blob([x], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = fileName1;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });

      // weather Group file
      const fileName2 = 'WeatherIndexGroupSchedule.pdf';
      this.service.downloadWeatherGroupFile(this.policySequnce).subscribe(x => {
        if (x.size === 0) {
          return;
        }
        const newBlob = new Blob([x], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = fileName2;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });
    } else if (this.productType === 'LIVST') {
      // Livestock individual file
      const fileName1 = 'LivestockSchedule.pdf';
      this.service.downloadLivestockIndividualFile(this.policySequnce).subscribe(x => {
        if (x.size === 0) {
          return;
        }
        const newBlob = new Blob([x], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = fileName1;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });

      // Livestock Group file
      const fileName2 = 'LivestockGroupSchedule.pdf';
      this.service.downloadLivestockGroupFile(this.policySequnce).subscribe(x => {
        if (x.size === 0) {
          return;
        }
        const newBlob = new Blob([x], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = fileName2;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });

    }
  }


  getDistricts() {
    this.landService.getDistricts().subscribe(resp => {
      const body = resp.body as any;
      this.districtsList = body.data;
    });
  }

  getDivisionns() {
    this.landService.getDivisions(this.selectedDistrict.code).subscribe(resp => {
      const body = resp.body as any;
      this.divisionsList = body.data;
    });
  }

  getGramasevaDivisions() {
    this.landService.getGramasevaDivisions(this.selectedDivision.code).subscribe(resp => {
      const body = resp.body as any;
      this.gramasevaDivisionsList = body.data;
    });
  }

  getagrarianServiceCenters() {
    this.landService.getagrarianServiceCenters().subscribe(resp => {
      const body = resp.body as any;
      this.getagrarianServiceCentersList = body.data;
    });
  }

  getOwnershipTypes() {
    this.landService.getOwnershipTypes().subscribe(resp => {
      const body = resp.body as any;
      this.ownershipTypesList = body.data;
    });
  }

  getMarkers(event) {
    if (event.length === 0) {
      this.isDrawMap = false;
    } else {
      this.pathList = event;
      this.pathList.push(this.pathList[0]);
      this.pathList.forEach(item => {
        this.pathString = this.pathString + item.latitude + ':' + item.longitude + ',';
      });
      this.landForm.controls['geometry'].setValue(this.pathString);
      this.isDrawMap = false;
    }

  }

  saveLand() {
    const land: LandModel = this.landForm.value;
    land.geometry = this.pathList;
    land.district = this.selectedDistrict.code;
    if (this.asAgent === 'Y') {
      land.division = this.selectedDivision.code;
      land.gramasevaDivision = this.selectedGramasevaDivision.code;
      land.partyCode = this.route.snapshot.paramMap.get('customerCode');
      this.routeString = '/customers';
    } else {
      land.division = this.selectedDivision.code;
      land.gramasevaDivision = this.selectedGramasevaDivision.code;
      land.partyCode = sessionStorage.getItem('partyCode');
      this.routeString = '/land';
    }
    this.landService.postLand(land).subscribe(resp => {
      const body1 = resp.body as any;
      this.service.getLocations(this.customerCode).subscribe(resp => {
        const body = resp.body as any;
        this.locations = body.data;
        this.service.getLocationFromUtility(body1.data).subscribe(resp => {
          const body2 = resp.body as any;
          // this.location = body2.data;
          top: for (let i = 0; i < this.locations.length; i++) {
            if (this.locations[i]['sequence'] === body2.data.sequence) {
              this.location = this.locations[i];
              break top;
            }
          }
          this.getSelectedLocation(this.location);
          this.viewAddLand = true;
          this.viewAddCultivation = false;
          this.viewAddLandPhotos = true;
          this.displaySuccessMessage(body1);
        }, error => {

        });
      }, error => {

      });
    }, error => {
      this.viewAddLand = false;
      this.viewAddCultivation = true;
      this.viewAddLandPhotos = true;
      this.displayErrorMessage(error);
    });
    this.isContinue = true;
  }

  getWeatherIndexParameters() {
    this.weatherIndexParameters = [];
    const selectedFromMonth = parseInt(this.fromMonth);
    const selectedToMonth = parseInt(this.toMonth);


    // tslint:disable-next-line:max-line-length
    this.service.getWeatherindexParameters(this.fromYear, selectedFromMonth, this.weatherStation, this.toYear, selectedToMonth).subscribe(resp => {
      const body = resp.body as any;
      this.weatherIndexParameters = body.data;
      for (let i = 0; i < this.weatherIndexParameters.length; i++) {
        const month = parseInt(this.weatherIndexParameters[i]['month']) - 1;
        this.weatherIndexParameters[i]['month'] = moment().month(month).format('MMMM');
      }
    }, error => {
      this.displayErrorMessage(error);
    });
  }

  gg() {
  }

  asd() {
    this.sendLandData.districtDescription = this.selectedDistrict.description;
    this.sendLandData.divisionDescription = this.selectedDivision.description;
    this.sendLandData.gramasevaDivisionDescription = this.selectedGramasevaDivision.description;
    this.isDrawMap = true;
  }

  clearWeatherPolicyForm() {
    this.cropType = '';
    this.weatherType = '';
    this.plan = '';
    this.businessChanel = '';
    this.startDate = null;
    this.endDate = null;
    this.paymentType = '';
    this.endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());
    this.cropTypeFormControl.reset();
    this.weatherTypeFormControl.reset();
    this.planFormControl.reset();
    this.businessChanelFormControl.reset();
    this.startDateFormControl.reset();
    this.endDateFormControl.reset();
    this.paymentFormControl.reset();
  }

  clearLivestockPolicyForm() {
    this.product = '';
    this.liveStockStartDate = null;
    this.liveStockEndDate = null;
    this.livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());
    this.liveStickBusinessChannel = '';
    this.premiumSettlementMethod = '';
    this.liveStockPlanFormControl.reset();
    this.liveStockStartDateFormControl.reset();
    this.liveStockEndDateFormControl.reset();
    this.liveStickBusinessChannelFormControl.reset();
    this.premiumSettlementMethodFormControl.reset();
  }


  clearLivestockForm() {

    this.tagNumber = '';
    this.liveStockType = '';
    this.breedType = '';
    this.gender = '';
    this.livestockPurchaseDate = null;
    this.ownerName = '';
    this.undewritingAssessor = '';
    this.newLivestockStartDate = null;
    this.riskSumInsured = '';
    this.age = '';
    this.usageType = '';
    this.tagNumberFormControl.reset();
    this.liveStockTypeFormControl.reset();
    this.breedTypeFormControl.reset();
    this.genderTypeFormControl.reset();
    this.ownerNameFormControl.reset();
    this.undewritingAssessorFormControl.reset();
    this.newLivestockStartDateFormControl.reset();
    this.riskSumInsuredFormControl.reset();
    this.ageTypeFormControl.reset();
    this.usageTypeFormControl.reset();
  }

  clearLandForm() {
    this.farmerName = '';
    this.cropVariance = '';
    this.weatherStation = 0;
    this.weatherLandStartDate = null;
    this.weatherLandEndDate = null;
    this.size1 = '';
    this.noOfUnits = '';
    this.price = '';
    this.markLandPlotString = '';
    this.size2 = '';
    this.weatherLandPremium = 0;
    this.farmerNameFormControl.reset();
    this.cropVarianceFormControl.reset();
    this.landWeatherStationFormControl.reset();
    this.weatherLandStartDateFormControl.reset();
    this.weatherLandEndDateFormControl.reset();
    this.size1FormControl.reset();
    this.size1UnitTypeFormControl.reset();
    this.markLandPlotString = '';
    this.noOfunitsFormControl.reset();
    this.priceFormControl.reset();
    this.landForm.reset();
  }

  clearCoverForm() {
    this.otherPeril = {};
    this.excessAmount = 0;
    this.excessPercentage = 0;
    this.perilPerilSumInsured = 0;
    this.excessAmountFormControl.reset();
    this.excessPercentageFormControl.reset();
    this.perilPerilSumInsuredFormControl.reset();
  }

  openDialog() {
    const dialogRef = this.dialog.open(PolicyPopupComponent, {
      disableClose: true,
      backdropClass: 'pop-overlay-s'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
      }
    });
  }

  /* Get outstanding */
  getPolicyOutstandings() {
    if (this.policyNumber !== null) {
      this.service.getPolicyOutstandings(this.policyNumber).subscribe(resp => {
        const body = resp.body as any;
        if (body.data.length === 0) {
          this.isDisplayPrintButton = true;
        } else {
          if ((this.paymentType !== 'UPFRN' || this.premiumSettlementMethod !== 'UPFRN') && parseInt(this.totalPremium) !== 0) {
            this.isDisplayPrintButton = true;
          } else {
            this.isDisplayPrintButton = false;
          }
        }
      }, error => {
      });
    } else {
      if ((this.paymentType !== 'UPFRN' || this.premiumSettlementMethod !== 'UPFRN') && parseInt(this.totalPremium) !== 0) {
        this.isDisplayPrintButton = false;
      }
    }
  }

  /* Indemnity Functions */
  indemnityLandStartDateChanged() {
    this.indemnityLandEndDateMin = moment(this.indemnityLandStartDate.value).add(1, 'day').toDate();
  }

  /**
   * common functions
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';

  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }

  viewImage(value: string) {
    const image: ImageModel = new ImageModel();
    image.value = value;
this.imageViewerService.openDialog(image);
  }
}
