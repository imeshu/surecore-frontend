import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyPopupComponent } from './policy-popup.component';

describe('PolicyPopupComponent', () => {
  let component: PolicyPopupComponent;
  let fixture: ComponentFixture<PolicyPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
