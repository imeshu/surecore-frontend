import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPolicyWeatherRiskComponent } from './add-policy-weather-risk.component';

describe('AddPolicyWeatherRiskComponent', () => {
  let component: AddPolicyWeatherRiskComponent;
  let fixture: ComponentFixture<AddPolicyWeatherRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPolicyWeatherRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPolicyWeatherRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
