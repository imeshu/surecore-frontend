import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyLivestockAdditionalDetailsComponent } from './policy-livestock-additional-details.component';

describe('PolicyLivestockAdditionalDetailsComponent', () => {
  let component: PolicyLivestockAdditionalDetailsComponent;
  let fixture: ComponentFixture<PolicyLivestockAdditionalDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyLivestockAdditionalDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyLivestockAdditionalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
