import {Component, Input, OnInit} from '@angular/core';
import {PolicyCommonData} from '../models/policy-common-data.model';
import {PoliciesService} from '../services/policies.service';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-policy-weather-additional-details',
  templateUrl: './policy-weather-additional-details.component.html',
  styleUrls: ['./policy-weather-additional-details.component.scss']
})
export class PolicyWeatherAdditionalDetailsComponent implements OnInit {
  @Input() policyCommonData: PolicyCommonData;
  constructor(private service: PoliciesService, public notificationService: NotificationDataTransferService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    alert('checking');
    this.service.getRiskListSelectedPolicy(this.policyCommonData.policySequence).subscribe(resp => {
  console.log(resp);
    }, error => {
      console.log(error);
    });
  }

}
