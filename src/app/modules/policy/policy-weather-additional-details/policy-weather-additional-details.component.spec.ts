import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyWeatherAdditionalDetailsComponent } from './policy-weather-additional-details.component';

describe('PolicyWeatherAdditionalDetailsComponent', () => {
  let component: PolicyWeatherAdditionalDetailsComponent;
  let fixture: ComponentFixture<PolicyWeatherAdditionalDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyWeatherAdditionalDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyWeatherAdditionalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
