import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPolicyLivestockRiskComponent } from './add-policy-livestock-risk.component';

describe('AddPolicyLivestockRiskComponent', () => {
  let component: AddPolicyLivestockRiskComponent;
  let fixture: ComponentFixture<AddPolicyLivestockRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPolicyLivestockRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPolicyLivestockRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
