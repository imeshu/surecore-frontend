import {Component, OnInit} from '@angular/core';
import {Policy} from '../models/policy.model';
import * as moment from 'moment';
import {PoliciesService} from '../services/policies.service';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import {ActivatedRoute, Router} from '@angular/router';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {CommonType} from '../../../model/CommonType';
import {PolicyCommonInformation} from '../models/policy-common-information.model';
import {Intermediary} from '../models/intermediary.model';
import {PolicyCommonData} from '../models/policy-common-data.model';

@Component({
  selector: 'app-policies-main',
  templateUrl: './policies-main.component.html',
  styleUrls: ['./policies-main.component.scss']
})
export class PoliciesMainComponent implements OnInit {
  partyCode = '';
  asCustomer = '';
  asAgent = '';
  branchCode = '';
  intermediaryType = '';
  customerCode = '';
  parentPage = '';
  addressSequnce = '';
  policySequnce = '';
  productType = 'WHINX';
  private policyCommonInformation: PolicyCommonInformation[];
  public policyCommonData: PolicyCommonData = new PolicyCommonData();
  /**
   * components displaying variables
   */
  private displayComponent = 'displayAppPolicy';
  /**
    displayAppPolicyWeatherAdditionalDetails
    displayAppPolicyLivestockAdditionalDetails
    displayAppPolicyWeatherRisk
    displayAppPolicyLivestockRisk
    displayAppPolicyLivestockCover
    displayAppPolicyLivestockRiskAdditionalDetails
    displayAppPolicy
   */
  // tslint:disable-next-line:max-line-length
  constructor(private service: PoliciesService, public notificationService: NotificationDataTransferService, public router: Router, public route: ActivatedRoute) {
  }
  ngOnInit() {
    this.getDataFromSessionStorage();
  }

  /**
   * Create Policy related functions
   */
  getDataFromSessionStorage() {
    this.partyCode = sessionStorage.getItem('partyCode');
    this.asCustomer = sessionStorage.getItem('asCustomer');
    this.asAgent = sessionStorage.getItem('asAgent');
    this.branchCode = sessionStorage.getItem('branchCode');
    this.intermediaryType = sessionStorage.getItem('intermediaryType');
    if (this.router.url.includes('/policy')) {
      this.customerCode = 'CC00000329';
      // this.customerCode = this.route.snapshot.paramMap.get('customerCode');
      //  this.parentPage = this.route.snapshot.paramMap.get('parent');
      this.service.getCustomerDetails(this.customerCode).subscribe(resp => {
        const body = resp.body as any;
        this.addressSequnce = body.data.address.sequence;
      });
      this.parentPage = 'p';
    }
  }
  savePolicyFirstStep(formData: Object) {
    const policy: Policy = new Policy();
    this.service.getClassCode(formData['plan']).subscribe(resp => {
      const body1 = resp.body as any;
      policy.classCode = body1.data.code;
      // tslint:disable-next-line:no-shadowed-variable
      this.service.getCurrencyCode().subscribe(resp => {
        const body3 = resp.body as any;
        policy.currencyCode = body3.data;
        if (this.asCustomer === 'Y') {
          // tslint:disable-next-line:no-shadowed-variable
          this.service.getModeOfBusinessCustomer().subscribe(resp => {
            const body2 = resp.body as any;
            policy.modeOfBusinessCode = body2.data;
            this.savePolicySecondStep(policy, formData);
          }, error => {
            console.log(error);
          });
        } else if (this.asAgent === 'Y') {
          // tslint:disable-next-line:no-shadowed-variable
          this.service.getModeOfBusinessAgent().subscribe(resp => {
              const body2 = resp.body as any;
              policy.modeOfBusinessCode = body2.data.profile.intermediaryType;
              this.savePolicySecondStep(policy, formData);
            },
            error => {
              return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
            }
          );
        }
      }, error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      });
    }, error => {
      return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    });
  }
  savePolicySecondStep(policy, formData) {
    policy.branchCode = this.branchCode;
    policy.channelOfBuinessCode = formData['businessChannel'];
    policy.customerAddressSequence = this.addressSequnce;
    policy.customerCode = this.customerCode;
    policy.settlementModeCode = formData['paymentType'];
    policy.periodFromDate = moment(formData['startDate']).format('YYYY-MM-DD HH:mm:ss');
    policy.periodToDate = moment(formData['endDate']).format('YYYY-MM-DD HH:mm:ss');
    policy.policyRemark = '';
    policy.productCode = formData['plan'];
    this.service.savePolicy(policy).subscribe(resp => {
      const body = resp.body as any;
      this.policySequnce = body.data;
      this.policyCommonData.policySequence = this.policySequnce;
      this.getPolicyDetails(formData);
    }, error => {
      return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
    });
  }
  getPolicyDetails(formData) {
    this.service.getPolicyDetails(this.policySequnce).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(resp => {
      this.productType = resp.productType;
      if (this.productType === 'WHINX') {
        this.getPolicyCommonInformation(formData);
      } else if (this.productType === 'LIVST') {
        this.saveDefaultLocation();
      }
      this.policyCommonData.productType = this.productType;
      this.policyCommonData.proposalNumber = resp.proposalNumber;
      this.policyCommonData.policyNumber = resp.policyNumber;
    });
  }
  getPolicyCommonInformation(formData) {
    this.service.getPolicyCommonInformation(this.policySequnce).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(typeList => {
      this.policyCommonInformation = typeList;
      this.policyCommonInformation.forEach(entry => {
        const description = entry.description;
        switch (description) {
          case 'CROP': {
            entry.value = formData['cropType'];
            break;
          }
          case 'WEATHER TYPE': {
            entry.value = formData['weatherType'];
            break;
          }
        }
      });
      this.updatePolicyCommonInformation();
    });
  }
  updatePolicyCommonInformation() {
    this.service.updatePolicyCommonInformation(this.policySequnce, this.policyCommonInformation).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(resp => {
      this.saveIntermediaries();
    });
  }
  saveIntermediaries() {
    const intermediary: Intermediary = new Intermediary();
    if (this.asCustomer === 'Y') {
      intermediary.intermediaryTypeCode = this.intermediaryType;
      intermediary.intermediaryCode = this.branchCode;
      intermediary.isBusinessParty = 'Y';
      intermediary.isCommissionParty = 'N';
      intermediary.businessPartyshare = 100;
    } else if (this.asAgent === 'Y') {
      intermediary.intermediaryTypeCode = this.intermediaryType;
      intermediary.intermediaryCode = this.partyCode;
      intermediary.isBusinessParty = 'Y';
      intermediary.isCommissionParty = 'Y';
      intermediary.businessPartyshare = 100;
    }
    this.service.saveIntermediaries(this.policySequnce, intermediary).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(resp => {
      // Todo - add notifications service
      this.displayComponent = 'displayAppPolicyWeatherAdditionalDetails';
    });
  }
  saveDefaultLocation() {
    this.service.saveDefaultLocation(this.policySequnce).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(resp => {
      const body = resp.body as any;
      this.policyCommonData.locationSequence = body.data.locationSequence;
     this.saveIntermediaries();
    });
  }
}

