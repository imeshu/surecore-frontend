import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoliciesMainComponent } from './policies-main.component';

describe('PoliciesMainComponent', () => {
  let component: PoliciesMainComponent;
  let fixture: ComponentFixture<PoliciesMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoliciesMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoliciesMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
