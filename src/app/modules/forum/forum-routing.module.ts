import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ForumMainComponent} from './forum-main/forum-main.component';

const routes: Routes = [
  {
    path: 'forum',
    component: ForumMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForumRoutingModule { }
