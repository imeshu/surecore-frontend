import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForumRoutingModule } from './forum-routing.module';
import { ForumMainComponent } from './forum-main/forum-main.component';
import {CommonComponentsModule} from '../common-components/common-components.module';

@NgModule({
  declarations: [ForumMainComponent],
  imports: [
    CommonModule,
    ForumRoutingModule,
    CommonComponentsModule
  ]
})
export class ForumModule { }
