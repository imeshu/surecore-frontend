import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-forum-main',
  templateUrl: './forum-main.component.html',
  styleUrls: ['./forum-main.component.scss']
})
export class ForumMainComponent implements OnInit {
  private url = 'http://52.221.145.163/forum/loginjwt/';
  private JWT_ACCESS_TOKEN: string;

  constructor(public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.JWT_ACCESS_TOKEN = sessionStorage.getItem('access_token');
  }

}
