import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { RequestRecoveryComponent } from './request-recovery/request-recovery.component';
import { RecoveryDetailsComponent } from './recovery-details/recovery-details.component';
import { SmspinComponent } from './smspin/smspin.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignUpLoginComponent } from './sign-up-login/sign-up-login.component';
import { SimpinSignupComponent } from './simpin-signup/simpin-signup.component';
import { DashboardComponent } from '../dashboard/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: 'auth/sign_in',
    component: SignInComponent
  },
  {
    path: 'auth/forgot_password',
    component: RequestRecoveryComponent
  },
  {
    path: 'auth/recovery_options',
    component: RecoveryDetailsComponent
  },
  {
    path: 'auth/sms_pin',
    component: SmspinComponent
  },
  {
    path: 'auth/reset_password',
    component: PasswordResetComponent
  },
  {
    path: 'auth/sign_up',
    component: SignUpComponent
  },
  //signup_login_details_enter page
  {
    path: 'auth/login_details',
    component: SignUpLoginComponent
  },
  {
    path: 'auth/sign_up_sms_pin',//this is used for sms pin in signup
    component: SimpinSignupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
