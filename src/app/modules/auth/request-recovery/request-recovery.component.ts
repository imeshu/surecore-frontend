import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-request-recovery',
  templateUrl: './request-recovery.component.html',
  styleUrls: ['./request-recovery.component.scss']
})
export class RequestRecoveryComponent implements OnInit {

  username = '';
  isInvalidUsername = false;
  isUsernameInFocus = false;
  isExistrecoveryOptions = false;


  // Multi languages
  checkedEng = true;
  checkedSin = false;
  checkedTam = false;

  constructor(private service: AuthService, private router: Router , private translate: TranslateService) {
    translate.addLangs(['en', 'ta', 'sin']);
    translate.setDefaultLang('en');
    translate.use('en');
    const lang = sessionStorage.getItem('selectedLang');
    this.changeLang(lang);
  }

  ngOnInit() {
  }
  changeLang(lang: string) {
    if (lang === 'en') {
      this.checkedEng = true;
      this.checkedSin = false;
      this.checkedTam = false;
    } else if (lang === 'ta') {
      this.checkedEng = false;
      this.checkedSin = false;
      this.checkedTam = true;
    } else if (lang === 'sin') {
      this.checkedEng = false;
      this.checkedSin = true;
      this.checkedTam = false;
    }
    this.translate.use(lang);
    sessionStorage.setItem('selectedLang', lang);
  }
  submitUsername() {
    this.service.submitUsernameForRecovery(this.username)
      .subscribe(resp => {
        const status = resp.status;
        const body = resp.body as any;
        console.log(status);
        if (body.data == null) {
          console.log('invalid username');
          this.isExistrecoveryOptions = true;
          this.isInvalidUsername = false;
        } else {
          sessionStorage.setItem('username', this.username);
          this.router.navigate(['/auth/recovery_options']);

        }
      },
        error => {
          console.log('invalid username');
          this.isInvalidUsername = true;
          this.isExistrecoveryOptions = false;
        });
  }

  returnToLogin() {
    this.router.navigate(['auth/sign_in']);
  }

  applyUsernameFieldValidClass() {
    console.log(this.isExistrecoveryOptions);
    if (!(this.isInvalidUsername  || this.isExistrecoveryOptions)) {
      this.isUsernameInFocus = true;
    }
  }

  disableNextButton() {
    if (this.username === '') {
      return true;
    }
  }

   // username convert to uppercase
   convertToUppercase(data) {
    this.username = data.toUpperCase();
  }

}
