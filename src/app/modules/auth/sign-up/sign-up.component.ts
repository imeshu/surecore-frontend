import { Component, OnInit } from '@angular/core';
import { SignUpService } from '../services/sign-up.service';
import { $ } from 'protractor';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
declare var google: any;
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  //Agent
  agentCode = '';
  agentNicNumber = '';
  agentOriginatorCode = 'AG';

  //Response data from Agent
  agentMobileNumber = '';
  agenPartyCode = '';

  //Farmer
  farmerName = '';
  farmerMobileNumber = '';
  farmerNicNumber = '';
  farmerOriginatorCode = 'AG';

  //Farmer Response Data
  agentPartyCode = '';

  signupTitle = 'Farmer';

  //Validate Feilds
  isValidateMobileNumber = false;
  isValidateNIC = false;
  isValidAgenNIC = false;
  validNIC = false;
  validMobileNumber = false;
  validAgentNIC = false;

  //disable continue button
  isDisabledContinueButton: boolean = true;

  //Multi languages
  checkedEng: boolean = true;
  checkedSin: boolean = false;
  checkedTam: boolean = false;


  constructor(private service: SignUpService, public router: Router, private translate: TranslateService, public notificationService: NotificationDataTransferService) {
    translate.addLangs(['en', 'ta', 'sin']);
    translate.setDefaultLang('en');
    translate.use('en');
    let lang= sessionStorage.getItem('selectedLang');
    if(lang === null){
      lang === 'en';
    }
    this.changeLang(lang);
  }
  changeLang(lang: string) {
    if(lang === 'en'){
      this.checkedEng = true;
      this.checkedSin = false;
      this.checkedTam = false;
    }else if(lang === 'ta'){
      this.checkedEng = false;
      this.checkedSin = false;
      this.checkedTam = true;
    }else if(lang === 'sin'){
      this.checkedEng = false;
      this.checkedSin = true;
      this.checkedTam = false;
    }
    this.translate.use(lang);
    sessionStorage.setItem('selectedLang', lang );
  }
  ngOnInit() { }


  //Signup Agent
  signupAgent() {
    let agent = {
      agentCode: this.agentCode,
      nicNumber: this.agentNicNumber,
      originatorCode: this.agentOriginatorCode
    }
    this.service.signupAgent(agent).subscribe(resp => {
      const body = resp.body as any;
      if (body.success) {
        this.agentMobileNumber = body.data.mobileNumber;
        this.agenPartyCode = body.data.partyCode;
        this.displaySuccessMessage(body);
        sessionStorage.setItem('party_code', this.agenPartyCode);
        sessionStorage.setItem('mobile_number', this.agentMobileNumber);

        //save agent object inside session storage
        sessionStorage.setItem('agentCode', this.agentCode);
        sessionStorage.setItem('agentNicNumber', this.agentNicNumber);
        sessionStorage.setItem('agentOriginatorCode', this.agentOriginatorCode);

        this.router.navigate(['/auth/sign_up_sms_pin']);
      }
    }, error => {
      this.displayErrorMessage(error);
      console.log();
    });


  }
  //Signup Farmer
  signupFarmer() {
    let farmer = {
      name: this.farmerName,
      mobileNumber: this.farmerMobileNumber,
      nicNumber: this.farmerNicNumber,
      originatorCode: this.farmerOriginatorCode
    }

    this.service.signupFarmer(farmer).subscribe(resp => {
      const body = resp.body as any;
      if (body.success) {
        this.agentPartyCode = body.data;
        sessionStorage.setItem('party_code', this.agentPartyCode);
        sessionStorage.setItem('mobile_number', this.farmerMobileNumber);
        this.displaySuccessMessage(body);
        //save agent object inside session storage
        sessionStorage.setItem('farmerName', this.farmerName);
        sessionStorage.setItem('farmerMobileNumber', this.farmerMobileNumber);
        sessionStorage.setItem('farmerNicNumber', this.farmerNicNumber);
        sessionStorage.setItem('farmerOriginatorCode', this.farmerOriginatorCode);

        this.router.navigate(['/auth/sign_up_sms_pin']);
      }
    }, error => {
      this.displayErrorMessage(error);
      console.log();
    });




  }
  //Signup  Button
  signup() {
    if (this.signupTitle === 'Agent') {
      sessionStorage.setItem('signupTitle', this.signupTitle);
      this.signupAgent();
    } else if (this.signupTitle === 'Farmer') {
      this.signupFarmer();
      sessionStorage.setItem('signupTitle', this.signupTitle);
    }
  }

  //Change signup title
  changeSignupTitle(data) {
    if (data === 'A') {
      this.signupTitle = 'Agent';
      this.isFilledInputs();
    } else if (data === 'F') {
      this.signupTitle = 'Farmer';
      this.isFilledInputs();
    }
  }

  validateMobileNumber(mobileNumber) {
    // Mobile  number validation
    const PhoneNumberRegex = new RegExp('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$');
    let test = PhoneNumberRegex.test(mobileNumber);
    return test;
  }
  validateNIC(nic) {
    //NIC validation
    const PhoneNumberRegex = new RegExp('^([0-9]{9}[x|X|v|V]|[0-9]{12})$');
    let test = PhoneNumberRegex.test(nic);
    return test;
  }



  validateFeilds() {
    if (this.signupTitle === 'Agent') {

      if (this.agentNicNumber !== '') {
        if (this.validateNIC(this.agentNicNumber)) {
          this.isValidAgenNIC = false;
        } else {
          this.isValidAgenNIC = true;
        }
      }

      if ((!this.isValidAgenNIC) && (this.agentNicNumber !== '' && this.agentCode !== '')) {
        this.isDisabledContinueButton = false;
      } else {
        this.isDisabledContinueButton = true;
      }

    } else if (this.signupTitle === 'Farmer') {

      //NIC
      if (this.farmerNicNumber !== '') {
        if (this.validateNIC(this.farmerNicNumber)) {
          this.isValidateNIC = false;
        } else {
          //make active error message
          this.isValidateNIC = true;
        }
      }
      //mobile number
      if (this.farmerMobileNumber !== '') {
        if (this.validateMobileNumber(this.farmerMobileNumber)) {
          this.isValidateMobileNumber = false;
        } else {
          //make active error message
          this.isValidateMobileNumber = true;
        }
      }

      if ((!this.isValidateNIC) && (!this.isValidateMobileNumber) && (this.farmerMobileNumber !== '' && this.farmerName !== '' && this.farmerNicNumber !== '')) {
        this.isDisabledContinueButton = false;
      } else {
        this.isDisabledContinueButton = true;
      }

    }
  }

  isFilledInputs() {
    if (this.signupTitle === 'Farmer') {

      if ((!this.isValidateNIC) && (!this.isValidateMobileNumber) && (this.farmerMobileNumber !== '' && this.farmerName !== '' && this.farmerNicNumber !== '')) {
        this.isDisabledContinueButton = false;
      } else {
        this.isDisabledContinueButton = true;
      }
    } else if (this.signupTitle === 'Agent') {

      if ((!this.isValidAgenNIC) && (this.agentNicNumber !== '' && this.agentCode !== '')) {
        this.isDisabledContinueButton = false;
      } else {
        this.isDisabledContinueButton = true;
      }
    }

  }

  /**
   * common functions
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }


}
