import { Component, OnInit, Compiler } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { retry } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  username = '';
  password = '';
  lastSubmittedUsername = '';
  lastSubmittedPassword = '';
  isInvalidCredentials = false;
  isUsernameInFocus = false;
  isPasswordInFocus = false;
  isPasswordChanged = false;
  passwordVisible = false;
  rememberMe = false;


  // Multi languages
  checkedEng = true;
  checkedSin = false;
  checkedTam = false;




  constructor(private service: AuthService, private router: Router, private translate: TranslateService , private _compiler: Compiler) {
  //  this._compiler.clearCache();
    translate.addLangs(['en', 'ta', 'sin']);
    translate.setDefaultLang('en');
    translate.use('en');
    const lang = sessionStorage.getItem('selectedLang');
    if (lang === null) {
      // tslint:disable-next-line:no-unused-expression
      lang === 'en';
    }
    this.changeLang(lang);
    this.disableLoginButton();
  }

  ngOnInit() {

    sessionStorage.removeItem('token');

  }
  changeLang(lang: string) {
    if (lang === 'en') {
      this.checkedEng = true;
      this.checkedSin = false;
      this.checkedTam = false;
    } else if (lang === 'ta') {
      this.checkedEng = false;
      this.checkedSin = false;
      this.checkedTam = true;
    } else if (lang === 'sin') {
      this.checkedEng = false;
      this.checkedSin = true;
      this.checkedTam = false;
    }
    this.translate.use(lang);
    sessionStorage.setItem('selectedLang', lang);
  }
  login() {
    const credentials = { username: this.username, password: this.password };
    this.service.login(credentials)
      .subscribe(resp => {
        const body = resp.body as any;
        if (this.rememberMe) {
          localStorage.setItem('access_token', body.access_token);
        } else {
          sessionStorage.setItem('access_token', body.access_token);
        }
        this.router.navigate(['/']);
      },
        error => {
          console.log(error);
          this.isInvalidCredentials = true;
          this.lastSubmittedUsername = credentials.username;
          this.lastSubmittedPassword = credentials.password;
          this.router.navigate(['/']);
        });
  }

  navigateToForgotPassword() {
    // console.log('test');
    this.router.navigate(['/auth/forgot_password']);
  }

  // passwordToggleEye() {
  //   if (this.passwordVisible === false) {
  //     this.passwordVisible = true;
  //   } else {
  //     this.passwordVisible = false;
  //   }
  // }

  // if at least one field is empty
  // or if current credentials are same as last submitted credentials
  disableLoginButton() {
    if (this.username === '' && this.password === '') {
      return true;
    } else if (this.username !== '' && this.password === '') {
      return true;
    } else if (this.username === '' && this.password !== '') {
      return true;
    } else if (this.isInvalidCredentials && (this.username === '' && this.password === '')) {
      return true;
    } else if (this.isInvalidCredentials && (this.username !== '' && this.password !== '') &&
      (this.lastSubmittedUsername === this.username && this.lastSubmittedPassword === this.password)) {
      return true;
    } else {
      return false;
    }
  }

  // applyUsernameFieldValidClass() {
  //   if (!this.isInvalidCredentials) {
  //     this.isUsernameInFocus = true;
  //   }
  // }

  // applyPasswordFieldValidClass() {
  //   if (!this.isInvalidCredentials) {
  //     this.isPasswordInFocus = true;
  //   }
  // }

  setRememberMe() {
    this.rememberMe = !this.rememberMe;
    console.log('remember me: ', this.rememberMe);
  }

  // username convert to uppercase
  convertToUppercase(data) {
    this.username = data.toUpperCase();
  }



}
