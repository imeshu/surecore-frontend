import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpinSignupComponent } from './simpin-signup.component';

describe('SimpinSignupComponent', () => {
  let component: SimpinSignupComponent;
  let fixture: ComponentFixture<SimpinSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpinSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpinSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
