import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { SignUpService } from '../services/sign-up.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-simpin-signup',
  templateUrl: './simpin-signup.component.html',
  styleUrls: ['./simpin-signup.component.scss']
})
export class SimpinSignupComponent implements OnInit {

  //Agent sms pin number
  agentResponsePinNumber = '';
  agentResponsePartyCode = '';
  agentResponseMobileNumber = '';

  digit1 = '';
  digit2 = '';
  digit3 = '';
  digit4 = '';
  digit5 = '';
  digit6 = '';

  isInvalidCode = false;
  selectedNumber = '';
  errorDescription: string = '';

  //mobile number
  mobileNumber = '';
  //displaying mobile number
  mobileNumberFirstPart = '';
  mobileNumberLastPart = '';

  //Retrive session storage items
  signupTitle = '';
  //Agent
  agentCode = '';
  agentNicNumber = '';
  agentOriginatorCode = 'AG';

  //Farmer
  farmerName = '';
  farmerMobileNumber = '';
  farmerNicNumber = '';
  farmerOriginatorCode = 'AG';


    //Multi languages
    checkedEng: boolean = true;
    checkedSin: boolean = false;
    checkedTam: boolean = false;

  constructor(private service: SignUpService, private authService: AuthService, private router: Router,  private translate: TranslateService) {
    translate.addLangs(['en', 'ta', 'sin']);
    translate.setDefaultLang('en');
    translate.use('en');
    let lang= sessionStorage.getItem('selectedLang');
    this.changeLang(lang);
   }

  ngOnInit() {
    this.agentResponsePinNumber = sessionStorage.getItem('sms_number');
    this.agentResponseMobileNumber = sessionStorage.getItem('mobile_number');
    this.mobileNumberFirstPart =  this.agentResponseMobileNumber .substr(0, 3);
    this.mobileNumberLastPart =  this.agentResponseMobileNumber .substr(7, 10);

    //check signup title and get its object to use when user click on resed
    this.signupTitle = sessionStorage.getItem('signupTitle');
    if (this.signupTitle === 'Agent') {
      this.agentCode = sessionStorage.getItem('agentCode');
      this.agentNicNumber = sessionStorage.getItem('agentNicNumber');
      this.agentOriginatorCode = sessionStorage.getItem('agentOriginatorCode');
    } else if (this.signupTitle === 'Farmer') {
      this.farmerName = sessionStorage.getItem('farmerName');
      this.farmerMobileNumber = sessionStorage.getItem('farmerMobileNumber');
      this.farmerNicNumber = sessionStorage.getItem('farmerNicNumber');
      this.farmerOriginatorCode = sessionStorage.getItem('farmerOriginatorCode');
    }
  }
  changeLang(lang: string) {
    if(lang === 'en'){
      this.checkedEng = true;
      this.checkedSin = false;
      this.checkedTam = false;
    }else if(lang === 'ta'){
      this.checkedEng = false;
      this.checkedSin = false;
      this.checkedTam = true;
    }else if(lang === 'sin'){
      this.checkedEng = false;
      this.checkedSin = true;
      this.checkedTam = false;
    }
    this.translate.use(lang);
    sessionStorage.setItem('selectedLang', lang);
  }

  resendCode() {
    this.digit1 = '';
    this.digit2 = '';
    this.digit3 = '';
    this.digit4 = '';
    this.digit5 = '';
    this.digit6 = '';


    // alert(this.signupTitle);
    if (this.signupTitle === 'Agent') {
      sessionStorage.setItem('signupTitle', this.signupTitle);
      this.signupAgent();
    } else if (this.signupTitle === 'Farmer') {
      this.signupFarmer();
      sessionStorage.setItem('signupTitle', this.signupTitle);
    }

  }

  //Signup Farmer
  signupFarmer() {
    let farmer = {
      name: this.farmerName,
      mobileNumber: this.farmerMobileNumber,
      nicNumber: this.farmerNicNumber,
      originatorCode: this.farmerOriginatorCode
    }
    this.service.signupFarmer(farmer).subscribe(resp => {
      const body = resp.body as any;
      if (body.success) {
        this.agentResponsePartyCode = body.data;
        sessionStorage.setItem('party_code', this.agentResponsePartyCode);
        sessionStorage.setItem('mobile_number', this.farmerMobileNumber);

        //save agent object inside session storage
        sessionStorage.setItem('farmerName', this.farmerName);
        sessionStorage.setItem('farmerMobileNumber', this.farmerMobileNumber);
        sessionStorage.setItem('farmerNicNumber', this.farmerNicNumber);
        sessionStorage.setItem('farmerOriginatorCode', this.farmerOriginatorCode);


      }
    }, error => {
      console.log();
    });
  }

  //Signup Agent
  signupAgent() {
    let agent = {
      agentCode: this.agentCode,
      nicNumber: this.agentNicNumber,
      originatorCode: this.agentOriginatorCode
    }
    this.service.signupAgent(agent).subscribe(resp => {
      const body = resp.body as any;
      if (body.success) {
        this.agentResponsePinNumber = body.data.mobileNumber;
        this.agentResponsePartyCode = body.data.partyCode;
        sessionStorage.setItem('party_code', this.agentResponsePartyCode);
        sessionStorage.setItem('mobile_number', this.agentResponseMobileNumber);

        // save agent object inside session storage
        sessionStorage.setItem('agentCode', this.agentCode);
        sessionStorage.setItem('agentNicNumber', this.agentNicNumber);
        sessionStorage.setItem('agentOriginatorCode', this.agentOriginatorCode);


      }
    }, error => {
      console.log();
    });
  }

  returnToSignUp() {
    this.router.navigate(['/auth/sign_up']);
  }

  res
  submitPIN() {
    const pin = this.digit1 + this.digit2 + this.digit3 + this.digit4 + this.digit5 + this.digit6;
    const partyCode = sessionStorage.getItem('party_code');
    this.service.verifyPin(partyCode, pin)
      .subscribe(resp => {
        const body = resp.body as any;
        this.isInvalidCode = false;
        sessionStorage.setItem('pinNumber', pin);
        this.router.navigate(['/auth/login_details']);

      },
        error => {
          this.isInvalidCode = true;
          const errorObj = error.error as any;
          this.errorDescription = errorObj.description;
        });
  }
  // resendCode() {
  //   this.digit1 = '';
  //   this.digit2 = '';
  //   this.digit3 = '';
  //   this.digit4 = '';
  //   this.digit5 = '';
  //   this.digit6 = '';

  //   const username = sessionStorage.getItem('partyCode');
  //   const data = { username: username, contatctType: 'CMOBL' };
  //   this.service.submitRecoveryOption(data)
  //     .subscribe(resp => {
  //       const body = resp.body as any;
  //       console.log(body);

  //       sessionStorage.setItem('tokenType', 'CMOBL');
  //       sessionStorage.setItem('token', body.data);
  //     },
  //       error => {
  //         console.log(error);
  //       });
  // }

  disableVerifyCodeButton() {
    if (this.digit1 === '' || this.digit2 === '' || this.digit3 === '' || this.digit4 === '' || this.digit5 === '' || this.digit6 === '') {
      return true;
    }
  }

}

