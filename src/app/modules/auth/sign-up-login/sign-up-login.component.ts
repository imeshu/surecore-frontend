import { Component, OnInit } from '@angular/core';
import { SignUpComponent } from '../sign-up/sign-up.component';
import { SignUpService } from '../services/sign-up.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sign-up-login',
  templateUrl: './sign-up-login.component.html',
  styleUrls: ['./sign-up-login.component.scss']
})
export class SignUpLoginComponent implements OnInit {


  partyCode = '';
  username = '';
  pinNumber = '';

  newPassword: string = '';
  reenteredPassword: string = '';

  id = '';
  token = '';
  type = '';

  newPasswordVisible: boolean = false;
  reenteredPasswordVisible: boolean = false;

  isValidatedPassword: boolean = false;
  isMatchedPassword: boolean = false;

  showWeekPasswordlbl: boolean = false;
  showStrongPasswordLbl: boolean = false;
  showStrongMatchingPasswordlbl: boolean = false;
  showDoNotMatchLbl: boolean = false;

  isfocusNewPasswordfeild: boolean = false;

  //password validate checkboxes
  checkedMinimum8Characters: boolean = false;
  checkedUppsercaseLetter: boolean = false;
  checkedLowerCaseLetter: boolean = false;
  checkedNumber: boolean = false;
  checkedSymbol: boolean = false;

    //Multi languages
    checkedEng: boolean = true;
    checkedSin: boolean = false;
    checkedTam: boolean = false;
  constructor(private service: SignUpService, private authService: AuthService, private activatedRoute: ActivatedRoute,
    private router: Router, private notificationService: NotificationDataTransferService, private translate: TranslateService) {
      translate.addLangs(['en', 'ta', 'sin']);
      translate.setDefaultLang('en');
      translate.use('en');
      let lang= sessionStorage.getItem('selectedLang');
      this.changeLang(lang);
    }

  ngOnInit() {
    // setTimeout(() => {
    //   this.router.navigate(['/']);
    // }, 600000);  // set to 10 mints
    this.partyCode = sessionStorage.getItem('party_code');
    this.pinNumber = sessionStorage.getItem('pinNumber');

  }
  changeLang(lang: string) {
    if(lang === 'en'){
      this.checkedEng = true;
      this.checkedSin = false;
      this.checkedTam = false;
    }else if(lang === 'ta'){
      this.checkedEng = false;
      this.checkedSin = false;
      this.checkedTam = true;
    }else if(lang === 'sin'){
      this.checkedEng = false;
      this.checkedSin = true;
      this.checkedTam = false;
    }
    this.translate.use(lang);
    sessionStorage.setItem('selectedLang', lang);
  }

  createAccount() {

    let account = {
      "partyCode": this.partyCode,
      "username": this.username,
      "password": this.newPassword,
      "pinNumber": this.pinNumber
    }
    this.service.createAccount(account).subscribe(resp => {
      const body = resp.body as any;
      // this.notificationService.isMessageDisplayed = true;
      // this.notificationService.messageContent = 'Your account has been created successfully.';
      this.displaySuccessMessage(body);
      this.router.navigate(['auth/sign_in']);
    }, error => {
      this.displayErrorMessage(error);
      console.log(error);
    });

  }


//username convert to uppercase
convertToUppercase(data) {
  this.username = data.toUpperCase();
}

checkPassowrd() {

  if (this.newPassword !== '') {

    //contains minimum 8 characters
    if (new RegExp('^(?=.{8,})').test(this.newPassword)) {
      this.checkedMinimum8Characters = true;
    } else {
      this.checkedMinimum8Characters = false;
    }

    //contains a Uppercase letter
    if (new RegExp('^(?=.*[A-Z])').test(this.newPassword)) {
      this.checkedUppsercaseLetter = true;
    } else {
      this.checkedUppsercaseLetter = false;
    }

    //contains a lowercase letter
    if (new RegExp('^(?=.*[a-z])').test(this.newPassword)) {
      this.checkedLowerCaseLetter = true;
    } else {
      this.checkedLowerCaseLetter = false;
    }

    //contains a number
    if (new RegExp('^(?=.*[0-9])').test(this.newPassword)) {
      this.checkedNumber = true;
    } else {
      this.checkedNumber = false;
    }

    //contains a symbol
    if (new RegExp('^(?=.*[!@#\$%\^&\*\-])').test(this.newPassword)) {
      this.checkedSymbol = true;
    } else {
      this.checkedSymbol = false;
    }


  }

}



validatePassword() {

  if (this.newPassword !== '' && this.newPassword.length >= 8) {
    const regex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\-])(?=.{8,})');

    this.isValidatedPassword = regex.test(this.newPassword);
    if (this.isValidatedPassword) {
      if (this.reenteredPassword == '') {


        this.showWeekPasswordlbl = false;
        this.showStrongPasswordLbl = true;
        this.showDoNotMatchLbl = false;
        this.showStrongMatchingPasswordlbl = false;
      } else {
        this.matchPassword();
      }
    } else {
      this.showWeekPasswordlbl = true;
      this.showStrongPasswordLbl = false;
      this.showDoNotMatchLbl = false;
      this.showStrongMatchingPasswordlbl = false;
    }
    this.isfocusNewPasswordfeild = false;
    //  }
    //password validation checkbox

  } else {
    this.showWeekPasswordlbl = false;
    this.isValidatedPassword = false;
    this.showStrongPasswordLbl = false;
    this.showDoNotMatchLbl = false;
    this.showStrongMatchingPasswordlbl = false;
    this.isfocusNewPasswordfeild = true;
  }

  //password validation checkbox
  this.checkPassowrd();
}

matchPassword() {
  if (this.newPassword !== '' && this.reenteredPassword !== '') {
    const regex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})');

    if ((this.newPassword == this.reenteredPassword && regex.test(this.newPassword) == true)) {

      this.isMatchedPassword = true;
      this.showDoNotMatchLbl = false;
      this.showStrongMatchingPasswordlbl = true;
      this.showStrongPasswordLbl = false;
      this.showWeekPasswordlbl = false;

    } else {

      if (regex.test(this.newPassword) == true) {
        this.isMatchedPassword = false;
        this.showDoNotMatchLbl = true;
        this.showStrongMatchingPasswordlbl = false;
        this.showStrongPasswordLbl = false;
        this.showWeekPasswordlbl = false;
      } else {
        this.isMatchedPassword = false;
        this.showDoNotMatchLbl = false;
        this.showStrongMatchingPasswordlbl = false;
        this.showStrongPasswordLbl = false;
        this.showWeekPasswordlbl = true;
      }


    }
    this.isfocusNewPasswordfeild = false;
  } else {

    if (this.isValidatedPassword) {

      this.isMatchedPassword = false;
      this.showDoNotMatchLbl = false;
      this.showStrongMatchingPasswordlbl = false;
      this.showWeekPasswordlbl = false;
      this.showStrongPasswordLbl = true;
    } else {

      this.isMatchedPassword = false;
      this.showDoNotMatchLbl = false;
      this.showStrongMatchingPasswordlbl = false;
      this.showWeekPasswordlbl = false;
      this.showStrongPasswordLbl = false;
      this.isfocusNewPasswordfeild = true;
    }

  }

}



  newPasswordToggleEye() {
    if (this.newPasswordVisible === false) {
      this.newPasswordVisible = true;
    } else {
      this.newPasswordVisible = false;
    }

  }

  reenteredPasswordToggleEye() {
    if (this.reenteredPasswordVisible === false) {
      this.reenteredPasswordVisible = true;
    } else {
      this.reenteredPasswordVisible = false;
    }

  }

  onfocusNewPasswordfeild() {
    this.isfocusNewPasswordfeild = true;
  }

  onBlurNewPasswordfeild() {
    this.isfocusNewPasswordfeild = false;
  }

   // displaying error messages
   displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }


}
