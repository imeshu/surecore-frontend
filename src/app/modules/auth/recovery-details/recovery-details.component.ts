import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-recovery-details',
  templateUrl: './recovery-details.component.html',
  styleUrls: ['./recovery-details.component.scss']
})
export class RecoveryDetailsComponent implements OnInit {

  optionsList = [];
  selectedOption = '';
  isSendInstructionButtonDisabled = false; // this is for disabling the button after details are sent
  isResendEmailHidden = true;
  disableAfterEmailSent = false;



  // Multi languages
  checkedEng = true;
  checkedSin = false;
  checkedTam = false;
  constructor(private service: AuthService, private router: Router, private notificationService: NotificationDataTransferService, private translate: TranslateService) {
    translate.addLangs(['en', 'ta', 'sin']);
    translate.setDefaultLang('en');
    translate.use('en');
    const lang = sessionStorage.getItem('selectedLang');
    this.changeLang(lang);

  }

  ngOnInit() {
    const username = sessionStorage.getItem('username');
    this.service.submitUsernameForRecovery(username)
      .subscribe(resp => {
        const status = resp.status;
        const body = resp.body as any;
        this.optionsList = body.data;
        console.log('optionslist: ', this.optionsList);
        // set 'this.selectedOption = 'CMOBL' if have 2 options
        if (this.optionsList.length === 2) {
          this.selectedOption = 'CMOBL';
        }
        // set default one if have one option
        if (this.optionsList.length === 1) {
          for (let i = 0; i < this.optionsList.length; i++) {
            if (this.optionsList[i]['type'] === 'CEMIL') {
              this.selectedOption = 'CEMIL';
            } else {
              this.selectedOption = 'CMOBL';
            }
          }
        }
      });
  }

  changeLang(lang: string) {
    if (lang === 'en') {
      this.checkedEng = true;
      this.checkedSin = false;
      this.checkedTam = false;
    } else if (lang === 'ta') {
      this.checkedEng = false;
      this.checkedSin = false;
      this.checkedTam = true;
    } else if (lang === 'sin') {
      this.checkedEng = false;
      this.checkedSin = true;
      this.checkedTam = false;
    }
    this.translate.use(lang);
    sessionStorage.setItem('selectedLang', lang);
  }

  submitOption() {
    this.isSendInstructionButtonDisabled = true;
    const username = sessionStorage.getItem('username');
    const data = {username: username, contatctType: this.selectedOption};
    this.service.submitRecoveryOption(data)
      .subscribe(resp => {
        const body = resp.body as any;
        console.log(body);

        sessionStorage.setItem('tokenType', this.selectedOption);
   //     sessionStorage.setItem('token', body.data);

        if (this.selectedOption === 'CMOBL') {

          for (let i = 0; i < this.optionsList.length; i++) {
            if (this.optionsList[i].type === this.selectedOption) {
              sessionStorage.setItem('sms_number', this.optionsList[i].number);
            }
          }

          this.router.navigate(['/auth/sms_pin']);
        }

        if (this.selectedOption === 'CEMIL') {
          // this.router.navigate(['/auth/login']);
          this.isResendEmailHidden = false;
          this.notificationService.isMessageDisplayed = true;
          this.notificationService.messageContent = 'Recovery instructions have been sent to your email, please have a look.';
          this.disableAfterEmailSent = true;
        }
      });
  }

  returnToLogin() {
    this.router.navigate(['auth/sign_in']);
  }

  disableSendInstructionsButton() {
    if (this.selectedOption === '' || this.isSendInstructionButtonDisabled) {
      return true;
    }
  }

}
