import { Component, OnInit } from '@angular/core';

import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationDataTransferService } from '../../../services/notification-data-transfer.service';
import { timeout } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {

  newPassword = '';
  reenteredPassword = '';

  id = '';
  token = '';
  type = '';

  newPasswordVisible = false;
  reenteredPasswordVisible = false;

  isValidatedPassword = false;
  isMatchedPassword = false;

  showWeekPasswordlbl = false;
  showStrongPasswordLbl = false;
  showStrongMatchingPasswordlbl = false;
  showDoNotMatchLbl = false;

  isfocusNewPasswordfeild = false;

  // password validate checkboxes
  checkedMinimum8Characters = false;
  checkedUppsercaseLetter = false;
  checkedLowerCaseLetter = false;
  checkedNumber = false;
  checkedSymbol = false;


  // Multi languages
  checkedEng = true;
  checkedSin = false;
  checkedTam = false;

  constructor(private service: AuthService, private activatedRoute: ActivatedRoute,
    private router: Router, private notificationService: NotificationDataTransferService,
    private translate: TranslateService) {
      translate.addLangs(['en', 'ta', 'sin']);
    translate.setDefaultLang('en');
    translate.use('en');
    const lang = sessionStorage.getItem('selectedLang');
    this.changeLang(lang);
      setTimeout(() => {
        this.router.navigate(['auth/sign_in']);
    }, 600000);  // set to 10 mints
     }

  ngOnInit() {
    if (sessionStorage.getItem('tokenType') === null) {
      this.activatedRoute.queryParams.subscribe(params => {
        this.id = params['id'];
        this.token = params['token'];
        this.type = params['type'];
      });
    } else {
      this.id = sessionStorage.getItem('username');
      this.token = sessionStorage.getItem('token');
      this.type = sessionStorage.getItem('tokenType');
    }
    // verifying email link expire or not
    if (this.type === 'CEMIL') {
      this.verifyEmailLink();
    }
  }

  changeLang(lang: string) {
    if (lang === 'en') {
      this.checkedEng = true;
      this.checkedSin = false;
      this.checkedTam = false;
    } else if (lang === 'ta') {
      this.checkedEng = false;
      this.checkedSin = false;
      this.checkedTam = true;
    } else if (lang === 'sin') {
      this.checkedEng = false;
      this.checkedSin = true;
      this.checkedTam = false;
    }
    this.translate.use(lang);
    sessionStorage.setItem('selectedLang', lang);
  }


  checkPassowrd() {

    if (this.newPassword !== '') {

      // contains minimum 8 characters
      if (new RegExp('^(?=.{8,})').test(this.newPassword)) {
        this.checkedMinimum8Characters = true;
      } else {
        this.checkedMinimum8Characters = false;
      }

      // contains a Uppercase letter
      if (new RegExp('^(?=.*[A-Z])').test(this.newPassword)) {
        this.checkedUppsercaseLetter = true;
      } else {
        this.checkedUppsercaseLetter = false;
      }

      // contains a lowercase letter
      if (new RegExp('^(?=.*[a-z])').test(this.newPassword)) {
        this.checkedLowerCaseLetter = true;
      } else {
        this.checkedLowerCaseLetter = false;
      }

      // contains a number
      if (new RegExp('^(?=.*[0-9])').test(this.newPassword)) {
        this.checkedNumber = true;
      } else {
        this.checkedNumber = false;
      }

      // contains a symbol
      if (new RegExp('^(?=.*[!@#\$%\^&\*\-])').test(this.newPassword)) {
        this.checkedSymbol = true;
      } else {
        this.checkedSymbol = false;
      }


    }

  }



  validatePassword() {

    if (this.newPassword !== '' && this.newPassword.length >= 8) {
      const regex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\-])(?=.{8,})');

      this.isValidatedPassword = regex.test(this.newPassword);
      if (this.isValidatedPassword) {
        if (this.reenteredPassword === '') {


          this.showWeekPasswordlbl = false;
          this.showStrongPasswordLbl = true;
          this.showDoNotMatchLbl = false;
          this.showStrongMatchingPasswordlbl = false;
        } else {
          this.matchPassword();
        }
      } else {
        this.showWeekPasswordlbl = true;
        this.showStrongPasswordLbl = false;
        this.showDoNotMatchLbl = false;
        this.showStrongMatchingPasswordlbl = false;
      }
      this.isfocusNewPasswordfeild = false;
      //  }
      // password validation checkbox

    } else {
      this.showWeekPasswordlbl = false;
      this.isValidatedPassword = false;
      this.showStrongPasswordLbl = false;
      this.showDoNotMatchLbl = false;
      this.showStrongMatchingPasswordlbl = false;
      this.isfocusNewPasswordfeild = true;
    }

    // password validation checkbox
    this.checkPassowrd();
  }

  matchPassword() {
    // alert(this.reenteredPassword.length + " " + this.isValidatedPassword);
    if (this.newPassword !== '' && this.reenteredPassword !== '') {
      const regex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})');

      if ((this.newPassword === this.reenteredPassword && regex.test(this.newPassword) === true)) {

        this.isMatchedPassword = true;
        this.showDoNotMatchLbl = false;
        this.showStrongMatchingPasswordlbl = true;
        this.showStrongPasswordLbl = false;
        this.showWeekPasswordlbl = false;

      } else {

        // tslint:disable-next-line:triple-equals
        if (regex.test(this.newPassword) === true) {
          this.isMatchedPassword = false;
          this.showDoNotMatchLbl = true;
          this.showStrongMatchingPasswordlbl = false;
          this.showStrongPasswordLbl = false;
          this.showWeekPasswordlbl = false;
        } else {
          this.isMatchedPassword = false;
          this.showDoNotMatchLbl = false;
          this.showStrongMatchingPasswordlbl = false;
          this.showStrongPasswordLbl = false;
          this.showWeekPasswordlbl = true;
        }


      }
      this.isfocusNewPasswordfeild = false;
    } else {

      if (this.isValidatedPassword) {

        this.isMatchedPassword = false;
        this.showDoNotMatchLbl = false;
        this.showStrongMatchingPasswordlbl = false;
        this.showWeekPasswordlbl = false;
        this.showStrongPasswordLbl = true;
      } else {

        this.isMatchedPassword = false;
        this.showDoNotMatchLbl = false;
        this.showStrongMatchingPasswordlbl = false;
        this.showWeekPasswordlbl = false;
        this.showStrongPasswordLbl = false;
        this.isfocusNewPasswordfeild = true;
      }

    }

  }

  submitNewPassword() {
    const data = {
      username: this.id,
      newPassword: this.newPassword,
      token: this.token,
      tokenType: this.type
    };
    console.log(data);

    this.service.submitNewPassword(data)
      .subscribe(resp => {
        const body = resp.body as any;
        console.log(body);
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.success('Password has been changed, please enter new password and login');
        this.router.navigate(['auth/sign_in']);
      },
        error => {
          console.log(error);
        });
  }

  newPasswordToggleEye() {
    if (this.newPasswordVisible === false) {
      this.newPasswordVisible = true;
    } else {
      this.newPasswordVisible = false;
    }

  }

  reenteredPasswordToggleEye() {
    if (this.reenteredPasswordVisible === false) {
      this.reenteredPasswordVisible = true;
    } else {
      this.reenteredPasswordVisible = false;
    }

  }

  onfocusNewPasswordfeild() {
    this.isfocusNewPasswordfeild = true;
  }

  onBlurNewPasswordfeild() {
    this.isfocusNewPasswordfeild = false;
  }


  // reset password email link verifying - expire within 1 hour
  verifyEmailLink() {
    this.service.verifyEmailLink(this.id, this.token)
    .subscribe(resp => {
      const body = resp.body as any;
      console.log(body);
    },
      error => {
        console.log(error);
        const errorObj = error.error as any;
        this.notificationService.isMessageDisplayed = true;
        this.notificationService.messageContent =  errorObj.description;
        this.router.navigate(['/auth/sign_in']);

      });
  }
}
