import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmspinComponent } from './smspin.component';

describe('SmspinComponent', () => {
  let component: SmspinComponent;
  let fixture: ComponentFixture<SmspinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmspinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmspinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
