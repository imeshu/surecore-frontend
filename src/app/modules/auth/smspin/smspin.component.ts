import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
@Component({
  selector: 'app-smspin',
  templateUrl: './smspin.component.html',
  styleUrls: ['./smspin.component.scss']
})
export class SmspinComponent implements OnInit {

  digit1 = '';
  digit2 = '';
  digit3 = '';
  digit4 = '';
  digit5 = '';
  digit6 = '';

  isInvalidCode = false;
  selectedNumber = '';
  errorDescription = '';

    // Multi languages
    checkedEng = true;
    checkedSin = false;
    checkedTam = false;

  // tslint:disable-next-line:max-line-length
  constructor(private service: AuthService, private router: Router, private translate: TranslateService, public notificationService: NotificationDataTransferService) {
    translate.addLangs(['en', 'ta', 'sin']);
    translate.setDefaultLang('en');
    translate.use('en');
     const lang = sessionStorage.getItem('selectedLang');
     this.changeLang(lang);
  }

  ngOnInit() {
    this.selectedNumber = sessionStorage.getItem('sms_number');
  }
  changeLang(lang: string) {
    if (lang === 'en') {
      this.checkedEng = true;
      this.checkedSin = false;
      this.checkedTam = false;
    } else if (lang === 'ta') {
      this.checkedEng = false;
      this.checkedSin = false;
      this.checkedTam = true;
    } else if (lang === 'sin') {
      this.checkedEng = false;
      this.checkedSin = true;
      this.checkedTam = false;
    }
    this.translate.use(lang);
    sessionStorage.setItem('selectedLang', lang);
  }

  submitPIN() {
    const pin = this.digit1 + this.digit2 + this.digit3 + this.digit4 + this.digit5 + this.digit6;
    const username = sessionStorage.getItem('username');
    sessionStorage.setItem('token', pin);
    this.service.verifySMSPIN(username, pin)
      .subscribe(resp => {
        const body = resp.body as any;
        this.router.navigate(['/auth/reset_password']);
      },
        error => {
          this.isInvalidCode = true;
          const errorObj = error.error as any;
          this.errorDescription = 'Invalid code please retry';
        });
  }

  returnToLogin() {
    this.router.navigate(['auth/sign_in']);
  }

  resendCode() {
    this.digit1 = '';
    this.digit2 = '';
    this.digit3 = '';
    this.digit4 = '';
    this.digit5 = '';
    this.digit6 = '';
    const username = sessionStorage.getItem('username');
    const data = {username: username, contatctType: 'CMOBL'};
    this.service.submitRecoveryOption(data)
      .subscribe(resp => {
        const body = resp.body as any;
        console.log(body);
        this.displaySuccessMessage(body);

        sessionStorage.setItem('tokenType', 'CMOBL');
      //   sessionStorage.setItem('token', body.data);
      });
  }

  disableVerifyCodeButton() {
    if (this.digit1 === '' || this.digit2 === '' || this.digit3 === '' || this.digit4 === '' || this.digit5 === '' || this.digit6 === '') {
      return true;
    }
  }

  /**
   * common functions
   */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';
    console.log(error);
  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }


}
