import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { RecoveryDetailsComponent } from './recovery-details/recovery-details.component';
import { RequestRecoveryComponent } from './request-recovery/request-recovery.component';
import { SmspinComponent } from './smspin/smspin.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SignUpLoginComponent } from './sign-up-login/sign-up-login.component';
import { SimpinSignupComponent } from './simpin-signup/simpin-signup.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [SignInComponent, SignUpComponent, PasswordResetComponent, RecoveryDetailsComponent, RequestRecoveryComponent, SmspinComponent, SignUpLoginComponent, SimpinSignupComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    DashboardModule,
    FormsModule,
    TranslateModule
  ]

})
export class AuthModule { }
