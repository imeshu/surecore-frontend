import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { grantType, clientID, clientSecret, baseUrl } from 'src/app/config/enum';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  protected baseAPI = baseUrl;

  constructor(private http: HttpClient, private router: Router) { }

  login(credentials): Observable<HttpResponse<Object>> {
    const path = 'oauth/token';
    const formdata = new FormData();
    formdata.append('username', credentials.username);
    formdata.append('password', credentials.password);
    formdata.append('grant_type', grantType);
    formdata.append('client_id', clientID);
    formdata.append('client_secret', clientSecret);

    return this.http.post(this.baseAPI + path, formdata, { observe: 'response' });
  }

  submitUsernameForRecovery(username): Observable<HttpResponse<Object>> {
    const path = 'auth/forgetUser?username=' + username;
    return this.http.get(this.baseAPI + path, { observe: 'response' });
  }

  submitRecoveryOption(data): Observable<HttpResponse<Object>> {
    const path = 'auth/resetPassword';
    return this.http.post(this.baseAPI + path, data, { observe: 'response' });
  }

  verifySMSPIN(username, pin): Observable<HttpResponse<Object>> {
    const path = 'auth/verifyPin?username=' + username + '&pinNo=' + pin;
    return this.http.get(this.baseAPI + path, { observe: 'response' });
  }

  submitNewPassword(data): Observable<HttpResponse<Object>> {
    const path = 'auth/userPassword';
    return this.http.put(this.baseAPI + path, data, { observe: 'response' });
  }

  //Verify email link for reset password
  verifyEmailLink(code, emailToken): Observable<HttpResponse<Object>> {
    const path = 'auth/verifyEmailLink?code=' + code + '&token=' + emailToken;
    return this.http.get(this.baseAPI + path, { observe: 'response' });
  }

  

}
