import { Injectable } from '@angular/core';
import { baseUrl } from 'src/app/config/enum';
import { Observable } from 'rxjs';
import { HttpResponse, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {

  protected baseAPI = baseUrl;

  constructor(private http: HttpClient) { }

  //signup Agent
  signupAgent(data): Observable<HttpResponse<Object>> {
    const path = 'base/agent/signup';
    return this.http.post(this.baseAPI + path, data, { observe: 'response' });
  }

  //Signup Farmer
  signupFarmer(data): Observable<HttpResponse<Object>>{
    const path = 'base/farmer/signup';
    return this.http.post(this.baseAPI + path, data, { observe: 'response' });
  }

  //verify pin
  verifyPin(partyCode, pinNumber): Observable<HttpResponse<Object>>{
    const path = 'base/verifyPin?partyCode=' + partyCode + '&pinNumber=' + pinNumber;
    return this.http.get(this.baseAPI + path, { observe: 'response' });
  }

  //insert login details - after sign up update with login details
  updateSigninDetails(data): Observable<HttpResponse<Object>>{
    const path = 'base/createAccount';
    return this.http.put(this.baseAPI + path, data, { observe: 'response' });
  }

  //create account
  createAccount(data): Observable<HttpResponse<Object>>{
    const path = 'base/createAccount';
    return this.http.put(this.baseAPI + path, data, { observe: 'response' });
  }

}
