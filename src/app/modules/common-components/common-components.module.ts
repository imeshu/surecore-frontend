import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselComponent } from './carousel/carousel.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomPipeModule } from '../../pipes/CustomPipesModule';
import { RatingComponent } from './rating/rating.component';
import { HeaderComponent } from '../header/header.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ToolbarComponent } from '../dashboard/toolbar/toolbar.component';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { RouterModule } from '@angular/router';
import { MatToolbarModule } from '@angular/material';

@NgModule({
  declarations: [CarouselComponent, RatingComponent, ToolbarComponent, MenuComponent, HeaderComponent],
  exports: [
    CarouselComponent, HeaderComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    RouterModule,
    CustomPipeModule,
    MatToolbarModule
  ]
})
export class CommonComponentsModule { }
