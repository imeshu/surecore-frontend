import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MarketplaceService} from '../../marketplace/services/marketplace.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {NotificationDataTransferService} from '../../../services/notification-data-transfer.service';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  ratingLocked = false;
  starsList: boolean[] = [false, false, false, false, false];
  starsListRatedState: boolean[] = this.starsList;

  rating = 0;
  productID: string;

  constructor(public dialogRef: MatDialogRef<RatingComponent>, private service: MarketplaceService,
              private router: Router, private route: ActivatedRoute, @Inject(MAT_DIALOG_DATA) public data: any,
              private notificationService: NotificationDataTransferService) { }

  ngOnInit() {
    this.productID = this.data.productID;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  setRatingValue(value: number) {

    // Execution order is important here. Do not change unless you know what you are doing.
    this.ratingLocked = true;
    this.hideStars();

    if (value < this.rating) {
      this.starsList = [false, false, false, false, false];
    }

    this.rating = value;
    this.showStars(value);
    this.starsListRatedState = Array.from(this.starsList);

  }

  submitRating() {
    this.service.submitRating(this.productID, this.rating).pipe(
      catchError(error => {
        return throwError(error.status + ' : ' + error.error + ' -> ' + error.message);
      })
    ).subscribe(similarResult => {
      this.notificationService.isMessageDisplayed = true;
      this.notificationService.messageContent = 'Operation completed successfully';
      this.onClose();
    });
  }

  showStars(value: number) {
    for (let i = 0; i < value ; i++) {
      this.starsList[i] = true;
    }
  }

  hideStars() {
    this.starsList = [false, false, false, false, false];

    if (this.ratingLocked) {
      this.starsList = Array.from(this.starsListRatedState);
    }

  }

}
