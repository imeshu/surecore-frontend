import { Component, OnInit, Input, KeyValueDiffer, KeyValueDiffers } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(400, style({ opacity: 1 }))
      ])
    ])
  ]
})
export class CarouselComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('images')
  private images: ImageModel[] = new Array();
  private selectedImg = '';
  private count = 0;
  private list: string[] = new Array();
  differ: KeyValueDiffer<string, any>;
  constructor(private differs: KeyValueDiffers) {
    this.differ = this.differs.find({ this: ImageModel }).create();
  }

  ngOnInit() {
    if (this.images.length !== 0) {
      this.selectedImg = this.images[0].url;
    }
    this.onImageChanged();
  }

  setImg(index: number) {
    this.selectedImg = this.images[index].url;
    this.onImageChanged();
  }

  goRight() {
    if (this.count === this.images.length - 1) {
      this.selectedImg = this.images[this.count].url;
      this.count = -1;
    }
    this.count = this.count + 1;
    this.selectedImg = this.images[this.count].url;
    this.onImageChanged();
  }

  goLeft() {
    if (this.count === 0) {
      this.selectedImg = this.images[this.count].url;
      this.count = this.images.length;
    }
    this.count = this.count - 1;
    this.selectedImg = this.images[this.count].url;
    this.onImageChanged();
  }

  onImageChanged() {
    this.list = new Array();
    this.list.push(this.selectedImg);
  }


  ngDoCheck() {
    const change = this.differ.diff(this);
    if (change) {
      change.forEachChangedItem(item => {
        if (item.key === 'images') {
          if (this.images.length !== 0) {
            this.selectedImg = this.images[0].url;
          }
          this.onImageChanged();
        }
      });
    }
  }

}

export class ImageModel {
  url: string;
}
