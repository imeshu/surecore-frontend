import {Component, OnInit} from '@angular/core';
import {SearchedDataModel} from '../models/searched-data-model';
import {Quotation2Service} from '../services/quotation2.service';

@Component({
  selector: 'app-quotation-main',
  templateUrl: './quotation-main.component.html',
  styleUrls: ['./quotation-main.component.scss']
})
export class QuotationMainComponent implements OnInit {
  private listData: SearchedDataModel[] = new Array();
  private selectedQuotation: SearchedDataModel;

  constructor(private service: Quotation2Service) {
  }

  ngOnInit() {
    this.getSearchedData();
  }

  getSearchedData() {
    this.service.getSerchList().subscribe(resp => {
      const body = resp.body as any;
      this.listData = body.data;
      // console.log(this.listData);
    });
  }

  onSelectQuotation(index: number) {
    this.selectedQuotation = this.listData[index];
    console.log(this.selectedQuotation);
  }
}
