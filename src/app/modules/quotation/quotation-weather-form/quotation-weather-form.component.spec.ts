import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationWeatherFormComponent } from './quotation-weather-form.component';

describe('QuotationWeatherFormComponent', () => {
  let component: QuotationWeatherFormComponent;
  let fixture: ComponentFixture<QuotationWeatherFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotationWeatherFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotationWeatherFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
