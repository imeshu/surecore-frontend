import { TestBed } from '@angular/core/testing';

import { Quotation2Service } from './quotation2.service';

describe('Quotation2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Quotation2Service = TestBed.get(Quotation2Service);
    expect(service).toBeTruthy();
  });
});
