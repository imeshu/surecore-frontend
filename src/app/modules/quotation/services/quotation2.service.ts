import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {HTTPCallService} from '../../../services/httpcall.service';

@Injectable({
  providedIn: 'root'
})
export class Quotation2Service {

  constructor(private http: HTTPCallService) {
  }


  getSerchList(): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/search?salesPersonCode=&status=A&classCode=MC&productCode=&quotationNumber=&customerName=&riskName=&customerCode=';
    return this.http.getResource(path);
  }

  getMasterData() {
    const path = 'uw/quotations/search?salesPersonCode=&status=A&classCode=MC&productCode=&quotationNumber=&customerName=&riskName=&customerCode=';
    return this.http.getResource(path);
  }
}
