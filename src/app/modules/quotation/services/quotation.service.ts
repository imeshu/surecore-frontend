import {Injectable} from '@angular/core';
import {HTTPCallService} from '../../../services/httpcall.service';
import {Observable} from 'rxjs';
import {HttpResponse, HttpEvent} from '@angular/common/http';
import {fileServer} from '../../../config/enum';

@Injectable({
  providedIn: 'root'
})
export class QuotationService {

  constructor(private http: HTTPCallService) {
  }

  getSerchList(): Observable<HttpResponse<Object>> {
    // tslint:disable-next-line:max-line-length
    const path = 'uw/quotations/search?salesPersonCode=&status=A&classCode=MC&productCode=&quotationNumber=&customerName=&riskName=&customerCode=';
    return this.http.getResource(path);
  }

  // Crop Types
  getCropTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/cropTypes';
    return this.http.getResource(path);
  }

  // Weather Types
  getWeatherTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/weatherTypes';
    return this.http.getResource(path);
  }

  // Plans
  getPlans(branchCode, cropTypeCode, weatherTypeCode): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/products/search?branchCode=' + branchCode + '&cropTypeCode=' +
      cropTypeCode + '&weatherTypeCode=' + weatherTypeCode;
    return this.http.getResource(path);
  }

  // Business Chanels
  getBusinessChanels(): Observable<HttpResponse<Object>> {
    const path = 'base/businessChannels';
    return this.http.getResource(path);
  }

  // Payment Types
  getPaymentTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/premiumSettlementMethods';
    return this.http.getResource(path);
  }

  // save policy
  saveQuotation(data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations';
    return this.http.postResource(path, data);
  }

  /**
   *
   *  get policy common information
   */
  getQuotation(quotationSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/commonInformation';
    return this.http.getResource(path);
  }

  getQuotationWeatherCommonInformationAndRisk(quotationSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/detailView';
    return this.http.getResource(path);
  }

  updateQuotation(sequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + sequence + '/commonInformation';
    return this.http.putResource(path, data);
  }

  getClassCode(planCode): Observable<HttpResponse<Object>> {
    const path = 'base/products/' + planCode + '/classes';
    return this.http.getResource(path);
  }

  // Mode of Business of the policy - CUSTOMER
  getModeOfBusinessCustomer(): Observable<HttpResponse<Object>> {
    const path = 'base/defaultModeOfBusinessCode';
    return this.http.getResource(path);
  }

  // Mode of Business of the policy - AGENT
  getModeOfBusinessAgent(): Observable<HttpResponse<Object>> {
    const path = 'base/users/profiles';
    return this.http.getResource(path);
  }

  // get Currency code
  getCurrencyCode(): Observable<HttpResponse<Object>> {
    const path = 'base/localCurrencyCode';
    return this.http.getResource(path);
  }


  // --------------------------- Land Details ----------------------------------------------//

  // weather stations
  getWeatherStations(): Observable<HttpResponse<Object>> {
    const path = 'base/weatherStations/search?name=';
    return this.http.getResource(path);
  }

  // crop variances
  getCropVariances(cropType): Observable<HttpResponse<Object>> {
    const path = 'base/cropTypes/' + cropType + '/cropVariances';
    return this.http.getResource(path);
  }


  // getFramers(){
  //   const path = 'base/cropTypes/'+cropType+'/cropVariances';
  //   return this.http.getResource(path);
  // }

  getLocations(customerCode): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/search?partyCode=' + customerCode;
    return this.http.getResource(path);
  }

  getLocation(locationSequence): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/' + locationSequence;
    return this.http.getResource(path);
  }


  addLocation(quotationSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/locations';
    return this.http.postResource(path, data);
  }

  addRisks(locationSequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/locations/' + locationSequence + '/risks';
    return this.http.postResource(path, data);
  }

  getRiskCommonInformation(riskSequence): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/risks/' + riskSequence + '/commonInformation';
    return this.http.getResource(path);
  }

  UpdateRiskCommonInformation(riskSequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/risks/' + riskSequence + '/commonInformation';
    return this.http.putResource(path, data);
  }


  /**
   * Live stock part - when selected on livestock from dropdown
   */

  getLiveStockTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/livestockTypes';
    return this.http.getResource(path);
  }

  getBreedTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/breedTypes';
    return this.http.getResource(path);
  }

  getGenders(): Observable<HttpResponse<Object>> {
    const path = 'base/genders';
    return this.http.getResource(path);
  }

  getUsageTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/usageTypes';
    return this.http.getResource(path);
  }

  /** insepected by */
  getUnderwritingAssessors(): Observable<HttpResponse<Object>> {
    const path = 'base/parties/uwAssessors';
    return this.http.getResource(path);
  }


  /***
   * Livestock policy
   */
  getProducts(branchCode): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/products/livestock?branchCode=' + branchCode;
    return this.http.getResource(path);
  }

  getPremiumSettlementMethods(): Observable<HttpResponse<Object>> {
    const path = 'base/premiumSettlementMethods';
    return this.http.getResource(path);
  }

  getDefaultLocation(policySequence): Observable<HttpResponse<Object>> {
    // const path = 'uw/policies/' + policySequence + '/defaultLocation';
    const path = 'uw/policies/' + policySequence + '/locations';
    return this.http.getResource(path);
  }


  // get assessors to manage
  getAssessors(riskSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/' + riskSequnce + '/assessments';
    return this.http.getResource(path);
  }

  // update assessors
  updateInspectedDetails(assessorSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/assessments/' + assessorSequnce;
    return this.http.putResource(path, data);
  }

  getAssessorDetails(assessorSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/assessments/' + assessorSequnce;
    return this.http.getResource(path);
  }

  /**
   *  Perils / Covers
   */
  saveCovers(riskSequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/risks/' + riskSequence + '/perils';
    return this.http.postResource(path, data);
  }

  getOtherPerils(riskSequence): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/risks/' + riskSequence + '/otherPerils';
    return this.http.getResource(path);
  }

  getRiskPerils(riskSequence) {
    const path = 'uw/quotations/risks/' + riskSequence + '/perils';
    return this.http.getResource(path);
  }


  saveDefaultLocation(quotationSequnce): Observable<HttpResponse<Object>> {
    const data = {};
    const path = 'uw/quotations/' + quotationSequnce + '/defaultLocation';
    return this.http.postResource(path, data);
  }

  saveExcess(perilSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/perils/' + perilSequnce + '/excesses';
    return this.http.postResource(path, data);
  }

  getRiskDocuments(riskSequence): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/risks/' + riskSequence + '/documents';
    return this.http.getResource(path);
  }

  /**
   *  view and search related services
   */

  searchedPolicies(searchValue, customerCode, salePersonCode, searchFeild, searchStatus): Observable<HttpResponse<Object>> {
    const classCode = 'MC';
    if (searchFeild === 'CN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/quotations/search?branchCode=&classCode=' + classCode + '&productCode=&quotationNumber=&customerName=' + searchValue + '&riskName=&status=' + searchStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&customerNIC=&productDescription=';
      return this.http.getResource(path);
    } else if (searchFeild === 'RN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/quotations/search?branchCode=&classCode=' + classCode + '&productCode=&quotationNumber=&customerName=&riskName=' + searchValue + '&status=' + searchStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&customerNIC=&productDescription=';
      return this.http.getResource(path);
    } else if (searchFeild === 'QN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/quotations/search?branchCode=&classCode=' + classCode + '&productCode=&quotationNumber=' + searchValue + '&customerName=&riskName=&status=' + searchStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&customerNIC=&productDescription=';
      return this.http.getResource(path);
    } else if (searchFeild === 'PN') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/quotations/search?branchCode=&classCode=' + classCode + '&productCode=&quotationNumber=&customerName=&riskName=&status=' + searchStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&customerNIC=&productDescription=' + searchValue;
      return this.http.getResource(path);
    } else if (searchFeild === 'CNIC') {
      // tslint:disable-next-line:max-line-length
      const path = 'uw/quotations/search?branchCode=&classCode=' + classCode + '&productCode=&quotationNumber=&customerName=&riskName=&status=' + searchStatus + '&salesPersonCode=' +
        salePersonCode + '&customerCode=' + customerCode + '&customerNIC=' + searchValue + '&productDescription=';
      return this.http.getResource(path);
    }
  }

  getQuotattionDetails(quotationSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce;
    return this.http.getResource(path);
  }

  /**
   * get documents - policy sequnce
   */
  getDocumentSelectedPolicySequnce(policySequence): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/documents';
    return this.http.getResource(path);
  }

  getRisksSelectedQuotationSequnce(quotationSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/locations/risks';
    return this.http.getResource(path);
  }

  /**
   * manage policy
   */
  updatePolicyDetails(policySequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence;
    return this.http.putResource(path, data);
  }

  /**
   *
   * get risk
   */
  getRiskDetails(riskSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/locations/risks/' + riskSequnce;
    return this.http.getResource(path);
  }

  getPolicyImages(policySequence): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/documents';
    return this.http.getResource(path);
  }

  uploadImage(file: File): Observable<HttpEvent<Object>> {
    const fd = new FormData();
    fd.append('file', file);
    const path = fileServer + 'v1/files/upload?path=quotation';
    return this.http.postImage(path, fd);
  }

  updateCompleteQuotation(quotationSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/completed';
    return this.http.putResource(path, data);
  }

  getCompleteQuotation(quotationSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/completed';
    return this.http.getResource(path);
  }

  updatePolicyForm(policySequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequnce;
    return this.http.putResource(path, data);
  }

  updateLand(policySequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/completed';
    return this.http.getResource(path);
  }

  // upload images
  saveWeatherImages(documentSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/documents/' + documentSequnce + '/images';
    return this.http.postResource(path, data);
  }

  getPolicyDocumentSequnce(policySequence): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/' + policySequence + '/documents';
    return this.http.getResource(path);
  }

  saveRiskImages(documentSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/documents/' + documentSequnce + '/images';
    return this.http.postResource(path, data);
  }

  getRiskDocumentSequnce(riskSequence): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/risks/' + riskSequence + '/documents';
    return this.http.getResource(path);
  }

  // get cover deatils
  getCoverDetails(perilSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/perils/' + perilSequnce;
    return this.http.getResource(path);
  }

  updateCover(perilSequnce, data) {
    const path = 'uw/risks/perils/' + perilSequnce;
    return this.http.putResource(path, data);
  }

  updateRisk(riskSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/locations/risks/' + riskSequnce;
    return this.http.putResource(path, data);
  }

  getExcess(perilSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/perils/' + perilSequnce + '/excesses';
    return this.http.getResource(path);
  }

  updateLocation(locationSequence, data): Observable<HttpResponse<Object>> {
    const path = 'uw/policies/locations/' + location;
    return this.http.putResource(path, data);
  }

  updateExcess(perilSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/perils/excesses/' + perilSequnce;
    return this.http.putResource(path, data);
  }

  downloadImages(documentSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/documents/' + documentSequnce + '/images';
    return this.http.getResource(path);
  }

  getAuthorizedQuotation(quotationSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/authorizations';
    return this.http.getResource(path);
  }

  updateAuthorizedQuotation(quotationSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/authorizations';
    return this.http.putResource(path, data);
  }

  downloadWeatherIndividualFile(quotationSequnce) {
    const path = 'uw/quotations/' + quotationSequnce + '/weatherIndexSchedule/print ';
    return this.http.downloadFile(path);
  }

  downloadWeatherGroupFile(quotationSequnce) {
    const path = 'uw/quotations/' + quotationSequnce + '/weatherGroupSchedule/print';
    return this.http.downloadFile(path);
  }

  downloadLivestockIndividualFile(quotationSequnce) {
    const path = 'uw/quotations/' + quotationSequnce + '/livestockSchedule/print';
    return this.http.downloadFile(path);
  }

  downloadLivestockGroupFile(quotationSequnce) {
    const path = 'uw/quotations/' + quotationSequnce + '/livestockGroupSchedule/print ';
    return this.http.downloadFile(path);
  }


  getUnitPriceOnRisk(weatherStation): Observable<HttpResponse<Object>> {
    const path = 'base/products/WP/unitPrices/search?weatherStationSequence=' + weatherStation;
    return this.http.getResource(path);
  }

  getLocationFromUtility(seqNo) {
    const path = 'utility/locations/' + seqNo;
    return this.http.getResource(path);
  }

  removeRisk(riskSequence): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/' + riskSequence;
    return this.http.deleteResource(path);
  }

  removeCovers(perilSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/risks/perils/' + perilSequnce;
    return this.http.deleteResource(path);
  }


  getNearestWeatherStation(data): Observable<HttpResponse<Object>> {
    const path = 'uw/nearestWeatherStation';
    return this.http.postResource(path, data);
  }

  getcustomerDetails(customerCode) {
    const path = 'base/parties/' + customerCode + '/profiles';
    return this.http.getResource(path);
  }

  addIntermediaries(quotationSequnce, data): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/intermediaries';
    return this.http.postResource(path, data);
  }

  getLocationByQuotation(quotationSequnce): Observable<HttpResponse<Object>> {
    const path = 'uw/quotations/' + quotationSequnce + '/locations';
    return this.http.getResource(path);
  }

  getWeatherindexParameters(fromYear, fromMonth, weatherStationSequnce, toYear, toMonth): Observable<HttpResponse<Object>> {
    // tslint:disable-next-line:max-line-length
    const path = 'base/weatherIndexParameters/search?fromYear=' + fromYear + '&fromMonth=' + fromMonth + '&weatherStationSequence=' + weatherStationSequnce + '&toYear=' + toYear + '&toMonth=' + toMonth;
    return this.http.getResource(path);
  }

  getWeatherProductMaxDayCount(): Observable<HttpResponse<Object>> {
    const path = 'base/products/WP';
    return this.http.getResource(path);
  }

}
