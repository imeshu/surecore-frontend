/* tslint:disable:quotemark radix no-shadowed-variable */
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { FormControl, Validators, FormGroup } from '@angular/forms';

import { HttpEventType } from '@angular/common/http';

import { stat } from 'fs';

import { ActivatedRoute, Router } from '@angular/router';
import { QuotationService } from '../../services/quotation.service';
import { NotificationDataTransferService } from 'src/app/services/notification-data-transfer.service';
import { ImageModel } from 'src/app/modules/land/models/img-model';
import { LandService } from 'src/app/modules/land/services/land.service';
import { LocationModel } from 'src/app/modules/land/models/location-model';
import { CommonType } from 'src/app/model/CommonType';
import { LandModel } from 'src/app/modules/customer/models/land-model';

@Component({
  selector: 'app-quotations',
  templateUrl: './quotations.component.html',
  styleUrls: ['./quotations.component.scss']
})
export class QuotationsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router, public service: QuotationService, public notificationService: NotificationDataTransferService, public landService: LandService) { }

  // owner name
  weightSequence = '';
  weightDescription = '';
  weightVersion = '';
  weightValue = '';

  // purchase date
  milkCapacitySequence = '';
  milkCapacityDescription = '';
  milkCapacityVersion = '';
  milkCapacityValue = null;
  today = new Date();

  // common variables
  branchCode = '';
  quotationSequnce = '';
  partyCode = '';
  asCustomer = '';
  asAgent = '';
  addressSequnce = '';
  locationSequence = '';
  cropTypeGlobal = '';
  riskSequnce = '';

  weatherDayCount = 0;
  livestockDayCount = 0;

  public quotations = [];
  public searchValue = '';
  public risks = [];
  public images = [];
  public cropTypeDescription = '';
  public weatherDescription = '';
  public channelOfBuinessDescription = '';
  public periodFromDate = null;
  public periodToDate = null;
  public paySettleModeDescription = '';
  public productName = '';
  public quotationNumber = '';
  public totalPremium = '';
  public productCode = '';
  public policyType = '';
  public productType = '';
  public customerName = '';

  // weather form
  // Crop Types
  cropTypes = [];
  cropType = '';
  // Weather Type
  weatherTypes = [];
  weatherType = '';
  // Plans
  plans = [];
  plan = '';
  // Business Chanels
  businessChanels = [];
  businessChanel = '';
  // payment types
  paymentTypes = [];
  paymentType = '';

  // weather form form-controls
  cropTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  weatherTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  planFormControl = new FormControl('', [
    Validators.required
  ]);
  businessChanelFormControl = new FormControl('', [
    Validators.required
  ]);
  paymentFormControl = new FormControl('', [
    Validators.required
  ]);
  startDateFormControl = new FormControl('', [
    Validators.required
  ]);
  endDateFormControl = new FormControl('', [
    Validators.required
  ]);
  // Policy
  classCode = '';
  currencyCode = '';
  modeOfBusinessCode = '';
  settlementModeCode = '';
  policyRemark = '';
  startDate = null;
  endDate = null;
  maxDate1 = new Date(moment().subtract(1, 'days').format());
  maxDate2 = new Date(moment().format());
  minDate = new Date();
  endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());


  // Weather Type
  weatherSequenceNo = '';
  weatherVersion = '';
  weatherValue = '';

  // Crop Type
  cropTypeSequence = '';
  cropTypeVersion = '';
  cropTypeValue = '';

  // Livestock form
  // products
  products = [];
  product = '';
  // business channels
  liveStickBusinessChannels = [];
  liveStickBusinessChannel = '';
  // premium settlement methods
  premiumSettlementMethods = [];
  premiumSettlementMethod = '';

  liveStockStartDate = null;
  liveStockEndDate = null;

  livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());


  // Livestock form controls
  liveStockPlanFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStickBusinessChannelFormControl = new FormControl('', [
    Validators.required
  ]);
  premiumSettlementMethodFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStockStartDateFormControl = new FormControl('', [
    Validators.required
  ]);
  liveStockEndDateFormControl = new FormControl('', [
    Validators.required
  ]);


  // weather land
  // policy additional details


  // policy weather land form related variables
  locations = [];
  location = {};

  cropVariances = [];
  cropVariance = '';

  weatherStations = [];
  weatherStation = 0;

  weatherLandPhotos = [];


  // location object
  locationAddress = '';
  locationOrderNo = 1;


  // risk
  riskOrder = 1;
  riskName = '';
  riskRefNo = 1;
  riskSectionCode = '';
  riskSumInsured = '';
  effectiveDate = null;
  effectiveToDate = null;
  size1 = '';
  size1UnitType = '';
  size2 = '';
  size2UnitType = '';
  weatherLandStartDate = null;
  weatherLandEndDate = null;
  farmerName = '';
  weatherLandPremium = 0;
  markLandPlot = [];
  markLandPlotString = '';



  // crop variance
  cropVarianceSequence = '';
  cropVarianceDescription = '';
  cropVarianceVersion = '';
  cropVarianceValue = '';

  // weather stations
  weatherStationSequence = '';
  weatherStationDescription = '';
  weatherStationVersion = '';
  weatherStationValue = '';

  // no of unsits
  noOfUnitsSequence = '';
  noOfUnitsDescription = '';
  noOfUnitsVersion = '';
  noOfUnitsValue = '';

  farmerNameSequnce = '';
  farmerNameDescription = '';
  farmerNameVersion = '';
  farmerNameValue = '';



  // policy weather land form controls
  locationFormControl = new FormControl('', [
    Validators.required
  ]);

  cropVarianceFormControl = new FormControl('', [
    Validators.required
  ]);


  landWeatherStationFormControl = new FormControl('', [
    Validators.required
  ]);

  farmerNameFormControl = new FormControl('', [
    Validators.required
  ]);
  weatherLandStartDateFormControl = new FormControl('', [
    Validators.required
  ]);
  weatherLandEndDateFormControl = new FormControl('', [
    Validators.required
  ]);
  size1UnitTypeFormControl = new FormControl('', [
    Validators.required
  ]);
  size1FormControl = new FormControl('', [
    Validators.required
  ]);
  markLandPlotFormControl = new FormControl('', [
    Validators.required
  ]);
  noOfunitsFormControl = new FormControl('', [
    Validators.required
  ]);
  priceFormControl = new FormControl('', [
    Validators.required
  ]);



  weightFormControl = new FormControl('', [
    Validators.required
  ]);

  milkFormControl = new FormControl('', [
    Validators.required
  ]);




  // add live stock form
  newLivestockStartDate = null;
  // live stock types
  liveStockTypes = [];
  liveStockType = '';

  // Breed types
  breedTypes = [];
  breedType = '';

  // genders
  genders = [];
  gender = '';

  // usage types
  usageTypes = [];
  usageType = '';

  // undewriting assessors
  undewritingAssessors = [];
  undewritingAssessor = '';



  age = '';

  tagNumber = '';
  ownerName = '';
  livestockPurchaseDate = null;

  // Live stock
  liveStockTypeSequnce = '';
  liveStockTypeDescription = '';
  liveStockTypeVersion = '';
  liveStockTypeValue = '';

  // Breed
  breedSequence = '';
  breedDescription = '';
  breedVersion = '';
  breedValue = '';

  // gender
  genderSequence = '';
  genderDescription = '';
  genderVersion = '';
  genderValue = '';

  // usage
  usageSequence = '';
  usageDescription = '';
  usageVersion = '';
  usageValue = '';

  //  age in years
  ageInYearsSequence = '';
  ageInYearsDescription = '';
  ageInYearsVersion = '';
  ageInYearsValue = this.age;

  // owner name
  ownerNameSequence = '';
  ownerNameDescription = '';
  ownerNameVersion = '';
  ownerNameValue = '';

  // purchase date
  purchaseDateSequence = '';
  purchaseDateDescription = '';
  purchaseDateVersion = '';
  purchaseDateValue = null;




  tagNumberFormControl = new FormControl('', [
    Validators.required
  ]);

  breedTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  liveStockTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  genderTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  ageTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  ownerNameFormControl = new FormControl('', [
    Validators.required
  ]);

  usageTypeFormControl = new FormControl('', [
    Validators.required
  ]);

  undewritingAssessorFormControl = new FormControl('', [
    Validators.required
  ]);

  newLivestockStartDateFormControl = new FormControl('', [
    Validators.required
  ]);

  riskSumInsuredFormControl = new FormControl('', [
    Validators.required
  ]);

  /**
 *  covers / Perils
 */
  otherPerils = [];
  otherPeril = {};

  covers = [];
  riskImages = [];

  perilCode = '';
  perilPerilSumInsured = 0;
  perilPercentage = 0;
  perilRate = 0;

  perilSequnce = '';

  excessAmount = 0;
  excessPercentage = 0;
  riskPremium = '';

  otherPerilFormControl = new FormControl('', [
    Validators.required
  ]);

  perilPerilSumInsuredFormControl = new FormControl('', [
    Validators.required
  ]);
  excessPercentageFormControl = new FormControl('', [
    Validators.required
  ]);
  excessAmountFormControl = new FormControl('', [
    Validators.required
  ]);

  managePerilDescription = '';


parentPage = 'q';

  // views
  public displayManageWeatherForm = false;
  public displayPolicyDetails = true;
  public displayManageLivestockForm = false;
  public displayManageLandForm = false;
  public displayLivestockAdditionalDetails = false;
  public displayManageCoversForm = false;
  public displayLivestockRiskDetailsDiv = false;

  public isDataLoaded = true;

  public authorizedPolicy = false;


  // selected value
  selectedValue = 'quotationNumber';
  // seletedstatus
  selectedStatus = 'all';

  searchSelectCriteria = '';
  searchSelectedStatus = '';

  // Add land page hide and show variables
  viewAddLand = true;
  viewAddCultivation = true;
  viewAddLandPhotos = true;

  disabledLocation = false;

  displayPayment = false;

  debitNoteNumber = '';

  customerCode = '';

  landName = '';
  address = '';
  landSize = '';
  noOfUnits = '';

  quotationStatus = '';

  /**
    *
    *  policy weather land
    */
  landEdit = true;

  manageLivestockRisk = true;
  documentSequnce = '';
  manageCover = true;

  private progressBar: string;
  private imagList: ImageModel[] = new Array();


  // add land form - from land module
  private districtsList: CommonType[] = new Array();
  private divisionsList: CommonType[] = new Array();
  private gramasevaDivisionsList: CommonType[] = new Array();
  private getagrarianServiceCentersList: CommonType[] = new Array();
  private ownershipTypesList: CommonType[] = new Array();

  private selectedDistrict: string;
  private selectedDivision: string;
  private selectedGramasevaDivision: string;
  private selectedgetagrarianServiceCenter: string;
  private selectedOwnershipType: string;

  private isDrawMap = false;
  private pathList: LocationModel[] = new Array();
  private pathString = '';

  landForm = new FormGroup({
    plotName: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    division: new FormControl('', Validators.required),
    gramasevaDivision: new FormControl('', Validators.required),
    agrarianServiceCenter: new FormControl('', Validators.required),
    sizeInAcres: new FormControl('', Validators.required),
    sizeInPerches: new FormControl('', Validators.required),
    ownership: new FormControl('', Validators.required),
    geometry: new FormControl('', Validators.required),
  });

  milkCapacity = '';
  weight = '';

  ngOnInit() {
    // get party information form session storage
    this.partyCode = sessionStorage.getItem('partyCode');
    this.asCustomer = sessionStorage.getItem('asCustomer');
    this.asAgent = sessionStorage.getItem('asAgent');
    this.branchCode = sessionStorage.getItem('branchCode');
    //  this.addressSequnce = sessionStorage.getItem('addressSequnce');
    this.searchSelectCriteria = 'QN';
    this.searchSelectedStatus = '';
    this.getSearchedPolicies();
  }


  getSearchedPolicies() {
    this.clearCommonPlaces();
    this.quotations = [];
    let customerCode = '';
    let salePersonCode = '';
    if (this.asCustomer === 'Y') {
      customerCode = this.partyCode;
      salePersonCode = '';
    } else if (this.asAgent === 'Y') {
      salePersonCode = this.partyCode;
      customerCode = ''; // sessionStorage.getItem('customerCode');
    }
    // tslint:disable-next-line:max-line-length
    this.service.searchedPolicies(this.searchValue, customerCode, salePersonCode, this.searchSelectCriteria, this.searchSelectedStatus).subscribe(resp => {
      const body = resp.body as any;
      if (body.data.length === 0) {
        this.quotations = [];
        this.isDataLoaded = false;
      } else {
        this.quotations = body.data;
        this.quotationSequnce = this.quotations[0]['sequence'];
        this.getpolicyDetails(this.quotationSequnce);
        this.isDataLoaded = false;
      }


    }, error => {
      this.isDataLoaded = false;

    });
  }

  getpolicyDetails(data) {
    this.clearCommonPlaces();
    this.imagList = [];
    this.isDataLoaded = true;
    this.clearData();
    this.quotationSequnce = data;
    const documentSequnce = '';
    this.clearCommonPlaces();
    // alert(' on search'+this.quotationSequnce);
    this.service.getQuotattionDetails(this.quotationSequnce).subscribe(resp => {
      const body1 = resp.body as any;

      this.periodFromDate = moment(body1.data.fromDate, 'YYYY-MM-DD HH:mm:ss').format(' DD MMMM YYYY');
      this.periodToDate = moment(body1.data.toDate, 'YYYY-MM-DD HH:mm:ss').format(' DD MMMM YYYY');
      this.startDate = new Date(body1.data.fromDate);
      this.endDate = new Date(body1.data.toDate);
      this.liveStockStartDate = new Date(body1.data.fromDate);
      this.liveStockEndDate = new Date(body1.data.toDate);
      this.productName = body1.data.productDescription;
      this.productCode = body1.data.productCode;
      this.quotationNumber = body1.data.quotationNumber;
      this.totalPremium = body1.data.totalPremium;
      this.productType = body1.data.productType;
      this.customerName = body1.data.customerName;
      this.productType = body1.data.productType;
      this.totalPremium = body1.data.totalPremium;
      this.quotationStatus = body1.data.quotationStatus;
      // set values
      // set customer code
      this.customerCode = body1.data.customerCode;
      // check  policy completed one or not
      const completed = body1.data.completed.completed;
      if (completed === true) {
        this.authorizedPolicy = true;
      } else {
        this.authorizedPolicy = false;
      }


      this.service.getQuotationWeatherCommonInformationAndRisk(this.quotationSequnce).subscribe(resp => {
        const body2 = resp.body as any;

        // filter weather type and crop type
        for (let i = 0; i < body2.data.quotation.quotationInformation.length; i++) {
          const obj = body2.data.quotation.quotationInformation[i];
          if (obj['description'] === 'CROP') {
            this.cropTypeDescription = obj['valueDescription'];
            this.cropType = obj['value'];
          }
          if (obj['description'] === 'WEATHER TYPE') {
            this.weatherDescription = obj['valueDescription'];
            this.weatherType = obj['value'];
          }
        }
        if (body2.data.location.length > 0) {
          for (let i = 0; i < body2.data.location.length; i++) {
            const obj = body2.data.location[i];
            this.address = obj['locationAddress'];
            for (let j = 0; j < obj['risks'].length; j++) {
              const commonInfoObj = obj['risks'][j];
              for (let k = 0; k < commonInfoObj['riskInformation'].length; k++ ) {
                if (commonInfoObj['riskInformation'][k]['description'] === 'WEATHER STATION') {
                  this.weatherStationDescription = commonInfoObj['riskInformation'][k]['valueDescription'];
                }
              }

              if (this.landSize !== null) {
                this.landSize = commonInfoObj['size1'] + ' Acres / ' + commonInfoObj['size1UnitType'] + ' Perches';
              }

              this.landName = commonInfoObj['riskName'];
              if (this.noOfUnits !== null) {
                this.noOfUnits = commonInfoObj['size2'] + ' Units  / LKR ' + commonInfoObj['size2UnitType'];
              }
            }

          }
        } else {
          this.landSize = '';
          this.noOfUnits = '';
        }

        // get risks
        this.service.getRisksSelectedQuotationSequnce(this.quotationSequnce).subscribe(resp => {
          const body3 = resp.body as any;
          this.risks = body3.data;
          this.getRiskAdditionalDetails();
          this.isDataLoaded = false;
          this.displayLivestockAdditionalDetails = false;
          this.displayManageCoversForm = false;
          this.displayManageLandForm = false;
          this.displayManageLivestockForm = false;
          this.displayPolicyDetails = true;
          this.displayManageWeatherForm = false;
          this.displayLivestockRiskDetailsDiv = false;
        }, error => {
          this.isDataLoaded = false;
        });
      }, error => {
        this.isDataLoaded = false;
      });
    }, error => {
      this.isDataLoaded = false;
    });
  }

  managePolicy() {
    this.clearWeatherPolicyForm();
    this.clearCommonPlaces();
    // alert(this.quotationSequnce);
    // this.quotationSequnce = data;
    this.displayManageWeatherForm = true;
    this.displayPolicyDetails = false;
    this.displayLivestockRiskDetailsDiv = false;

    // get weather form lovs
    this.getBusinessChanels();
    this.getCropTypes();
    this.getPaymentTypes();
    this.getWeatherTypes();


    // get livestock form lovs
    this.loadproducts();
    this.loadBusinessChannels();
    this.loadSettlementMethods();
    this.getPolicyFormDetails();


  }

  /** weather form related functions */

  // Crop Types
  getCropTypes() {
    this.service.getCropTypes().subscribe(resp => {
      const body = resp.body as any;
      this.cropTypes = body.data;
    }), error => {

    };
  }
  getWeatherTypes() {
    this.service.getWeatherTypes().subscribe(resp => {
      const body = resp.body as any;
      this.weatherTypes = body.data;
    }, error => {

    });
  }
  getPlans() {
    const branchCode = this.branchCode;
    this.plans = [];
    if (this.cropType !== '' && this.weatherType !== '') {
      this.service.getPlans(branchCode, this.cropType, this.weatherType).subscribe(resp => {
        const body = resp.body as any;
        this.plans = body.data;
      }, error => {

      });
    }

  }
  getBusinessChanels() {
    this.service.getBusinessChanels().subscribe(resp => {
      const body = resp.body as any;
      this.businessChanels = body.data;
    }, error => {

    });
  }
  getPaymentTypes() {
    this.service.getPaymentTypes().subscribe(resp => {
      const body = resp.body as any;
      this.paymentTypes = body.data;
    }, error => {

    });
  }


  // *** Livestock functions
  loadproducts() {
    this.service.getProducts(this.branchCode).subscribe(resp => {
      const body = resp.body as any;
      this.products = body.data;
    }, error => {

    });
  }
  loadBusinessChannels() {
    this.service.getBusinessChanels().subscribe(resp => {
      const body = resp.body as any;
      this.liveStickBusinessChannels = body.data;
    }, error => {

    });
  }
  loadSettlementMethods() {
    this.service.getPremiumSettlementMethods().subscribe(resp => {
      const body = resp.body as any;
      this.premiumSettlementMethods = body.data;
    }, error => {

    });
  }
  backToPolicyDetails() {
    this.displayManageWeatherForm = false;
    this.displayPolicyDetails = true;
    this.displayManageLandForm = false;
    this.displayManageLivestockForm = false;
    this.displayLivestockAdditionalDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    const sequence = this.quotationSequnce;
    this.getpolicyDetails(sequence);
  }

  backToLiveStockAdditinalDetails() {
    this.displayManageWeatherForm = false;
    this.displayPolicyDetails = false;
    this.displayLivestockAdditionalDetails = true;
    this.displayManageLandForm = false;
    this.displayManageLivestockForm = true;
    this.displayManageCoversForm = false;
    this.displayLivestockRiskDetailsDiv = false;
    const sequence = this.quotationSequnce;
    this.getRiskAdditionalDetails();
  }


  clearData() {
    this.quotationSequnce = '';
    this.risks = [];
    this.images = [];
    this.cropTypeDescription = '';
    this.weatherDescription = '';
    this.channelOfBuinessDescription = '';
    this.periodFromDate = null;
    this.periodToDate = null;
    this.paySettleModeDescription = '';
    this.productName = '';
    this.quotationNumber = '';
    this.totalPremium = '';
    this.productCode = '';
    this.customerName = '';
  }


  getPolicyFormDetails() {
    this.isDataLoaded = true;
    this.service.getQuotattionDetails(this.quotationSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.businessChanel = body.data.channelOfBuinessCode;
      if (this.productType === 'WHINX') {
        this.startDate = new Date(body.data.periodFromDate);
        this.endDate = new Date(body.data.periodToDate);
        this.setEndDate();
        this.setDayCountWeather();
      }


      this.paymentType = body.data.settlementModeCode;
      if (this.productType === 'LIVST') {
        this.liveStockStartDate = new Date(body.data.periodFromDate);
        this.liveStockEndDate = new Date(body.data.periodToDate);
        this.setLivestockPolicyEndDate();
        this.setDayCountLivestock();
      }

      this.businessChanel = body.data.channelOfBuinessCode;
      this.premiumSettlementMethod = body.data.settlementModeCode;
      this.productCode = body.data.productCode;
      this.productType = body.data.productType;
      this.loadproducts();
      this.product = body.data.productCode;
      this.liveStickBusinessChannel = body.data.channelOfBuinessCode;

      if (this.productType === 'WHINX') {

        this.service.getQuotation(this.quotationSequnce).subscribe(resp => {
          const body4 = resp.body as any;
          const data = body4.data;

          // filter weather type and crop type
          for (let i = 0; i < data.length; i++) {
            const obj = data[i];
            if (obj['description'] === 'CROP') {
              // this.cropTypeSequence = obj['sequenceNo'];
              // this.cropTypeVersion = obj['version'];
              // this.cropTypeDescription = obj['description'];
              this.cropType = obj['value'];
              this.cropTypeGlobal = obj['value'];
            }
            if (obj['description'] === 'WEATHER TYPE') {
              // this.weatherSequenceNo = obj['sequenceNo'];
              // this.weatherVersion = obj['version'];
              // this.weatherDescription = obj['description'];
              this.weatherType = obj['value'];
            }
            if (obj['description'] === 'FARMER NAME') {
              this.farmerName = obj['value'];
            }

            setTimeout(() => {
              this.isDataLoaded = false;
            }, 2000);

          }
          const branchCode = this.branchCode;
          this.plans = [];
          if (this.cropType !== '' && this.weatherType !== '') {
            this.service.getPlans(branchCode, this.cropType, this.weatherType).subscribe(resp => {
              const body5 = resp.body as any;
              this.plans = body5.data;
              this.plan = body.data.productCode;
            }, error => {

            });
          }

        });

      }
      this.isDataLoaded = false;
    }, error => {
      this.isDataLoaded = false;

    });
  }

  updatePolicyWeather() {

    this.service.getQuotattionDetails(this.quotationSequnce).subscribe(resp => {
      const body1 = resp.body as any;


      const obj = {
        "sequence": this.quotationSequnce,
        "classCode": body1.data.classCode,
        "productCode": this.plan,
        "branchCode": body1.data.branchCode,
        "currencyCode": body1.data.currencyCode,
        "customerAddressSequence": body1.data.customerAddressSequence,
        "proposalNumber": body1.data.proposalNumber,
        "policyNumber": body1.data.policyNumber,
        "periodFromDate": moment(this.startDate).format('YYYY-MM-DD HH:mm:ss'),
        "periodToDate": moment(this.endDate).format('YYYY-MM-DD HH:mm:ss'),
        "modeOfBusinessCode": body1.data.modeOfBusinessCode,
        "settlementModeCode": this.paymentType,
        "debtorCode": body1.data.debtorCode,
        "channelOfBuinessCode": body1.data.channelOfBuinessCode,
        "policyRemark": body1.data.policyRemark,
        "customerCode": body1.data.customerCode,
        "fromNarration": body1.data.fromNarration,
        "toNarration": body1.data.toNarration,
        "status": body1.data.status,
        "sumInsured": body1.data.sumInsured,
        "eventLimit": body1.data.eventLimit,
        "eventPercentage": body1.data.eventPercentage,
        "eventNarration": body1.data.eventNarration,
        "annualLimit": body1.data.annualLimit,
        "annualPercentage": body1.data.annualPercentage,
        "annualNarration": body1.data.annualNarration,
        "premium": body1.data.premium,
        "totalPremium": body1.data.totalPremium,
        "transactionAmount": body1.data.transactionAmount,
        "totalTransactionAmount": body1.data.totalTransactionAmount,
        "reinsuranceExtensionRequiredFlag": body1.data.reinsuranceExtensionRequiredFlag,
        "transactionType": body1.data.transactionType,
        "currencyDate": body1.data.currencyDate,
        "authorisedDate": body1.data.authorisedDate,
        "creditAuthRequiredFlag": body1.data.creditAuthRequiredFlag,
        "restrictListAuthRequiredFlag": body1.data.restrictListAuthRequiredFlag,
        "reinsuranceAuthRequiredFlag": body1.data.reinsuranceAuthRequiredFlag,
        "version": body1.data.version,
        "quotationSequence": body1.data.quotationSequence,
        "quotationNumber": body1.data.quotationNumber,
        "quotationVersionNumber": body1.data.quotationVersionNumber
      };
      this.service.updatePolicyForm(this.quotationSequnce, obj).subscribe(resp => {
        const body3 = resp.body as any;
        this.service.getQuotation(this.quotationSequnce).subscribe(resp => {
          const body4 = resp.body as any;
          const data = body4.data;
          // filter weather type and crop type
          for (let i = 0; i < data.length; i++) {
            const obj = data[i];
            if (obj['description'] === 'CROP') {
              this.cropTypeSequence = obj['sequenceNo'];
              this.cropTypeVersion = obj['version'];
              this.cropTypeDescription = obj['description'];
            }
            if (obj['description'] === 'WEATHER TYPE') {
              this.weatherSequenceNo = obj['sequenceNo'];
              this.weatherVersion = obj['version'];
              this.weatherDescription = obj['description'];
            }
          }

          // update function
          const updated_josn = [{
            'sequenceNo': this.weatherSequenceNo,
            'description': this.weatherDescription,
            'value': this.weatherType,
            'version': this.weatherVersion
          }, {
            'sequenceNo': this.cropTypeSequence,
            'description': this.cropTypeDescription,
            'value': this.cropType,
            'version': this.cropTypeVersion
          }];

          // tslint:disable-next-line:no-shadowed-variable
          this.service.updateQuotation(this.quotationSequnce, updated_josn).subscribe(resp => {
            const body5 = resp.body as any;
            this.displaySuccessMessage(body5);
            this.getpolicyDetails(this.quotationSequnce);
            this.displayPolicyDetails = true;
            this.displayManageWeatherForm = false;
            this.displayLivestockRiskDetailsDiv = false;
            //   this.router.navigate(['/policies']);
          }, error => {
            this.displayErrorMessage(error);
          });


        }, error => {
          this.displayErrorMessage(error);
        });
      }, error => {

      });
    }, error => {

    });

  }

  updatePolicyLivestock() {
    this.service.getQuotattionDetails(this.quotationSequnce).subscribe(resp => {
      const body1 = resp.body as any;


      const obj = {
        "sequence": this.quotationSequnce,
        "classCode": body1.data.classCode,
        "productCode": this.productCode,
        "branchCode": body1.data.branchCode,
        "currencyCode": body1.data.currencyCode,
        "customerAddressSequence": body1.data.customerAddressSequence,
        "proposalNumber": body1.data.proposalNumber,
        "policyNumber": body1.data.policyNumber,
        "periodFromDate": moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
        "periodToDate": moment(this.liveStockEndDate).format('YYYY-MM-DD HH:mm:ss'),
        "modeOfBusinessCode": body1.data.modeOfBusinessCode,
        "settlementModeCode": this.premiumSettlementMethod,
        "debtorCode": body1.data.debtorCode,
        "channelOfBuinessCode": this.businessChanel,
        "policyRemark": body1.data.policyRemark,
        "customerCode": body1.data.customerCode,
        "fromNarration": body1.data.fromNarration,
        "toNarration": body1.data.toNarration,
        "status": body1.data.status,
        "sumInsured": body1.data.sumInsured,
        "eventLimit": body1.data.eventLimit,
        "eventPercentage": body1.data.eventPercentage,
        "eventNarration": body1.data.eventNarration,
        "annualLimit": body1.data.annualLimit,
        "annualPercentage": body1.data.annualPercentage,
        "annualNarration": body1.data.annualNarration,
        "premium": body1.data.premium,
        "totalPremium": body1.data.totalPremium,
        "transactionAmount": body1.data.transactionAmount,
        "totalTransactionAmount": body1.data.totalTransactionAmount,
        "reinsuranceExtensionRequiredFlag": body1.data.reinsuranceExtensionRequiredFlag,
        "transactionType": body1.data.transactionType,
        "currencyDate": body1.data.currencyDate,
        "authorisedDate": body1.data.authorisedDate,
        "creditAuthRequiredFlag": body1.data.creditAuthRequiredFlag,
        "restrictListAuthRequiredFlag": body1.data.restrictListAuthRequiredFlag,
        "reinsuranceAuthRequiredFlag": body1.data.reinsuranceAuthRequiredFlag,
        "version": body1.data.version,
        "quotationSequence": body1.data.quotationSequence,
        "quotationNumber": body1.data.quotationNumber,
        "quotationVersionNumber": body1.data.quotationVersionNumber
      };
      this.service.updatePolicyForm(this.quotationSequnce, obj).subscribe(resp => {
        const body3 = resp.body as any;
        this.displaySuccessMessage(body3);
        this.getpolicyDetails(this.quotationSequnce);
        this.displayPolicyDetails = true;
        this.displayManageLivestockForm = false;
        this.displayManageWeatherForm = false;
        this.displayLivestockRiskDetailsDiv = false;
      }, error => {

      });
    }, error => {

    });
  }

  manageLand(data) {
    this.clearLandForm();
    this.clearCommonPlaces();
    this.landEdit = true;
    this.riskSequnce = data;
    this.getLocations();
    this.getWeatherStations();
    this.getCropVariance(this.cropType);
    this.displayManageLandForm = true;
    this.displayLivestockRiskDetailsDiv = false;
    this.displayPolicyDetails = false;
    this.getLandDetails();
  }
  getPolicyLocation() {

  }

  getLocations() {
    this.locations = [];
    this.service.getLocations(this.customerCode).subscribe(resp => {
      const body = resp.body as any;
      this.locations = body.data;

    }, error => {

    });
  }

  // get weather stations
  getWeatherStations() {
    this.service.getWeatherStations().subscribe(resp => {
      const body = resp.body as any;
      this.weatherStations = body.data;
    }, error => {

    });
  }
  // get crop variances
  getCropVariance(data) {
    this.service.getCropVariances(data).subscribe(resp => {
      const body = resp.body as any;
      this.cropVariances = body.data;
    }, error => {

    });
  }


  // livestock form
  //  manage livestock form

  manageLivestock(data) {
    this.clearLivestockForm();
    this.manageLivestockRisk = true;
    this.clearCommonPlaces();
    this.riskSequnce = data;
    this.displayManageLivestockForm = true;
    this.displayLivestockAdditionalDetails = true;
    this.getRiskAdditionalDetails();
    this.displayPolicyDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    // get dropdown data
    this.getLiveStockTypes();
    this.getBreedTypes();
    this.getGenders();
    this.getUsageTypes();
    this.getUnderwritingAssessors();

    this.getLivestockDetails();

  }


  getLiveStockTypes() {
    this.service.getLiveStockTypes().subscribe(resp => {
      const body = resp.body as any;
      this.liveStockTypes = body.data;
    }, error => {

    });
  }

  getBreedTypes() {
    this.service.getBreedTypes().subscribe(resp => {
      const body = resp.body as any;
      this.breedTypes = body.data;
    }, error => {

    });
  }

  getGenders() {
    this.service.getGenders().subscribe(resp => {
      const body = resp.body as any;
      this.genders = body.data;
    }, error => {

    });
  }

  getUsageTypes() {
    this.service.getUsageTypes().subscribe(resp => {
      const body = resp.body as any;
      this.usageTypes = body.data;
    }, error => {

    });
  }

  getUnderwritingAssessors() {
    this.service.getUnderwritingAssessors().subscribe(resp => {
      const body = resp.body as any;
      this.undewritingAssessors = body.data;
    }, error => {

    });
  }

  // risk additibal details
  getRiskAdditionalDetails() {
    this.riskImages = [];
    this.covers = [];
    const documentSequnce = '';
    // get risk images
    // this.service.getRiskDocuments(this.riskSequnce).subscribe(resp => {
    //   const body = resp.body as any;
    //   documentSequnce = body.data[0]['sequence'];

    //   this.service.downloadImages(documentSequnce).subscribe(resp => {
    //     const body1 = resp.body as any;
    //     this.riskImages = body1.data;
    //     if (this.riskImages.length > 0) {
    //       for (let i = 0; i < this.riskImages.length; i++) {
    //         let im = new ImageModel();
    //         im.imageKey = this.riskImages[i]['key'];
    //         im.value = this.riskImages[i]['url'];
    //         this.imagList.push(im);
    //       }
    //     }


        this.service.getRiskPerils(this.riskSequnce).subscribe(resp => {
          const body1 = resp.body as any;
          this.covers = body1.data;
          this.service.getQuotattionDetails(this.quotationSequnce).subscribe(resp => {
            const body2 = resp.body as any;
            this.totalPremium = body2.data.totalPremium;
            this.riskPremium = body2.data.totalPremium;
            this.isDataLoaded = false;


          }, error => {
            this.isDataLoaded = false;

          });
        }, error => {

        });
    //   }, error => {
    //
    //   })




    // }, error => {
    //
    // });

  }

  // get livestock data
  getLandDetails() {


    this.service.getRiskDetails(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.service.getLocations(this.customerCode).subscribe(resp => {
        this.locations = [];
        const body1 = resp.body as any;
        this.locations = body1.data;
        top: for (let i = 0; i < this.locations.length; i++) {
          if (this.locations[i]['address'] === body.data.location.location) {
            this.location = this.locations[i];
            this.getSelectedLocation(this.location);
            break top;

          }
        }
        // risk
        this.riskOrder = body.data.riskOrder;
        this.riskName = body.data.riskName;
        this.farmerName = body.data.riskName;
        this.riskRefNo = body.data.riskRefNo;
        this.riskSectionCode = body.data.riskSectionCode;
        this.riskSumInsured = body.data.riskSumInsured;
        this.effectiveDate = new Date(body.data.effectiveDate);
        this.effectiveToDate = new Date(body.data.effectiveDate);
        this.size1 = body.data.size1;
        this.size1UnitType = body.data.size1UnitType;
        this.size2 = body.data.size2;
        this.size2UnitType = body.data.size2UnitType;
        this.weatherLandStartDate = new Date(body.data.effectiveDate);
        this.weatherLandPremium = body.data.totalPremium;
        this.weatherLandEndDate = new Date
          (body.data.effectiveToDate);

        // get risk commont information
        this.service.getRiskCommonInformation(this.riskSequnce).subscribe(resp => {
          const body_3 = resp.body as any;
          const resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            const obj = resultArr[i];
            if (obj['description'] === 'CROP VARIANCE') {
              // crop variance
              this.cropVariance = obj['value'];
            }
            if (obj['description'] === 'WEATHER STATION') {
              this.weatherStation = parseInt(obj['value']);
            }
            if (obj['description'] === 'FARMER NAME') {
              this.farmerName = obj['value'];
            }
          }


          // get units and price
          this.service.getUnitPriceOnRisk(this.weatherStation).subscribe(resp => {
            const body_4 = resp.body as any;
            // this.size2 = body_4.data.unitPrice;
            this.size2UnitType = body_4.data.unitPrice;

            this.riskImages = [];
            this.covers = [];
            let documentSequnce = '';
            // get risk images
            this.service.getRiskDocuments(this.riskSequnce).subscribe(resp => {
              const body = resp.body as any;
              documentSequnce = body.data[0]['sequenceNo'];

              this.service.downloadImages(documentSequnce).subscribe(resp => {
                const body1 = resp.body as any;
                this.weatherLandPhotos = body1.data;

                for (let i = 0; i < this.weatherLandPhotos.length; i++) {
                  const im = new ImageModel();
                  im.imageKey = this.weatherLandPhotos[i]['key'];
                  im.value = this.weatherLandPhotos[i]['url'];
                  this.imagList.push(im);
                }
              }, error => {

              });
            }, error => {

            });

          }, error => {

          });


        }, error => {
          this.displayErrorMessage(error);

        });
      }, error => {

      });
      this.locations = [];



    }, error => {

    });
  }


  /////  add new land realted functions
  addLand() {

    this.clearLandForm();
    this.disabledLocation = false;
    this.clearCommonPlaces();
    this.landEdit = false;
    this.getLocations();
    this.getWeatherStations();
    this.getCropVariance(this.cropType);
    this.displayManageLandForm = true;
    this.displayPolicyDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.riskRefNo = this.riskRefNo + 1;

  }

  saveWeatherLand() {
    this.isDataLoaded = true;
    this.riskRefNo = this.riskRefNo + 1;


    const locationObj = {
      "location": this.location['address'],
      "locationOrder": this.locationOrderNo
    };

    this.service.addLocation(this.quotationSequnce, locationObj).subscribe(resp => {

      const body_1 = resp.body as any;
      this.locationSequence = body_1.data;

      // add risk

      const riskObj = {
        "riskOrder": null,
        "riskName": this.location['plotName'],
        "riskRefNo": null,
        "riskSumInsured": this.riskSumInsured,
        "effectiveDate": moment(this.weatherLandStartDate).format('YYYY-MM-DD HH:mm:ss'),
        "effectiveToDate": moment(this.weatherLandEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        "size1": this.size1,
        "size1UnitType": this.size1UnitType,
        "size2": this.size2,
        "size2UnitType": this.size2UnitType
      };

      this.service.addRisks(this.locationSequence, riskObj).subscribe(resp => {
        const body_2 = resp.body as any;
        this.riskSequnce = body_2.data;

        // get risk commont information
        this.service.getRiskCommonInformation(this.riskSequnce).subscribe(resp => {
          const body_3 = resp.body as any;
          const resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            const obj = resultArr[i];
            if (obj['description'] === 'NO OF UNITS') {
              this.noOfUnitsSequence = obj['sequenceNo'];
              this.noOfUnitsDescription = obj['description'];
              this.noOfUnitsVersion = obj['version'];
              this.noOfUnitsValue = this.size2;
            } else if (obj['description'] === 'CROP VARIANCE') {
              // crop variance
              this.cropVarianceSequence = obj['sequenceNo'];
              this.cropVarianceDescription = obj['description'];
              this.cropVarianceVersion = obj['version'];
              this.cropVarianceValue = this.cropVariance;
            } else if (obj['description'] === 'WEATHER STATION') {
              // weather stations
              this.weatherStationSequence = obj['sequenceNo'];
              this.weatherStationDescription = obj['description'];
              this.weatherStationVersion = obj['version'];
              this.weatherStationValue = this.weatherStation + '';
            } else if (obj['description'] === 'FARMER NAME') {
              this.farmerNameSequnce = obj['sequenceNo'];
              this.farmerNameDescription = obj['description'];
              this.farmerNameVersion = obj['version'];
              this.farmerNameValue = this.farmerName;
            }

          }
          // update common references
          const jsonObj = [{
            "sequenceNo": this.noOfUnitsSequence,
            "description": this.noOfUnitsDescription,
            "value": this.noOfUnitsValue,
            "version": this.noOfUnitsVersion
          },
          {
            "sequenceNo": this.cropVarianceSequence,
            "description": this.cropVarianceDescription,
            "value": this.cropVariance,
            "version": this.cropVarianceVersion
          },
          {
            "sequenceNo": this.weatherStationSequence,
            "description": this.weatherDescription,
            "value": this.weatherStation,
            "version": this.weatherStationVersion
          },
          {
            "sequenceNo": this.farmerNameSequnce,
            "description": this.farmerNameDescription,
            "value": this.farmerNameValue,
            "version": this.farmerNameVersion
          }
          ];

          this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
            const body_4 = resp.body as any;
            // set msg display
            this.displaySuccessMessage(body_4);
            // set msg display
            this.viewAddLand = true;
            this.viewAddCultivation = true;
            this.viewAddLandPhotos = false;
            this.disabledLocation = true;
            this.displayLivestockRiskDetailsDiv = false;
            this.displaySuccessMessage(body_4);
            this.isDataLoaded = false;
          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);

          });

        }, error => {
          this.isDataLoaded = false;
          this.displayErrorMessage(error);

        });
      }, error => {
        this.isDataLoaded = false;
        this.displayErrorMessage(error);

      });
    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);

    });
  }


  // add live stock
  addLivestock() {

    this.clearLivestockForm();
    this.manageLivestockRisk = false;
    this.clearCommonPlaces();
    // get dropdown data
    this.getLiveStockTypes();
    this.getBreedTypes();
    this.getGenders();
    this.getUsageTypes();
    this.getUnderwritingAssessors();
    this.displayManageLivestockForm = true;
    this.displayPolicyDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
  }
  saveLiveStockRisk() {

    this.service.getLocationByQuotation(this.quotationSequnce).subscribe(resp => {
      const body0 = resp.body as any;
      this.locationSequence = body0.data[0]['sequence'];

      this.riskRefNo += this.riskRefNo + 1;
      const riskObj = {
        'orderNumber': null,
        'riskName': this.riskRefNo,
        'referenceNumber': null,
        'sumInsured': this.riskSumInsured
      };


      this.service.addRisks(this.locationSequence, riskObj).subscribe(resp => {
        const body = resp.body as any;
        this.riskSequnce = body.data;
        // get risk informations

        this.service.getRiskCommonInformation(this.riskSequnce).subscribe(resp => {
          const body_3 = resp.body as any;
          const resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            const obj = resultArr[i];
            if (obj['description'] === 'LIVESTOCK TYPE') {
              // live stock type
              this.liveStockTypeSequnce = obj['sequence'];
              this.liveStockTypeDescription = obj['description'];
              this.liveStockTypeVersion = obj['version'];
              this.liveStockTypeValue = this.liveStockType;
            } else if (obj['description'] === 'BREED') {
              // breed
              this.breedSequence = obj['sequence'];
              this.breedDescription = obj['description'];
              this.breedVersion = obj['version'];
              this.breedValue = this.breedType;
            } else if (obj['description'] === 'GENDER') {
              // gender
              this.genderSequence = obj['sequence'];
              this.genderDescription = obj['description'];
              this.genderVersion = obj['version'];
              this.genderValue = this.gender;
            } else if (obj['description'] === 'USAGE') {
              // usage type
              this.usageSequence = obj['sequence'];
              this.usageDescription = obj['description'];
              this.usageVersion = obj['version'];
              this.usageValue = this.usageType;
            } else if (obj['description'] === 'AGE IN YEARS') {
              // age in years
              this.ageInYearsSequence = obj['sequence'];
              this.ageInYearsDescription = obj['description'];
              this.ageInYearsVersion = obj['version'];
              this.ageInYearsValue = this.age;
            } else if (obj['description'] === 'WEIGHT') {
              // owner name
              this.weightSequence = obj['sequence'];
              this.weightDescription = obj['description'];
              this.weightVersion = obj['version'];
              this.weightValue = this.weight;
            } else if (obj['description'] === 'MILKING CAPACITY PER DAY') {
              // purchase date
              this.milkCapacitySequence = obj['sequence'];
              this.milkCapacityDescription = obj['description'];
              this.milkCapacityVersion = obj['version'];
              this.milkCapacityValue = this.milkCapacity;
            }


          }

          // update common references for risk
          const jsonObj = [{
            'sequence': this.liveStockTypeSequnce,
            'description': this.liveStockTypeDescription,
            'value': this.liveStockTypeValue,
            'version': this.liveStockTypeVersion
          },
            {
              'sequence': this.breedSequence,
              'description': this.breedDescription,
              'value': this.breedValue,
              'version': this.breedVersion
            },
            {
              'sequence': this.genderSequence,
              'description': this.genderDescription,
              'value': this.genderValue,
              'version': this.genderVersion
            },
            {
              'sequence': this.usageSequence,
              'description': this.usageDescription,
              'value': this.usageValue,
              'version': this.usageVersion
            },
            {
              'sequence': this.ageInYearsSequence,
              'description': this.ageInYearsDescription,
              'value': this.ageInYearsValue,
              'version': this.ageInYearsVersion
            },
            {
              'sequence': this.weightSequence,
              'description': this.weightDescription,
              'value': this.weightValue,
              'version': this.weightVersion
            },
            {
              'sequence': this.milkCapacitySequence,
              'description': this.milkCapacityDescription,
              'value': this.milkCapacityValue,
              'version': this.milkCapacityVersion
            }
          ];
          this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
            const body_4 = resp.body as any;
            this.displaySuccessMessage(body_4);
            this.getRiskAdditionalDetails();

            this.displaySuccessMessage(body_4);
            this.displayPolicyDetails = false;
            this.displayLivestockAdditionalDetails = true;
            this.displayManageLivestockForm = false;
            this.displayManageCoversForm = false;
            this.displayLivestockRiskDetailsDiv = false;
            this.getRiskAdditionalDetails();
            this.isDataLoaded = false;
          }, error => {
            this.displayErrorMessage(error);

          });
        }, error => {
          this.displayErrorMessage(error);

        });
      }, error => {
        this.displayErrorMessage(error);

      });
    }, error => {

    });



  }



  // save  images keys
  savePolicyImageKeys() {

    // get document sequnce
    this.service.getPolicyDocumentSequnce(this.quotationSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.documentSequnce = body.data[0]['sequenceNo'];

      const obj = [];
      for (let i = 0; i < this.imagList.length; i++) {
        obj.push(this.imagList[i]['imageKey']);
      }
      // make image key array

      this.service.saveWeatherImages(this.documentSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.displaySuccessMessage(body);
      }, error => {
        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);
    });


  }

  saveRiskImageKeys() {
    // get document sequnce
    this.service.getRiskDocumentSequnce(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.documentSequnce = body.data[0]['sequence'];

      const obj = [];
      for (let i = 0; i < this.imagList.length; i++) {
        const j = { 'key': this.imagList[i]['imageKey'] };
        obj.push(j);
      }
      this.service.saveRiskImages(this.documentSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.displaySuccessMessage(body);
      }, error => {

        this.displayErrorMessage(error);
      });
    }, error => {
      this.displayErrorMessage(error);

    });
  }

  addCovers() {
    this.clearCoverForm();
    this.clearCommonPlaces();
    this.manageCover = false;
    this.displayManageCoversForm = true;
    this.displayLivestockAdditionalDetails = false;
    this.displayManageLivestockForm = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.getOtherPerils();
  }

  getOtherPerils() {
    this.service.getOtherPerils(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.otherPerils = body.data;
    }, error => {

    });
  }
  saveCovers() {
    let peril = {};
    if (this.otherPeril['perilPercentage'] === null) {
      peril = {
        'code': this.otherPeril['perilCode'],
        'sumInsured': this.perilPerilSumInsured,
        'rate': this.otherPeril['perilPercentage']
      };
    } else {
      peril = {
        'code': this.otherPeril['perilCode'],
        'sumInsured': this.perilPerilSumInsured,
        'percentage': this.otherPeril['perilPercentage']
      };
    }

    this.service.saveCovers(this.riskSequnce, peril).subscribe(resp => {
      const body = resp.body as any;
      this.perilSequnce = body.data;

      // excess insert
      const excess = {
        'code': 'EXGEN',
        'amount': this.excessAmount,
        'percentage': this.excessPercentage
      };

      this.service.saveExcess(this.perilSequnce, excess).subscribe(resp => {
        const body1 = resp.body as any;
        this.displaySuccessMessage(body1);
        this.displayLivestockAdditionalDetails = true;
        this.displayManageLivestockForm = true;
        this.displayManageCoversForm = false;
        this.displayLivestockRiskDetailsDiv = false;
        this.getRiskAdditionalDetails();
      }, error => {

        this.displayErrorMessage(error);
      });

    }, error => {
      this.displayErrorMessage(error);

    });
  }
  manageCovers(data) {
    //  alert(data);
    this.clearCoverForm();
    this.perilSequnce = data;
    this.clearCommonPlaces();
    this.manageCover = true;
    this.displayManageCoversForm = true;
    this.displayManageLivestockForm = false;
    this.displayLivestockAdditionalDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
    this.getOtherPerils();
    this.getCoverDetails();
  }
  getCoverDetails() {
    this.service.getCoverDetails(this.perilSequnce).subscribe(resp => {
      const body = resp.body as any;
      this.perilSequnce = body.data.seqNo;
      this.otherPeril = body.data.perilCode;
      this.perilPerilSumInsured = body.data.perilPerilSumInsured;
      this.managePerilDescription = body.data.perilName;

      this.service.getExcess(this.perilSequnce).subscribe(resp => {
        const body2 = resp.body as any;
        this.excessAmount = body2.data.excessAmount;
        this.excessPercentage = body2.data.excessPercentage;
      }, error => {

      });
    }, error => {

    });
  }

  // update land details
  updateLandRisk() {
    this.isDataLoaded = true;
    this.service.getRiskDetails(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;

      const obj = {
        "annualLimit": body.data.annualLimit,
        "annualNarration": body.data.annualNarration,
        "annualPercentage": body.data.annualPercentage,
        "cancelledAmount": body.data.cancelledAmount,
        "certificateNo": body.data.certificateNo,
        "coverNoteNo": body.data.coverNoteNo,
        "deletedDate": body.data.deletedDate,
        "deletedEndorsementNo": body.data.deletedEndorsementNo,
        "effectEndorsementNo": body.data.effectEndorsementNo,
        "effectiveDate": moment(this.weatherLandStartDate).format('YYYY-MM-DD HH:mm:ss'),
        "effectiveToDate": moment(this.weatherLandEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        "eventLimit": body.data.eventLimit,
        "eventNarration": body.data.eventNarration,
        "eventPercentage": body.data.eventPercentage,
        "geometry": [

        ],
        "mainRiskRelationship": body.data.mainRiskRelationship,
        "premium": body.data.premium,
        "relatedMainRiskCode": body.data.relatedMainRiskCode,
        "riskDescription": body.data.riskDescription,
        "riskName": body.data.riskName,
        "riskOrder": this.riskOrder,
        "riskPolicyNo": body.data.riskPolicyNo,
        "riskRefNo": body.data.riskRefNo,
        "riskSectionCode": body.data.riskSectionCode,
        "riskStatus": body.data.riskStatus,
        "riskSumInsured": this.riskSumInsured,
        "seqNo": this.riskSequnce,
        "totalCancelledAmount": body.data.totalCancelledAmount,
        "totalPremium": body.data.totalPremium,
        "totalTransactionAmount": body.data.totalTransactionAmount,
        "transactionAmount": body.data.transactionAmount,
        "size1": this.size1,
        "size2": this.size2,
        "size1UnitType": this.size1UnitType,
        "size2UnitType": this.size2UnitType,
        "version": body.data.version
      };

      const locationObj = {
        "annualLimit": body.data.location.annualLimit,
        "annualNarration": body.data.location.annualNarration,
        "annualPercentage": body.data.location.annualPercentage,
        "cancelledAmount": body.data.location.cancelledAmount,
        "deletedDate": body.data.location.deletedDate,
        "deletedEndorsementNo": body.data.location.deletedEndorsementNo,
        "effectEndorsementNo": body.data.location.effectEndorsementNo,
        "effectiveDate": body.data.location.effectiveDate,
        "eventLimit": body.data.location.eventLimit,
        "eventNarration": body.data.location.eventNarration,
        "eventPercentage": body.data.location.eventPercentage,
        "geometry": body.data.location.geometry,
        "location": this.location,
        "locationOrder": this.locationOrderNo,
        "locationStatus": body.data.location.locationOrder,
        "locationSumInsured": body.data.location.locationSumInsured,
        "locPolicyNo": body.data.location.locPolicyNo,
        "premium": body.data.location.premium,
        "seqNo": body.data.location.seqNo,
        "version": body.data.location.version,
        "totalCancelledAmount": body.data.location.totalCancelledAmount,
        "totalPremium": body.data.location.totalPremium,
        "totalTransactionAmount": body.data.totalTransactionAmount,
        "transactionAmount": body.data.transactionAmount
      };

      this.locationSequence = body.data.location.seqNo,
        this.service.updateRisk(this.riskSequnce, obj).subscribe(resp => {
          const body2 = resp.body as any;
          // this.service.updateLocation(this.locationSequence, locationObj).subscribe(resp => {
          //   const body3 = resp.body as any;
          //   this.displaySuccessMessage(body);
          // }, error => {
          //
          // });
          // get risk commont information
          this.service.getRiskCommonInformation(this.riskSequnce).subscribe(resp => {
            const body_3 = resp.body as any;
            const resultArr = body_3.data;
            for (let i = 0; i < resultArr.length; i++) {
              const obj = resultArr[i];
              if (obj['description'] === 'NO OF UNITS') {
                this.noOfUnitsSequence = obj['sequenceNo'];
                this.noOfUnitsDescription = obj['description'];
                this.noOfUnitsVersion = obj['version'];
                this.noOfUnitsValue = this.size2;
              } else if (obj['description'] === 'CROP VARIANCE') {
                // crop variance
                this.cropVarianceSequence = obj['sequenceNo'];
                this.cropVarianceDescription = obj['description'];
                this.cropVarianceVersion = obj['version'];
                this.cropVarianceValue = this.cropVariance;
              } else if (obj['description'] === 'WEATHER STATION') {
                // weather stations
                this.weatherStationSequence = obj['sequenceNo'];
                this.weatherStationDescription = obj['description'];
                this.weatherStationVersion = obj['version'];
                this.weatherStationValue = this.weatherStation + '';
              } else if (obj['description'] === 'FARMER NAME') {
                this.farmerNameSequnce = obj['sequenceNo'];
                this.farmerNameDescription = obj['description'];
                this.farmerNameVersion = obj['version'];
                this.farmerNameValue = this.farmerName;
              }

            }
            // update common references
            const jsonObj = [{
              "sequenceNo": this.noOfUnitsSequence,
              "description": this.noOfUnitsDescription,
              "value": this.noOfUnitsValue,
              "version": this.noOfUnitsVersion
            },
            {
              "sequenceNo": this.cropVarianceSequence,
              "description": this.cropVarianceDescription,
              "value": this.cropVariance,
              "version": this.cropVarianceVersion
            },
            {
              "sequenceNo": this.weatherStationSequence,
              "description": this.weatherDescription,
              "value": this.weatherStation + '',
              "version": this.weatherStationVersion
            },
            {
              "sequenceNo": this.farmerNameSequnce,
              "description": this.farmerNameDescription,
              "value": this.farmerNameValue,
              "version": this.farmerNameVersion
            }
            ];

            this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
              const body_4 = resp.body as any;
              // set msg display
              this.viewAddLand = true;
              this.viewAddCultivation = true;
              this.viewAddLandPhotos = false;
              this.disabledLocation = true;
              this.displayLivestockRiskDetailsDiv = false;

              this.displaySuccessMessage(body_4);
              this.isDataLoaded = false;


              // this.displayManageLandForm = false;
              // this.displayPolicyDetails = true;
              // this.getPolicyFormDetails();
            }, error => {
              this.isDataLoaded = false;
              this.displayErrorMessage(error);

            });

          }, error => {
            this.isDataLoaded = false;
            this.displayErrorMessage(error);

          });

        }, error => {
          this.isDataLoaded = false;

          this.displayErrorMessage(error);
        });

    }, error => {
      this.isDataLoaded = false;
      this.displayErrorMessage(error);
    });
  }


  // get livestock data
  getLivestockDetails() {


    this.service.getRiskDetails(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;

      // risk
      this.riskOrder = body.data.riskOrder;
      this.tagNumber = body.data.riskName;
      this.riskRefNo = body.data.riskRefNo;
      this.riskSectionCode = body.data.riskSectionCode;
      this.riskSumInsured = body.data.riskSumInsured;
      this.locationSequence = body.data.location.seqNo;
      // this.liveStockEndDate = new Date(body.data.effectiveDate);

      // get risk commont information
      this.service.getRiskCommonInformation(this.riskSequnce).subscribe(resp => {
        const body_3 = resp.body as any;
        const resultArr = body_3.data;
        for (let i = 0; i < resultArr.length; i++) {
          const obj = resultArr[i];
          if (obj['description'] === 'LIVESTOCK TYPE') {
            // crop variance
            this.liveStockType = obj['value'];
          }
          if (obj['description'] === 'BREED') {
            this.breedType = obj['value'];

          }
          if (obj['description'] === 'GENDER') {
            this.gender = obj['value'];
          }
          if (obj['description'] === 'USAGE') {
            this.usageType = obj['value'];
          }
          if (obj['description'] === 'AGE IN YEARS') {
            this.age = parseInt(obj['value']) + '';
          }
          if (obj['description'] === 'OWNER NAME') {
            this.ownerName = obj['value'];
          }
          if (obj['description'] === 'PURCHASE DATE') {
            if (obj['value'] === null) {
              this.livestockPurchaseDate = null;
            } else {
              this.livestockPurchaseDate = new Date(obj['value']);
            }

          }
        }

        // get asssessors and date
        this.service.getAssessors(this.riskSequnce).subscribe(resp => {
          const body_4 = resp.body as any;
          this.undewritingAssessor = body_4.data[0]['assessorCode'];
          // get assessor sequnce and get inspectd date
          this.service.getAssessorDetails(body_4.data[0]['sequence']).subscribe(resp => {
            const body_5 = resp.body as any;
            this.newLivestockStartDate = new Date(body_5.data.datetime);
          });

        }, error => {

        });
      }, error => {
        this.displayErrorMessage(error);

      });
    }, error => {

    });
  }


  updateLivestockRisk() {

    this.service.getRiskDetails(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;

      const obj = {
        "annualLimit": body.data.annualLimit,
        "annualNarration": body.data.annualNarration,
        "annualPercentage": body.data.annualPercentage,
        "cancelledAmount": body.data.cancelledAmount,
        "certificateNo": body.data.certificateNo,
        "coverNoteNo": body.data.coverNoteNo,
        "deletedDate": body.data.deletedDate,
        "deletedEndorsementNo": body.data.deletedEndorsementNo,
        "effectEndorsementNo": body.data.effectEndorsementNo,
        "effectiveDate": moment(this.liveStockStartDate).format('YYYY-MM-DD HH:mm:ss'),
        "effectiveToDate": moment(this.liveStockEndDate, 'MMMM D , YYYY').format('YYYY-MM-DD HH:mm:ss'),
        "eventLimit": body.data.eventLimit,
        "eventNarration": body.data.eventNarration,
        "eventPercentage": body.data.eventPercentage,
        "geometry": body.data.geometry,
        "mainRiskRelationship": body.data.mainRiskRelationship,
        "premium": body.data.premium,
        "relatedMainRiskCode": body.data.relatedMainRiskCode,
        "riskDescription": body.data.riskDescription,
        "riskName": this.tagNumber,
        "riskOrder": this.riskOrder,
        "riskPolicyNo": body.data.riskPolicyNo,
        "riskRefNo": body.data.riskRefNo,
        "riskSectionCode": body.data.riskSectionCode,
        "riskStatus": body.data.riskStatus,
        "riskSumInsured": this.riskSumInsured,
        "seqNo": this.riskSequnce,
        "totalCancelledAmount": body.data.totalCancelledAmount,
        "totalPremium": body.data.totalPremium,
        "totalTransactionAmount": body.data.totalTransactionAmount,
        "transactionAmount": body.data.transactionAmount,
        "size1": body.data.size1,
        "size2": body.data.size2,
        "size1UnitType": body.data.size1UnitType,
        "size2UnitType": body.data.size2UnitType,
        "version": body.data.version
      };

      this.service.updateRisk(this.riskSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        this.service.getRiskCommonInformation(this.riskSequnce).subscribe(resp => {
          const body_3 = resp.body as any;
          const resultArr = body_3.data;
          for (let i = 0; i < resultArr.length; i++) {
            const obj = resultArr[i];
            if (obj['description'] === 'LIVESTOCK TYPE') {
              // live stock type
              this.liveStockTypeSequnce = obj['sequenceNo'];
              this.liveStockTypeDescription = obj['description'];
              this.liveStockTypeVersion = obj['version'];
              this.liveStockTypeValue = this.liveStockType;
            } else if (obj['description'] === 'BREED') {
              // breed
              this.breedSequence = obj['sequenceNo'];
              this.breedDescription = obj['description'];
              this.breedVersion = obj['version'];
              this.breedValue = this.breedType;
            } else if (obj['description'] === 'GENDER') {
              // gender
              this.genderSequence = obj['sequenceNo'];
              this.genderDescription = obj['description'];
              this.genderVersion = obj['version'];
              this.genderValue = this.gender;
            } else if (obj['description'] === 'USAGE') {
              // usage type
              this.usageSequence = obj['sequenceNo'];
              this.usageDescription = obj['description'];
              this.usageVersion = obj['version'];
              this.usageValue = this.usageType;
            } else if (obj['description'] === 'AGE IN YEARS') {
              // age in years
              this.ageInYearsSequence = obj['sequenceNo'];
              this.ageInYearsDescription = obj['description'];
              this.ageInYearsVersion = obj['version'];
              this.ageInYearsValue = this.age;
            } else if (obj['description'] === 'OWNER NAME') {
              // owner name
              this.ownerNameSequence = obj['sequenceNo'];
              this.ownerNameDescription = obj['description'];
              this.ownerNameVersion = obj['version'];
              this.ownerNameValue = this.ownerName;
            } else if (obj['description'] === 'PURCHASE DATE') {
              // purchase date
              this.purchaseDateSequence = obj['sequenceNo'];
              this.purchaseDateDescription = obj['description'];
              this.purchaseDateVersion = obj['version'];
              this.purchaseDateValue = moment(this.livestockPurchaseDate).format('YYYY-MM-DD HH:mm:ss');
            }


          }

          // update common references for risk
          const jsonObj = [{
            "sequenceNo": this.liveStockTypeSequnce,
            "description": this.liveStockTypeDescription,
            "value": this.liveStockTypeValue,
            "version": this.liveStockTypeVersion
          },
          {
            "sequenceNo": this.breedSequence,
            "description": this.breedDescription,
            "value": this.breedValue,
            "version": this.breedVersion
          },
          {
            "sequenceNo": this.genderSequence,
            "description": this.genderDescription,
            "value": this.genderValue,
            "version": this.genderVersion
          },
          {
            "sequenceNo": this.usageSequence,
            "description": this.usageDescription,
            "value": this.usageValue,
            "version": this.usageVersion
          },
          {
            "sequenceNo": this.ageInYearsSequence,
            "description": this.ageInYearsDescription,
            "value": this.ageInYearsValue,
            "version": this.ageInYearsVersion
          },
          {
            "sequenceNo": this.ownerNameSequence,
            "description": this.ownerNameDescription,
            "value": this.ownerNameValue,
            "version": this.ownerNameVersion
          },
          {
            "sequenceNo": this.purchaseDateSequence,
            "description": this.purchaseDateDescription,
            "value": this.purchaseDateValue,
            "version": this.purchaseDateVersion
          }
          ];
          this.service.UpdateRiskCommonInformation(this.riskSequnce, jsonObj).subscribe(resp => {
            const body_4 = resp.body as any;


            // get asssessors and date
            this.service.getAssessors(this.riskSequnce).subscribe(resp => {
              const body_5 = resp.body as any;

              const objAssessor = {
                'sequence': body_5.data[0]['sequence'],
                'assessorCode': this.undewritingAssessor,
                'assessorName': body_5.data[0]['assessorName'],
                'assessorType': body_5.data[0]['assessorType'],
                'remarks': body_5.data[0]['remarks'],
                'version': body_5.data[0]['version'],
                'datetime': moment(this.newLivestockStartDate).format('YYYY-MM-DD HH:mm:ss')
              };
              this.service.updateInspectedDetails(body_5.data[0]['sequence'], objAssessor).subscribe(resp => {
                const body_6 = resp.body as any;
                this.displaySuccessMessage(body_5);
              }, error => {
                this.displayErrorMessage(error);

              });
            }, error => {
              this.displayErrorMessage(error);

            });



          }, error => {
            this.displayErrorMessage(error);

          });
        }, error => {
          this.displayErrorMessage(error);

        });

      }, error => {

        this.displayErrorMessage(error);
      });

    }, error => {
      this.displayErrorMessage(error);
    });
  }

  setDayCountWeather() {
    const start = moment(this.startDate);
    const end = moment(this.endDate);
    this.weatherDayCount = end.diff(start, 'day');
  }

  setDayCountLivestock() {

    const start = moment(this.liveStockStartDate);
    const end = moment(this.liveStockEndDate);
    this.livestockDayCount = end.diff(start, 'day');

  }
  // update covers
  updateRiskCovers() {
    this.service.getCoverDetails(this.perilSequnce).subscribe(resp => {
      const body = resp.body as any;
      // update
      const obj = {
        "seqNo": this.perilSequnce,
        "perilCode": body.data.perilCode,
        "perilPerilSumInsured": this.perilPerilSumInsured,
        "perilPercentage": body.data.perilPercentage,
        "perilReinsSumInsured": body.data.perilReinsSumInsured,
        "perilPremium": body.data.perilPremium,
        "perilTotalPremium": body.data.perilTotalPremium,
        "perilTransactionAmount": body.data.perilTransactionAmount,
        "perilotalTransactionAmount": body.data.perilotalTransactionAmount,
        "perilCancelledAmount": body.data.perilCancelledAmount,
        "perilTotalCancelledAmount": body.data.perilTotalCancelledAmount,
        "perilrEffectEndorsementNo": body.data.perilrEffectEndorsementNo,
        "perilEffectiveDate": body.data.perilEffectiveDate,
        "perilPerilStatus": body.data.perilPerilStatus,
        "perilDeletedDate": body.data.perilDeletedDate,
        "perilDeletedEndorsementNo": body.data.perilDeletedEndorsementNo,
        "perilDefaultPercentage": body.data.perilDefaultPercentage,
        "perilDefaultRate": body.data.perilDefaultRate,
        "perilRate": body.data.perilRate,
        "perilEventLimit": body.data.perilEventLimit,
        "perilEventPercentage": body.data.perilEventPercentage,
        "perilEventNarration": body.data.perilEventNarration,
        "perilAnnualLimit": body.data.perilAnnualLimit,
        "perilAnnualPercentage": body.data.perilAnnualPercentage,
        "perilAnnualNarration": body.data.perilAnnualNarration,
        "version": body.data.version,
        "perilReinClassCode": body.data.perilReinClassCode,
        "productPerilSeqNo": body.data.productPerilSeqNo,
        "perilName": body.data.perilName
      };
      this.service.updateCover(this.perilSequnce, obj).subscribe(resp => {
        const body1 = resp.body as any;

        // excess
        this.service.getExcess(this.perilSequnce).subscribe(resp => {
          const body2 = resp.body as any;

          if (body2.data === null) {
            // excess insert
            const excess = {
              "excessAmount": this.excessAmount,
              "excessPercentage": this.excessPercentage
            };

            this.service.saveExcess(this.perilSequnce, excess).subscribe(resp => {
              const body1 = resp.body as any;
              this.displaySuccessMessage(body1);
              this.displayLivestockAdditionalDetails = true;
              this.displayManageLivestockForm = true;
              this.displayManageCoversForm = false;
              this.getRiskAdditionalDetails();
            }, error => {

              this.displayErrorMessage(error);
            });
          } else {
            // make object
            const obj = {
              "sequenceNo": body2.data.sequenceNo,
              "excessLevel": body2.data.excessLevel,
              "excessAmount": this.excessAmount,
              "excessPercentage": this.excessPercentage,
              "excessNarration": body2.data.excessNarration,
              "status": body2.data.status,
              "version": body2.data.version
            };
            this.service.updateExcess(body2.data.sequenceNo, obj).subscribe(resp => {
              const body_3 = resp.body as any;
              this.displaySuccessMessage(body);
              this.displayLivestockAdditionalDetails = true;
              this.displayManageLivestockForm = true;
              this.displayManageCoversForm = false;
              this.displayLivestockRiskDetailsDiv = false;
              this.getRiskAdditionalDetails();
            }, error => {

              this.displayErrorMessage(error);
            });
          }


        }, error => {
          this.displayErrorMessage(error);

        });
      }, error => {
        this.displayErrorMessage(error);

      });

    }, error => {
      this.displayErrorMessage(error);

    });
  }





  // complete policy

  completeQuotation() {


    const authArray = [];
    this.service.getCompleteQuotation(this.quotationSequnce).subscribe(resp => {
      const body = resp.body as any;
      const version = body.data.version;
      // update completed
      const obj = {
        'completed': true// ,
        // 'version': version
      };
      this.service.updateCompleteQuotation(this.quotationSequnce, obj).subscribe(resp => {
        const body = resp.body as any;
        // authorize policy
        this.service.getAuthorizedQuotation(this.quotationSequnce).subscribe(resp => {
          const body = resp.body as any;
          for (let i = 0; i < body.data.length; i++) {
            const obj = {
              "sequence": body.data[i]['sequence'],
              "levelCode": body.data[i]['levelCode'],
              "authorized": true,
              "version": body.data[i]['version']
            };
            authArray.push(obj);
          }
          this.service.updateAuthorizedQuotation(this.quotationSequnce, authArray).subscribe(resp => {
            const body = resp.body as any;
            this.authorizedPolicy = true;
            this.debitNoteNumber = body.data;

            this.displaySuccessMessage(body);

            // download pdf and redirect to policy list
            if (this.productType === 'WHINX') {

              // weather Individual file
              const fileName1 = 'WeatherIndexSchedule.pdf';
              this.service.downloadWeatherIndividualFile(this.quotationSequnce).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], { type: 'application/pdf' });
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = fileName1;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });


              // weather Group file
              const fileName2 = 'WeatherIndexGroupSchedule.pdf';
              this.service.downloadWeatherGroupFile(this.quotationSequnce).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], { type: 'application/pdf' });
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = fileName2;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });
            } else  if (this.productType === 'LIVST') {
              // Livestock individual file
              const fileName1 = 'LivestockSchedule.pdf';
              this.service.downloadLivestockIndividualFile(this.quotationSequnce).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], { type: 'application/pdf' });
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = fileName1;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });

              // Livestock Group file
              const fileName2 = 'LivestockGroupSchedule.pdf';
              this.service.downloadLivestockGroupFile(this.quotationSequnce).subscribe(x => {
                if (x.size === 0) {
                  return;
                }
                const newBlob = new Blob([x], { type: 'application/pdf' });
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(newBlob);
                  return;
                }
                const data = window.URL.createObjectURL(newBlob);
                const link = document.createElement('a');
                link.href = data;
                link.download = fileName2;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

                setTimeout(function () {
                  // For Firefox it is necessary to delay revoking the ObjectURL
                  window.URL.revokeObjectURL(data);
                  link.remove();
                }, 100);
              });

            }
            this.router.navigate(['/quotations']);

          }, error => {

            this.authorizedPolicy = false;
            this.displayErrorMessage(error);
          });
        }, error => {
          this.authorizedPolicy = false;
          this.displayErrorMessage(error);

        });

        // set disable  steps
        // enable payment link
      }, error => {

        this.authorizedPolicy = false;
        this.displayErrorMessage(error);
      });
    }, error => {
      this.authorizedPolicy = false;
      this.displayErrorMessage(error);

    });
  }

   CancelLivestockAdditionalDetails() {
    this.displayLivestockAdditionalDetails = false;
    this.displayManageCoversForm = false;
    this.displayManageLivestockForm = true;
    this.displayPolicyDetails = false;
    this.displayLivestockRiskDetailsDiv = false;
  }

  uploadImg(file: File, index: number) {

    this.service.uploadImage(file).subscribe(resp => {
      if (resp.type === HttpEventType.UploadProgress) {
        this.imagList[index].progress = Math.round(resp.loaded / resp.total * 100) + '%';
      } else if (resp.type === HttpEventType.Response) {
        const body = resp.body as any;
        this.imagList[index].imageKey = body.data.object.key;
        this.imagList[index].value = body.data.object.url;
        this.imagList[index].file = null;
      }
    });

    // alert(this.imagList.length);
  }

  addImg(event) {
    const img: ImageModel = new ImageModel();
    img.file = event.target.files[0];
    this.imagList.push(img);
    this.uploadImg(img.file, this.imagList.indexOf(img));
    // alert(this.imagList.length);
  }
  removeImg(index) {
    this.imagList.splice(index, 1);
  }

  viewLivestockRisk(sequence, productCode) {
    this.isDataLoaded = true;
    this.clearCommonPlaces();
    this.riskSequnce = sequence;
    this.displayLivestockRiskDetailsDiv = true;
    this.displayPolicyDetails = false;

    this.service.getRiskDetails(this.riskSequnce).subscribe(resp => {
      const body = resp.body as any;

      // risk
      this.riskOrder = body.data.riskOrder;
      this.tagNumber = body.data.riskName;
      this.riskRefNo = body.data.riskRefNo;
      this.riskSectionCode = body.data.riskSectionCode;
      this.riskSumInsured = body.data.sumInsured;

      // this.liveStockEndDate = new Date(body.data.effectiveDate);

      // get risk commont information
      this.service.getRiskCommonInformation(this.riskSequnce).subscribe(resp => {
        const body_3 = resp.body as any;
        const resultArr = body_3.data;
        for (let i = 0; i < resultArr.length; i++) {
          const obj = resultArr[i];
          if (obj['description'] === 'LIVESTOCK TYPE') {
            // crop variance
            this.liveStockType = obj['valueDescription'];
          }
          if (obj['description'] === 'BREED') {
            this.breedType = obj['valueDescription'];

          }
          if (obj['description'] === 'GENDER') {
            this.gender = obj['valueDescription'];
          }

          if (obj['description'] === 'AGE IN YEARS') {
            this.age = parseInt(obj['value']) + '';
          }
          if (obj['description'] === 'WEIGHT') {
            this.weight = obj['value'];
          }
          if (obj['description'] === 'MILKING CAPACITY PER DAY') {
            if (obj['value'] === null) {
              this.milkCapacity = null;
            } else {
              this.milkCapacity = obj['value'];
            }
          }
        }

        this.riskImages = [];
        this.covers = [];
        let documentSequnce = '';
        // get risk images
        this.service.getRiskDocuments(this.riskSequnce).subscribe(resp => {
          // tslint:disable-next-line:no-shadowed-variable
          const body = resp.body as any;
          documentSequnce = body.data[0]['sequence'];

          this.service.downloadImages(documentSequnce).subscribe(resp => {
            // tslint:disable-next-line:no-shadowed-variable
            const body1 = resp.body as any;
            this.clearCommonPlaces();
            this.riskImages = body1.data;
            if (this.riskImages.length > 0) {
              for (let i = 0; i < this.riskImages.length; i++) {
                const im = new ImageModel();
                im.imageKey = this.riskImages[i]['key'];
                im.value = this.riskImages[i]['url'];
                this.imagList.push(im);
              }
            }

            this.service.getRiskPerils(this.riskSequnce).subscribe(resp => {
              // tslint:disable-next-line:no-shadowed-variable
              const body1 = resp.body as any;
              this.covers = body1.data;
              this.isDataLoaded = false;

            }, error => {

              this.isDataLoaded = false;
            });

          }, error => {

            this.isDataLoaded = false;
          });
        }, error => {

          this.isDataLoaded = false;
        });
      }, error => {
        this.displayErrorMessage(error);

        this.isDataLoaded = false;
      });
    }, error => {

      this.isDataLoaded = false;
    });

  }

  clearCommonPlaces() {
    this.imagList = [];
  }


  searchByFeild() {
    let searchFeild = '';
    const selectedValue = this.selectedValue;
    if (selectedValue === 'customerName') {
      searchFeild = 'CN';
      this.searchSelectCriteria = searchFeild;
    } else if (selectedValue === 'riskName') {
      searchFeild = 'RN';
      this.searchSelectCriteria = searchFeild;
    } else if (selectedValue === 'quotationNumber') {
      searchFeild = 'QN';
      this.searchSelectCriteria = searchFeild;
    } else if (selectedValue === 'customerNIC') {
      searchFeild = 'CNIC';
      this.searchSelectCriteria = searchFeild;
    } else if (selectedValue === 'productName') {
      searchFeild = 'PN';
      this.searchSelectCriteria = searchFeild;
    }
    if (this.searchValue !== '') {
      this.getSearchedPolicies();
    }

  }
  searchByStatus() {

    let searchFeild = '';
    const selectedValue = this.selectedStatus;
    if (selectedValue === 'all') {
      searchFeild = '';
      this.searchSelectedStatus = searchFeild;
    } else if (selectedValue === 'active') {
      searchFeild = 'A';
      this.searchSelectedStatus = searchFeild;
    } else if (selectedValue === 'expired') {
      searchFeild = 'E';
      this.searchSelectedStatus = searchFeild;
    }

    this.getSearchedPolicies();

  }
  calculatePremium() {
    if (this.size2UnitType === null || this.size2 === null) {
      this.weatherLandPremium = 0;
    } else {
      this.weatherLandPremium = parseFloat(this.size2UnitType) * parseFloat(this.size2);
    }

  }

  // set weather policy end date
  setEndDate() {
    this.endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());
  }

  setLivestockPolicyEndDate() {
    this.livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());
  }

  getSelectedLocation(data) {
    if (data === 'add-new') {
      this.getDistricts();
      this.getagrarianServiceCenters();
      this.getOwnershipTypes();
      this.viewAddLand = false;
      this.viewAddCultivation = true;
      this.viewAddLandPhotos = true;
    } else if (data !== 'select-land') {

      this.size1 = this.location['sizeInAcres'];
      this.size1UnitType = this.location['sizeInPerches'];
      this.markLandPlot = this.location['geometry'];
      this.pathList = this.location['geometry'];
      const markLandPlotArr = [];
      this.viewAddLand = true;
      this.viewAddCultivation = false;
      this.viewAddLandPhotos = true;
      this.displayLivestockRiskDetailsDiv = false;
      //
      this.weatherStation = 84;
      for (let i = 0; i < this.pathList.length; i++) {
        this.markLandPlotString = this.markLandPlotString + this.pathList[i]['latitude'] + ':' + this.pathList[i]['longitude'] + ',';
      }

      // get nearest weather station
      this.service.getNearestWeatherStation(this.location['geometry']).subscribe(resp => {
        const body = resp.body as any;
        this.weatherStation = parseInt(body.data.sequence);
        // get units and price
        this.service.getUnitPriceOnRisk(this.weatherStation).subscribe(resp => {
          const body_4 = resp.body as any;
          this.size2UnitType = body_4.data.unitPrice;
        }, error => {

        });
      }, error => {

      });

    }
  }

  generatePdf() {
    // download pdf and redirect to policy list
    if (this.productType === 'WHINX') {

      // weather Individual file
      const fileName1 = 'WeatherIndexSchedule.pdf';
      this.service.downloadWeatherIndividualFile(this.quotationSequnce).subscribe(x => {
        if (x.size === 0) {
          return;
        }
        const newBlob = new Blob([x], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = fileName1;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });


      // weather Group file
      const fileName2 = 'WeatherIndexGroupSchedule.pdf';
      this.service.downloadWeatherGroupFile(this.quotationSequnce).subscribe(x => {
        if (x.size === 0) {
          return;
        }
        const newBlob = new Blob([x], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = fileName2;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });
    } else  if (this.productType === 'LIVST') {

// Livestock individual file
      const fileName1 = 'LivestockSchedule.pdf';
      this.service.downloadLivestockIndividualFile(this.quotationSequnce).subscribe(x => {
        if (x.size === 0) {
          return;
        }
        const newBlob = new Blob([x], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = fileName1;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });
      // Livestock Group file
      const fileName2 = 'LivestockGroupSchedule.pdf';
      this.service.downloadLivestockGroupFile(this.quotationSequnce).subscribe(x => {
        if (x.size === 0) {
          return;
        }
        const newBlob = new Blob([x], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = fileName2;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          // For Firefox it is necessary to delay revoking the ObjectURL
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      });

    }
  }
  getDistricts() {
    this.landService.getDistricts().subscribe(resp => {
      const body = resp.body as any;
      this.districtsList = body.data;
    });
  }

  getDivisionns() {
    this.landService.getDivisions(this.selectedDistrict).subscribe(resp => {
      const body = resp.body as any;
      this.divisionsList = body.data;
    });
  }

  getGramasevaDivisions() {
    this.landService.getGramasevaDivisions(this.selectedDivision).subscribe(resp => {
      const body = resp.body as any;
      this.gramasevaDivisionsList = body.data;
    });
  }

  getagrarianServiceCenters() {
    this.landService.getagrarianServiceCenters().subscribe(resp => {
      const body = resp.body as any;
      this.getagrarianServiceCentersList = body.data;
    });
  }

  getOwnershipTypes() {
    this.landService.getOwnershipTypes().subscribe(resp => {
      const body = resp.body as any;
      this.ownershipTypesList = body.data;
    });
  }

  getMarkers(event) {

    this.pathList = [];
    this.pathList = event;
    this.pathList.push(this.pathList[0]);
    this.isDrawMap = false;
    this.pathList.forEach(item => {
      this.pathString = this.pathString + item.latitude + ':' + item.longitude + ',';
    });
    this.markLandPlotString = '';
    if (this.location === 'add-new') {
      this.viewAddLand = false;
      this.viewAddCultivation = true;
      this.viewAddLandPhotos = true;

    } else if (this.location !== 'select-land') {
      this.markLandPlot = this.pathList;
      for (let i = 0; i < this.pathList.length; i++) {
        this.markLandPlotString = this.markLandPlotString + this.pathList[i]['latitude'] + ':' + this.pathList[i]['longitude'] + ',';
      }
      this.viewAddLand = true;
      this.viewAddLandPhotos = true;
      this.viewAddCultivation = false;
    }

    // set weather station
    this.service.getNearestWeatherStation(this.pathList).subscribe(resp => {
      const body = resp.body as any;
      this.weatherStation = body.data.sequence;
      // get units and price
      this.service.getUnitPriceOnRisk(this.weatherStation).subscribe(resp => {
        const body_4 = resp.body as any;
        this.size2UnitType = body_4.data.unitPrice;
      }, error => {

      });

    }, error => {

    });

    this.landForm.controls['geometry'].setValue(this.pathString);
  }
  saveLand() {
    // console.log(this.landForm.value);
    const land: LandModel = this.landForm.value;
    land.geometry = this.pathList;
    land.partyCode = this.customerCode;
    this.landService.postLand(land).subscribe(resp => {
      const body1 = resp.body as any;
      this.service.getLocations(this.customerCode).subscribe(resp => {
        const body = resp.body as any;
        this.locations = body.data;
        this.service.getLocationFromUtility(body1.data).subscribe(resp => {
          const body2 = resp.body as any;
          // this.location = body2.data;
          top: for (let i = 0; i < this.locations.length; i++) {
            if (this.locations[i]['sequence'] === body2.data.sequence) {

              this.location = this.locations[i];
              break top;
            }
          }
          this.getSelectedLocation(this.location);

          this.viewAddLand = true;
          this.viewAddCultivation = false;
          this.viewAddLandPhotos = true;

          this.displaySuccessMessage(body1);
        }, error => {

        });



      }, error => {

      });


    }, error => {
      this.viewAddLand = false;
      this.viewAddCultivation = true;
      this.viewAddLandPhotos = true;
      this.displayErrorMessage(error);
    });
  }

  displayWeatherLivestockAdditionalDetails() {
    this.clearCommonPlaces();
    this.getpolicyDetails(this.quotationSequnce);
    this.displayPolicyDetails = false;
    this.displayManageWeatherForm = false;
    this.displayManageLivestockForm = false;
    this.displayLivestockAdditionalDetails = false;

  }
  clearWeatherPolicyForm() {
    this.cropType = '';
    this.weatherType = '';
    this.plan = '';
    this.businessChanel = '';
    this.startDate = null;
    this.endDate = null;
    this.paymentType = '';
    this.endDateMindate = new Date(moment(this.startDate).add(1, 'days').format());
    this.cropTypeFormControl.reset();
    this.weatherTypeFormControl.reset();
    this.planFormControl.reset();
    this.businessChanelFormControl.reset();
    this.startDateFormControl.reset();
    this.endDateFormControl.reset();
    this.paymentFormControl.reset();
  }

  clearLivestockPolicyForm() {
    this.product = '';
    this.liveStockStartDate = null;
    this.liveStockEndDate = null;
    this.livestockPolicyEnddateMindate = new Date(moment(this.liveStockStartDate).add(1, 'days').format());
    this.liveStickBusinessChannel = '';
    this.premiumSettlementMethod = '';
    this.liveStockPlanFormControl.reset();
    this.liveStockStartDateFormControl.reset();
    this.liveStockEndDateFormControl.reset();
    this.liveStickBusinessChannelFormControl.reset();
    this.premiumSettlementMethodFormControl.reset();
  }


  clearLivestockForm() {

    this.tagNumber = '';
    this.liveStockType = '';
    this.breedType = '';
    this.gender = '';
    this.milkCapacity = null;
    this.weight = '';
    this.undewritingAssessor = '';
    this.newLivestockStartDate = null;
    this.riskSumInsured = '';
    this.age = '';
    this.usageType = '';
    this.tagNumberFormControl.reset();
    this.liveStockTypeFormControl.reset();
    this.breedTypeFormControl.reset();
    this.genderTypeFormControl.reset();
    this.weightFormControl.reset();
    this.undewritingAssessorFormControl.reset();
    this.newLivestockStartDateFormControl.reset();
    this.riskSumInsuredFormControl.reset();
    this.ageTypeFormControl.reset();
    this.usageTypeFormControl.reset();
    this.milkFormControl.reset();


  }
  clearLandForm() {
    this.farmerName = '';
    this.cropVariance = '';
    this.weatherStation = 0;
    this.weatherLandStartDate = null;
    this.weatherLandEndDate = null;
    this.size1 = '';
    this.size1UnitType = '';
    this.markLandPlotString = '';
    this.size2 = '';
    this.size2UnitType = '';
    this.weatherLandPremium = 0;
    this.farmerNameFormControl.reset();
    this.cropVarianceFormControl.reset();
    this.landWeatherStationFormControl.reset();
    this.weatherLandStartDateFormControl.reset();
    this.weatherLandEndDateFormControl.reset();
    this.size1FormControl.reset();
    this.size1UnitTypeFormControl.reset();
    this.markLandPlotString = '';
    this.noOfunitsFormControl.reset();
    this.priceFormControl.reset();
    this.landForm.reset();
  }
  clearCoverForm() {
    this.otherPeril = {};
    this.excessAmount = 0;
    this.excessPercentage = 0;
    this.perilPerilSumInsured = 0;
    this.excessAmountFormControl.reset();
    this.excessPercentageFormControl.reset();
    this.perilPerilSumInsuredFormControl.reset();
  }

  /**
 * common functions
 */
  // displaying error messages
  displayErrorMessage(error) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = error.message;
    this.notificationService.messageType = 'error';

  }

  // displaying success messages
  displaySuccessMessage(success) {
    this.notificationService.isMessageDisplayed = true;
    this.notificationService.messageContent = success.message;
    this.notificationService.messageType = 'success';
  }
}
