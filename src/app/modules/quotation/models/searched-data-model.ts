export class SearchedDataModel {

  branchCode: string;
  branchDescription: string;
  channelOfBusinessCode: string;
  channelOfBusinessDescription: string;
  classCode: string;
  classDescription: string;
  currencyCode: string;
  currencyDescription: string;
  customerAddressSequence: string;
  customerCode: string;
  customerName: string;
  intermediaryName: string;
  modeOfBusinessCode: string;
  modeOfBusinessDescription: string;
  periodFromDate: string;
  periodToDate: string;
  premium: string;
  productCode: string;
  productDescription: string;
  quotationNumber: string;
  quotationVersion: string;
  riskName: string;
  sequence: string;
  settlementModeCode: string;
  settlementModeDescription: string;
  status: string;
  statusDescription: string;
  sumInsured: string;
  totalPremium: string;
  totalTransactionAmount: string;
  transactionAmount: string;
  transactionType: string;
  validFromDate: string;
  validToDate: string;
}
