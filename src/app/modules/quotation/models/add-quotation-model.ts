export class AddQuotationModel {
  branchCode: string;
  businessChannelCode: string;
  classCode: string;
  currencyCode: string;
  customerAddressSequence: string;
  customerCode: string;
  businessModeCode: string;
  settlementMethodCode: string;
  fromDate: string;
  toDate: string;
  productCode: string;
  validFromDate: string;
  validToDate: string;
}
