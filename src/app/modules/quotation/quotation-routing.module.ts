import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QuotationMainComponent} from './quotation-main/quotation-main.component';
import {DashboardComponent} from '../dashboard/dashboard/dashboard.component';
import { QuotationsComponent } from './quotation/quotations/quotations.component';
import { QuotationFormComponent } from './quotation/quotation-form/quotation-form.component';

const routes: Routes = [
  {
    path: 'quotations',
    component: QuotationsComponent
  },
  {
    path: 'quotation/:customerCode/:parent',
    component: QuotationFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuotationRoutingModule { }
