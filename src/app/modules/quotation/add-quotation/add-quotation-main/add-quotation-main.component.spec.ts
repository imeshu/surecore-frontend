import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddQuotationMainComponent } from './add-quotation-main.component';

describe('AddQuotationMainComponent', () => {
  let component: AddQuotationMainComponent;
  let fixture: ComponentFixture<AddQuotationMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddQuotationMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddQuotationMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
