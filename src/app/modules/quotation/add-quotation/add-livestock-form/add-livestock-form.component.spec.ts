import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLivestockFormComponent } from './add-livestock-form.component';

describe('AddLivestockFormComponent', () => {
  let component: AddLivestockFormComponent;
  let fixture: ComponentFixture<AddLivestockFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLivestockFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLivestockFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
