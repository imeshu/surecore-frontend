import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkLandQuotationComponent } from './mark-land-quotation.component';

describe('MarkLandQuotationComponent', () => {
  let component: MarkLandQuotationComponent;
  let fixture: ComponentFixture<MarkLandQuotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkLandQuotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkLandQuotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
