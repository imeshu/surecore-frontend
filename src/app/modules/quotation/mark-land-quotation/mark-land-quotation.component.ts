import {Component, EventEmitter, Output, OnInit, Input} from '@angular/core';
import { LocationModel } from '../../land/models/location-model';
import { MapLoaderService } from '../../land/mark-land/map.loader';
import {LandModel} from '../../customer/models/land-model';

declare const google: any;
@Component({
  selector: 'app-mark-land-quotation',
  templateUrl: './mark-land-quotation.component.html',
  styleUrls: ['./mark-land-quotation.component.scss']
})
export class MarkLandQuotationComponent  {
  @Output() output: EventEmitter<LocationModel[]> = new EventEmitter();
  @Output() output1: EventEmitter<any[]> = new EventEmitter();
  @Output() output2: EventEmitter<any[]> = new EventEmitter();
  @Input() data: LandModel;
  @Input() geometryList: LocationModel[] = new Array();
  // @Output() output: EventEmitter<LocationModel[]> = new EventEmitter();
  map: any;
  geocoder: any; // Geocoder to generate Lat, Lang from address
  address = ''; // Store the address

  drawingManager: any;
  pathList = [];
  drawnShapes = [];
  goHome = 'hello';
  isHaveData: boolean = false;

  constructor() {
    console.log('mapppp');
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    MapLoaderService.load().then(() => {
      this.drawPolygon();
      console.log(this.data);
    });
  }

  drawPolygon() {

    if (this.data) {
      this.address = this.data.gramasevaDivisionDescription + ' ' + this.data.districtDescription; // Generate address to zoom into
      this.geocoder = new google.maps.Geocoder(); // Initialize geocoder

      const _this = this; // Create hook to DOM to this scope ( Otherwise "this" is not accessible inside of the following callback  )

      this.geocoder.geocode({ address: this.address }, function(results, status) {
        // Create map options object with Default values to failsafe
        const mapOpt = {
          center: { lat: 7.593408, lng: 80.684714 }, // Lat-Lang of Sri Lanka is loaded if the selected region not found in Gmaps
          zoom: 8, // Zoom level to show the whole country
          mapTypeId: google.maps.MapTypeId.HYBRID // Show the labels in Satelite view
        };

        // If Geocode Generated
        if (status === 'OK') {
          const lat = results[0].geometry.location.lat(); // Get the Latitude of the Gramasewa region
          const lang = results[0].geometry.location.lng(); // Get the longitude of the Gramasewa region

          // Update map options
          mapOpt.center.lat = lat;
          mapOpt.center.lng = lang;
          mapOpt.zoom = 14;
        }

        // Draw the polygon if points are existing
        if (_this.data.geometry.length > 0) {
          // Since a polygon is there, zoom into it.
          mapOpt.zoom = 14;

          const cordArr = [];

          // Points to Find Center
          let minLat = _this.data.geometry[0].latitude;
          let maxLat = _this.data.geometry[0].latitude;
          let minLng = _this.data.geometry[0].longitude;
          let maxLng = _this.data.geometry[0].longitude;

          // Convert cordinates to LatLng objcts to load polygon
          _this.data.geometry.forEach(geoPoint => {
            cordArr.push( new google.maps.LatLng({ lat: geoPoint.latitude, lng: geoPoint.longitude }));
            // cordArr.push([ geoPoint.latitude, geoPoint.longitude ]);

            // Process to Find Center
            if (geoPoint.latitude < minLat) {
              minLat = geoPoint.latitude;
            }

            if (geoPoint.longitude < minLng) {
              minLng = geoPoint.longitude;
            }

            if (geoPoint.latitude > maxLat) {
              maxLat = geoPoint.latitude;
            }

            if (geoPoint.longitude > maxLng) {
              maxLng = geoPoint.longitude;
            }

          });

          // console.log(minLat, maxLat, minLng, maxLng);

          // Center points
          const centerLat = ((+maxLat - +minLat) + minLat);
          const centerLng = ((+maxLng - +minLng) + minLng);

          console.log('center: ', centerLat, centerLng);

          // Set Center point
          mapOpt.center.lat = +centerLat;
          mapOpt.center.lng = +centerLng;

          // Initialize map with mapobjects - This time zoom into the drawn polygon
          _this.map = new google.maps.Map(document.getElementById('map'), mapOpt);

          // Remove the last element of the array
          cordArr.splice(cordArr.length - 1, 1);

          // Create land polygon
          const landOverlay = new google.maps.Polygon({
            paths: cordArr,
            map: _this.map,
            draggable: true,
            editable: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
          });

          // Push to PathList
          _this.pathList = _this.data.geometry;
          console.log('cordArr: ', cordArr);

          // Show the polygon on Map
          landOverlay.setMap(_this.map);

          // Add the Polygon to Drawing list, so that it can be cleared.
          _this.drawnShapes.push(landOverlay);
        } else {
          // Initialize map with mapobjects.
          _this.map = new google.maps.Map(document.getElementById('map'), mapOpt);
        }

        // Initialize drawing manager to allow user to draw a region
        _this.drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: google.maps.drawing.OverlayType.POLYGON,
          drawingControl: true,
          drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: ['polygon']
          },
          polygonOptions: {
            draggable: true,
            editable: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
          }
        });

        // Set drawing manager to Map
        _this.drawingManager.setMap(_this.map);

        // Create listner to get the user event of completing the drawing a region
        google.maps.event.addListener(_this.drawingManager, 'overlaycomplete', event => {
            // Store the Shape Overlay
            _this.drawnShapes.push(event.overlay);

            // Polygon drawn
            if (event.type === google.maps.drawing.OverlayType.POLYGON) {
              // Get the points array of the drawn region / Path
              const pathArr = event.overlay.getPath().getArray();

              // Push the cordinates of path points to the global array
              for (let i = 0; i < pathArr.length; i++) {
                _this.pathList.push({
                  latitude: pathArr[i].lat(),
                  longitude: pathArr[i].lng()
                });
              }

              // Enable the Submit button
              _this.isHaveData = true;
            }
          }
        );
      });
    }
  }

  clearPaths() {
    // Set drawing manager to Map
    this.drawnShapes.forEach(overlay => {
      overlay.setMap(null);
    });

    // Disable the Submit button
    this.isHaveData = false;
  }

  finished() {
    this.output.emit(this.pathList);
    console.log('push data');
  }

  cancel() {
    const empty = []
    this.output.emit(empty);
  }
}
