import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuotationRoutingModule } from './quotation-routing.module';
import { QuotationMainComponent } from './quotation-main/quotation-main.component';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomPipeModule } from 'src/app/pipes/CustomPipesModule';
import { PolicyRoutingModule } from '../policy/policy-routing.module';
import { MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule, MatOptionModule, MatSelectModule, MatIconModule } from '@angular/material';
import { CommonComponentsModule } from '../common-components/common-components.module';
import { AddLivestockFormComponent } from './add-quotation/add-livestock-form/add-livestock-form.component';
import { AddQuotationMainComponent } from './add-quotation/add-quotation-main/add-quotation-main.component';
import { QuotationsComponent } from './quotation/quotations/quotations.component';
import { QuotationFormComponent } from './quotation/quotation-form/quotation-form.component';
import {QuotationWeatherFormComponent} from './quotation-weather-form/quotation-weather-form.component';
import { MarkLandComponent } from '../land/mark-land/mark-land.component';
import { MarkLandQuotationComponent } from './mark-land-quotation/mark-land-quotation.component';


@NgModule({
  declarations: [QuotationMainComponent, AddLivestockFormComponent, AddQuotationMainComponent, QuotationWeatherFormComponent, QuotationsComponent, QuotationFormComponent, MarkLandQuotationComponent],
  imports: [
    CommonModule,
    QuotationRoutingModule,
    CustomPipeModule,
    PolicyRoutingModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    CommonComponentsModule
  ],
  exports: [QuotationMainComponent, AddLivestockFormComponent, AddQuotationMainComponent, QuotationWeatherFormComponent, QuotationsComponent, QuotationFormComponent,  MarkLandQuotationComponent]
})
export class QuotationModule { }
