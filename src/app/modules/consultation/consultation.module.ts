import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultationRoutingModule } from './consultation-routing.module';
import { ConsultationMainComponent } from './consultation-main/consultation-main.component';
import { LandAndSoilConditionsComponent } from './land-and-soil-conditions/land-and-soil-conditions.component';
import {
  MatInputModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatButtonModule,
  MatExpansionModule,
  MatStepperModule,
  MatCheckboxModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HarvestComponent } from './harvest/harvest.component';
import { CommonComponentsModule } from '../common-components/common-components.module';

@NgModule({
  declarations: [ConsultationMainComponent, LandAndSoilConditionsComponent, HarvestComponent],
  imports: [
    CommonModule,
    ConsultationRoutingModule,
    ReactiveFormsModule,
    FormsModule,

    MatSelectModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule,
    MatExpansionModule,
    MatStepperModule,
    MatCheckboxModule,
    CommonComponentsModule
  ],
  exports: [ConsultationMainComponent]
})
export class ConsultationModule { }
