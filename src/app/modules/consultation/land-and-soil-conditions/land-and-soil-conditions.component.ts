import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ConsultationService } from '../services/consultation.service';
import { CommonType } from 'src/app/model/CommonType';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LandModel } from '../models/land-model';
import { SoilCondition } from '../models/soil-condition';

@Component({
  selector: 'app-land-and-soil-conditions',
  templateUrl: './land-and-soil-conditions.component.html',
  styleUrls: ['./land-and-soil-conditions.component.scss']
})
export class LandAndSoilConditionsComponent implements OnInit {
  @Output() outputData: EventEmitter<SoilCondition> = new EventEmitter();
  private soilCondition: SoilCondition = new SoilCondition();
  private soilTypes: CommonType[] = [];
  private aoilConditions: CommonType[] = [];
  private lands: LandModel[] = [];
  private isLandsLoaded = true;
  consultationForm = new FormGroup({
    land: new FormControl('', Validators.required),
    soilType: new FormControl('', Validators.required),
    soilCondition: new FormControl('', Validators.required),
    phValue: new FormControl('', Validators.required),
    nitrogenValue: new FormControl('', Validators.required),
    phosphorusValue: new FormControl('', Validators.required),
    potassiumValue: new FormControl('', Validators.required),
    landSizeAcres: new FormControl('', Validators.required),
    landSizePerches: new FormControl('', Validators.required),
    cultivationStartDate: new FormControl('', Validators.required),
  });

  constructor(private service: ConsultationService) { }

  ngOnInit() {
    this.getSoilTypes();
    this.getSoilConditions();
    this.getLands();
  }

  getLands() {
    this.service.getLands().subscribe(resp => {
      const body = resp.body as any;
      this.lands = body.data;
      this.isLandsLoaded = false;
    }, error => {
      console.log(error.message);
    });
  }

  getSoilTypes() {
    this.service.getSoilTypes().subscribe(resp => {
      const body = resp.body as any;
      this.soilTypes = body.data;
    }, error => {
      console.log(error.message);
    });
  }

  getSoilConditions() {
    this.service.getSoilConditions().subscribe(resp => {
      const body = resp.body as any;
      this.aoilConditions = body.data;
    }, error => {
      console.log(error.message);
    });
  }

  passDataToMain() {
    this.soilCondition.isHasData = true;
    this.outputData.emit(this.soilCondition);
  }

  getv() {
    this.soilCondition = this.consultationForm.value;
    this.passDataToMain();
  }
}
