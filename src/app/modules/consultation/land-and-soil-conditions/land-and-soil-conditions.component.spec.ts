import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandAndSoilConditionsComponent } from './land-and-soil-conditions.component';

describe('LandAndSoilConditionsComponent', () => {
  let component: LandAndSoilConditionsComponent;
  let fixture: ComponentFixture<LandAndSoilConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandAndSoilConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandAndSoilConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
