import { Injectable } from '@angular/core';
import { HTTPCallService } from 'src/app/services/httpcall.service';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { SoilCondition } from '../models/soil-condition';

@Injectable({
  providedIn: 'root'
})
export class ConsultationService {

  constructor(private http: HTTPCallService) { }

  getLands(): Observable<HttpResponse<Object>> {
    const path = 'utility/locations/search?partyCode=' + sessionStorage.getItem('partyCode');
    return this.http.getResource(path);
  }

  getSoilTypes(): Observable<HttpResponse<Object>> {
    const path = 'base/utility/agriConsultation/soilTypes';
    return this.http.getResource(path);
  }

  getSoilConditions(): Observable<HttpResponse<Object>> {
    const path = 'base/utility/agriConsultation/soilConditions';
    return this.http.getResource(path);
  }

  getCropTypes() {
    const path = 'base/references?type=CRPTY';
    return this.http.getResource(path);
  }

  getCropVariances(type: string) {
    const path = 'base/references?type=CRPVR&parentCode=' + type;
    return this.http.getResource(path);
  }

  calculateStats(cropType: string, variance: string, soilConditions: SoilCondition ) {
    const path = 'utility/agriConsultation/calculateStats?cropType=' + cropType + '&cropVariance=' + variance ;
    return this.http.postResource(path, soilConditions);
  }
}
