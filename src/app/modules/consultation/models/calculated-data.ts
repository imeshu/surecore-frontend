export class RecommendationModel {
    seeding: StageModel = new StageModel();
    vegetative: StageModel = new StageModel();
    budding: StageModel = new StageModel();
    flowering: StageModel = new StageModel();
}

export class StageModel {
    nitrogen: string = '0';
    phosphorus: string = '0';
    potassium: string = '0';
    ph: string = '0';
    recommendation: string = 'No Fertilizer Predicted';
}

export class IncomeModel {
    currentMarket: MarketModel = new MarketModel();
    lastYearMarket: MarketModel = new MarketModel();
    marketAverage: MarketModel = new MarketModel();
}
export class MarketModel {
    currentUnitPrice: string = '0';
    estimatedIncome: string = '0';
}

export class CalculatedData {
    crop: string;
    varience: string;
    isHasData: boolean = false;
    income: IncomeModel = new IncomeModel();
    recommendation: RecommendationModel = new RecommendationModel();
}