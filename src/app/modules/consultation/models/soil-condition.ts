export class SoilCondition {
    land: string;
    soilType: string;
    soilCondition: string;
    phValue: string;
    nitrogenValue: string;
    phosphorusValue: string;
    potassiumValue: string;
    landSizeAcres: string;
    landSizePerches: string;
    cultivationStartDate: string;
    isHasData: boolean;
}