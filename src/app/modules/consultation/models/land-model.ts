import { GeometryModel } from './geometry-model';

export class LandModel {
    sequence: string;
    address: string;
    agrarianServiceCenter: string;
    district: string;
    division: string;
    geometry: GeometryModel[];
    gramasevaDivision: string;
    ownership: string;
    plotName: string;
    partyCode: string;
    status: string;
    sizeInAcres: string;
    sizeInPerches: string;
    agrarianServiceCenterDescription: string;
    districtDescription: string;
    divisionDescription: string;
    gramasevaDivisionDescription: string;
    ownershipDescription: string;
    partyName: string;
    version: string;
}