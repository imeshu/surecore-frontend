import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultationMainComponent } from './consultation-main/consultation-main.component';

const routes: Routes = [
  {
    path: 'consultation',
    component: ConsultationMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultationRoutingModule { }
