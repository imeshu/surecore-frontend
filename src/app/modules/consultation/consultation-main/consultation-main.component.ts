import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SoilCondition } from '../models/soil-condition';
import { CalculatedData } from '../models/calculated-data';

@Component({
  selector: 'app-consultation-main',
  templateUrl: './consultation-main.component.html',
  styleUrls: ['./consultation-main.component.scss']
})
export class ConsultationMainComponent implements OnInit {
  private soilCondition: SoilCondition = new SoilCondition();
  private isSoilCondition = true;


  constructor() { }

  ngOnInit() {
  }

  output(event) {
    this.soilCondition = event;
    this.isSoilCondition = false;
  }

}
