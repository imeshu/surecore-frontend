import {Component, OnInit, Input} from '@angular/core';
import {ConsultationService} from '../services/consultation.service';
import {CommonType} from 'src/app/model/CommonType';
import {SoilCondition} from '../models/soil-condition';
import {CalculatedData} from '../models/calculated-data';

@Component({
  selector: 'app-harvest',
  templateUrl: './harvest.component.html',
  styleUrls: ['./harvest.component.scss']
})
export class HarvestComponent implements OnInit {
  @Input() private soilCondition: SoilCondition = new SoilCondition();
  private calculatedData: CalculatedData = new CalculatedData();
  private compareList: CalculatedData[] = [];
  private cropTypes: CommonType[] = [];
  private variences: CommonType[] = [];
  private selectedCropType: CommonType = new CommonType();
  private selectedVariences: CommonType = new CommonType();
  private isAddNew: boolean;
  private isVariance: boolean;


  constructor(private service: ConsultationService) {
  }

  ngOnInit() {
    this.initVariables();
    this.getCropTypes();
  }

  initVariables() {
    this.isAddNew = true;
    this.isVariance = false;
  }

  getCropTypes() {
    this.isVariance = false;
    this.service.getCropTypes().subscribe(resp => {
      const body = resp.body as any;
      this.cropTypes = body.data;
    }, error => {
      console.log(error.message);
    });
  }

  getVariances() {
    this.service.getCropVariances(this.selectedCropType.code).subscribe(resp => {
      const body = resp.body as any;
      this.variences = body.data;
      this.isVariance = true;
    }, error => {
      this.isVariance = false;
      console.log(error.message);
    });
  }

  calculateStats() {
    this.calculatedData = new CalculatedData();
    this.service.calculateStats(this.selectedCropType.code, this.selectedVariences.code, this.soilCondition).subscribe(resp => {
      const body = resp.body as any;
      this.calculatedData = body.data;
      this.calculatedData.isHasData = true;
    }, error => {
      console.log(error.message);
    });
  }

  addItemToCompareList(item: CalculatedData) {
    this.calculatedData.crop = this.selectedCropType.description;
    this.calculatedData.varience = this.selectedVariences.description;
    this.compareList.push(item);
    this.calculatedData = new CalculatedData();
    this.selectedCropType = new CommonType();
    this.selectedVariences = new CommonType();
    this.isAddNew = false;
  }

  deleteCompareListItem(index) {
    this.compareList.splice(index, 1);
  }
}
