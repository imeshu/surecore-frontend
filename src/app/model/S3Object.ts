export class S3Object {
  id: number;
  name: string;
  key: string;
  url: string;
  status: string;
  version: number;
}
