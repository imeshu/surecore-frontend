export class ImageModel {
  imageKey: string;
  value: string;
  progress: string;
  file: File;
  fileType: string;
}
