export class SuccessResponse {
  data: any;
  message: string;
  success: boolean;
  code: number;

  constructor(data: any, message: string, success: boolean, code: number) {
    this.data = data;
    this.message = message;
    this.success = success;
    this.code = code;
  }

}
