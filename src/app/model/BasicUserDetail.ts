import {Contact} from './Contact';

export class BasicUserDetail {
  name: string;
  profileImageURL: string;
  rating: string;
  contacts: Contact[];
}
