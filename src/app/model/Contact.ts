export class Contact {
  contactType: string;
  contactNumber: string;
  status: string;
  sequence: string;
  version: number;
  contactTypeDescription: string;
  default: boolean;
}
