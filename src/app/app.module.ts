import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
// PrimeNG
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// Bootstrap
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SignInComponent} from './modules/auth/sign-in/sign-in.component';
import {SignUpComponent} from './modules/auth/sign-up/sign-up.component';
import {PasswordResetComponent} from './modules/auth/password-reset/password-reset.component';
import {RecoveryDetailsComponent} from './modules/auth/recovery-details/recovery-details.component';
import {RequestRecoveryComponent} from './modules/auth/request-recovery/request-recovery.component';
import {SmspinComponent} from './modules/auth/smspin/smspin.component';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {SignUpLoginComponent} from './modules/auth/sign-up-login/sign-up-login.component';
import {SimpinSignupComponent} from './modules/auth/simpin-signup/simpin-signup.component';


import {DashboardModule} from './modules/dashboard/dashboard.module';
import {UserProfileModule} from './modules/user-profile/user-profile.module';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {MarketPriceModule} from './modules/market-price/market-price.module';
import {NewsModule} from './modules/news/news.module';
import {CustomerModule} from './modules/customer/customer.module';

import {MarketplaceModule} from './modules/marketplace/marketplace.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {ConsultationModule} from './modules/consultation/consultation.module';
import {PolicyModule} from './modules/policy/policy.module';
import {HttpErrorInterceptor} from './interceptors/http-error.interceptor';
import {LandModule} from './modules/land/land.module';
import {OutstandingDebtModule} from './modules/outstanding-debt/outstanding-debt.module';
import {ReceiptingModule} from './modules/receipting/receipting.module';
import {LoyaltyModule} from './modules/loyalty/loyalty.module';

import {ClaimsModule} from './modules/claims/claims.module';
import {QuotationModule} from './modules/quotation/quotation.module';
import {PopupComponent} from './modules/claims/add-claim-notification/popup/popup.component';
import {ForumModule} from './modules/forum/forum.module';
import {ImageViewerModule} from './modules/common-modules/image-viewer/image-viewer.module';

// import {DialogDataExampleDialog,DialogDataExample} from "./modules/claims/add-claim-notification/add-claim-notification.component";


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

// @ts-ignore
// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    SignUpLoginComponent,
    PasswordResetComponent,
    RecoveryDetailsComponent,
    RequestRecoveryComponent,
    SmspinComponent,
    SimpinSignupComponent
  ],
  imports: [
    MatSidenavModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // App modules
    DashboardModule,
    UserProfileModule,
    MarketPriceModule,
    NewsModule,
    MarketplaceModule,
    ConsultationModule,
    PolicyModule,

    CustomerModule,
    LandModule,
    ClaimsModule,
    OutstandingDebtModule,
    ReceiptingModule,
    LoyaltyModule,
    QuotationModule,
    ForumModule,

    // common-modules
    ImageViewerModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}


