import { NgModule } from '@angular/core';
import { urlDecoder } from 'src/app/pipes/urlDecoder';
import {Filler} from './Filler';

@NgModule({
    declarations: [ urlDecoder, Filler ],
    exports: [ urlDecoder, Filler ]
  })
  export class CustomPipeModule {}
