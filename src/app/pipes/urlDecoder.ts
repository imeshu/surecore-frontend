import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'decodeUrl'})
export class urlDecoder implements PipeTransform {
  transform(value: string): string {
    const decodedURI = decodeURIComponent(value);
    return decodedURI;
  }
}