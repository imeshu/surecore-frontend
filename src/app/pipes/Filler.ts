import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'fill'})
export class Filler implements PipeTransform {

  transform(value) {
    return (new Array(value)).fill(1);
  }

}
